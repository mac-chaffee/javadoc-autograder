---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements TransitGroup. PreSecurity area in airport where passengers go before going to security checkpoint. Has a PassengerQueue for passengers
*
*/
public class PreSecurity implements TransitGroup {

/** Queue of passengers outside of security */
private PassengerQueue outsideSecurity;

/**
* Creates a PreSecurity area. If number of passengers is less than one, throws exception
* @param passengers number of passengers to be created
* @param log log for all passengers that will be created
* @throws IllegalArgumentException if number of passengers is less than 1
*/
public PreSecurity(int passengers, Reporter log) {

 - Throws: IllegalArgumentException

/**
* returns time to depart of passenger at front of presecurity
* @return next time to depart
*/
public int departTimeNext() {

/**
* returns next passenger
* @return next passenger to go
*/
public Passenger nextToGo() {

/**
* determines if there is a next passenger
* @return true if there is at least one passenger false otherwise
*/
public boolean hasNext() {

/**
* removes next passenger
* @return passenger to remove
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Security CheckPoint with passengerQueue of passengers in line. 
*
*
*/
public class CheckPoint {

/** Time when checkpoint will be empty **/
private int timeWhenAvailable;

/** line of passengers */
private PassengerQueue line;

/**
* Creates a checkpoint
*/
public CheckPoint() {

/**
* number of passenger in line at checkpoint
* @return passenger queue size
*/
public int size() {

/**
* removes passenger at front from line
* @return Passenger that is removed
*/
public Passenger removeFromLine() {

/**
* Determines whether or not there is a passenger in line
* @return true if there is a passenger, false otherwise
*/
public boolean hasNext() {

/**
* finds depart time of passenger at front of line
* @return depart time of passenger
*/
public int departTimeNext() {

/**
* Passenger that is at the front of the line
* @return Passenger that is next
*/
public Passenger nextToGo() {

/**
* adds Passenger to line. Sets passengers wait time based on when line will be empty and the passenger's arrival time, then sets
* timeWhenAvailable according to passenger's arrivalTime, waitTime, and processTime
* @param passenger to add to line
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements TransitGroup. Security area composed with array of checkpoints. Passenger can be added to security area by being added to a checkpoint.
*
*
*/
public class SecurityArea implements TransitGroup {

/** Max number of checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** Min number of checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** Error message for invalid number of checkpoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** Error message for invalid checkpoint index */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** largest index of fast pass line */
private int largestFastIndex;

/** index of trusted traveler line */
private int tsaPreIndex;

/** array of checkpoints */
private CheckPoint[] check;

/**
* Creates a security area. Sets largestFastIndex and tsaPreIndex based on number of checkpoints in security area. 
* Throws IllegalArgumentException if number of checkpoints is invalid
* @param numCheckpoints number of checkpoints
* @throws IllegalArgumentException if number of checkpoints is less than 3 or greater than 17
*/
public SecurityArea(int numCheckpoints) {

 - Throws: IllegalArgumentException

/**
* check if number of checkpoints is valid
* @param num number of checkpoints
* @return true if valid, false otherwise
*/
private boolean numGatesOk(int num) {

/**
* adds passenger to checkpoint with given index. Throws IllgalArgumentException if index is invalid.
* @param index index of checkpoint
* @param passenger passenger to add
* @throws IllegalArgumentException if index is less than 0 or greater than tsaPreIndex
*/
public void addToLine(int index, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
* finds shortest line for ordinary passengers
* @return index of shortest line
*/
public int shortestRegularLine() {

/**
* finds shortest fast track line
* @return index of shortest line
*/
public int shortestFastTrackLine() {

/**
* finds shortest TSA precheck line
* @return index of shortest line
*/
public int shortestTSAPreLine() {

/**
* calculates how long line is. Throws IllgalArgumentException if index is invalid.
* @param index index of line
* @return length of line
* @throws IllegalArgumentException if index is less than 0 or greater than tsaPreIndex
*/
public int lengthOfLine(int index) {

 - Throws: IllegalArgumentException

/**
* Determines next passenger to go
* @return next passenger to go
*/
@Override
public Passenger nextToGo() {

/**
* Determines depart time of passenger with lowest depart time
* @return depart time of next passenger
*/
@Override
public int departTimeNext() {

/**
* Removes passenger at front from security area and line
* @return passenger that will be removed
*/
@Override
public Passenger removeNext() {

/**
* Determines shortest line in given range
* @param index1 index of lower limit of range, inclusive
* @param index2 index of higher limit of range, exclusive
* @return index of shortest line
*/
private int shortestLineInRange(int index1, int index2) {

/**
* determines checkpoint in security area that will be the first to process their passenger
* @return index of checkpoint
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Extends Passenger to be a passenger that has signed up for Fast Track. Uses special Fast track functionality to assign the passenger
* to the correct line and give it the correct color.
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** Maximum expected process time for a Fast pass passenger*/
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** color of passenger */
private Color color;

/**
* Constructs fast track passenger. Sets color according to process time.
* @param arrivalTime arrival time of passenger
* @param processTime process time of passenger
* @param log log for passenger to enter data into
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* picks line for passenger
* @param securityArea securityArea of checkpoints
* @return index of line
*/
private int pickLine(TransitGroup securityArea) {

/**
* returns color of passenger
* @return color of passenger
*/
@Override
public Color getColor() {

/**
* Puts passenger in a line. Passenger will pick shortest line that is not the TSA Pre-check line.
* @param securityArea security area with all checkpoints
*/
@Override
public void getInLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Extends passenger to be a passenger that has not chosen to participate in any special programs. Uses ordinary passenger to assign passenger
* to unrestricted lines and assign correct color to passenger.
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum expected process time for an ordinary passenger*/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** color of passenger */
private Color color;

/**
* Constructs ordinary passenger. Sets passenger color according to process time.
* @param arrivalTime arrival time of passenger
* @param processTime process time of passenger
* @param log log for passenger to enter data into
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* puts passenger in a line. Passenger chooses shortest line that is not restricted to TSA Precheck or Fast Track passengers.
* @param securityArea security area with all checkpoints
*/
@Override
public void getInLine(TransitGroup securityArea) {

/**
* picks line for passenger
* @param securityArea security area with all checkpoints
* @return index of line
*/
private int pickLine(TransitGroup securityArea) {

/**
* returns color of passenger
* @return color of passenger
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Passenger that is either fast track, trusted traveler, or ordinary. Keeps track of arrivaal, process, and wait time of passenger.
* Also has personal log of passenger and manages information on whether passenger is in the security area and what line they are in.
* 
*
*/
public abstract class Passenger {

/** Minimum process time for all passengers */
public final static int MIN_PROCESS_TIME = 20;

/** arrival time of passenger */
private int arrivalTime;

/** wait time of passenger */
private int waitTime;

/** process time of passenger */
private int processTime;

/** line index of passenger */
private int lineIndex;

/** whether or not passenger is awaiting processing */
private boolean waitingProcessing;

/** log of data for passenger */
private Log myLog;

/**
* Creates passenger. Sets fields according to parameters. Throws exception if process time is invalid
* @param arrivalTime arrival time of passenger
* @param processTime process time of passenger
* @param log log for passenger to enter data into
* @throws IllegalArgumentException if processTime is less than 20
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

 - Throws: IllegalArgumentException

/**
* gets arrival time of passenger
* @return the arrivalTime
*/
public int getArrivalTime() {

/**
* gets wait time of passenger
* @return the waitTime
*/
public int getWaitTime() {

/**
* sets the wait time of passenger
* @param waitTime the waitTime to set
*/
public void setWaitTime(int waitTime) {

/**
* gets the process time of the passenger
* @return the processTime
*/
public int getProcessTime() {

/**
* gets the line index of passenger. Returns -1 if passenger is not in line yet.
* @return the lineIndex
*/
public int getLineIndex() {

/**
* sets the line index of passenger
* @param lineIndex the lineIndex to set
*/
protected void setLineIndex(int lineIndex) {

/**
* determines whether or not passenger is waiting in line at checkpoint
* @return true if waiting at checkpoint, false otherwise
*/
public boolean isWaitingInSecurityLine() {

/**
* clears passenger from security area and logs data from passenger into log.
*/
public void clearSecurity() {

/**
* puts passenger in a line
* @param securityArea security area with all checkpoints
*/
public abstract void getInLine(TransitGroup securityArea);

/**
* returns color of passenger
* @return color of passenger
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Extends Passenger to be a passenger that has signed up for Trusted Traveler or TSA PreCheck. Uses special trusted traveler functionality to assign the passenger
* to the correct line and give it the correct color. 
*
*/
public class TrustedTraveler extends Passenger {

/** Maximum expected process time for an trusted traveler */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** color of passenger */
private Color color;

/**
* Constructs trusted traveler. Sets passenger color according to process time.
* @param arrivalTime arrival time of passenger
* @param processTime process time of passenger
* @param log log for passenger to enter data into
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter log) {

/**
* Puts passenger in a line. Passenger picks TSA Pre-check line as long as it is not twice as long as any ordinary line.
* Otherwise, it will pick the shortest ordinary line, if the TSA precheck line is twice as long.
* @param securityArea security area with all checkpoints
*/
@Override
public void getInLine(TransitGroup securityArea) {

/**
* picks line for passenger
* @param securityArea security area of checkpoints
* @return index of line passenger will pick
*/
private int pickLine(TransitGroup securityArea) {

/**
* returns color of passenger
* @return color of passenger
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The event calendar for the simulation. Keeps track of which passenger should act next in simulation.
*
*
*/
public class EventCalendar {

/** transit group that is in ticketing */
private TransitGroup highPriority;

/** transit group that is in security area */
private TransitGroup lowPriority;

/**
* Creates the event calendar
* @param highPriority high priority transit group
* @param lowPriority low priority transit group
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* determines next passenger to act
* @return the passenger
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Logs all the information of the simulation. Keeps track of number of passengers processed, total wait time of simulation
* total process time of simulation, and calculates both averages.
*
*
*/
public class Log implements Reporter {

/** number of passengers that have passed security */
private int numCompleted;

/** total wait time of passengers who have finished so far */
private int totalWaitTime;

/** total process time of passengers who have finished so far */
private int totalProcessTime;

/**
* Creates log. Initializes all values to 0.
*/
public Log() {

/**
* calculates average wait time by dividing totalWaitTime by numCompleted. If numCompleted is still 0
* returns 0.0 instead dividing by zero
* @return the average wait time
*/
@Override
public double averageWaitTime() {

/**
* calculates average process time by dividing totalProcessTime by numCompleted. If numCompleted is still 0
* returns 0.0 instead dividing by zero
* @return the average process time
*/
@Override
public double averageProcessTime() {

/**
* get number of passengers who have completed simulation
* @return numCompleted passengers
*/
@Override
public int getNumCompleted() {

/**
* logs the data from the passenger. Increments numCompleted and logs process and wait time.
* @param p Passenger to log data from
*/
@Override
public void logData(Passenger p) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Airport Security Simulator. Accepts values for number of passengers, number of checkpoints, and percents of passenger composition.
* Creates PreSecurity area with passengers. Creates SecurityArea with checkpoints. Uses EventCalendar to determine which passenger should act next.
* Keeps track of number of passengers and current passenger in simulation.
* 
*
*
*/
public class Simulator {

/** number of passenger inputed by user */
private int numPassengers;

/** passengers in ticketing */
private PreSecurity inTicketing;

/** passengers in security area */
private SecurityArea inSecurity;

/** reporter for simulation */
private Log log;

/** event calendar for simulation */
private EventCalendar calendar;

/** current passenger */
private Passenger currentPassenger;

/**
* Creates simulation. Creates security and ticketing area. Creates log. Creates Event Calendar with security and ticketing groups. Throws exception if number of checkpoints or passengers, or any percents are invalid.
* @param numPassengers number of passengers
* @param numCheckpoints number of checkpoints
* @param percentTrusted percent trusted traveler passengers
* @param percentFastTrack percent fast track passengers
* @param percentOrdinary percent of ordinary passengers
* @throws IllegalArgumentException if numPassengers is less than 0, if numCheckpoints is less than 0 or greater than 17, if any percents are less than 0, or if percents don't add up to 100
*/
public Simulator(int numCheckpoints, int numPassengers, int percentTrusted, int percentFastTrack, int percentOrdinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Checks all user inputed parameters. Throws exception if any are invalid.
* @param numCheckpoints number of checkpoints
* @param percentOrdinary percent ordinary passengers
* @param percentTrusted percent trusted travelers
* @param percentFastTrack percent fast track passengers
* @throws IllegalArgumentException if numCheckpoints is less than 3 or greater than 17, if any percent is less than 0, or if percents do not add up to 100.
*/
private void checkParameters(int numCheckpoints, int percentOrdinary, int percentTrusted, int percentFastTrack) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* sets up passenger composition in Ticketing for passenger creation in PreSecurity
* @param percentOrdinary percent ordinary passengers
* @param percentTrusted percent trusted travelers
* @param percentFastTrack percent fast track passenger
*/
private void setUp(int percentOrdinary, int percentTrusted, int percentFastTrack) {

/**
* gets reporter for simulation
* @return Reporter for simulation
*/
public Reporter getReporter() {

/**
* takes next step. Uses Event calendar to determine next passenger to act, and sets passenger as currentPassenger. 
* If currentPassenger is in PreSecurity, then removes currentPassenger from PreSecurity and assign passenger to enter security area.
* If currentPassenger is in SecurityArea, then removes currentPassenger from simulation and SecurityArea.
*/
public void step() {

/**
* determines whether there are more steps. There are more steps if the number of passengers that have been process is
* less than the number of passengers that were entered by the user.
* @return true if there are more steps, false otherwise
*/
public boolean moreSteps() {

/**
* gets index of line of current passenger. Returns -1 if currentPassenger is null or has not entered the SecurityArea.
* @return line index of currentPassenger
*/
public int getCurrentIndex() {

/**
* gets color of current passenger. Returns null if current passenger is null
* @return Color of current passenger
*/
public Color getCurrentPassengerColor() {

/**
* determines whether or not current passenger has cleared security
* @return true if current passenger has, false otherwise or if current passenger is null
*/
public boolean passengerClearedSecurity() {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class which encapsulates user actions (or incident transitions) that cause
* the state of a ManagedIncident to update.
* 
*/
public class Command {

/** On-hold message when waiting for the caller */
public static final String OH_CALLER = "Awaiting Caller";

/** On-hold message when awaiting a change */
public static final String OH_CHANGE = "Awaiting Change";

/** On-hold message when waiting for the vendor */
public static final String OH_VENDOR = "Awaiting Vendor";

/** Resolution code when the problem is permanently solved */
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** Resolution code when the problem is solved with a workaround */
public static final String RC_WORKAROUND = "Workaround";

/** Resolution code when the problem could not be solved */
public static final String RC_NOT_SOLVED = "Not Solved";

/** Resolution code when the caller requests removal */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** Cancellation code when the problem is a duplicate */
public static final String CC_DUPLICATE = "Duplicate";

/** Cancellation code when a solution is not necessary */
public static final String CC_UNNECESSARY = "Unnecessary";

/** Cancellation code when the problem is not a problem */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** Owner of the incident; referred to as the caller */
private String ownerId;

/** Note entered by the user detailing reasons for issuing this command */
private String note;

/** The incident's cancellation code (restricted by enum) */
private CancellationCode cancellationCode;

/** The incident's resolution code (restricted by enum) */
private ResolutionCode resolutionCode;

/** The incident's on-hold reason (restricted by enum) */
private OnHoldReason onHoldReason;

/** The type of command given for this incident (restricted by enum) */
private CommandValue c;

/**
* Constructor for a Command. Takes inputs generated at the GUI level and
* formulates a new command describing operations to be applied to an incident.
* Values unnecessary for the CommandValue are accepted as null.
* 
* @param c                the command requested to be performed on an incident
* @param ownerId          the person responsible for handling the incident
* @param onHoldReason     reason the incident is on hold
* @param resolutionCode   explanation for a resolved incident
* @param cancellationCode explanation for a cancelled incident
* @param note             a note included by the owner/user
* @throws IllegalArgumentException in the following situations:A Command with a
*                                  null CommandValue parameter. A Command must
*                                  have a CommandValue. A Command with a
*                                  CommandValue of INVESTIGATE and a null or
*                                  empty string ownerId. A Command with a
*                                  CommandValue of HOLD and a null
*                                  onHoldReason. A Command with a CommandValue
*                                  of RESOLVE and a null resolutionCode. A
*                                  Command with a CommandValue of CANCEL and a
*                                  null cancellationCode. A Command with a note
*                                  that is null or an empty string.
*/
public Command(CommandValue c, String ownerId, OnHoldReason onHoldReason,
/**
* Checks the constructor parameters for validity.
* 
* @param c                the command requested to be performed on an incident
* @param ownerId          the person responsible for handling the incident
* @param onHoldReason     reason the incident is on hold
* @param resolutionCode   explanation for a resolved incident
* @param cancellationCode explanation for a cancelled incident
* @param note             a note included by the owner/user
* @throws IllegalArgumentException in the following situations:A Command with a
*                                  null CommandValue parameter. A Command must
*                                  have a CommandValue. A Command with a
*                                  CommandValue of INVESTIGATE and a null or
*                                  empty string ownerId. A Command with a
*                                  CommandValue of HOLD and a null
*                                  onHoldReason. A Command with a CommandValue
*                                  of RESOLVE and a null resolutionCode. A
*                                  Command with a CommandValue of CANCEL and a
*                                  null cancellationCode. A Command with a note
*                                  that is null or an empty string.
* 
*/
private static void checkParameters(CommandValue c, String ownerId, OnHoldReason onHoldReason,
 - Throws: IllegalArgumentException

/**
* Gets the command value.
* 
* @return the command value, c
*/
public CommandValue getCommand() {

/**
* Gets the owner's ID.
* 
* @return the ownerId
*/
public String getOwnerId() {

/**
* Gets the resolution code.
* 
* @return the resolutionCode
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the work note.
* 
* @return the note
*/
public String getWorkNote() {

/**
* Gets the on-hold reason.
* 
* @return the onHoldReason
*/
public OnHoldReason getOnHoldReason() {

/**
* Gets the cancellation code.
* 
* @return the cancellationCode
*/
public CancellationCode getCancellationCode() {

/**
* Enumeration of command values.
* 
*/
public enum CommandValue {

/** Investigate */
/** Hold */
/** Resolve */
/** Confirm */
/** Reopen */
/** Cancel */
/**
* Enumeration of on-hold reasons.
* 
*/
public enum OnHoldReason {

/** Awaiting caller */
/** Awaiting change */
/** Awaiting vendor */
/**
* Enumeration of resolution codes.
* 
*/
public enum ResolutionCode {

/** Permanently solved */
/** Workaround */
/** Not solved */
/** Caller closed */
/**
* Enumeration of cancellation codes.
* 
*/
public enum CancellationCode {

/** Deuplicate */
/** Unnecessary */
/** Not an incident */
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* ManagedIncident represents an incident tracked by the incident management
* system. Each ManagedIncident has its own state, which is updated from
* Commands propagated to it from the UI.
* 
*/
public class ManagedIncident {

/** Text for the Inquiry category */
public static final String C_INQUIRY = "Inquiry";

/** Text for the Software category */
public static final String C_SOFTWARE = "Software";

/** Text for the Hardware category */
public static final String C_HARDWARE = "Hardware";

/** Text for the Network category */
public static final String C_NETWORK = "Network";

/** Text for the Database category */
public static final String C_DATABASE = "Database";

/** Text for the Urgent priority level */
public static final String P_URGENT = "Urgent";

/** Text for the High priority level */
public static final String P_HIGH = "High";

/** Text for the Medium priority level */
public static final String P_MEDIUM = "Medium";

/** Text for the Low priority level */
public static final String P_LOW = "Low";

/** The incident's unique incrementally generated ID */
private int incidentId;

/** Person who opened the incident */
private String caller;

/** Person in charge of the incident */
private String owner;

/** Name of the incident */
private String name;

/** Change request information for the incident */
private String changeRequest;

/** List of notes given concerning the incident */
private ArrayList<String> notes;

/** Cancellation code of the incident */
private CancellationCode cancellationCode;

/** Resolution code of the incident */
private ResolutionCode resolutionCode;

/** On-hold reason of the incident */
private OnHoldReason onHoldReason;

/** Initial state */
private final NewState newState;

/** On Hold state */
private final OnHoldState onHoldState;

/** Resolved state */
private final ResolvedState resolvedState;

/** Closed state */
private final ClosedState closedState;

/** Canceled state */
private final CanceledState canceledState;

/** In Progress state */
private final InProgressState inProgressState;

/** Current state of the incident */
private IncidentState state;

/** Category of the incident */
private Category category;

/** Priority level of the incident */
private Priority priority;

/** Name for the New state */
public static final String NEW_NAME = "New";

/** Name for the In Progress state */
public static final String IN_PROGRESS_NAME = "In Progress";

/** Name for the On Hold state */
public static final String ON_HOLD_NAME = "On Hold";

/** Name for the Resolved state */
public static final String RESOLVED_NAME = "Resolved";

/** Name for the Closed state */
public static final String CLOSED_NAME = "Closed";

/** Name for the Canceled state */
public static final String CANCELED_NAME = "Canceled";

/** Keeps track of the number of incidents created */
private static int counter = 0;

/**
* Constructor for a ManagedIncident.
* 
* @param caller   person who opened the incident request
* @param category category of the incident (restricted by enum)
* @param priority priority of the incident (restricted by enum)
* @param name     name of the incident
* @param workNote a note describing the incident
* @throws IllegalArgumentException if any of the String fields are null or
*                                  empty
*/
public ManagedIncident(String caller, Category category, Priority priority, String name,
 - Throws: IllegalArgumentException

/**
* Constructor for ManagedIncident. Accepts an incident in XML format and
* populates the state from it.
* 
* @param i XML incident from which information is pulled to populate the state
* @throws IllegalArgumentException if category or priority does not match the
*                                  expected possible values defined by the
*                                  Category or Priority enum, if any of the
*                                  codes are defined as an unacceptable value
*/
public ManagedIncident(Incident i) {

 - Throws: IllegalArgumentException

/**
* Increments the counter by 1.
*/
public static void incrementCounter() {

/**
* Gets the incident ID.
* 
* @return the incidentId
*/
public int getIncidentId() {

/**
* Gets the change request.
* 
* @return the changeRequest
*/
public String getChangeRequest() {

/**
* Gets the category.
* 
* @return the category
*/
public Category getCategory() {

/**
* Gets the category in string form.
* 
* @return the category as a string
*/
public String getCategoryString() {

/**
* Sets the category.
* 
* @param category the category to set
* @throws IllegalArgumentException if the category does not match any values in
*                                  the Category enum
*/
private void setCategory(String category) {

/**
* Gets the priority in string form.
* 
* @return the priority as a string
*/
public String getPriorityString() {

/**
* Sets the priority.
* 
* @param priority the priority to set
* @throws IllegalArgumentException if the priority does not match any values in
*                                  the Priority enum
*/
private void setPriority(String priority) {

/**
* Gets the on-hold reason in string form.
* 
* @return the onHoldReason as a string
*/
public String getOnHoldReasonString() {

/**
* Sets the on-hold reason.
* 
* @param onHoldReason the onHoldReason to set
* @throws IllegalArgumentException if the onHoldReason is non-null but not an
*                                  allowed value
*/
private void setOnHoldReason(String onHoldReason) {

/**
* Gets the cancellation code in string form.
* 
* @return the cancellationCode as a string
*/
public String getCancellationCodeString() {

/**
* Sets the cancellation code.
* 
* @param cancellationCode the cancellationCode to set
* @throws IllegalArgumentException if the cancellationCode is non-null but not
*                                  an allowed value
*/
private void setCancellationCode(String cancellationCode) {

/**
* Gets the current state.
* 
* @return the current state
*/
public IncidentState getState() {

/**
* Sets the current state.
* 
* @param state the state to set
*/
private void setState(IncidentState state) {

/**
* Gets the resolution code.
* 
* @return the resolutionCode
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the resolution code in string form.
* 
* @return the resolutionCode as a string
*/
public String getResolutionCodeString() {

/**
* Sets the resolution code.
* 
* @param resolutionCode the resolutionCode to set
* @throws IllegalArgumentException if the resolutionCode is non-null but not an
*                                  allowed value
*/
private void setResolutionCode(String resolutionCode) {

/**
* Gets the owner.
* 
* @return the owner
*/
public String getOwner() {

/**
* Gets the name.
* 
* @return the name
*/
public String getName() {

/**
* Gets the caller.
* 
* @return the caller
*/
public String getCaller() {

/**
* Gets the notes array.
* 
* @return the notes array
*/
public ArrayList<String> getNotes() {

/**
* Gets the notes as a string.
* 
* @return the notes as a string
*/
public String getNotesString() {

/**
* Updates the incident in accordance to a command dictated by the user.
* 
* @param command the command describing the desired changes
*/
public void update(Command command) {

/**
* Gets the incident in XML format.
* 
* @return the incident in XML format
*/
public Incident getXMLIncident() {

/**
* Sets the counter value.
* 
* @param num the value to be set
*/
public static void setCounter(int num) {

/**
* Interpolates a CancellationCode from a string. Possible matches are from the
* CancellationCode enum in Command. Null is allowed.
* 
* @param cancellationCode cancellation code as a string from XML incident
* @return matched CancellationCode from possible enum values
* @throws IllegalArgumentException if the cancellationCode is non-null but not
*                                  an allowed value
*/
private static CancellationCode determineCancellationCode(String cancellationCode) {

 - Throws: IllegalArgumentException

/**
* Interpolates a OnHoldReason from a string. Possible matches are from the
* OnHoldReason enum in Command. Null is allowed.
* 
* @param onHoldReason on-hold reason as a string from XML incident
* @return matched OnHoldReason from possible enum values
* @throws IllegalArgumentException if the onHoldReason is non-null but not an
*                                  allowed value
*/
private static OnHoldReason determineOnHoldReason(String onHoldReason) {

 - Throws: IllegalArgumentException

/**
* Interpolates a ResolutionCode from a string. Possible matches are from the
* ResolutionCode enum in Command. Null is allowed.
* 
* @param resolutionCode resolution code as a string from XML incident
* @return matched ResolutionCode from possible enum values
* @throws IllegalArgumentException if the resolutionCode is non-null but not an
*                                  allowed value
*/
private static ResolutionCode determineResolutionCode(String resolutionCode) {

 - Throws: IllegalArgumentException

/**
* Interpolates a Category from a string. Possible matches are from the Category
* enum.
* 
* @param category category as a string from XML incident
* @return matched Category from possible enum values
* @throws IllegalArgumentException if the category does not match any values in
*                                  the Category enum
*/
private static Category determineCategory(String category) {

 - Throws: IllegalArgumentException

/**
* Interpolates a Priority from a string. Possible matches are from the Priority
* enum.
* 
* @param priority priority as a string from XML incident
* @return matched Priority from possible enum values
* @throws IllegalArgumentException if the priority does not match any values in
*                                  the Priority enum
*/
private static Priority determinePriority(String priority) {

 - Throws: IllegalArgumentException

/**
* Inner class representing the New state of this FSM.
* 
*/
private class NewState implements IncidentState {

/**
* Updates the incident and its state given a command.
* 
* @param command a command describing how the incident should be modified
*/
@Override
public void updateState(Command command) {

 - Throws: IllegalArgumentException

 - Throws: UnsupportedOperationException

/**
* Gets the state name.
* 
* @return the state name
*/
@Override
public String getStateName() {

/**
* Inner class representing the In Progress state of this FSM.
* 
*/
private class InProgressState implements IncidentState {

/**
* Updates the incident and its state given a command.
* 
* @param command a command describing how the incident should be modified
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name.
* 
* @return the state name
*/
@Override
public String getStateName() {

/**
* Inner class representing the On Hold state of this FSM.
* 
*/
private class OnHoldState implements IncidentState {

/**
* Updates the incident and its state given a command.
* 
* @param command a command describing how the incident should be modified
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name.
* 
* @return the state name
*/
@Override
public String getStateName() {

/**
* Inner class representing the Resolved state of this FSM.
* 
*/
private class ResolvedState implements IncidentState {

/**
* Updates the incident and its state given a command.
* 
* @param command a command describing how the incident should be modified
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name.
* 
* @return the state name
*/
@Override
public String getStateName() {

/**
* Inner class representing the Closed state of this FSM.
* 
*/
private class ClosedState implements IncidentState {

/**
* Updates the incident and its state given a command.
* 
* @param command a command describing how the incident should be modified
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name.
* 
* @return the state name
*/
@Override
public String getStateName() {

/**
* Inner class representing the Canceled state of this FSM.
* 
*/
private class CanceledState implements IncidentState {

/**
* Updates the incident and its state given a command.
* 
* @param command a command describing how the incident should be modified
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name.
* 
* @return the state name
*/
@Override
public String getStateName() {

/**
* Enumeration of categories.
* 
*/
public enum Category {

/** Inquiry */
/** Software */
/** Hardware */
/** Network */
/** Database */
/**
* Enumeration of priority levels.
* 
*/
public enum Priority {

/** Urgent */
/** High */
/** Medium */
/** Low */
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* IncidentManager manages a list of incidents. IncidentManager works with the
* XML files that contain the ManagedIncidents in a file when the application is
* not in use. IncidentManager also provides information to the GUI.
* 
*/
public class IncidentManager {

/** Stored instance of this class to ensure only one instantiation */
private static IncidentManager singleton;

/** A list of all incidents currently being managed */
private ManagedIncidentList incidentList;

/**
* Constructor for IncidentManager. Method is private in accordance with the
* Singleton design.
*/
private IncidentManager() {

/**
* If no instance yet exists, creates a new one and returns it. Otherwise,
* returns the existing instance. Preserves Singleton design.
* 
* @return the single instance of IncidentManager
*/
public static IncidentManager getInstance() {

/**
* Saves incidents to an XML file.
* 
* @param fileName XML file to which incidents are to be saved
* @throws IllegalArgumentException if there is an IO error writing to the XML
*                                  file
*/
public void saveManagedIncidentsToFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Loads incidents from an XML file.
* 
* @param fileName XML file from which incidents are to be read
* @throws IllegalArgumentException if there is an IO error reading from the XML
*                                  file
*/
public void loadManagedIncidentsFromFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Begins the use of a new ManagedIncidentList, causing the old list to be lost.
*/
public void createNewManagedIncidentList() {

/**
* Generates a 2D array of incidents. Columns are populated as follows: [0 -
* ManagedIncidents id number] [1 - ManagedIncidents category as a string] [2
* - ManagedIncidents state name] [3 - ManagedIncidents priority as a string]
* [4 - ManagedIncidents name]
* 
* @return a 2D array with contents as described above
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Generates a 2D array of incidents whose category matches the desired
* category. Columns are populated as follows: [0 - ManagedIncidents id number]
* [1 - ManagedIncidents category as a string] [2 - ManagedIncidents state
* name] [3 - ManagedIncidents priority as a string] [4 - ManagedIncidents
* name]
* 
* @param category this category is the criteria on which incidents are added to
*                 the returned array
* @return a 2D array with contents as described above
* @throws IllegalArgumentException if category is null
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Gets an incident from the incident list with a matching ID.
* 
* @param id the id of the desired incident
* @return the incident with the desired id
*/
public ManagedIncident getManagedIncidentById(int id) {

/**
* Carries out the instructions of a Command on the incident with the given ID.
* 
* @param id      the id of the incident to be updated
* @param command describes the changes to be made to the incident
*/
public void executeCommand(int id, Command command) {

/**
* Deletes an incident with the specified ID.
* 
* @param id the id of the incident to be deleted.
*/
public void deleteManagedIncidentById(int id) {

/**
* Adds managed incident to the incident list.
* 
* @param caller   caller of the incident
* @param category category of the incident
* @param priority priority of the incident
* @param name     name of the incident
* @param workNote notes with the incident
* @throws IllegalArgumentException if any of the String fields are null or
*                                  empty
*/
public void addManagedIncidentToList(String caller, Category category, Priority priority,
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A ManagedIncidentList maintains a List of ManagedIncidents.
* ManagedIncidentList supports the following list operations:
* 
* add a ManagedIncident to the list. add a List of Incidents from the XML
* library to the list. remove a ManagedIncident from the list. search for and
* get a ManagedIncident in the list by id. update a ManagedIncident in the list
* through execution of a Command. return the entire list or sublists of itself
* (for example, the ManagedIncidentList can return a List of ManagedIncidents
* filtered by Category).
* 
*/
public class ManagedIncidentList {

/** Array-based list of managed incidents. */
private ArrayList<ManagedIncident> incidents;

/**
* Constructor for ManagedIncidentList. Sets the incident counter to 0.
* Initializes the list of incidents.
*/
public ManagedIncidentList() {

/**
* Adds a managed incident to the incident list.
* 
* @param caller   the caller of the incident
* @param category the incident category
* @param priority the incident priority
* @param name     the incident name
* @param workNote the incident notes
* @return the id of the new incident
* @throws IllegalArgumentException if any of the String fields are null or
*                                  empty
*/
public int addIncident(String caller, Category category, Priority priority, String name,
/**
* Adds incidents of type Incident to the list of incidents. A list of Incident
* objects is generated by IncidentXML's IncidentReader class. Sets the incident
* counter to the new greatest ID value found among incidents.
* 
* @param list a list of Incident objects to be added to the list of incidents
* @throws IllegalArgumentException if category or priority does not match the
*                                  expected possible values defined by
*                                  theCategory or Priority enum, if any of the
*                                  codes are defined as an unacceptable value
*/
public void addXMLIncidents(List<Incident> list) {

/**
* Gets the incidents list.
* 
* @return the incidents list
*/
public List<ManagedIncident> getManagedIncidents() {

/**
* Finds and returns a list of incidents with the category requested.
* 
* @param category the criteria for incidents being added to the returned list
* @return a list of only incidents with the requested category
* @throws IllegalArgumentException if category is null
*/
public List<ManagedIncident> getIncidentsByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Finds and returns an incident with the desired ID. Returns null if it isn't
* found.
* 
* @param id the id of the desired incident
* @return the incident with the desired id
*/
public ManagedIncident getIncidentById(int id) {

/**
* Carries out the instructions of a Command on the incident with the given ID.
* 
* @param id      the id of the incident to be updated
* @param command describes the changes to be made to the incident
*/
public void executeCommand(int id, Command command) {

/**
* Deletes an incident with the specified ID.
* 
* @param id the id of the incident to be deleted.
*/
public void deleteIncidentById(int id) {


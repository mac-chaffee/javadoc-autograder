---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Maintains the data structures for the entire application.
*
*/
public class WolfResultsManager extends Observable implements Observer{

/** The last used filename*/
private String fileName;

/** Status of whether or not unsaved changes have been made to the list*/
private boolean changed;

/** The singleton instance of the WolfResultsManager class*/
private static WolfResultsManager instance;

/** The race list*/
private RaceList list;

/**
* Returns the singleton instance of the WolfResultsManager class
* @return the instance
*/
public static WolfResultsManager getInstance() {

/**
* Constructs a WolfResultsManager object
*/
private WolfResultsManager() {

/**
* Creates a new empty RaceList and registers the WolfResultsManager as an Observer
*/
public void newList() {

/**
* Returns the value stored in changed
* @return the changed status
*/
public boolean isChanged() {

/**
* 
* @param change
*/
private void setChanged(boolean change) {

/**
* Returns the name of the last used file
* @return the filename
*/
public String getFilename() {

/**
* Sets the filename being used
* @param fileName
*/
public void setFilename(String fileName) {

 - Throws: IllegalArgumentException

/**
* Reads the WolfResults from the given filename and adds the WolfResultsManager as an observer to the created RaceList
* @param fileName the file being loaded in
*/
public void loadFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Saves the RaceList to the given filename
* @param fileName the file the results are being saved to
*/
public void saveFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Returns the RaceList
* @return the race list
*/
public RaceList getRaceList() {

/**
* Updates the Race List display
* @param o The WolfResultsManager's observer
* @param arg
*/
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reads WolfResults files.
*/
public class WolfResultsReader {

/**
* Empty constructor for WolfResultsReader
*/
public WolfResultsReader() {

/**
* Generates and returns a RaceList from a given file.
* @param fileName name of file
* @return RaceList
* @throws FileNotFoundException if the file cannot be found
* @throws IllegalArgumentException if there is an error in the formatting of the file
*/
public static RaceList readRaceListFile(String fileName) throws FileNotFoundException {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Writes WolfResults to a file
*/
public class WolfResultsWriter {

/**
* Empty constructor for WolfResultsWriter
*/
public WolfResultsWriter() {

/**
* Writes the given RaceList to a file
* @param fileName name of file
* @param list RaceList
* @throws FileNotFoundException if the file cannot be found
*/
public static void writeRaceFile(String fileName, RaceList races) throws FileNotFoundException {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Object to store the result information of a single runner in a particular race.
*
*/
public class IndividualResult implements Comparable<IndividualResult>, Observer{

/** Name of the runner.*/
private String name;

/** Age of the runner.*/
private String age;

/** Race runner participated in.*/
private Race race;

/** Runner's pace during the race.*/
private RaceTime pace;

/** Time runner took to finish race.*/
private RaceTime time;

/**
* Constructs an IndividualResult from the provided information. Checks the parameters for
* incorrectness. Sets fields to parameters, while trimming the name of the runner of any leading
* or trailing whitespace. Calculates the runner's pace and adds this instance as an observer of the specific race.
* 
* @param race Race the runner participated in.
* @param name Name of the runner.
* @param age Age of the runner.
* @param time Time the runner took to finish the race.
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the name of the runner.
* @return the name of the runner
*/
public String getName() {

/**
* Returns the age of the runner.
* @return the age of the runner
*/
public String getAge() {

/**
* The race the runner participated in.
* @return the race the runner participated in.
*/
public Race getRace() {

/**
* Returns the runner's pace.
* @return the pace of the runner.
*/
public RaceTime getPace() {

/**
* Returns the time the runner took to finish the race.
* @return the time the runner took to finish the race.
*/
public RaceTime getTime() {

/**
* Returns a String representation of the Individual Result in the format necessary for the .md file type.
*/
public String toString() {

/**
* Delegates the comapareTo operation to the RaceTime override of the method.
*/
@Override
public int compareTo(IndividualResult o) {

/**
* Changes the runner's pace if the distance of the race the runner participated in 
* changes.
*/
@Override
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Object that represents a single race characterized by a name, distance,
* date, location and list of runners.
*
*/
public class Race extends Observable{

/** Name of the race.*/
private String name;

/** Length of race.*/
private double distance;

/** Date race took place.*/
private LocalDate date;

/** Location that race took place.*/
private String location;

/** List of runners associated with the race.*/
private RaceResultList results;

/**
* Constructs a race with the given parameters. Checks the legality of each of the
* parameters. Sets the fields to the parameter values while trimming the name and location
* of leading and trailing whitespace. Constructs an empty list of runners.
* 
* @param name Name of the race.
* @param distance Length of the race.
* @param date Date race took place.
* @param location Location that race took place.
*/
public Race(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructs a race with the given parameters. Checks the legality of each of the
* parameters. Sets the fields to the parameter values while trimming the name and location
* of leading and trailing whitespace.
* 
* @param name Name of race.
* @param distance Length of race.
* @param date Date race took place.
* @param location Location that race took place.
* @param results List of runners that participated in the race.
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

/**
* Returns the Name of the race.
* @return the name of the race.
*/
public String getName() {

/**
* Returns the length of the race.
* @return the distance of the race.
*/
public double getDistance() {

/**
* Returns the date that the race took place.
* @return the date that the race took place.
*/
public LocalDate getDate() {

/**
* Returns the location that the race took place.
* @return the location that the race took place.
*/
public String getLocation() {

/**
* Returns the list of runners in the race.
* @return the results of the race.
*/
public RaceResultList getResults() {

/**
* Adds a runner to the list of runners in the race.
* @param result runner to be added to the list of runners in the race.
*/
public void addIndividualResult(IndividualResult result) {

/**
* Sets the distance of the race to the new number. Notifies observers of the race that 
* the race has changed.
* 
* @param distance the new distance to be set.
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* Returns the formated string representation of the race.
*/
@Override
public String toString() {

* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

/**
* Delegates the filtering operation to the RaceResultList method of the same name.
* 
* @param minAge Youngest age of runners to be added.
* @param maxAge Oldest age of runners to be added.
* @param minPace Quickest pace of runners to be added.
* @param maxPace Slowest pace of runners to be added.
* @return A filtered list of runners.
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Object representing the list of all races in the system.
*
*/
public class RaceList extends Observable implements Observer {

/** List of all races in the system.*/
private ArrayList races;

/**
* Constructs an empty list to store races.
*/
public RaceList() {

/**
* Adds a non-null race to the list of races, sets the list as an observer of the race 
* and notifies observers that a race was added.
* @param race Race to added to the system.
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

/**
* Adds a race to the system by constructing a race object then adding it to the
* list of races. Sets the list as an observer of the race, and notifies the list's 
* observers that a new race was added.
* 
* @param name Name of race to be added.
* @param distance Distance of race to be added.
* @param date Date of race to be added.
* @param location Location of race to be added.
*/
public void addRace(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* Removes a specific race from the list and notifies the list's observers
* that the race was removed.
* @param idx Position of the race to be removed in the list of races.
*/
public void removeRace(int idx) {

/**
* Returns a specific race.
* @param idx Position of the race to be returned in the list of races.
* @return the requested race.
*/
public Race getRace(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the number of races in the system.
* @return the size of the races list.
*/
public int size() {

/**
* Notifies observers of the list that one of the races has changed.
*/
public void update (Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents a list of all of the runners that participated in a particular race.
*
*/
public class RaceResultList {

/** List to store all runners and their respective information.*/
private SortedLinkedList<IndividualResult> results;

/**
* Constructs an empty list to store runners.
*/
public RaceResultList() {

/**
* Adds a runner to the list of runners for a specific race.
* @param result The runner to be added.
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* Adds a runner to the race by constructing an IndividualResult object
* then adding it to the list of runners.
* 
* @param race Race that the runner participated in.
* @param name Name of the runner.
* @param age Age of the runner.
* @param time Time that the runner took to finish the race.
*/
public void addResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* Returns a specific runner from the list of runners.
* @param idx Position of the runner in the list.
* @return the requested runner.
*/
public IndividualResult getResult(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the total number of runners in the race.
* @return the size of the results list.
*/
public int size() {

/**
* Returns the list of runners as an array containing their name, age, 
* time to finish and pace.
* @return Array of all runners for a race.
*/
public String[][] getResultsAsArray(){

/**
* Filters the list of results by a range of ages and paces and returns a
* new list containing only those runners that fall between the ranges.
* 
* @param minAge Youngest age of runners to be returned.
* @param maxAge Oldest age of runners to be returned.
* @param minPace Quickest pace of runners to be returned.
* @param maxPace Slowest pace of runners to be returned.
* @return
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Custom ArrayList implementation
*
*/
public class ArrayList implements List{

/** Represents the class version */
@SuppressWarnings("unused")
private static final long serialVersionUID = 1L;

/** Size the ArrayList is initialized to */
private static final int RESIZE = 10;

/** List of Objects */
private Object[] list;

/** Size of list */
private int size;

/** Constructs a new ArrayList of default size 10 */
public ArrayList() {

/**
* Constructs a new ArrayList of the given size
* @param size size of list
*/
public ArrayList(int size) {

/**
* Returns the size of the list
* @return size of the list
*/
@Override
public int size() {

/**
* Checks if the ArrayList is empty
* @return true if the ArrayList is empty and false otherwise
*/
@Override
public boolean isEmpty() {

/**
* Checks if the list contains the given object
* @param o object
* @return true if the list contains the object and false otherwise
*/
@Override
public boolean contains(Object o) {

/**
* Adds the given object to the end of the list
* @param index at which the object is added
* @param o object to be added
* @throws NullPointerException if the object is null
* @return true if the object is added and false otherwise
*/
@Override
public boolean add(Object o) {

 - Throws: NullPointerException

/**
* Returns the data element at the given index
* @param index index of element
* @throws IndexOutOfBoundsException if the index is invalid
* @return element
*/
@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Inserts the specified element at the specified position in this list.
* Shifts the element currently at that position and any subsequent 
* elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index > size())
*/
@Override
public void add(int index, Object element) {

 - Throws: NullPointerException

 - Throws: IndexOutOfBoundsException

 - Throws: IllegalArgumentException

/**
* Increases the capacity of the array to 2 times its current size
*/
private void growArray() {

/**
* Removes the element containing the given object
* @param object to be removed
* @return true if the object was removed and false otherwise
*/
@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of an object in the list, if it exists
* @param o object
* @throws NullPointerException if the object is null
* @return index of object
*/
@Override
public int indexOf(Object o) {

 - Throws: NullPointerException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true (as specified by {@link Collection#add})
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index > size())
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceTime represents a race time
*
*/
public class RaceTime {

/** Whole amount of hours within the race*/
private int hours;

/** Whole amount of minutes within the race*/
private int minutes;

/** Whole amount of seconds within the race*/
private int seconds;

/**
* Constructs a RaceTime object with hours, minutes, and seconds as parameters
* @param hours Whole amount of hours within the race
* @param minutes Whole amount of minutes within the race
* @param seconds Whole amount of seconds within the race
*/
public RaceTime(int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructs a RaceTime object using a String in hh:mm:ss format 
* @param time A string representing the race time in hh:mm:ss format
*/
public RaceTime(String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the whole amount of hours in the race
* @return the hours
*/
public int getHours() {

/**
* Returns the whole amount of minutes in the race
* @return the minutes
*/
public int getMinutes() {

/**
* Returns the whole amount of seconds in the race
* @return the seconds
*/
public int getSeconds() {

/**
* Returns the total amount of seconds that transpire in the race
* @return total race time in seconds
*/
public int getTimeInSeconds() {

/**
* Returns the race time in hh:mm:ss format
* @return race time
*/
@Override
public String toString() {

/**
* Compares two RaceTime objects based on their total race time
* @param time A RaceTime object
* @return how these two RaceTime objects compare
*/
public int compareTo(RaceTime time) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* An implementation of the SortedList interface with a data structure of linked Nodes. Sorts added elements by
* implementing the compareTo() method of the Comparable interface.
* @param <E> generic object
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E>{

/** Size of list */
private int size;

/** ListNode at the front of the list */
private ListNode front;

/**
* Constructs an empty SortedLinkedList
*/
public SortedLinkedList() {

/**
* Returns the size of the SortedLinkedList
* @return size of list
*/
@Override
public int size() {

/**
* Checks if the list is empty
* @return true if the list is empty and false otherwise
*/
@Override
public boolean isEmpty() {

/**
* Checks if the list contains the given object
* @param o object
* @return true if the list contains the object and false otherwise
*/
@Override
public boolean contains(E e) {

/**
* Adds the given object to the end of the list
* @param o object to be added
* @throws NullPointerException if the object is null
* @return true if the object is added and false otherwise
*/
@Override
public boolean add(E e) {

 - Throws: NullPointerException

/**
* Returns the data element at the given index
* @param index index of element
* @throws IndexOutOfBoundsException if the index is out of range (index < 0 || index > size())
* @return element
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Removes the element containing the given object
* @param object to be removed
* @throws IndexOutOfBoundsException if the index is out of range (index < 0 || index > size())
* @return true if the object was removed and false otherwise
*/
@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of an object in the list, if it exists
* @param o object
* @throws NullPointerException if the object is null
* @return index of object
*/
@Override
public int indexOf(E e) {

 - Throws: NullPointerException

/**
* Inner class of SortedLinkedList<E> that handles the functionality of ListNodes, individual objects containing data
* and references to other objects in the list. 
*/
private class ListNode {

/** The actual data element in a ListNode */
private E value;

/** Represents the reference to the next ListNode of the current ListNode */
private ListNode next;

/**
* Constructor of ListNode that sets the element and reference to
* the next ListNode of the current ListNode.
* @param value data element in the node
* @param next next node
*/
public ListNode(E value, ListNode next) {

/**
* Ensures that two objects being compared with the equals() method that are the same will be properly considered
* equal by setting their hashcodes the same.
*/
@Override
public int hashCode() {

/**
* Compares two objects.
* @param obj object to compare
* @return true if the objects are equal and false otherwise, or if object parameter is null
*/
@Override
public boolean equals(Object obj) {

@SuppressWarnings("unchecked")
@SuppressWarnings("rawtypes")
private SortedLinkedList getOuterType() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

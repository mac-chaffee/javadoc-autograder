---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents information about a command executed by the user through
* the UI that results in an incident moving from one state to another.
* 
*
*/
public class Command {

/** Value of the on-hold reason for waiting on the incident caller */
public static final String OH_CALLER = "Awaiting Caller";

/** Value of the on-hold reason for waiting on a change */
public static final String OH_CHANGE = "Awaiting Change";

/** Value of the on-hold reason for waiting on a vendor */
public static final String OH_VENDOR = "Awaiting Vendor";

/** Value of the resolution code for a permanently solved incident */
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** Value of the resolution code for an incident with a workaround */
public static final String RC_WORKAROUND = "Workaround";

/** Value of the resolution code for an unsolved incident */
public static final String RC_NOT_SOLVED = "Not Solved";

/** Value of the resolution code for an incident closed by the caller */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** Value of the cancellation code for a duplicate incident */
public static final String CC_DUPLICATE = "Duplicate";

/** Value of the cancellation code for an unnecessary incident */
public static final String CC_UNNECESSARY = "Unnecessary";

/** Value of the cancellation code for a non-incident */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** Identifier of the individual assigned to work on the incident */
private String ownerId;

/** The text entered by the user when updating the state */
private String note;

/** The type of command */
private CommandValue command;

/** Applicable to the HOLD command - reason that the incident is being put on hold */
private OnHoldReason onHoldReason;

/** Applicable to the RESOLVE command - reason that the incident is being resolved */
private ResolutionCode resolutionCode;

/** Applicable to the CANCEL command - reason that the incident is being cancelled */
private CancellationCode cancellationCode;

/**
* Create a new Command by specifying the attributes of the command that have changed.
* 
* @param commandValue 		type of command executed
* @param ownerId	        identifier of the individual assigned to work on the incident
* @param onHoldReason      the reason that the incident was put on-hold
* @param resolutionCode    the reason that the incident was resolved
* @param cancellationCode  the reason that the incident was cancelled
* @param note				user submitted text describing incident state change
*/
public Command(CommandValue commandValue, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode,
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Get the current command.
* 
* @return the current command value
*/
public CommandValue getCommand() {

/**
* Get the identifier of the individual assigned to work on the incident
* 
* @return identifier of the individual assigned to work on the incident
*/
public String getOwnerId() {

/**
* Get the resolution code of the incident.
* 
* @return the resolution code of the incident
*/
public ResolutionCode getResolutionCode() {

/**
* Get the work note that was created when the command was executed.
* 
* @return work note that was created when the command was executed
*/
public String getWorkNote() {

/**
* Get the code specifying the reason that the command was put on-hold.
* 
* @return on-hold reason code
*/
public OnHoldReason getOnHoldReason() {

/**
* Get the code specifying the reason that the command was cancelled.
* 
* @return cancellation reason code
*/
public CancellationCode getCancellationCode() {

/**
* Enumeration containing the actions that can be taken for an incident.
* 
*
*/
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/**
* Enumeration containing all possible reasons that an incident could be put on hold.
* 
*
*/
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/**
* Enumeration containing all possible reasons that an incident could be resolved.
* 
*
*/
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/**
* Enumeration containing all possible reasons that an incident could be cancelled.
* 
*
*/
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT };

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents an incident in the incident management system. Includes
* all attributes that describe the incident, as well as the current 
* state of the incident. Contains nested classes that implement
* the IncidentState interface.
* 
*
*/
public class ManagedIncident {

/** Value for the inquiry category */
public static final String C_INQUIRY = "Inquiry";

/** Value for the software category */
public static final String C_SOFTWARE = "Software";

/** Value of the hardware category */
public static final String C_HARDWARE = "Hardware";

/** Value of the network category */
public static final String C_NETWORK = "Network";

/** Value of the database category */
public static final String C_DATABASE = "Database";

/** Value of the urgent priority */
public static final String P_URGENT = "Urgent";

/** Value of the high priority */
public static final String P_HIGH = "High";

/** Value of the medium priority */
public static final String P_MEDIUM = "Medium";

/** Value of the low priority */
public static final String P_LOW = "Low";

/** Unique identifier of the incident */
private int incidentId;

/** Identifier of the individual that submitted the incident */
private String caller;

/** Identifier of the individual assigned to work on the incident */
private String owner;

/** Name of the incident */
private String name;

/** Reason that the incident was moved from on hold to in progress */
private String changeRequest;

/** Category of the incident */
private Category category;

/** Priority of the incident */
private Priority priority;

/** Cancellation code of the incident - only applies if incident is in cancelled state */
private CancellationCode cancelCode;

/** On hold reason of the incident - only applies if incident is in on hold */
private OnHoldReason onHoldReason;

/** Resolution code of the incident - only applies if the incident has been resolved */
private ResolutionCode resolutionCode;

/** All notes created during incident state changes */
private ArrayList<String> notes;

/** Value of the new state */
public static final String NEW_NAME = "New";

/** Value of the in-progress state */
public static final String IN_PROGRESS_NAME = "In Progress";

/** Value of the on-hold state */
public static final String ON_HOLD_NAME = "On Hold";

/** Value of the resolved state */
public static final String RESOLVED_NAME = "Resolved";

/** Value of the closed state */
public static final String CLOSED_NAME = "Closed";

/** Value of the cancelled state */
public static final String CANCELED_NAME = "Canceled";

/** The highest id value set in the system */
private static int counter = 0;

/** The current state of the incident */
private IncidentState state;

/** The new state of the incident */
private final NewState newState;

/** The in progress state of the incident */
private final InProgressState inProgressState;

/** The on hold state of the incident */
private final OnHoldState onHoldState;

/** The resolved state of the incident */
private final ResolvedState resolvedState;

/** The closed state of the incident */
private final ClosedState closedState;

/** The cancelled state of the incident */
private final CancelledState cancelledState;

/**
* Create a new ManagedIncident object using specified values for the incident attributes.
* 
* @param caller   individual who submitted the incident
* @param category category of the incident
* @param priority priority of the incident
* @param name     name of the incident
* @param workNote first work note to add
*/
public ManagedIncident(String caller, Category category, Priority priority, String name, String workNote) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Create a new ManagedIncident object from an existing Incident object.
* 
* @param incident incident to use when creating the ManagedIncident
*/
public ManagedIncident(Incident incident) {

/**
* Increase the counter variable by one.
*/
public static void incrementCounter() {

/**
* Get the unique identifier of the incident.
* 
* @return the unique identifier of the incident
*/
public int getIncidentId() {

/**
* Get the change request information for the incident. An incident will have
* this information if it was on-hold and then reopened.
* 
* @return the change request information
*/
public String getChangeRequest() {

/**
* Get the category of the incident.
* 
* @return the category of the incident
*/
public Category getCategory() {

/**
* Get a String representation of the incident category.
* 
* @return a String representation of the incident category
*/
public String getCategoryString() {

/**
* Set the incident category.
* 
* @param category the category to set
* @throws IllegalArgumentException if the category does not exist
*/
private void setCategory(String category) {

 - Throws: IllegalArgumentException

/**
* Get a String representation of the incident priority.
* 
* @return a String representation of the incident priority - urgent, high, medium, or low
*/
public String getPriorityString() {

/**
* Set the incident priority.
* 
* @param priority the priority to set
* @throws IllegalArgumentException if the priority does not exist
*/
private void setPriority(String priority) {

 - Throws: IllegalArgumentException

/**
* Get a String representation of the incident's on-hold reason code.
* 
* @return a String representation of the incident's on-hold reason code
*/
public String getOnHoldReasonString() {

/**
* Set the on-hold reason code of the incident.
* 
* @param reason the reason code to set
* @throws IllegalArgumentException if the on hold reason does not exist
*/
private void setOnHoldReason(String reason) {

 - Throws: IllegalArgumentException

/**
* Get a String representation of the incident's cancellation code.
* 
* @return a String representation of the incident's cancellation code
*/
public String getCancellationCodeString() {

/**
* Set the cancellation code of the incident.
* 
* @param code the cancellation code to set
* @throws IllegalArgumentException if the cancellation code does not exist
*/
private void setCancellationCode(String code) {

 - Throws: IllegalArgumentException

/**
* Get the current state of the incident.
* 
* @return the current state of the incident.
*/
public IncidentState getState() {

/**
* Set the incident state to a new state.
* 
* @param stateToSet the new state to set
* @throws IllegalArgumentException if the state does not exsist
*/
private void setState(String stateToSet) {

 - Throws: IllegalArgumentException

/**
* Get the incident's resolution code.
* 
* @return the resolution code of the incident
*/
public ResolutionCode getResolutionCode() {

/**
* Get a String representation of the incident's resolution code.
* 
* @return a String representation of the incident's resolution code
*/
public String getResolutionCodeString() {

/**
* Set the resolution code of the incident.
* 
* @param code the resolution code to set
* @throws IllegalArgumentException if the resolution code does not exist
*/
private void setResolutionCode(String code) {

 - Throws: IllegalArgumentException

/**
* Get the incident owner.
* 
* @return the incident owner
*/
public String getOwner() {

/**
* Get the name of the incident.
* 
* @return the name of the incident
*/
public String getName() {

/**
* Get the identifier of the individual who created the incident.
* 
* @return the identifier of the individual who created the incident
*/
public String getCaller() {

/**
* Get all notes created during incident state changes.
* 
* @return all notes created during incident state changes
*/
public ArrayList<String> getNotes() {

/**
* Get a String representation of all notes created during incident state changes.
* 
* @return a String representation of all notes created during incident state changes
*/
public String getNotesString() {

/**
* Update the incident by executing the specified command, which can update the incident state.
* 
* @param command the command to execute against the incident
*/
public void update(Command command) {

/**
* Get an XMLIncident representation of the incident.
* 
* @return an XMLIncident representation of the incident
*/
public Incident getXMLIncident() {

/**
* Set the incident counter, which is used when setting incident IDs.
* 
* @param counterValueToSet the value to set the counter
*/
public static void setCounter(int counterValueToSet) {

/**
* Enum representing all possible categories of incidents.
* 
*
*/
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/**
* Enum representing all possible incident priorities.
* 
*
*/
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
/**
* Concrete implementation of the IncidentState that represents an
* incident that is on-hold.
* 
*
*/
public class OnHoldState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**
* Concrete implementation of the IncidentState that represents a new
* incident.
* 
*
*/
public class NewState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**
* Concrete implementation of the IncidentState that represents a 
* resolved incident.
* 
*
*/
public class ResolvedState implements IncidentState {

/**
* Update the state using the information in the provided command.
* 
* @param command command containing information to update the state
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* Concrete implementation of the IncidentState that represents a 
* closed incident.
* 
*
*/
public class ClosedState implements IncidentState {

/**
* Update the state using the information in the provided command.
* 
* @param command command containing information to update the state
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* Concrete implementation of the IncidentState that represents a 
* cancelled incident.
* 
*
*/
public class CancelledState implements IncidentState {

/**
* Update the state using the information in the provided command.
* 
* @param command command containing information to update the state
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* Concrete implementation of the IncidentState that represents an
* incident that is currently in progress.
* 
*
*/
public class InProgressState implements IncidentState {

/**
* Update the state using the information in the provided command.
* 
* @param command command containing information to update the state
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Singleton class containing utility methods for working with a 
* collection of incidents.
* 
*
*/
public class IncidentManager {

private static IncidentManager manager;

private ManagedIncidentList incidentList = new ManagedIncidentList();

/**
* Private constructor to ensure that only a single instance of the class 
* can exist.
*/
private IncidentManager() {

/**
* Get the single instance of IncidentManager. If no instance exists, create
* one.
* 
* @return the instance of IncidentManager
*/
public static IncidentManager getInstance() {

/**
* Save a collection of ManagedIncident objects to the specified file.
* 
* @param file the name of the file to save the incidents to
*/
public void saveManagedIncidentsToFile(String file) {

 - Throws: IllegalArgumentException

/**
* Load a collection of ManagedIncident objects from a file.
* 
* @param file the file from which to load the incidents
* @throws IllegalArgumentException if there is an error reading from the file
*/
public void loadManagedIncidentsFromFile(String file) {

 - Throws: IllegalArgumentException

/**
* Create a new collection of ManagedIncident objects. The collection
* will initially be empty, and any existing collections will be cleared.
*/
public void createNewManagedIncidentList() {

/**
* Get a matrix representation of all incidents. The array has a row for each
* incident, and the column attributes include the id, category, state,
* priority, and name.
* 
* @return matrix representation of all incidents
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Get a matrix representation of all incidents for the specified category. 
* The array has a row for each incident, and the column attributes include 
* the id, category, state, priority, and name.
* 
* @param category the category of events to return in the array
* 
* @return matrix representation of all incidents for the specified category
* @throws IllegalArgumentException if the category is null
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Get an incident by it's ID. 
* 
* @param id the ID of the incident to retrieve
* @return the incident
*/
public ManagedIncident getManagedIncidentById(int id) {

/**
* Execute a command against an incident with the specified ID.
* 
* @param id	  the ID of the incident
* @param command the command to execute
*/
public void executeCommand(int id, Command command) {

/**
* Remove the incident with the specified ID from the collection of 
* incidents.
* 
* @param id the ID of the incident to remove
*/
public void deleteManagedIncidentById(int id) {

/**
* Create a new incident and add it to the list.
* 
* @param caller   the individual who reported the incident
* @param category the incident category
* @param priority priority of the incident
* @param owner    the individual assigned to work on the incident
* @param name	   name of the incident
*/
public void addManagedIncidentToList(String caller, Category category, Priority priority, String owner, String name) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Encapsulates a collection of ManagedIncident objects and includes
* operations on the collection.
* 
*
*/
public class ManagedIncidentList {

/** List of incidents */
private ArrayList<ManagedIncident> managedIncidents;

/**
* Create a new ManagedIncidentList object.
*/
public ManagedIncidentList() {

/**
* Create a new incident and add it to the list.
* 
* @param caller   the individual who reported the incident
* @param category the incident category
* @param priority priority of the incident
* @param owner    the individual assigned to work on the incident
* @param name	   name of the incident
* 
* @return the ID of the new incident added to the list
*/
public int addIncident(String caller, Category category, Priority priority, String owner, String name) {

/**
* Add one or more XML incident objects to the list.
* 
* @param incidents XML incidents to add
*/
public void addXMLIncidents(List<Incident> incidents) {

/**
* Retrieve all incidents in the list.
* 
* @return all incidents in the list
*/
public List<ManagedIncident> getManagedIncidents() {

/**
* Retrieve all incidents in the list that belong to the specified category.
* 
* @param category the category of the incidents to return
* @return all incidents with the specified category
*/
public List<ManagedIncident> getIncidentsByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Get an incident by it's ID. If there are no incidents in the list with that ID,
* return null.
* 
* @param id the ID of the incident to retrieve
* @return the incident
*/
public ManagedIncident getIncidentById(int id) {

/**
* Execute a command against an incident with the specified ID.
* 
* @param id	  the ID of the incident
* @param command the command to execute
*/
public void executeCommand(int id, Command command) {

/**
* Remove the incident with the specified ID from the collection of 
* incidents.
* 
* @param id the ID of the incident to remove
*/
public void deleteIncidentById(int id) {


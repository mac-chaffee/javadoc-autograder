---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class which represents the passengers in the terminal who have not yet
* joined a security checkpoint line
* 
*/
public class PreSecurity implements TransitGroup {

/** From PreSecurity */
private PassengerQueue outsideSecurity;

/**
* Creates specified number of passengers adding each to their respective
* PassengerQueue
* 
* @param numberOfPassengers passengers to join a line
* @param loggingReporter    reporter for logging
* @throws IllegalArgumentException if the number of passengers is less than one
*/
public PreSecurity(int numberOfPassengers, Reporter loggingReporter) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

/**
* Returns the next passengers depart time
* 
* @return depart time for next passenger
*/
public int departTimeNext() {

/**
* Returns the next passenger ready to go.
* 
* @return the next passenger ready to go.\
*/
public Passenger nextToGo() {			
/**
* Checks if there is another passenger
* 
* @return true or false respectively for whether or not there is another
*         passenger
*/
public boolean hasNext() {

/**
* Removes the next passenger 
* 
* @return removed passenger
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The CheckPoint object class used to handle a CheckPoints passengers in its
* line and more.
* 
*/
public class CheckPoint {

/** The line to edit at this checkpoint */
private PassengerQueue line;

/** The time value indicating when the checkpoint will be available */
private int timeWhenAvailable;

/**
* Creates a CheckPoint object.
*/
public CheckPoint() {

/**
* Function that returns the lines size at a checkpoint.
* 
* @return line size
*/
public int size() {

/**
* Removes the next passenger from the line.
* 
* @return the Passenger being removed
*/
public Passenger removeFromLine() {

/**
* Function that determines whether or not the checkpoint line has a passenger
* next up.
* 
* @return true or false whether or not there is a passenger next up in line
*/
public boolean hasNext() {

/**
* Function that returns the next passengers depart time.
* 
* @return the next passengers depart time
*/
public int departTimeNext() {

/**
* Function that returns the next passenger ready to go.
* 
* @return the passenger that is next to go
*/
public Passenger nextToGo() {

/**
* Adds a specified passenger to the line.
* 
* @param passenger passenger to add to the line
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents the collection of security checkpoints and their waiting lines.
* 
*/
public class SecurityArea implements TransitGroup {

/** The checkpoint instance to be used */
private CheckPoint[] check;

/** The maximum number of checkpoints possible */
private static final int MAX_CHECKPOINTS = 17;

/** The minimum number of checkpoints possible */
private static final int MIN_CHECKPOINTS = 3;

/** The error message for input checkpoint values */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** The error message for an improper index being used for a security area */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** The largest index for lines reserved for Fast Track passengers */
private int largestFastIndex;

/** The index of the line reserved for TSA PreCheck/Trusted Travelers */
private int tsaPreIndex;

/**
* Creates a SecurityArea object which handles passengers ready to join a
* checkpoint line.
* 
* @param numberOfCheckpoints number of checkpoints to include
*/
public SecurityArea(int numberOfCheckpoints) {

 - Throws: IllegalArgumentException

/**
* Determines if the number of gates is permissible.
* 
* @param numberOfGates to analyze
* @return true or false based on whether or not they should be allowed
*/
private boolean numGatesOK(int numberOfGates) {

/**
* Adds the passenger to the security line with a given index.
* 
* @param index     index in the line
* @param passenger passenger to add to the line
* @throws 
*/
public void addToLine(int index, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
* Function that returns the index of the shortest security line that ordinary
* passengers are permitted to select.
* 
* @return index of shortest security line to select
*/
public int shortestRegularLine() {

/**
* Function that returns the index of the shortest security line that Fast Track
* passengers are permitted to select.
* 
* @return index of shortest security line to select
*/
public int shortestFastTrackLine() {

/**
* Function that returns the index of the shortest security line that TSA
* PreCheck passengers are permitted to select.
* 
* @return index of shortest security line to select
*/
public int shortestTSAPreLine() {

/**
* A function that returns the number of passengers in the line with a given
* index.
* 
* @param index the index to search for
* @return the number of passengers in the line with the given index
* @throws IllegalArgumentException if index is out of range
*/
public int lengthOfLine(int index) {

/**
* Returns the time that the next passenger will finish clearing security, or
* integer.MAX_VALUE if all lines are empty.
* 
* @return time that next passenger will finish clearing security
*/
public int departTimeNext() {

/**
* Function that returns the next passenger to clear security, or null if there
* is none.
* 
* @return the passenger next to clear security or null
*/
public Passenger nextToGo() {

/**
* Removes and returns the next passenger to clear security.
* 
* @return next passenger to clear security to remove
*/
public Passenger removeNext() {

/**
* Returns the shortest line value within a specified range.
* 
* @param min for range
* @param max for range
* @return shortest line value in range
*/
private int shortestLineInRange(int min, int max) {

/**
* Function that returns the line value that's next to clear.
* 
* @return line value next to clear.
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
public Passenger nextToGo();

/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Object class representing the Fast Track type of Passenger.
* 
*/
public class FastTrackPassenger extends Passenger {

/** The maximum expected process time for the passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** The passengers designated color */
private Color color;

/**
* Constructs a FastTrackPassenger object.
* 
* @param arrivalTime the arrival time of the passenger
* @param processingTime the processing time of the passenger
* @param reporter the reporter to use for the passenger
*/
public FastTrackPassenger(int arrivalTime, int processingTime, Reporter reporter) {

/**
* Enters the passenger into the designated transit group line.
* 
* @param securityArea the transitGroup to enter
*/
@Override
public void getInLine(TransitGroup securityArea) {

/**
* Retrieves the proper passenger color to set for this passenger.
* 
* @return the passengers color
*/
@Override
public Color getColor() {

/**
* Returns the index of the line to join.
* 
* @param securityArea the security area to pick from
* @return the line value for the passenger to identify
*/
private int pickLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Object class representing the Ordinary Passenger type of Passenger.
* 
*/
public class OrdinaryPassenger extends Passenger {

/** The maximum expected process time for the passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** The passengers designated color */
private Color color;

/**
* Constructs a OrdinaryPassenger object.
* 
* @param arrivalTime    the arrival time of the passenger
* @param processingTime the processing time of the passenger
* @param reporter       the reporter to use for the passenger
*/
public OrdinaryPassenger(int arrivalTime, int processingTime, Reporter reporter) {

/**
* Retrieves the proper passenger color to set for this passenger.
* 
* @return the passengers color
*/
@Override
public Color getColor() {

/**
* Enters the passenger into the designated transit group line.
* 
* @param securityArea the line to position the passenger in
*/
@Override
public void getInLine(TransitGroup securityArea) {

/**
* Picks a spot in the line of a transit group for the passenger.
* 
* @param securityArea the security area to pick from
* @return the line value for the passenger to identify
*/
private int pickLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Object representing a passenger which holds data about the passenger such as
* arrival time, wait time, etc. as well as multiple functions for getting in a
* checkpoints line and more.
* 
*/
public abstract class Passenger {

/** This passengers log */
private Reporter myLog;

/** The minimum amount of time a passenger could be processed for */
public static final int MIN_PROCESS_TIME = 20;

/** The passengers set arrival time */
private int arrivalTime;

/** The passengers set wait time */
private int waitTime;

/** The passengers set process time */
private int processTime;

/** The passengers index in their respective line */
private int lineIndex;

/** Boolean for identifying whether or not passenger is awaiting processing */
private boolean waitingProcessing;

/**
* Creates a passenger object with a specified arrivalTime, processingTime, and
* reporter.
* 
* @param arrivalTime    the arrival time of the passenger
* @param processingTime the designated processing time for the passenger
* @param reporter       the reporter to use for the passenger
*/
public Passenger(int arrivalTime, int processingTime, Reporter reporter) {

/**
* Retrieves the arrival time of the passenger.
* 
* @return the arrival time of the passenger
*/
public int getArrivalTime() {

/**
* Retrieves the wait time of the passenger.
* 
* @return the arrival time of the passenger
*/
public int getWaitTime() {

/**
* Sets the wait time for the passenger.
* 
* @param newWaitTime the new wait time set for the passenger
*/
public void setWaitTime(int newWaitTime) {

/**
* Retrieves the process time value of a passenger.
* 
* @return the process time of a passenger
*/
public int getProcessTime() {

/**
* Returns the line index of a passenger.
* 
* @return the line index of a passenger
*/
public int getLineIndex() {

/**
* Determines whether or not the passenger is waiting in the security line.
* 
* @return true or false based on if or if not the passenger is in the security line
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when he/she finishes security
* checks.
*/
public void clearSecurity() {

/**
* Sets the line index for a passenger in a checkpoint line.
* 
* @param newLineIndex the new index to set
*/
protected void setLineIndex(int newLineIndex) {

/**
* Positions the passenger into a line.
* 
* @param securityArea the line to position the passenger in
*/
abstract public void getInLine(TransitGroup securityArea);

/**
* Retrieves the proper passenger color to set for this passenger.
* 
* @return the passengers color
*/
abstract public Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

 - Throws: NoSuchElementException

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Object class representing the Trusted Traveler type of Passenger.
* 
*/
public class TrustedTraveler extends Passenger {

/** The passengers designated color */
private Color color;

/** The maximum expected process time for the passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Constructs a TrustedTraveler Passenger object.
* 
* @param arrivalTime the arrival time of the passenger
* @param processingTime the processing time of the passenger
* @param reporter the reporter to use for the passenger
*/
public TrustedTraveler(int arrivalTime, int processingTime, Reporter reporter) {

/**
* Retrieves the proper passenger color to set for this passenger.
* 
* @return the passengers color
*/
@Override
public Color getColor() {

/**
* Enters the passenger into the designated transit group line.
* 
* @param securityArea the line to position the passenger in
@Override
public void getInLine(TransitGroup securityArea) {

/**
* Picks a spot in the line of a transit group for the passenger.
* 
* @param securityArea the security area to pick from
* @return the line value for the passenger to identify
*/
private int pickLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents the event calendar that handles events for the simulation.
* 
*/
public class EventCalendar {

private TransitGroup highPriority;

private TransitGroup lowPriority;

/**
* Constructs an object representing the event calendar, which keeps track of
* and handles event order for the simulation.
* 
* @param highPriorityTransitGroup the higher prioritized transit group
* @param lowPriorityTransitGroup  the lower prioritized transit group
*/
public EventCalendar(TransitGroup highPriorityTransitGroup, TransitGroup lowPriorityTransitGroup) {

/**
* Returns the passenger from highPriority and lowPriority who will be the next
* to exit the group/line they're currently at the front of, and if there is a
* tie for the next to exit, the passenger from highPriority is returned,
* consequently, if both TransitGroups are empty (their next passengers are
* null), an IllegalStateException is thrown.
* 
* @return the passenger from highPriority and lowPriority who will be the next
*         to exit the group/line they're at the front of
* @throws IllegalStateException if both TransitGroups are empty (their next
*                               passengers are null)
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class representing the logging mechanism used for the simulation, with
* behaviors specified by the interface it implements (Reporter).
* 
*/
public class Log implements Reporter {

/** The number of passengers who have completed the security checks */
private int numCompleted;

/** The total amount of time waited between passengers in a completed run */
private int totalWaitTime;

/** The total amount of processing time between passengers in a completed run */
private int totalProcessTime;

/**
* Creates a Log object used for logging in the simulation.
*/
public Log() {

/**
* Function that returns the number of passengers who've completed the
* simulation.
* 
* @return return the number of passengers that have completed the sim
*/
public int getNumCompleted() {

/**
* Logs the data for the specified passenger.
* 
* @param passenger the passenger to log data for
*/
public void logData(Passenger passenger) {

/**
* Returns the average wait time between passengers.
* 
* @return the average wait time between passengers
*/
public double averageWaitTime() {

/**
* Returns the average process time between passengers.
* 
* @return the average process time between passengers
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Implements the back end process for simulating checkpoints and passengers
* through the system.
* 
*/
public class Simulator {

/** The event calendar for the simulator */
private EventCalendar myCalendar;

/** The log for the simulator to hold data */
private Reporter log;

/** The current passenger */
private Passenger currentPassenger;

/** The The transit group for the ticketing portion */
private TransitGroup inTicketing;

/** The transit group for the security portion */
private TransitGroup inSecurity;

/** The number of passengers present in the simulation */
private int numPassengers;

/**
* Constructs the main simulator object that implements the back end processes
* vital to simulating the checkpoints and passengers in the system.
* 
* @param numberOfCheckpoints     the number of checkpoints present in the
*                                simulation
* @param numberOfPassengers      the number of passengers present in the
*                                simulation
* @param percentTrustedTravelers the percent of trusted traveler passengers
* @param percentFastTrack        the percent of fast track passengers
* @param percentOrdinary         the percent of ordinary passengers
*/
public Simulator(int numberOfCheckpoints, int numberOfPassengers, int percentTrustedTravelers, int percentFastTrack,
/**
* Checks the proper parameters as part of the simulation.
* 
* @param numberOfPassengers      the number of passengers to check
* @param percentTrustedTravelers the percent trusted travelers to check
* @param percentFastTrack        the percent fast track to check
* @param percentOrdinary         the percent ordinary passengers to check
* @throws IllegalArgumentException if parameters are not valid
*/
private void checkParameters(int numberOfPassengers, int percentTrustedTravelers, int percentFastTrack,
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Function that sets up the Simulator object for each percent type of traveler
* 
* @param percentTrustedTravelers the percent of trusted travelers to set up
* @param percentFastTrack        the percent of fast track travelers to set up
* @param percentOrdinary         the percent of ordinary travelers to set up
*/
private void setUp(int numberOfPassengers, int percentTrustedTravelers, int percentFastTrack) {

/**
* Retrieves the reporter of the simulator.
* 
* @return the reporter of the simulator
*/
public Reporter getReporter() {

/**
* Goes through the action of the next passenger from the EventCalendar.
* 
* @throws IllegalStateException if there is no next passenger
*/
public void step() {		
 - Throws: IllegalStateException

/**
* Function that returns true or false based on whether or not there are more
* steps.
* 
* @return true or false as to whether or not there are or aren't more steps
*/
public boolean moreSteps() {

/**
* Function that returns the current index within the Simulator.
* 
* @return the current index
*/
public int getCurrentIndex() {

/**
* Function that returns the current passengers color.
* 
* @return the current passengers color;

*/
public Color getCurrentPassengerColor() {

/**
* Function that returns true or false based on whether or not the current
* passenger has cleared security or not.
* 
* @return true or false respectively for whether or not the current passenger
*         cleared security.
*/
public boolean passengerClearedSecurity() {


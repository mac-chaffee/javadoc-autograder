---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The command class creates objects that encapsulate user actions that cause the state of a 
* ManagedIncident to update. 
*
*/
public class Command {

/**String equivalent of AWAITING_CALLER enum*/
public static final String OH_CALLER = "Awaiting Caller";

/**String equivalent of AWAITING_CHANGE enum*/
public static final String OH_CHANGE = "Awaiting Change";

/**String equivalent of AWAITING_VENDOR enum*/
public static final String OH_VENDOR = "Awaiting Vendor";

/**String equivalent of PERMANENTLY_SOLVED enum*/
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/**String equivalent of WORKAROUND enum*/
public static final String RC_WORKAROUND = "Workaround";

/**String equivalent of NOT_SOLVED enum*/
public static final String RC_NOT_SOLVED = "Not Solved";

/**String equivalent of CALLER_CLOSED enum*/
public static final String RC_CALLER_CLOSED = "Caller Closed";

/**String equivalent of DUPLICATE enum*/
public static final String CC_DUPLICATE = "Duplicate";

/**String equivalent of UNNECESSARY enum*/
public static final String CC_UNNECESSARY = "Unnecessary";

/**String equivalent of NOT_AN_INCIDENT enum*/
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/**Owner id of incident*/
private String ownerId;

/**Work note field of incident*/
private String note;

/**Current command*/
private CommandValue c;

/**Current on hold reason*/
private OnHoldReason onHoldReason;

/**Current cancellation code*/
private CancellationCode cancellationCode;

/**Current resolution code*/
private ResolutionCode resolutionCode;

/**Enumeration of possible command values*/
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/**Enumeration of possible on hold reasons*/
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/**Enumeration of possible resolution codes*/
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/**Enumeration of possible cancellation codes*/
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT };

/**
* Command constructor with 6 parameters, throws an IllegalArgumentException if:
* > A Command with a null CommandValue parameter. A Command must have a CommandValue.
* > A Command with a CommandValue of INVESTIGATE and a null or empty string ownerId.
* > A Command with a CommandValue of HOLD and a null onHoldReason.
* > A Command with a CommandValue of RESOLVE and a null resolutionCode.
* > A Command with a CommandValue of CANCEL and a null cancellationCode.
* > A Command with a note that is null or an empty string.
* @param c CommandValue of given command
* @param ownerId Id of the owner of the incident
* @param onHoldReason OnHoldReason of the incident
* @param resolutionCode ResolutionCode of the incident
* @param cancellationCode CancellationCode of the incident
* @param note Note to be added to WorkNotes field
*/
public Command (CommandValue c, String ownerId, OnHoldReason onHoldReason,
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Getter method for CommandValue
* @return returns the CommandValue of the current command
*/
public CommandValue getCommand() {

/**
* Getter method for OwnerId
* @return returns a string of the incident owner's id
*/
public String getOwnerId() {

/**
* Getter method for ResolutionCode of incident
* @return returns the ResolutionCode of the incident
*/
public ResolutionCode getResolutionCode() {

/**
* Getter method for note
* @return returns a string of note
*/
public String getWorkNote() {

/**
* Getter method for OnHoldReason
* @return returns the OnHoldReason of the incident
*/
public OnHoldReason getOnHoldReason() {

/**
* Getter method for CancellationCode
* @return returns the CancellationCode of the incident
*/
public CancellationCode getCancellationCode() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Managed incident class. Acts as the current selected incident object.
*
*/
public class ManagedIncident {

/**String equivalent of inquiry*/
public static final String C_INQUIRY = "Inquiry";

/**String equivalent of hardware*/
public static final String C_SOFTWARE = "Software";

/**String equivalent of hardware*/
public static final String C_HARDWARE = "Hardware";

/**String equivalent of network*/
public static final String C_NETWORK = "Network";

/**String equivalent of database*/
public static final String C_DATABASE = "Database";

/**String equivalent of urgent*/
public static final String P_URGENT = "Urgent";

/**String equivalent of high*/
public static final String P_HIGH = "High";

/**String equivalent of medium*/
public static final String P_MEDIUM = "Medium";

/**String equivalent of low*/
public static final String P_LOW = "Low";

/**Incident id*/
private int incidentId;

/**Incident caller*/
private String caller;

/**Incident owner*/
private String owner;

/**Name of incident*/
private String name;

/**Change request*/
private String changeRequest;

/**Incident notes*/
private ArrayList<String> notes = new ArrayList<String>();

/**Name of new state*/
public static final String NEW_NAME = "New";

/**Name of in progress state*/
public static final String IN_PROGRESS_NAME = "In Progress";

/**Name of on hold state*/
public static final String ON_HOLD_NAME = "On Hold";

/**Name of resolved state*/
public static final String RESOLVED_NAME = "Resolved";

/**Name of closed state*/
public static final String CLOSED_NAME = "Closed";

/**Name of cancelled state*/
public static final String CANCELED_NAME = "Canceled";

/**Counter that keeps track of id to be given to next incident*/
public static int counter;

/**Current incident priority*/
private Priority priority;

/**Current category of incident*/
private Category category;

/**State of incident*/
private IncidentState state;

/**New state*/
private final IncidentState newState = new NewState();

/**In progress state*/
private final IncidentState inProgressState = new InProgressState();

/**On hold state*/
private final IncidentState onHoldState = new OnHoldState();

/**Resolved state*/
private final IncidentState resolvedState = new ResolvedState();

/**Closed state*/
private final IncidentState closedState = new ClosedState();

/**Cancelled state*/
private final IncidentState canceledState = new CanceledState();

/**On hold reason of incident*/
private OnHoldReason onHoldReason;

/**Cancellation code of incident*/
private CancellationCode cancellationCode;

/**Resolution code of incident*/
private ResolutionCode resolutionCode;

/**
* Constructor method for constructing new Incidents
* @param caller caller of incident
* @param category category of incident
* @param priority priority of incident
* @param name name of incident
* @param workNote work note of incident
*/
public ManagedIncident(String caller, Category category, Priority priority, String name, String workNote) {

 - Throws: IllegalArgumentException

/**
* Constructor method which constructs ManagedIncident from a passed incident
* @param i incident from which to create managed incident
*/
public ManagedIncident(Incident i) {

 - Throws: IllegalArgumentException

/**
* Method which increments counter, essentially updating the next incidents id
*/
public static void incrementCounter() {

/**
* Getter method for incidentId
* @return returns the id of the managed incident
*/
public int getIncidentId() {

/**
* Getter method for changeRequest
* @return returns the changeRequest of the managed incident
*/
public String getChangeRequest() {

/**
* Getter method for category
* @return returns the category of the managed incident
*/
public Category getCategory() {

/**
* Getter method for string equivalent of category
* @return returns the string equivalent of the category of the managed incident
*/
public String getCategoryString() {

/**
* setter method for Category
* @param category category of incident to be set
*/
private void setCategory(String category) {

/**
* Getter method for string equivalent of Priority
* @return string equivalent of the priority of the managed incident
*/
public String getPriorityString() {

/**
* Setter method for priority
* @param priority priority of incident to be set
*/
private void setPriority(String priority) {

/**
* Getter method for string equivalent of incident on hold reason
* @return returns string equivalent of onHold reason of incident
*/
public String getOnHoldReasonString() {

/**
* Setter method for onHoldReason
* @param onHoldReason onHoldReason of incident to be set
*/
private void setOnHoldReason(String onHoldReason) {

/**
* Getter method for string equivalent of Cancellation Code
* @return returns string equivalent of cancellationCode of incident
*/
public String getCancellationCodeString() {

/**
* Setter method for cancellationCode
* @param cancellationCode cancellationCode of incident to be set
*/
private void setCancellationCode(String cancellationCode) {

/**
* Getter method for incidentState of incident
* @return returns state of incident
*/
public IncidentState getState() {

/**
* Setter method for state of incident
* @param state state of incident to be set
*/
private void setState(String state) {

/**
* Getter method for resolutionCode of incident
* @return returns resolutionCode of incident
*/
public ResolutionCode getResolutionCode() {

/**
* Getter method for string equivalence of resolution code
* @return returns string equivalent of resolution code
*/
public String getResolutionCodeString() {

/**
* Setter method of resolutionCode of incident
* @param resolutionCode resolutionCode of incident to be set
*/
private void setResolutionCode(String resolutionCode) {

/**
* Getter method for owner of incident
* @return returns owner of incident as a string
*/
public String getOwner() {

/**
* Getter method for name of incident
* @return returns the name of the incident as a string
*/
public String getName() {

/**
* Getter method for name of caller
* @return returns the caller of the incident as a string
*/
public String getCaller() {

/**
* Getter method for notes of the incident
* @return returns the notes of the incident as an ArrayList of string
*/
public ArrayList<String> getNotes() {

/**
* Getter method for notes of the incident as a string
* @return returns the notes of the incident as a string
*/
public String getNotesString() {

/**
* Main method of the ManagedIncident class. Commands will be passed to the incident and then
* an FSM will handle input, updating state and fields of the ManagedIncident object as necessary
* @param c Command issued to the ManagedIncident object
*/
public void update(Command c) {

/** 
* Getter method for an XML version of managed incident
* Creates a new incident object and sets fields accordingly
* For work notes, need to construct a new work notes object and add notes
* as a string, and then finally add that work notes object to the incident
* @return returns an xml version of managed incident
*/
public Incident getXMLIncident() {

/**
* Setter method for counter
* @param n number to set counter to
*/
public static void setCounter(int n) {

/**
* newState class
*
*/
public class NewState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**
* inProgressState class
*
*/
public class InProgressState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**
* onHoldState class
*
*/
public class OnHoldState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**
* resolvedState class
*
*/
public class ResolvedState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**
* closedState class
*
*/
public class ClosedState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**
* cancelledState class
*
*/
public class CanceledState implements IncidentState {

@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

@Override
public String getStateName() {

/**Enumeration of possible incident category*/
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/**Enumeration of possible incident priorities*/
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* IncidnetManager class which controls a list of managedIncidents. This class interfaces with the
* GUI class in order to allow the user to perform actions on the incidents
*
*/
public class IncidentManager {

/**Singleton field*/
private static IncidentManager singleton = new IncidentManager();

/**List of incidents*/
private ManagedIncidentList incidentList;

/**
* Incident manager constructor
*/
private IncidentManager() { }
/**
* Getter method for global utilization of the IncidentManager class
* @return returns the current instance of IncidentManager
*/
public static IncidentManager getInstance() {

/**
* Saves current incidentList to file given in parameter
* @param file location of file to save to
*/
public void saveManagedIncidentsToFile(String file) {

/**
* Loads incidentList from file given in parameter
* @param file location of file to load from 
*/
public void loadManagedIncidentsFromFile(String file) {

 - Throws: IllegalArgumentException

/**
* Method for creating a new list of managed incidents
*/
public void createNewManagedIncidentList() {

/**
* Getter method for ManagedIncident information
* @return returns information about managed incidents appearing in incident list
* 		   as an array
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Getter method for ManagedIncident information filtered by category
* @param category Category to filter by
* @return returns information about managed incident found by filtering by category
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

/**
* Getter method for ManagedIncident information filtered by Id
* @param id id of selected incident
* @return returns selected managed incident found by filtering by id
*/
public ManagedIncident getManagedIncidentById(int id){

/**
* Method which executes command on selected managedIncident
* @param id id of incident to execute command on
* @param c command to execute on selected incident
*/
public void executeCommand(int id, Command c) {

/**
* Method which deletes selected incident from incidentList
* @param id id of incident to be deleted
*/
public void deleteManagedIncidentById(int id) {

/**
* Method to add a new incident to incidentList
* @param caller caller of incident
* @param category category of incident
* @param priority priority of incident
* @param name name of incident
* @param workNote work note to fill note field of incident
*/
public void addManagedIncidentToList(String caller, Category category, Priority priority,
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Java class for the ManagedIncidentList object. The managed incident list object acts as
* a collection of managed incidents which the user views and interacts with. 
*
*/
public class ManagedIncidentList {

/**Array list of managed incidents*/
private ArrayList<ManagedIncident> incidents;

/**
* Constructor for the ManagedIncidentList. Constructs a new empty list of managed
* incidents
*/
public ManagedIncidentList() {

/**
* Method for adding a new incident to the list of incidents
* @param caller caller of incident
* @param category category of incident
* @param priority priority of incident
* @param name name of incident
* @param workNotes notes to fill work note field
* @return returns the id of the incident 
*/
public int addIncident(String caller, Category category, Priority priority, String name, String workNotes) {

/**
* Method used when loading incident list from file
* @param list list of incidents gathered from XML file to be added to incident list
*/
public void addXMLIncidents(List<Incident> list) {

/**
* Getter method for incident list
* @return returns list of managed incidents
*/
public ArrayList<ManagedIncident> getManagedIncidents() {

/**
* Getter method for incident list filtered by category
* @param category category to filter list by
* @return returns the list of managed incidents as filtered by selected category
*/
public ArrayList<ManagedIncident> getIncidentsByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Getter method for a specific incident found by using id
* @param id id to search in the incident list
* @return returns an incident if its id matches the passed parameter
*/
public ManagedIncident getIncidentById(int id) {

/**
* Method for executing user command on selected incident
* @param id specific id of selected incident which is to have a command executed on it 
* @param c command to be performed on selected incident
*/
public void executeCommand(int id, Command c) {

/**
* Deletes selected incident by matching given id with and existing id in the incident list
* @param id id of the incident to be deleted
*/
public void deleteIncidentById(int id) {


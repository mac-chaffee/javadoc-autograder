---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**Represents passengers who have not joined a checkpoint line
*/
public class PreSecurity implements TransitGroup
/**LinkedList of passengers who have not joined a checkpoint line */
private PassengerQueue outsideSecurity;

/**Generates passengers and adds them to the passenger queue
* @param passengers Number of passengers to add to the queue
* @param reporter Reporter that collected information on this passenger's route through the simulation
*/
public PreSecurity(int passengers, Reporter reporter)
 - Throws: IllegalArgumentException

/**Returns the time the next passenger will depart
* @return int Index of the next passenger to depart
*/
public int departTimeNext()
/**Returns the next passenger to go
* @return Passenger that is next to go
*/
public Passenger nextToGo()
/**Returns whether or not the line has a next passenger
* @return True if the line has a next passenger, false otherwise
*/
public boolean hasNext()
/**Removed the next passenger from the line
* @return Passenger that was removed
*/
public Passenger removeNext()
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**Creates the checkpoint object
*/
public class CheckPoint 
/** The time that the last passenger will clear the security line */
private int timeWhenAvailable = 0;

/** PassengerQueue object representing the checkpoint's line */
private PassengerQueue line;

/**Constructs the check point object
*/
public CheckPoint()
/**Returns the size of the line
* @return size of the passenger queue line
*/
public int size()
/**Removed a passenger from the line
* @return Passenger that was removed
*/
public Passenger removeFromLine()
/**Returns whether or not there is another passenger in the line
* @return Whether or not there is another passenger in the line
*/
public boolean hasNext()
/**The amount of time before the next passenger will clear security (or Integer.MAX_VALUE if empty)
* @return Time before the next passenger will clear
*/
public int departTimeNext()
/**Returns the next passenger in line
* @return Passenger at the front of the line
*/
public Passenger nextToGo()
/**Adds a passenger to the line
* @param passenger Passenger to be added to the line
*/
public void addToLine(Passenger passenger)
* 
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**Class that represents the security area of the terminal
*/
public class SecurityArea implements TransitGroup
/** Maximum number of security checkpoints allowed in the terminal */
private static final int MAX_CHECKPOINTS = 17;

/** Minimum number of security checkpoints allowed in the terminal */
private static final int MIN_CHECKPOINTS = 3;

/** Error message displayed if an invalid number of checkpoints is entered */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** Error message displayed if an invalid range is used as an index */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** Checkpoint and checkpoint line */
private CheckPoint[] check;

//private ArrayList<CheckPoint> check = new ArrayList<CheckPoint>();

//private CheckPoint check = new CheckPoint();

/** The index of the farthest checkpoint for fast track passengers */
private int largestFastIndex = 0;

/** The index of the TSA precheck checkpoint */
private int tsaPreIndex = 0;

/**Constructs the SecurityArea object (presumably creates the checkpoints?)
* @param checkpoints Number of checkpoints in the terminal
*/
public SecurityArea(int checkpoints)
 - Throws: IllegalArgumentException

/**Helper method to determine if the number of checkpoints is within the valid range
* @param checkpoints Number of checkpoints passed to the constructor
* @return Whether or not the number is valid
*/
private boolean numGatesOK(int checkpoints)
/**Adds a passenger to a given checkpoint
* @param index Index of the destination checkpoint
* @param p Passenger to be added
*/
public void addToLine(int index, Passenger p)
 - Throws: IllegalArgumentException

/**Returns the index of the shortest ordinary checkpoint line
* @return Index of the checkpoint
*/
public int shortestRegularLine()
/**Returns the index of the shortest fast track checkpoint line
* @return Index of the checkpoint
*/
public int shortestFastTrackLine()
/**Returns the index of the shortest TSA checkpoint line
* @return Index of the checkpoint
*/
public int shortestTSAPreLine()
/**Returns the length of the checkpoint line with a given index
* @param index Index of checkpoint
* @return Length of checkpoint line
*/
public int lengthOfLine(int index)
 - Throws: IllegalArgumentException

/**Returns the time of the passenger that will depart next (or Integer.MAX_VALUE if the lines are empty)
* @return Time of next passenger
*/
public int departTimeNext()
/**Returns the next passenger to be processed
* @return Next passenger in line
*/
public Passenger nextToGo()
/**Removed next passenger
* @return Passenger that was removed
*/
public Passenger removeNext()
/**Helper method that returns the index of the shortest line in range
* @param lowerRange Lower end of requested range
* @param upperRange Upper end of requested range
* @return Shortest checkpoint line in range
*/
private int shortestLineInRange(int lowerRange, int upperRange) // Rename arguments once I know what this method does
/**Returns the index of the checkpoint line that will clear next
* @return Index of the checkpoint
*/
private int lineWithNextToClear()
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**Class for the FastTrackPassenger passenger
*/
public class FastTrackPassenger extends Passenger
/** Color that represents fast track passengers in the simulation */
private Color color = Color.BLUE;

/** Maximum expected process time for fast track passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**Constructs the fast track passenger object
* @param arrivalTime Arrival time of the passenger
* @param processTime Process time of the passenger
* @param reporter Reporter that collected information on this passenger's route through the simulation
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter reporter)
/**Returns the fast track passengers's color
* @return The passenger's color
*/
public Color getColor()
/**Has the passenger select the checkpoint line to join
* @param transitGroup The transit group this passenger belongs to
*/
public void getInLine(TransitGroup transitGroup)
/**Method for how fast track passenger selects a checkpoint line
* @param transitGroup The transit group this passenger belongs to
* @return Index of line the passenger selected
*/
private int pickLine(TransitGroup transitGroup)
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**Class for the ordinary passenger
*/
public class OrdinaryPassenger extends Passenger
/** Color that represents ordinary passenger in the simulation */
private Color color = Color.RED;

/** Maximum expected process time for ordinary passengers */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**Constructs the ordinary passenger object
* @param arrivalTime Arrival time of the passenger
* @param processTime Process time of the passenger
* @param reporter Reporter that collected information on this passenger's route through the simulation
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter reporter)
/**Returns the ordinary passenger's color
* @return The passenger's color
*/
public Color getColor()
/**Has the passenger select the checkpoint line to join
* @param transitGroup The transit group this passenger belongs to
*/
public void getInLine(TransitGroup transitGroup)
/**Method for how ordinary passenger selects a checkpoint line
* @param transitGroup The transit group this passenger belongs to
* @return Index of line the passenger selected
*/
private int pickLine(TransitGroup transitGroup)
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**Super class for the three types of passengers
*/
public abstract class Passenger 
/** Minimum time it takes a passenger to be processed */
public static final int MIN_PROCESS_TIME = 20;

/** Arrival time of the passenger */
private int arrivalTime;

/** Time it takes to process the passenger */
private int processTime;	
/** Index of the location of the passenger in the line */
private int lineIndex;

/** Wait time of the passenger */
private int waitTime;

/** Whether or not the passenger is waiting in the processing line */
private boolean waitingProcessing;

/** Reporter that keeps track of passenger information */
private Reporter myLog;

/**Constructs a passenger object
* @param arrivalTime Passenger's arrival time
* @param processTime Passenger's process time
* @param reporter Reporter that collected information on this passenger's route through the simulation
*/
public Passenger(int arrivalTime, int processTime, Reporter reporter)
 - Throws: IllegalArgumentException

/**Returns the passenger's arrival time
* @return Arrival time
*/
public int getArrivalTime()
/**Returns the passenger's wait time
* @return Wait time
*/
public int getWaitTime()
/**Sets the passenger's wait time
* @param waitTime New wait time
*/
public void setWaitTime(int waitTime)
/**Returns the passenger's process time
* @return Process time
*/
public int getProcessTime()
/**Returns the passenger's line index
* @return Line index
*/
public int getLineIndex()
/**Returns whether or not the passenger is waiting in the security line
* @return Waiting in the security line
*/
public boolean isWaitingInSecurityLine()
/**Sets the passenger's line index
* @param lineIndex New line index
*/
protected void setLineIndex(int lineIndex)
/**Removed passenger from the waiting line when the passenger finishes the security checks
*/
public void clearSecurity()
/**Decides how the passenger will pick a line (must be overridden in subclasses)
* @param transitGroup Subclass that the passenger belongs to
*/
public abstract void getInLine(TransitGroup transitGroup);

/**Returns color the passenger should be represented by in the simulator
* @return Color of the passenger
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**Class for the TrustedTraveler passenger
*/
public class TrustedTraveler extends Passenger
/** Color that represents trusted travelers in the simulation */
private Color color = Color.GREEN;

/** Maximum expected process time for trusted travelers */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**Constructs the trusted traveler object
* @param arrivalTime Arrival time of the passenger
* @param processTime Process time of the passenger
* @param reporter Reporter that collected information on this passenger's route through the simulation
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter reporter)
/**Returns the trusted traveler's color
* @return The Passenger's color
*/
public Color getColor()
/**Has the passenger select the checkpoint line to join
* @param transitGroup The transit group this passenger belongs to
*/
public void getInLine(TransitGroup transitGroup)
/**Method for how trusted traveler selects a checkpoint line
* @param transitGroup The transit group this passenger belongs to
* @return Index of line the passenger selected
*/
private int pickLine(TransitGroup transitGroup)
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**Class that decides which passenger will be the next to get processed
*/
public class EventCalendar 
/** The transit group with the higher priority in a tie */
private TransitGroup highPriority;

/** The transit group with lower priority in a tie */
private TransitGroup lowPriority;

/**Constructs the EventCalendar object with a high/low priority group
* @param highPriority The transit group with the higher priority in a tie
* @param lowPriority The transit group with lower priority in a tie
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority)
/**Decides which passenger is next to be processed
* @return The next passenger
*/
public Passenger nextToAct()
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**Class that logs timing information about the passengers
*/
public class Log implements Reporter
/** Number of passengers that made it through the simulation */
private int numCompleted = 0;

/** Total wait time of passengers that made it through the simulation */
private int totalWaitTime = 0;

/** Total process time of passengers that made it through the simulation */
private int totalProcessTime = 0;

@Override
public int getNumCompleted() 
@Override
public void logData(Passenger p) 
@Override
public double averageWaitTime() 
@Override
public double averageProcessTime() 
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**Class the does all the back end calculations for the simulation
*/
public class Simulator 
/** Number of passengers in the simulation */
//private int numPassengers = 0;

/** Group of passengers in the ticketing area */
private TransitGroup inTicketing;

/** Group of passengers in the security area */
private TransitGroup inSecurity;

/** The current passenger being processed */
private Passenger currentPassenger;

/** Reporter that holds passenger information */
private Reporter log;

/** Event calendar that handles the passengers */
private EventCalendar myCalendar;

/**Creates a simulation with beginning parameters
* @param checkpoints Number of checkpoints
* @param passengers Number of passengers
* @param precheckPercent Percent of passengers that are precheck
* @param fasttrackPercent Percent of passengers that are fast track
* @param ordinaryPercent Percent of passengers that are ordinary
*/
public Simulator(int checkpoints, int passengers, int precheckPercent, int fasttrackPercent, int ordinaryPercent)
/**Checks to see of the parameters are valid
* @param passengers Number of passengers 
* @param precheckPercent Percent of passengers that are precheck
* @param fasttrackPercent Percent of passengers that are fast track
* @param ordinaryPercent Percent of passengers that are ordinary
*/
private void checkParameters(int passengers, int precheckPercent, int fasttrackPercent, int ordinaryPercent) // Not exactly sure which 4 its wants
 - Throws: IllegalArgumentException

*/
 - Throws: IllegalArgumentException

*/
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**I am unsure of what this method does
* @param i ??
* @param j ??
* @param k ??
private void setUp(int i, int j, int k) // Again not sure what arguments these are
/**Returns the simulator reporter
* @return Reporter to be returned
*/
public Reporter getReporter()
/**Steps through the simulation
*/
public void step()
 - Throws: IllegalStateException

 - Throws: IllegalStateException

/**Returns whether or not the simulator has steps remaining
* @return Whether or not the simulator has steps remaining
*/
public boolean moreSteps()
/**Returns the index of the checkpoint the current passenger is in
* @return int Index of the checkpoint
*/
public int getCurrentIndex()
 - Throws: IllegalArgumentException

/**Returns the color of the current passenger
* @return Color of the current passenger
*/
public Color getCurrentPassengerColor()
/**Returns whether or not the current passenger has cleared security
* @return Whether or not the current passenger has cleared security
*/
public boolean passengerClearedSecurity()

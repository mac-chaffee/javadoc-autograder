---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Maintains the data structures for the entire application
*  
*/
public class WolfResultsManager extends Observable implements Observer {

/** Field for the list of Races */
private RaceList list;

/** Field for an instance of WolfResultsManager */
private static WolfResultsManager instance;

/** Field for the last used filename */
private String filename;

/** Field indicating whether the list has changed or not */
private boolean changed;

/**
* Returns the single WolfResultsManager instance
* @return An instance of WolfResultsManager
*/
public static WolfResultsManager getInstance() {

/**
* Constructor for WolfResultsManager
*/
private WolfResultsManager() {

/**
* Creates a new empty RaceList and registers the WolfResultsManager as an Observer
*/
public void newList() {

/**
* Returns the value stored in changed
* @return The value of changed
*/
public boolean isChanged() {

/**
* Sets the value of changed
* @param change The value to which changed is being set
*/
private void setChanged(boolean change) {

/**
* Returns the filename
* @return The filename
*/
public String getFilename() {

/**
* Sets the filename
* @param filename The String to which filename is being set
*/
public void setFilename(String filename) {

 - Throws: IllegalArgumentException

/**
* Reads the WolfResults from the given filename
* @param filename The name of the file from which the WolfResults are being read
*/
public void loadFile(String filename) {

 - Throws: IllegalArgumentException

/**
* Saves the RaceList to the given filename
* @param filename The name of the file to which list is being saved
*/
public void saveFile(String filename) {

/**
* Returns the RaceList list
* @return The RaceList list
*/
public RaceList getRaceList() {

/**
* Deals with a change in a RaceList being observed by WolfResultsManager
* @param o The Observable for the RaceList that changes
* @param arg The change in the RaceList
*/
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A class for reading Wolf Results files.
* 
*/
public class WolfResultsReader {

/**
* Constructor for WolfResultsReader
*/
public WolfResultsReader() {

/**
* Reads a list of races from a file
* 
* @param fileName The name of the file from which the race is being read
* @return A RaceList containing the races from the file
*/
public static RaceList readRaceListFile(String fileName) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A class for writing Wolf Results files
* 
*/
public class WolfResultsWriter {

/**
* Constructor for WolfResultsWriter
*/
public WolfResultsWriter() {

/**
* Writes a list of races to a file
* @param fileName The name of the file being written 
* @param list The list of races being written to the file
*/
public static void writeRaceFile(String fileName, RaceList list) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A representation of one persons race result for a single race in the
* WolfResultsManager application
* 
*/
public class IndividualResult extends Observable implements Observer, Comparable<IndividualResult> {

/** Field for the pace of the individual */
private RaceTime pace;

/** Field for the time it takes the individual to complete the race */
private RaceTime time;

/** Field for the name of the individual */
private String name;

/** Field for the age of the individual */
private int age;

/** Field for the race that the individual is participating in */
private Race race;

/**
* Constructor for IndividualResult
* 
* @param race The race the individual is participating in
* @param name The individual's name
* @param age  The individual's age
* @param time The time it took the individual to complete the race
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the race the individual competed in
* 
* @return The race the individual competed in
*/
public Race getRace() {

/**
* Returns the individual's name
* 
* @return The individual's name
*/
public String getName() {

/**
* Returns the individual's age
* 
* @return The individual's age
*/
public int getAge() {

/**
* Returns the time it took the individual to complete the race
* 
* @return The individual's time
*/
public RaceTime getTime() {

/**
* Returns the individual's pace
* 
* @return The individual's pace
*/
public RaceTime getPace() {

/**
* Sets the individual's pace
*/
private RaceTime setPace() {

/**
* Compares two IndividualResult objects
* 
* @param i The IndividualResult object being compare to the current object
* @return 0 if the two are the same, -1 if i is lower, and 1 if i is greater
*/
public int compareTo(IndividualResult i) {

/**
* Returns the IndividualResult as a String
* 
* @return A String representation of the IndividualResult
*/
public String toString() {

/**
* Updates the pace if the race changes
* 
* @param o   The Observable for the race
* @param arg The change in the race
*/
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A representation of a Race in the WolfResultsManager application
* 
*/
public class Race extends Observable {

/** Field for the name of the race */
private String name;

/** Field for the distance of the race */
private double distance;

/** Field for the date of the race */
private LocalDate date;

/** Field for the location of the race */
private String location;

/** Field for the results of the race */
private RaceResultList results;

/**
* Constructor for Race with parameters for name, distance, date, location, and
* results
* 
* @param name     The name of the race
* @param distance The distance of the race
* @param date     The date of the race
* @param location The location of the race
* @param results  The results of the race
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructor for Race with parameters for name, distance, date, and location
* 
* @param name     The name of the race
* @param distance The distance of the race
* @param date     The date of the race
* @param location The location of the race
*/
public Race(String name, double distance, LocalDate date, String location) {

/**
* Returns the name of the Race
* 
* @return the name
*/
public String getName() {

/**
* Returns the distance of the Race
* 
* @return the distance
*/
public double getDistance() {

/**
* Returns the date of the Race
* 
* @return the date
*/
public LocalDate getDate() {

/**
* Returns the location of the Race
* 
* @return the location
*/
public String getLocation() {

/**
* Returns the results of the Race
* 
* @return the results
*/
public RaceResultList getResults() {

/**
* Adds an IndividualResult to the list of results
* 
* @param i The IndividualResult being added
*/
public void addIndividualResult(IndividualResult i) {

/**
* Sets the distance of the Race
* 
* @param distance the distance to set
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* Returns Race as a String
* 
* @return A String representation of the Race
*/
public String toString() {

/**
* Generates the hashCode for the Race
* 
* @return The hashCode
*/
@Override
public int hashCode() {

/**
* Checks whether an Object is equal to the Race
* 
* @param obj The Object being compared
* @return True if the two are equal, false otherwise
*/
@Override
public boolean equals(Object obj) {

/**
* Filters a Race's results based on certain parameters
* 
* @param minAge  The minimum age
* @param maxAge  The maximum age
* @param minPace The minimum pace
* @param maxPace The maximum pace
* @return A RaceResultsList containing the filtered results
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* An ArrayList of Race objects
* 
*/
public class RaceList extends Observable implements Observer {

/** Field for the list of races */
private ArrayList races;

/**
* Constructor for RaceList
*/
public RaceList() {

/**
* Adds a Race to the ArrayList.
* 
* @param race The Race being added
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

/**
* Creates and adds a Race to the ArrayList
* 
* @param name     The name of the race being added
* @param distance The distance of the race being added
* @param date     The date of the race being added
* @param location The location of the race being added
* @throws IllegalArgumentException if the Race cannot be created properly form
*                                  the given fields
*/
public void addRace(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* Removes a Race from races
* 
* @param idx The index of the Race being removed
*/
public void removeRace(int idx) {

/**
* Returns the Race at a certain index
* 
* @param idx The index of the desired Race
* @return The Race at idx
*/
public Race getRace(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the size of the ArrayList races
* 
* @return The size of races
*/
public int size() {

/**
* Updates the Race if the race changes
* 
* @param o   The Observable for the race
* @param arg The change in the race
*/
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class containing a SortedLinkedList of IndividualResult objects
* 
*/
public class RaceResultList {

/** Field for the results of a race */
private SortedLinkedList<IndividualResult> results;

/**
* Constructor for RaceResultList
*/
public RaceResultList() {

/**
* Adds an IndividualResult to the list of results 
* @param result The IndividualResult being added
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* Creates and adds an IndividualResult to the list of results
* @param race The Race to which the IndividualResult is added 
* @param name The name of the individual
* @param age The age of individual
* @param time The time it took the individual to run the race
*/
public void addResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* Returns the IndividualResult at a certain index
* @param idx The index of the desired IndividualResult
* @return The IndividualResult at idx
*/
public IndividualResult getResult(int idx) {

 - Throws: IndexOutOfBoundsException

/** 
* Returns the size of the SortedLinkedList results
* @return The size of results
*/
public int size() {

/**
* Returns results as a String array
* @return Results as a String array
*/
public String[][] getResultsAsArray() {

/**
* Filters a Race's results based on certain parameters
* @param minAge The minimum age
* @param maxAge The maximum age
* @param minPace The minimum pace
* @param maxPace The maximum pace
* @return A RaceResultsList containing the filtered results
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* An implementation of the List interface with an array data structure.
* Citation: Some of the methods have been adapted from our lab work.
* 
*
*/
public class ArrayList implements List {

/** Constant for the serialVersionUID */
private static final long serialVersionUID = 13456;

/** Constant for the resizing of an ArrayList */
private static final int RESIZE = 10;

/** Field for the array of objects */
private Object[] list;

/** Field for the size of the ArrayList */
private int size;

/**
* Constructor for ArrayList without a capacity parameter
*/
public ArrayList() {

/**
* Constructor for ArrayList with a capacity parameter
* 
* @param capacity The capacity of the ArrayList
*/
public ArrayList(int capacity) {

 - Throws: IllegalArgumentException

/**
* Returns the size of ArrayList
* 
* @return The size of ArrayList
*/
@Override
public int size() {

/**
* Checks whether the ArrayList is empty
* 
* @return True if the ArrayList is empty, false otherwise
*/
@Override
public boolean isEmpty() {

/**
* Checks whether the ArrayList contains a certain Object
* 
* @param o The Object being checked for
* @return True if the Object is in the ArrayList, false otherwise
*/
@Override
public boolean contains(Object o) {

/**
* A private helper method to double the size of the array if it is at capacity.
* (Citation: taken form lab work)
*/
private void growArray() {

/**
* Adds an Object to the end of the ArrayList.(Citation: taken from lab work)
* 
* @param o The Object being added
* @return True if the Object is added successfully, false otherwise
*/
@Override
public boolean add(Object o) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Returns the Object at a certain index. (Citation: taken form lab work)
* 
* @param index The index of the Object
* @return The Object at that index
* @throws IndexOutOfBoundsException if the index is out of range
*/
@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Adds an Object to the ArrayList at a certain index (Citation: taken form lab
* work)
* 
* @param index   The index where the Object is being added
* @param element The Object being added
* @throws IllegalArgumentException  if the element is a duplicate
* @throws IndexOutOfBoundsException if the index is out of range
* @throws NullPointerException      if Object parameter is null
*/
@Override
public void add(int index, Object element) {

 - Throws: IndexOutOfBoundsException

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Removes the Object at a certain index. (Citation: taken form lab work)
* 
* @param index The index of the Object being removed
* @return The Object being removed
*/
@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of a certain Object
* 
* @param o The Object for which the index is desired
* @return The index of o
*/
@Override
public int indexOf(Object o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more than
* Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e such
* that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional operation).
*
* Lists that support this operation may place limitations on what elements may
* be added to this list. In particular, some lists will refuse to add null
* elements, and others will impose restrictions on the type of elements that
* may be added. List classes should clearly specify in their documentation any
* restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return boolean true (as specified by List add() method from Collections)
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (less than 0
*                                   or greater / equal to size
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if any)
* and any subsequent elements to the right (adds one to their indices).
*
* @param index   index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException      if the specified element is null and this
*                                   list does not permit null elements
* @throws IllegalArgumentException  if some property of the specified element
*                                   prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (less than 0 or greater than size)
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one from
* their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (less than 0 or greater than or equal to size)
*/
/**
* Returns the index of the first occurrence of the specified element in this
* list, or -1 if this list does not contain the element. More formally, returns
* the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))), or
* -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in this
*         list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A class that represents a race time h:mm:ss
* 
*/
public class RaceTime {

/** Field for the hours */
private int hours;

/** Field for the minutes */
private int minutes;

/** Field for the seconds */
private int seconds;

/**
* Constructor for RaceTime with parameters for hours, minutes, and seconds
* @param hours The hours
* @param minutes The minutes
* @param seconds The seconds
*/
public RaceTime (int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

/**
* Constructor for RaceTime with a parameter for the time as a String
* @param time The race time as a String
*/
public RaceTime (String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the hours of the RaceTime
* @return The hours
*/
public int getHours() {

/**
* Returns the minutes of the RaceTime
* @return The minutes
*/
public int getMinutes() {

/**
* Returns the seconds of the RaceTime
* @return The seconds
*/
public int getSeconds() {

/**
* Returns the RaceTime in seconds
* @return The RaceTime in seconds
*/
public int getTimeInSeconds() {

/**
* Returns RaceTime as a String
* @return A String containing the RaceTime in the correct format
*/
public String toString() {

/**
* Compares two RaceTime objects
* @param time The RaceTime object being compared to the RaceTime
* @return 0 if the two are equal, -1 if time is lower, and 1 if time is greater
*/
public int compareTo(RaceTime time) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* An implementation of the SortedList interface with a value structure of
* linked Nodes. Citation: adapted from our lab work (LinkedAbstractList).
* 
* 
* @param <E> The type of SortedLinkedList
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/** Front is a Node of type E that refers to the front Node in the list. */
private Node front;

/** Size of list */
private int size;

/**
* Constructor for SortedLinkedList
*/
public SortedLinkedList() {

/**
* Returns the size of the SortedLinkedList
* 
* @return The size
*/
@Override
public int size() {

/**
* Checks whether the SortedLinkedList is empty
* 
* @return True if the SortedLinkedList is empty, false otherwise
*/
@Override
public boolean isEmpty() {

/**
* Checks whether the SortedLinkedList contains a certain value
* 
* @param e The value being checked for
* @return True if the value is present, false otherwise
*/
@Override
public boolean contains(E e) {

/**
* Adds a value to the SortedLinkedList
* 
* @param e The value being added
* @return True if the value is added successfully, false otherwise
*/
@Override
public boolean add(E e) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Returns the element at a certain index
* 
* @param index The index of the element
* @return The element at that index
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Removes the element at a certain index
* 
* @param index The index of the element being removed
* @return The element being removed
*/
@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of a certain element
* 
* @param e The element for which the index is desired
* @return The index of e
*/
@Override
public int indexOf(E e) {

/**
* Generates the hashCode for the SortedLinkedList
* 
* @return The hashCode
*/
@Override
public int hashCode() {

/**
* Checks whether an Object is equal to the SortedLinkedList
* 
* @param obj The Object being compared to the SortedLinkedList
* @return True if the two are equal, false otherwise
*/
@SuppressWarnings("rawtypes")
@Override
public boolean equals(Object obj) {

/**
* Returns a String representation of SortedLinkedList
*/
@Override
public String toString() {

/**
* An inner class of SortedLinkedList that contains an E and a reference to the
* next Node in the SortedList
* 
*/
private class Node {

/** Field for the following Node */
protected Node next;

/** Field for the value of the Node */
protected E value;

/**
* Constructor for Node
* 
* @param value The value of the Node
* @param next  The Node that follows the one being constructed
*/
public Node(E value, Node next) {

/**
* Constructor with only the value in the node
* 
* @param value an E object, the value in the node
*/
private Node(E value) {

/**
* Generates hashCode for the Node
* 
* @return The hashCode
*/
@Override
public int hashCode() {

/**
* Checks whether an object equals the Node
* 
* @param obj The Object being compared
* @return True if the two are equal, false otherwise
*/
@SuppressWarnings("unchecked")
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @since 1.2
* 
* @param <E> The type of SortedList
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more than
* Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a such
* that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true as specified
* @throws NullPointerException     if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (less than 0
*                                   and greater than or equal to size)
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices). Returns
* the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (less than 0
*                                   and greater than or equal to size)
*/
/**
* Returns the index of the first occurrence of the specified element in this
* list, or -1 if this list does not contain the element. More formally, returns
* the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))), or
* -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in this
*         list, or -1 if this list does not contain the element
*/

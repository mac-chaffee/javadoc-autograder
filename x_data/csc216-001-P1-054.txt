---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* PreSecurity class implements TransitGroup and holds the PassengerQueue for
* Passengers that are waiting to enter a security line.
* 
*/
public class PreSecurity implements TransitGroup {

/** Passenger line for people outside of security */
private PassengerQueue outsideSecurity = new PassengerQueue();

/**
* PreSecurity constructor creates the Passengers outside of security.
* 
* @param passengers - number of passengers in the simulation.
* @param log        - Reporter that reports the results for each Passenger.
*/
public PreSecurity(int passengers, Reporter log) {

/**
* departTimeNext gets the time that the next Passenger will leave the for the
* security lines.
* 
* @return time - time that the next Passenger leaves for the security lines.
*/
public int departTimeNext() {

/**
* nextToGo gets the Passenger that is next to enter the security lines.
* 
* @return p - Passenger at that is next to enter security.
*/
public Passenger nextToGo() {

/**
* hasNext returns if the line has a Passenger in it.
* 
* @return b - true if the line is not empty, false otherwise.
*/
public boolean hasNext() {

/**
* removeNext removes the next Passenger from the line and returns that
* Passenger.
* 
* @return p - Passenger that is removed from the line.
*/
public Passenger removeNext() {

 - Throws: IllegalStateException

/**
* getOutsideSecurity returns the line of passengers outside security.
* @return outsideSecurity - line of passengers outside security.
*/
public PassengerQueue getOutsideSecurity() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* CheckPoint class holds the line for a singular checkpoint in the simulation.
* 
*/
public class CheckPoint {

/**
* The time that the last passenger in line will clear security and leave the
* line.
*/
private int timeWhenAvailable;

/** Line of Passengers for the checkpoint */
private PassengerQueue line = new PassengerQueue();

/**
* CheckPoint constructor creates an empty checkpoint.
*/
public CheckPoint() {

/**
* size returns the size of the line for the checkpoint.
* 
* @return size - the size of the line.
*/
public int size() {

/**
* removeFromLine removes the Passenger at the front of the line from the line.
* 
* @return p - Passenger that was removed from the line.
*/
public Passenger removeFromLine() {

/**
* hasNext returns if the line is empty or has another Passenger.
* 
* @return b - true if the line is not empty, false otherwise.
*/
public boolean hasNext() {

/**
* departTimeNext returns the process time for the person at the front of the
* line.
* 
* @return depart - the process time for the Passenger at the front of the line.
*/
public int departTimeNext() {

/**
* nextToGo returns the passenger at the front of the security line.
* 
* @return p - the Passenger at the front of the line.
*/
public Passenger nextToGo() {

/**
* addToLine adds the parameter to the line, updating the Passenger's waitTime,
* arrivalTime, and processTime.
* 
* @param p - the Passenger to add to the checkpoint line.
*/
public void addToLine(Passenger p) {

/**
* getTimeWhenAvailable gets the time when the last passenger will clear the
* line.
* 
* @return timeWhenAvailable - time when last passenger will clear security
*         line.
*/
public int getTimeWhenAvailable() {

/**
* getLine returns the line for the CheckPoint.
* 
* @return line - the line of Passengers for the CheckPoint.
*/
public PassengerQueue getLine() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* SecurityArea class implements TransitGroup and holds all the checkpoints for
* the simulator in an ArrayList with the necessary behavior for which line a
* Passenger should choose.
* 
*/
public class SecurityArea implements TransitGroup {

/** Maximum number of checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** Minimum number of checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** Error message for invalid number of checkpoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least " + MIN_CHECKPOINTS
/** Error message for invalid index */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** The largest index for lines reserved for Fast Track passengers. */
private int largestFastIndex;

/** The index for the fast track line. */
private int tsaPreIndex;

/** ArrayList for all of the CheckPoints in the simulator. */
private ArrayList<CheckPoint> check = new ArrayList<CheckPoint>();

/**
* SecurityArea constructor checks if the number of checkpoints is valid, sets
* the index for the largest fast track index, and sets the index for the TSA
* line.
* 
* @param checkpoints - number of checkpoints entered by the user.
*/
public SecurityArea(int checkpoints) {

/**
* numGatesOK checks if the number of gates entered by the user is OK, throws
* IllegalArgumentException if it is not.
* 
* @param checkpoints - number of checkpoints to check.
* @return true if number of gates is between 3 and 17 inclusive, false
*         otherwise.
*/
private boolean numGatesOK(int checkpoints) {

 - Throws: IllegalArgumentException

/**
* addToLine adds the Passenger to the security line with the index given by the
* first parameter.
* 
* @param lineIndex - index of the line to add the Passenger to.
* @param p         - Passenger to be added to a line.
*/
public void addToLine(int lineIndex, Passenger p) {

/**
* shortestRegularLine determines which security line is the shortest for
* Ordinary passengers.
* 
* @return index of the shortest security line for an Ordinary passenger.
*/
public int shortestRegularLine() {

/**
* shortestFastTrackLine determines which security line is the shortest for
* FastTrack passengers
* 
* @return index of the shortest security line for a FastTrack passenger.
*/
public int shortestFastTrackLine() {

/**
* shortestTSAPreLine determines which security line is the shortest for Trusted
* Traveler passengers.
* 
* @return lineIndex - index of the shortest security line for a Trusted
*         Traveler passenger.
*/
public int shortestTSAPreLine() {

/**
* lengthOfLine determines the number of passengers in a specified line.
* 
* @param lineIndex - index of line to determine number of passengers.
* @return length - the length of the line (number of passengers in the line).
*/
public int lengthOfLine(int lineIndex) {

 - Throws: IllegalArgumentException

/**
* departTimeNext returns the time that the next passenger will finish clearing
* security. Integer.MAX_VALUE if all lines are empty.
* 
* @return lowProcessTime - time that the next passenger will finish clearing
*         security.
*/
public int departTimeNext() {

/**
* nextToGo returns the next passenger to clear security, or null if there is
* none.
* 
* @return next - the next Passenger to go.
*/
public Passenger nextToGo() {

/**
* removeNext removes and returns the next passenger to clear security.
* 
* @return next - the next passenger to clear security.
*/
public Passenger removeNext() {

/**
* shortestLineInRange returns the shortest line within the two parameters.
* 
* @param lowIndex  - the low index for which to check the line lengths.
* @param highIndex - the high index for which to check the line lengths.
* @return index - index of the shortest line in range of the parameters.
*/
private int shortestLineInRange(int lowIndex, int highIndex) {

/**
* lineWithNextToClear returns the index of the line that is next to be empty.
* 
* @return index - index of line that is next to be empty.
*/
/**
private int lineWithNextToClear() {

*/
/**
* hasPassengers determines if the SecurityArea has passengers that still need
* to be cleared.
* 
* @return true if there are more Passengers, false otherwise.
*/
public boolean hasPassengers() {

/**
* getTSAPreIndex returns the index that is the TSA line.
* @return tsaPreIndex - the index of the TSA line.
*/
public int getTSAPreIndex() {

/**
* getLargestFastIndex returns the largest index for fast track passengers.
* @return largestFastIndex - largest index for fast track passengers.
*/
public int getLargestFastIndex() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* FastTrackPassenger class extends Passenger and holds information for a Fast
* Track passenger in the simulator.
* 
*/
public class FastTrackPassenger extends Passenger {

/** Maximum expected process time for fast track passenger. */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** Color for fast track passenger. */
private Color color;

/**
* FastTrackPassenger constructor calls the super constructor to construct the
* FastTrackPassenger.
* 
* @param arrivalTime - time passenger arrives to a security line.
* @param processTime - time passenger spends at front of security line to be
*                    processed.
* @param log         - reporter that logs the information about the passenger.
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* getColor method gets the color for the passenger.
* 
* @return c - color for passenger.
*/
public Color getColor() {

/**
* getInLine method selects the appropriate security line, sets the line index
* from the parent class, and places the passenger in the selected line.
* 
* @param t - transit group interface for the passenger.
*/
public void getInLine(TransitGroup t) {

/**
* pickLine method picks the appropriate security line for the passenger.
* 
* @param t - transit group interface for the passenger.
* @return index - index for the security line for the passenger to pick.
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* OrdinaryPassenger class extends Passenger and holds information for an
* Ordinary passenger in the simulator.
* 
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum expected process time for ordinary passenger. */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** Color for ordinary passenger. */
private Color color;

/**
* OrdinaryPassenger constructor calls the super constructor to construct the
* OrdinaryPassenger.
* 
* @param arrivalTime - time passenger arrives to a security line.
* @param processTime - time passenger spends at front of security line to be
*                    processed.
* @param log         - reporter that logs the information about the passenger.
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* getColor method gets the color for the passenger.
* 
* @return c - color for passenger.
*/
public Color getColor() {

/**
* getInLine method selects the appropriate security line, sets the line index
* from the parent class, and places the passenger in the selected line.
* 
* @param t - transit group interface for the passenger.
*/
public void getInLine(TransitGroup t) {

/**
* pickLine method picks the appropriate security line for the passenger.
* 
* @param t - transit group interface for the passenger.
* @return index - index for the security line for the passenger to pick.
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Abstract class Passenger is the parent class for FastTrackPassenger,
* OrdinaryPassenger, and TrustedTraveler. It holds data for the Passengers
* arrivalTime, waitTime, processTime, lineIndex, and if the Passenger is
* processing.
* 
*/
public abstract class Passenger {

/** Minimum process time for a Passenger */
public static final int MIN_PROCESS_TIME = 20;

/** Arrival time of Passenger */
private int arrivalTime;

/** Wait time of Passenger in security line */
private int waitTime;

/** Process time of Passenger at the front of security line */
private int processTime;

/** Line index of security line for the Passenger */
private int lineIndex;

/** True if the Passenger is in a security line, false otherwise. */
private boolean waitingProcessing;

/** Light color. */
protected static final int LIGHT_COLOR = 153;

/** Dark color. */
protected static final int DARK_COLOR = 255;

/** Reporter for the simulation results. */
private Reporter myLog;

/** Variable for determining if Passenger is in Ticketing line. */
private boolean inPreSecurity;

/**
* returns if the passenger is in the ticketing line or not.
* @return inPreSecurity - true if in the ticketing line, false otherwise.
*/
public boolean isInPreSecurity() {

/**
* setInPreSecurity sets the parameter inPreSecurity.
* @param inPreSecurity - if the passenger is in the ticketing line or not.
*/
public void setInPreSecurity(boolean inPreSecurity) {

/** Abstract method for a Passenger to get in line
*  @param t - TransitGroup for the Passenger.
*/
public abstract void getInLine(TransitGroup t);

/** Abstract method for getting Passenger color
*  @return Color - color for the Passenger.
*/
public abstract Color getColor();

/**
* Passenger constructor gets called by the child classes to construct the
* specific Passenger.
* 
* @param arrivalTime - arrival time for a Passenger
* @param processTime - process time for a Passenger
* @param log         - reporter for the results of the Passengers travel
*                    through security
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

/**
* getArrivalTime method returns the arrival time of a Passenger
* 
* @return arrivalTime - arrival time of Passenger
*/
public int getArrivalTime() {

/**
* getWaitTime method returns the wait time of a Passenger in security line
* 
* @return waitTime - wait time of Passenger in security line
*/
public int getWaitTime() {

/**
* setWaitTime sets the wait time in a security line for the Passenger
* 
* @param waitTime - the time a Passenger waits in a security line
*/
public void setWaitTime(int waitTime) {

/**
* getProcessTime gets the time a Passenger spends at the front of a security
* line
* 
* @return processTime - time spent at front of security line
*/
public int getProcessTime() {

/**
* getLineIndex gets the line index for the security line a Passenger is in.
* 
* @return lineIndex - index of security line Passenger is in.
*/
public int getLineIndex() {

/**
* isWaitingInSecurityLine returns if the Passenger is waiting in a security
* line.
* 
* @return waitingProcessing - true if Passenger is in security line, false
*         otherwise.
*/
public boolean isWaitingInSecurityLine() {

/**
* clearSecurity method removes the Passenger from the security line after
* processing
*/
public void clearSecurity() {

/**
* setLineIndex sets the line index for the Passenger
* 
* @param lineIndex - the line index for the Passenger
*/
protected void setLineIndex(int lineIndex) {

/**
* setWaitingProcessing sets if the Passenger is being processed.
* @param b - true if is processing, false otherwise.
*/
public void setWaitingProcessing(boolean b) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* TrustedTraveler class extends Passenger and holds information for a Trusted
* Traveler in the simulator.
* 
*/
public class TrustedTraveler extends Passenger {

/** Maximum expected process time for trusted traveler. */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** Color for trusted traveler. */
private Color color;

/**
* TrustedTraveler constructor calls the super constructor to construct the
* TrustedTraveler.
* 
* @param arrivalTime - time passenger arrives to a security line.
* @param processTime - time passenger spends at front of security line to be
*                    processed.
* @param log         - reporter that logs the information about the passenger.
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter log) {

/**
* getColor method gets the color for the passenger.
* 
* @return c - color for passenger.
*/
public Color getColor() {

/**
* getInLine method selects the appropriate security line, sets the line index
* from the parent class, and places the passenger in the selected line.
* 
* @param t - transit group interface for the passenger.
*/
public void getInLine(TransitGroup t) {

/**
* pickLine method picks the appropriate security line for the passenger.
* 
* @param t - transit group interface for the passenger.
* @return index - index for the security line for the passenger to pick.
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* EventCalendar determines which passenger to process next in the ticketing
* area or the security area.
* 
*/
public class EventCalendar {

/** The high priority Transit Group, the ticketing line */
/** The high priority Transit Group, the security line */
/**
* EventCalendar constructor initializes the high priority and low priority
* transit group from the parameters.
* 
* @param high - ticketing area transit group.
* @param low  - security area transit group.
*/
public EventCalendar(TransitGroup high, TransitGroup low) {

/**
* nextToAct returns the passenger from highPriority and lowPriority who will be
* the next to exit the line theyre currently at the front of. In case of a tie
* for the next to exit, the passenger from highPriority is returned. If both
* TransitGroups are empty (their next passengers are null), an
* IllegalStateException is thrown.
* 
* @return Passenger that will be the next to exit the line.
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Log implements Reporter and records the Passengers statistics in the simulator.
*/
public class Log implements Reporter {

/** The number of Passengers that have completed the simulation. */
private int numCompleted;

/** The total wait time for a Passenger. */
private int totalWaitTime;

/** The total process time for a Passenger. */
private int totalProcessTime;

/**
* Log constructor initializes the log for the Passengers by intitializing the instance variables to zero.
*/
public Log() {

/**
* logData records the data for the Passenger parameter by updating numCompleted, totalWaitTime, and totalProcesstime.
* @param p - Passenger to be logged.
*/
public void logData(Passenger p) {

/**
* averageWaitTime calculates the average wait time for each Passenger in the simulator.
* @return avg - the average wait time.
*/
public double averageWaitTime() {

/**
* averageProcessTime calculates the average process time for each Passenger in the simulator.
* @return avg - the average process time.
*/
public double averageProcessTime() {

/**
* getNumCompleted returns the number of Passengers that have been cleared through security.
* @return num - number of Passengers that have been cleared through security.
*/
@Override
public int getNumCompleted() {

/**
* getTotalWaitTime returns the total wait time for the passengers in the simulation.
* @return totalWaitTime - total wait time for passengers in the simulation.
*/
public int getTotalWaitTime() {

/**
* getTotalProcessTime returns the total process time for the passengers.
* @return totalProcessTime - total process time of the passengers.
*/
public int getTotalProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Simulator interacts with the user interface and deals with setting up and
* running the simulation.
* 
*/
public class Simulator {

/** The number of Passengers in the simulation entered by the user. */
private int numPassengers;

/** Reporter used for the simulation */
private Reporter log;

/**
* Initialized to null, is the passenger most recently returned by
* myCalendar.nextToAct().
*/
private Passenger currentPassenger;

/** Event calendar for the simulation. */
private EventCalendar eventCal;

/** Security area for the simulation. */
private TransitGroup inSecurity;

/** PreSecurity area for the simulation. */
private TransitGroup inTicketing;

/**
* Simulator constructor checks to ensure the entered values are valid,
* initializes a log to record the data, sets the passenger type distribution
* for Ticketing, establishes the security area, and creates the PreSecurity
* area, and the EventCalendar. Throws IllegalArgumentException if parameters
* are not valid.
* 
* @param checkpoints - number of checkpoints
* @param passengers  - number of passengers
* @param tsa         - % of passengers that are TSA PreCheck/Trusted Travelers
* @param ft          - % of passengers that are FastTrack
* @param ordinary    - % of passengers that are Ordinary
*/
public Simulator(int checkpoints, int passengers, int tsa, int ft, int ordinary) {

/**
* checkParameters checks that the number of passengers are valid, and that the
* % of passengers sums to 100.
* 
* @param passengers - number of passengers
* @param tsa        - % of passengers that are TSA Precheck/Trusted Travelers
* @param ft         - % of passengers that are FastTrack
* @param ordinary   - % of passengers that are Ordinary
*/
private void checkParameters(int passengers, int tsa, int ft, int ordinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* setUp sets up the passenger type distribution in Ticketing.
* 
* @param tsa      - % of passengers that are TSA Precheck/Trusted Travelers
* @param ft       - % of passengers that are FastTrack
* @param ordinary - % of passengers that are Ordinary
*/
private void setUp(int tsa, int ft, int ordinary) {

/**
* getReporter returns the reporter being used for the simulation.
* 
* @return log - Reporter for the simulation.
*/
public Reporter getReporter() {

/**
* step goes through the action of the next passenger from the EventCalendar.
* The passenger is removed from the corresponding area (ticketing or security
* area). If there is no next passenger, an IllegalStateException is thrown.
*/
public void step() {

 - Throws: IllegalStateException

/**
* moreSteps returns if there are more steps to be taken in the simulator.
* 
* @return b - true if there are more steps to take in the simulator, false
*         otherwise.
*/
public boolean moreSteps() {

/**
* getCurrentIndex returns the index of the security checkpoint line for
* currentPassenger.
* 
* @return index - index of the security checkpoint line for the
*         currentPassenger.
*/
public int getCurrentIndex() {

/**
* getCurrentPassengerColor returns the color for the currentPassenger.
* 
* @return c - color of currentPassenger.
*/
public Color getCurrentPassengerColor() {

/**
* passengerClearedSecurity true if currentPassenger just cleared a security
* checkpoint and finished processing, false otherwise.
* 
* @return b - true if currentPassenger cleared security checkpoint, false
*         otherwise.
*/
public boolean passengerClearedSecurity() {

/**
* getInSecurity returns the transit group for the security area.
* @return inSecurity - transit group for security area.
*/
public TransitGroup getInSecurity() {

/**
* getInTicketing returns the transit group for the ticketing area.
* @return inTicketing - transit group for ticketing area.
*/
public TransitGroup getInTicketing() {

/**
* getCurrentPassenger returns the current passenger in the simulator.
* @return currentPassenger - the current passenger in the simulator.
*/
public Passenger getCurrentPassenger() {


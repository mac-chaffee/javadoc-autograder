---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* WolfResultsManager is a singleton that maintains the RaceList, reads from,
* and writes to the *.md files for the WolfResults application. The
* WolfResultsManager has a RaceList , the last used filename, and if the list
* has changed.
* 
*/
public class WolfResultsManager extends Observable implements Observer {

/** The name of the file to use for the manager */
private String filename;

/** Boolean representing whether or not the observable has changed */
private boolean changed;

/** Singleton type of instance of wolfResultManger*/
private static WolfResultsManager instance = null;

/** race list*/
private RaceList raceList;

/**
* Returns the single WolfResultsManager instance. If the instance is null, a
* WolfResultsManager is constructed.
* 
* @return the single WolfResultsManager instance, or null if the instance is
*         null
*/
public static WolfResultsManager getInstance() {

/**
* Constructs the WolfResultsManager by creating an empty RaceList and
* registering as an Observer of the list. The constructor is private.
*/
private WolfResultsManager() {

/**
* Creates a new empty RaceList and registers the WolfResultsManager as an
* Observer. The method notifies observers of the change if no exception is
* thrown.
*/
public void newList() {

/**
* Returns the value stored in changed.
* 
* @return the value stored in changed
*/
public boolean isChanged() {

/**
* Marks the observable as changed.
* 
* @param newValue new observable boolean value
*/
private void setChanged(boolean newValue) {

/**
* Returns the filename.
* 
* @return the filename variable's string value
*/
public String getFilename() {

/**
* Method that handles setting the file name to a new one. If the filename
* parameter is null, the empty string, or string with whitespace only, an
* IllegalArgumentException is thrown, while leading and trailing whitespace
* should be trimmed from filename prior to assigning to field. Otherwise, the
* filename field is set. Additionally, setFileName() doesnt need to notify
* observers since the state of the RaceList is not changed by setFilename().
* 
* @param fileName the new file name
*/
public void setFilename(String fileName) {

 - Throws: IllegalArgumentException

/**
* Method that reads the WolfResults from the given filename and adds the
* WolfResultsManager as an observer to the created RaceList. The filename field
* is set to the parameter value (if valid). The method also sets the changed
* flag to false. Additionally, the method notifies observers of the change if
* no exception is thrown.
* 
* @param fileName the name of the file to load
*/
public void loadFile(String fileName) {

/**
* Method that saves the RaceList to the given filename, and sets the changed
* flag to false.
* 
* @param fileName the name of the file to save the data to
*/
public void saveFile(String fileName) {

/**
* Returns the RaceList. Use this method to access the RaceList an all of its
* contents from the GUI.
* 
* @return the RaceList
*/
public RaceList getRaceList() {

/**
* Method that determines if a RaceList observed by WolfResultsManager changes,
* and if so, the update() method is automatically called. WolfResultsManager
* should propagate the notification of the change to its Observers and changed
* should be updated to true.
* 
* @param observable the observable to update
* @param object the object to update
*/
public void update(Observable observable, Object object) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A class capable of reading Wolf Result files.
* 
*/
public class WolfResultsReader {

/**
* Constructor responsible for creating the WolfResultsReader object.
*/
public WolfResultsReader() {

/**
* Returns a RaceList from the given file. The file is processed as described in
* Use Case 2. An IllegalArgumentException is thrown on any error or incorrect
* formatting.
* 
* @param filename the name of the file to retrieve the RaceList from
* @return the RaceList from the specified file
* @throws IllegalArgumentException if the file could not be found
*/
@SuppressWarnings("unused")
public static RaceList readRaceListFile(String filename) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class capable of writing to WolfResults files.
* 
*/
public class WolfResultsWriter {

/**
* Constructor for the WolfResultsWriter object.
*/
public WolfResultsWriter() {

/**
* writes the given RaceList to the given file. The file is created as described
* in Use Case 3. An IllegalArgumentException is thrown on any error.
* 
* @param filename the name of the file to write data to
* @param list     the list to write data from
* @throws IllegalArgumentException if the file could not be found
*/
public static void writeRaceFile(String filename, RaceList list) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The representation of one person's race result for a single race in the
* application.
* 
*/
public class IndividualResult implements Observer, Comparable<IndividualResult> {

private String name;

private int age;

/** Race for runner attended */
private Race race;

/** Pace of runner */
private RaceTime pace;

/** Time for runner*/
private RaceTime time;

/**
* The constructor of an IndividualResult. should throw an
* IllegalArgumentException if (a) race is null; (b) name is null, empty string,
* or all whitespace; (c) age is negative; or (d) time is null. name should be
* trimmed of leading and/or trailing whitespace. The pace field should be set
* to the pace based on time and race distance. An Observer should be added for
* race.
* 
* @param race the race parameter
* @param name the respective name
* @param age  the respective age
* @param time the respective time
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* Returns the race of the IndividualResult.
* 
* @return the race parameter
*/
public Race getRace() {

/**
* Returns the respective name of the IndividualResult.
* 
* @return the respective name
*/
public String getName() {

/**
* Returns the Age for the respective IndividualResult.
* 
* @return the age
*/
public int getAge() {

/**
* Returns the Time for the respective IndividualResult.
* 
* @return the time
*/
public RaceTime getTime() {

/**
* Returns the Pace for the respective IndividualResult.
* 
* @return the pace
*/
public RaceTime getPace() {

/**
* Compares two IndividualResult objects based on their times. Make sure to
* delegate to RaceTime's compareTo method.
* 
* @param other the IndividualResult to compare to
* @return time comparison between IndividualResults
*/
public int compareTo(IndividualResult other) {

/**
* Makes a string representation of an IndividualResult based on its name, age,
* time, and pace fields. Should look like (IndividualResult[name=NAME, age=AGE,
* time=TIME, pace=PACE]).
* 
* @return string representation of IndividualResult
*/
public String toString() {

/**
* The pace is updated if the Race that the IndividualResult is observing
* notified its observers of a change.
* 
* @param o the observable to update
* @param arg the object to update
*/
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents a single race and all of its functionality.
* 
*/
public class Race extends Observable {

/** The name of the race */
private String name;

/** The distance of the race */
private double distance;

/** The local date of the race */
private LocalDate date;

/** The location of the race */
private String location;

/** results of race */
private RaceResultList results;

/**
* The constructor for a race. Should throw an IllegalArgumentException if name
* is null, an empty string, or all whitespace, or if distance is non-positive,
* or if date is null, or if location is null, or if results is null.
* Additionally, results should be assigned a new RaceResultList.
* 
* @param name     the name of the race
* @param distance the distance of the race
* @param date     the date of the race
* @param location the location of the race
*/
public Race(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* The constructor for a race. Should throw an IllegalArgumentException if name
* is null, an empty string, or all whitespace, or if distance is non-positive,
* or if date is null, or if location is null, or if results is null.
* Additionally, name and location should be trimmed of leading and/or trailing
* whitespace.
* 
* @param name     the name of the race
* @param distance the distance of the race
* @param date     the date of the race
* @param location the location of the race
* @param results  the results of the race
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

/**
* Returns the name of the race.
* 
* @return the name of the race
*/
public String getName() {

/**
* Returns the distance of the race.
* 
* @return the distance of the race
*/
public double getDistance() {

/**
* Returns the LocalDate of the race.
* 
* @return the LocalDate of the race
*/
public LocalDate getDate() {

/**
* Returns the location of the race.
* 
* @return the location of the race
*/
public String getLocation() {

/**
* Returns the RaceResultList of the race.
* 
* @return the RaceResultList of the race
*/
public RaceResultList getResults() {

/**
* Adds a result to results. Observers of Race are notified of the change.
* 
* @param result the result to add to results
*/
public void addIndividualResult(IndividualResult result) {

/**
* Sets the distance field to a newly specified one. Should throw an
* IllegalArgumentException if the distance is non-positive. Additionally,
* Observers of Race are notified of the change.
* 
* @param distance the newly specified distance
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* The string representation of a Race as specified in the name, distance, date,
* and location fields. It should look like (name (distance miles) on date at
* location).
* 
* @return the String representation of a Race
*/
public String toString() {

/**
* Returns a hashCode representation based on the name, distance, date, and
* location fields of a Race.
* 
* @return the hashCode for a Race
*/
public int hashCode() {

/**
* Determines whether or not two Race objects are equal to one another.
* 
* @param obj the object to check equality for
* @return true or false based on whether or not the Race objects are identical
*         respectively
*/
public boolean equals(Object obj) {

/**
* Returns a list of results such that runner's age is between minAge and maxAge
* (inclusive) and runner's pace is between minPace and maxPace (inclusive).
* Should throw an IllegalArgumentException if minPace or maxPace is not a valid
* RaceTime.
* 
* @param minAge  the minimum age to filter by
* @param maxAge  the maximum age to filter by
* @param minPace the minimum pace to filter by
* @param maxPace the maximum pace to filter by
* @return a filtered RaceResultList of Race objects
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Contains an ArrayList which holds Race objects.
* 
*/
public class RaceList extends Observable implements Observer {

/** ArrayList of race instance */
private ArrayList<Race> races;

/**
* The constructor for a RaceList.
*/
public RaceList() {

/**
* Adds a race to the list, or throws an IllegalArgumentException if the race is
* null. Additionally, Observer is added for the race, and Observers of RaceList
* are notified of the change.
* 
* @param race the race to add to the list
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructs a Race using the provided name, distance, date, and location
* parameters and throws an IllegalArgumentException if Race constructor throws
* an exception, and if not, adds the race to the list. Additionally, Observer
* is added for the race, and Observers of RaceList are notified of the change.
* 
* @param name     the name of the race
* @param distance the distance of the race
* @param date     the date the race took place
* @param location the location the race took place at
*/
public void addRace(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* Removes a race at a specified index idx. Additionally, Observers of RaceList
* are notified of the change.
* 
* @param idx the index to remove a race from
*/
public void removeRace(int idx) {

/**
* Gets a race at a specified index idx, or throws an IndexOutOfBoundsException
* if idx is out of bounds.
* 
* @param idx the index to get a race from
* @return the race at a specified index
*/
public Race getRace(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the number of races in the list.
* 
* @return the number of races in the list
*/
public int size() {

/**
* This update method is called if the Race that the RaceList is observing
* notified its observers of a change. If the Observable is an instance of Race,
* then the observers of the RaceKust should be updated. The current instance is
* passed to notifyObservers().
* 
* @param o   the observable for updating
* @param arg the object for updating
*/
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceResultList class responsible for holding RaceResults in a list.
* 
*/
public class RaceResultList {

/** A SortedLinkedList of runners */
private SortedLinkedList<IndividualResult> results;

/**
* Constructor that creates the SortedLinkedList.
*/
public RaceResultList() {

/**
* Adds a Result. Should throw an IllegalArgumentException if result is null.
* 
* @param result the result to add
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* Adds race to the list. The method constructs a IndividualResult using the
* provided parameters and throws an IllegalArgumentException if
* IndividualResult constructor throws an exception.
* 
* @param race race to add
* @param name name to add
* @param age  age to add
* @param time time to add
*/
public void addResult(Race race, String name, int age, RaceTime time) {

@SuppressWarnings("unused")
/**
* Gets the result at the specified index i. Should throw an
* IndexOutOfBoundsException if i is out of bounds.
* 
* @param i the index to search at
* @return the IndividualResult at the specified index
*/
public IndividualResult getResult(int i) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the number of results in the list.
* 
* @return the number of results in the list
*/
public int size() {

/**
* Returns a 2D array of strings representing results. First dimension is the
* result/runner (in sorted order by time). Second dimension are the different
* fields: [0]: name, [1]: age, [2]: time, [3]: pace. If the list is empty, an
* empty 2D array is returned.
* 
* @return a 2D array of strings representing results
*/
public String[][] getResultsAsArray() {

/**
* Returns list of results such that runners age is between minAge and maxAge
* (inclusive) and runners pace is between minPace and maxPace (inclusive).
* Throws IllegalArgumentException if minPace or maxPace is not a valid
* RaceTime.
* 
* @param minAge  the minimum age to filter
* @param maxAge  the maximum age to filter
* @param minPace the minimum pace to filter
* @param maxPace the maximum pace to filter
* @return the list of results after being filtered
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class representing an ArrayList and its functionality for containing and
* manipulating objects. Used some code from my lab group (Arman Karshenas)
* LLL-01 in creating this ArrayList class.
* 
*/
public class ArrayList implements Serializable, List {

/** The ArrayList.java class's unique identifier */
private static final long serialVersionUID = 1L;

/** The GUI resize incrementation value */
private static final int RESIZE = 10;

/** The list of objects in the ArrayList */
private Object[] list;

/** The size of the ArrayList */
private int size;

/**
* The constructor for an ArrayList without the size specified.
*/
public ArrayList() {

/**
* The constructor for an ArrayList with a specified size.
* 
* @param size the size of the ArrayList
*/
public ArrayList(int size) {

/**
* Returns the size of the list.
* 
* @return the size of the list
*/
@Override
public int size() {

/**
* Returns a boolean that states whether or not the list is empty.
* 
* @return true or false based on whether or not the list is empty or not empty
*         respectively
*/
@Override
public boolean isEmpty() {

/**
* Returns a boolean that states whether or not the specified object is present
* in the list.
* 
* @param object the object to check for
* @return true or false based on whether or not the object was found in the
*         list or not respectively
*/
@Override
public boolean contains(Object object) {

/**
* Adds an object to the list.
* 
* @param object the object to add to the list
* @return true or false based on whether or not the object could be added to
*         the list
*/
@Override
public boolean add(Object object) {

 - Throws: NullPointerException

 - Throws: IndexOutOfBoundsException

 - Throws: IllegalArgumentException

/**
* Adds a specified object to a specified index within the list.
* 
* @param index  the index to add the object at in the list
* @param object the object to add to the list
*/
@Override
public void add(int index, Object object) {

 - Throws: NullPointerException

 - Throws: IndexOutOfBoundsException

 - Throws: IllegalArgumentException

/**
* Returns an object at a specific index in the list.
* 
* @param index the index to check for the object in the list
* @return the object found at the specified index
*/
@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Removes an object from a specified index from the list.
* 
* @param index the index to remove the object from
* @return the object being removed
*/
@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of a specified object from the list.
* 
* @param object the object to check the index of in the list
* @return the index of the specified object in the list
*/
@Override
public int indexOf(Object object) {

/**
* Increase the local array's size
*/
private void growArray() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more than
* Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e such
* that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional operation).
*
* Lists that support this operation may place limitations on what elements may
* be added to this list. In particular, some lists will refuse to add null
* elements, and others will impose restrictions on the type of elements that
* may be added. List classes should clearly specify in their documentation any
* restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true if it is possible to add the object
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index is less
*                                   than 0 or idnex is greater than or equal to
*                                   size)
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if any)
* and any subsequent elements to the right (adds one to their indices).
*
* @param index   index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException      if the specified element is null and this
*                                   list does not permit null elements
* @throws IllegalArgumentException  if some property of the specified element
*                                   prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index is less
*                                   than zero or index is greater than size)
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one from
* their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index is less
*                                   than 0 or index is greater than or equal to
*                                   size
*/
/**
* Returns the index of the first occurrence of the specified element in this
* list, or -1 if this list does not contain the element. More formally, returns
* the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))), or
* -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in this
*         list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents a race time in the format: hh:mm:ss, where (a) hours may be listed
* as one digit or many digits with leading zeros and (b) minutes and seconds
* are always two digits between 0 and 59 (inclusive).
* 
*/
public class RaceTime {

/** The hours parameter for RaceTime */
private int hours;

/** The minutes parameter for RaceTime */
private int minutes;

/** The seconds parameter for RaceTime */
private int seconds;

/**
* Creates a RaceTime object. Should throw an IllegalArgumentException if (a)
* hours is negative or (b) minutes or seconds are not in between 0 and 59
* (inclusive).
* 
* @param hours   hours in the RaceTime
* @param minutes minutes in the RaceTime
* @param seconds seconds in the RaceTime
*/
public RaceTime(int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

/**
* Creates a RaceTimeObject. Should throw an IllegalArgumentException if (a)
* time is not in the correct format (hh:mm:ss), (b) hh is negative, or (c) mm
* or ss are not in between 0 and 59 (inclusive).
* 
* @param time the RaceTime to use
*/
public RaceTime(String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the hours parameter
* 
* @return the hours parameter
*/
public int getHours() {

/**
* Returns the minutes parameter.
* 
* @return the minutes parameter
*/
public int getMinutes() {

/**
* Returns the seconds parameter.
* 
* @return the seconds parameter
*/
public int getSeconds() {

/**
* Returns the total time in seconds. Example: 3:48:24 would be 13704 seconds.
* 
* @return the total time in seconds
*/
public int getTimeInSeconds() {

/**
* Returns the RaceTime as a String with the format hh:mm:ss, where (a) hours
* may be listed as one digit or many digits with leading zeros and (b) minutes
* and seconds are always two digits between 0 and 59 (inclusive).
* 
* @return the RaceTime as a string representation
*/
public String toString() {

/**
* Compares RaceTimes based on their total time.
* 
* @param other the other RaceTime to compare to
* @return the compared time
*/
public int compareTo(RaceTime other) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class responsible for managing SortedLinkedLists, which come from
* SortedList.java.
* 
* @param <E> a parameter of type E
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/** The head Node */
private Node head;

/** Size variable for a linked list */
private int size;

/**
* Constructor for a SortedLinkedList.
*/
public SortedLinkedList() {

/**
* Returns the size of the list.
* 
* @return the size of the list
*/
@Override
public int size() {

/**
* Returns a boolean that states whether or not the list is empty.
* 
* @return true or false based on whether or not the list is empty or not empty
*         respectively
*/
@Override
public boolean isEmpty() {

/**
* Returns a boolean that states whether or not the specified object is present
* in the list.
* 
* @param object the object to check for
* @return true or false based on whether or not the object was found in the
*         list or not respectively
*/
@Override
public boolean contains(E object) {

/**
* Adds an object to the list.
* 
* @param object the object to add to the list
* @return true or false based on whether or not the object could be added to
*         the list
*/
@Override
public boolean add(E object) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Returns an object at a specific index in the list.
* 
* @param index the index to check for the object in the list
* @return the object found at the specified index
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Removes an object from a specified index from the list.
* 
* @param index the index to remove the object from
* @return the object being removed
*/
@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

 - Throws: IllegalArgumentException

/**
* Returns the index of a specified object from the list.
* 
* @param object the object to check the index of in the list
* @return the index of the specified object in the list
*/
@Override
public int indexOf(E object) {

/**
* Creates and returns a hashCode representation of the SortedLinkedList.
* 
* @return the hashCode representation of the SortedLinkedList
*/
public int hashCode() {

/**
* Returns true or false based on whether or not the object specified is equal
* to this one.
* 
* @param object the object to check equality for
* @return true or false based on whether or not the object specified is equal
*         to this one
*/
public boolean equals(Object object) {

/**
* Returns the SortedLinkedList contents as a string.
* 
* @return the SortedLinkedList contesnts as a string
*/
public String toString() {

/**
* The Node class that represent Node objects within a SortedLinkedList.
* 
*
*/
@SuppressWarnings("hiding")
private class Node {

/** The E data value for the node */
/** The next Node */
/**
* The constructor for a Node.
* 
* @param data the data for the node
* @param node the node type data
*/
public Node(E data, Node node) {

/**
* Returns the hashCode representation of the node.
* 
* @return the hashCode representation of the node
*/
public int hashCode() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @since 1.2
* @param <E> the type E parameter
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more than
* Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a such
* that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true if possible to add value
* @throws NullPointerException     if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index is less
*                                   than zero or index is greater than or equal
*                                   to size)
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices). Returns
* the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index less
*                                   than or equal to zero or index is greater
*                                   than or equal to size)
*/
/**
* Returns the index of the first occurrence of the specified element in this
* list, or -1 if this list does not contain the element. More formally, returns
* the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))), or
* -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in this
*         list, or -1 if this list does not contain the element
*/

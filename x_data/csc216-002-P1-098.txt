---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* PreSecurity implements TransitGroup and represents passengers in the terminal who have not yet joined a security checkpoint line
*/
public class PreSecurity implements TransitGroup {

/** PassengerQueue object creation */
private PassengerQueue line;

/**
* The PreSecurity constructor has two parameters: the number of passengers and a logging Reporter, 
* and it can throw an IllegalArgumentException. 
* The constructor creates the specified number of passengers by successive calls Ticketing.generatePassenger(), 
* adding each to its PassengerQueue
* @param numPassengers number of passengers 
* @param five Reporter five
* @throws IllegalArgumentException thrown if passenger is less than 1
*/
public PreSecurity(int numPassengers, Reporter five) {

 - Throws: IllegalArgumentException

/**
* Returns the next depart time
* 
* @return depart time
*/
public int departTimeNext() {

/**
* Returns the passenger who is the next to go
* 
* @return Passenger who is the next to go
*/
public Passenger nextToGo() {

/**
* Tells if there is another passenger in line
* 
* @return false if their is not another passenger in the line
*/
public boolean hasNext() {

/**
* Removes the next passenger
* @return Passenger passenger that was removed
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* CheckPoint represents a security checkpoint line and its line, 
* where the front passenger is undergoing the actual security process. 
* It delegates strict queue activities to its instance variable line. 
*
*/
public class CheckPoint {

/** timeWhenAvailable is the time that the last passenger in line will clear security and leave the line. */
private int timeWhenAvailable;

/** PassengerQueue object creation */
private PassengerQueue line;

/**
* Constructor for Checkpoint
*/
public CheckPoint() {

/**
* Checks the number of people in the checkpoint.
* @return number of people
*/
public int size() {

/**
* Removes the passenger from the line
* @return the passenger
*/
public Passenger removeFromLine() {

/**
* Checks to see if there is another passenger
* @return true if there is another passenger
*/
public boolean hasNext() {

/**
* departTimeNext() returns the amount of time before the next passenger will clear security, 
* returning Integer.MAX_VALUE if the line is empty [UC4].
* @return the depart time
*/
public int departTimeNext() {

/**
* nextToGo() returns the passenger at the front of the security line.
* @return the passenger who is next to go
*/
public Passenger nextToGo() {

/**
* addToLine(Passenger) adds the parameter to the end of the line, 
* setting their waitTime according to timeWhenAvailable as well as their arrivalTime and processTime [UC5,S4], and updating timeWhenAvailable.
* @param f the passenger
*/
public void addToLine(Passenger f) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* SecurityArea represents the collection of security checkpoints and their waiting lines.
*
*/
public class SecurityArea implements TransitGroup {

/** Maximum number of checkpoints */
private final static int MAX_CHECKPOINTS = 17;

/** Minimum number of checkpoints */
private final static int MIN_CHECKPOINTS = 3;

/** Error in checkpoint */
private final static String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** ERROR_INDEX is the error message Index out of range for this security area, for any attempt to add to a security checkpoint line with an improper index. */
private final static String ERROR_INDEX = "Index out of range for this security area";

/** largestFastIndex is the largest index for lines reserved for Fast Track passengers and tsaPreIndex is the index of the line reserved for TSA PreCheck/Trusted Travelers. */
private int largestFastIndex;

/** tsa Pre Index */
private int tsaPreIndex;

private CheckPoint[] check;

/**
* Constructor for Security Area
* @param checkpoints number of checkpoints
*/
public SecurityArea(int checkpoints) {

/**
* checks to see if the number of checkpoints is valid
* @param checkpoints number of checkpoints
* @return true if the number of checkpoints is okay
* @throws IllegalArgumentException if checkpoint is less than 3 or greater than 17
*/
private boolean numGatesOK(int checkpoints) {

 - Throws: IllegalArgumentException

/**
* addToLine(int, Passenger) adds the passenger to the security line with the index given by the first parameter.
* @param idx number of line index
* @param person person to add
*/
public void addToLine(int idx, Passenger person) {

/**
* shortestRegularLine() returns the index of the shortest security line that ordinary passengers are permitted to select
* @return the number of the shortest line
*/
public int shortestRegularLine() {

/**
* shortestFastTrackLine() returns the index of the shortest security line restricted to Fast Track passengers
* @return the number of the shortest FastTrack Line
*/
public int shortestFastTrackLine() {

/**
* shortestTSAPreLine() returns the index of the shortest security line restricted to TSA PreCheck/Trusted Traveler passengers
* @return number of shortest TSA Pre line
*/
public int shortestTSAPreLine() {

/**
* lengthOfLine(int) is the number of passengers in the line with the given index. It throws an IllegalArgumentException if the index is out of range
* @param line line number
* @throws IllegalArgumentException if the index is out of range
* @return length of line
*/
public int lengthOfLine(int line) {

 - Throws: IllegalArgumentException

/**
* departTimeNext() returns the time that the next passenger will finish clearing security, or Integer.MAX_VALUE if all lines are empty
* @return depart time
*/
public int departTimeNext() {

/**
* nextToGo() returns the next passenger to clear security, or null if there is none.
* @return passenger
*/
public Passenger nextToGo() {

/**
* removeNext() removes and returns the next passenger to clear security.
* @return passenger who is next to go
*/
public Passenger removeNext() {

// /**
// private int shortestLineInRange(int departTime, int lengthOfTheLine) {

/**
* Determines the line with next passenger to clear
* @return line 
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in
* which there is a "next" passenger and a time when that passenger will leave
* the group.
* 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* 
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* 
* @return departure time of the next passenger or Integer.MAX_VALUE if the
*         group is empty
*/
/**
* Removes the next passenger to leave the group.
* 
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Makes a Fast Track Passenger. Fast Track extends passenger.
*
*/
public class FastTrackPassenger extends Passenger {

/** max expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** Color of passenger */
private Color color;

/**
* Constructor of FastTrackPassenger
* @param arrivalTime the time that a passenger arrives
* @param processTime the time that it takes to process
* @param passenger Reporter passenger
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter passenger) {

/**
* Gets the color
* @return color color of passenger
*/
public Color getColor() {

/**
* Gets in line 
* @param one TransitGroup one
*/
public void getInLine(TransitGroup one) {

/**
* Picks a line for the passenger
* @param one TransitGroup one
* @return line number
*/
private int pickLine(TransitGroup one) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates an ordinary passenger. It extends passenger.
*
*/
public class OrdinaryPassenger extends Passenger {

/** max expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** Color of passenger */
private Color color;

/**
* Constructor of OrdinaryPassenger
* @param one integer one
* @param two integer two
* @param passenger Reporter passenger
*/
public OrdinaryPassenger(int one, int two, Reporter passenger) {

/**
* Gets the color
* @return color color of passenger
*/
public Color getColor() {

/**
* Gets in line 
* @param one TransitGroup one
*/
public void getInLine(TransitGroup one) {

/**
* Picks a line for the passenger
* @param one TransitGroup one
* @return line number
*/
private int pickLine(TransitGroup one) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Determines information about a passenger
*
*/
public abstract class Passenger {

/** minimum process time */
public static final int MIN_PROCESS_TIME = 20;

/** arrivalTime is the time that the passenger joins a security line (the first parameter in the constructor) */
private int arrivalTime;

/** waitTime is the time the passenger will spend in a security line. It cannot be computed until the passenger joins a security line. */
private int waitTime;

/** processTime is the amount of time that the passenger spends at the front of the security line actually undergoing the security check (the second parameter in the constructor) */
private int processTime;

/** lineIndex is the index of the security checkpoint line that the passenger selects and joins. Before the passenger gets into line, the value should be -1. */
private int lineIndex;

/** waitingProcessing is false until the passenger selects and joins a security line, when it becomes true. */
private boolean waitingProcessing;

/** myLog is the reporting mechanism, initialized by the Reporter parameter in the constructor.*/
private Log myLog;

/**
* Constructs passenger
* @param arrivalTime the time the passenger arrives
* @param processTime the time the passenger was processed
* @param myLog Reporter
*/
public Passenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Gets the arrival time
* @return arrivalTime arrival time
*/
public int getArrivalTime() {

/**
* Gets the wait time
* @return waitTime wait time
*/
public int getWaitTime() {

/**
* Sets the wait time
* @param waitTime wait time
*/
public void setWaitTime(int waitTime) {

/** 
* Gets the process time
* @return processTime process time 
*/
public int getProcessTime() {

/**
* Gets the line index
* @return lineIndex line index
*/
public int getLineIndex() {

/**
* determines if passenger is waiting in security line
* @return waitingProcessing if waiting to be processed
*/
public boolean isWaitingInSecurityLine() {

/**
* clearSecurity() removes the passenger from the waiting line when he/she finishes security checks
*/
public void clearSecurity() {

/** 
* Sets the line index
* @param idx line index
*/
protected void setLineIndex(int idx) {

/**
* getInLine(TransitGroup) is an abstract method that adds the passenger to the appropriate security line. 
* The parameter is the list of security lines in the airport (instantiated with SecurityArea)
* @param one TransitGroup
*/ 
public abstract void getInLine(TransitGroup one);

/**
* Abstract method that gets the color of the passenger
* @return color
*/
public  abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
* 
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* 
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* 
* @param person the Passenger to add
*/
public void add(Passenger person) {

/**
* Removes and returns the front Passenger from the queue.
* 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() throws NoSuchElementException {

/**
* Gets the front Passenger of the queue without removing it, or null if the
* queue is empty. Does not remove the Passenger from the queue.
* 
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* 
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Trusted Traveler extension of Passenger 
*
*/
public class TrustedTraveler extends Passenger {

/** Color of passenger */
private Color color;

/** max expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Constructor of TrustedTraveler
* @param arrivalTime the time that the passenger arrives
* @param processTime the time that the passenger was processed
* @param passenger Reporter passenger
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter passenger) {

/**
* Gets the color
* @return color color of passenger
*/
public Color getColor() {

/**
* Gets in line 
* @param one TransitGroup one
*/
public void getInLine(TransitGroup one) {

/**
* Picks a line for the passenger
* @param one TransitGroup one
* @return line number
*/
private int pickLine(TransitGroup one) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Sets up the EventCalendar
*
*/
public class EventCalendar {

/** highPriority passenger */
private TransitGroup highPriority;

/** lowPriority passenger */
private TransitGroup lowPriority;

/**
* The constructor takes two TransitGroups as parameters. 
* The first parameter has priority over the second one: highPriority is initialized 
* with the first parameter and lowPriority is initialized with the second.
* @param highPriority highPriority passenger
* @param lowPriority lowPriority passenger
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* nextToAct() returns the passenger from highPriority and lowPriority who will be the next to exit the group/line theyre currently at the front of (See [UC5],[UC6]). 
* In case of a tie for the next to exit, the passenger from highPriority is returned. 
* If both TransitGroups are empty (their next passengers are null), an IllegalStateException is thrown.
* @return Passenger that goes next
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Keeps a log of everything
*
*/
public class Log implements Reporter {

/** number of Passengers that have gone through */
private int numCompleted;

/** the total wait time */
private int totalWaitTime;

/** total process time */
private int totalProcessTime;

/**
* Constructor for Log
*/
public Log() {

/**
* Returns the number of passengers who have gone through
* @return number of passengers
*/
public int getNumCompleted() {

/**
* Log data of a certain passenger
* @param one Passenger 
*/
public void logData(Passenger one) {

/**
* Finds out the average wait time
* @return averageWaitTime
*/
public double averageWaitTime() {

/**
* Finds out the average process time
* @return average process time
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group
* activities.
* 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* 
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* 
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* 
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* 
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the simulator that displays all the information that has been gathered.
*
*/
public class Simulator {

/** Creates a Log object called myLog */
private Log myLog;

/** 
* Constructor for simulator:
* Checks the parameters to make sure they are valid, throwing IllegalArgumentExceptions if they are not
* Initializes log to record the passenger statistics.
* Sets the passenger type distribution for the Ticketing factory.
* Establishes the security area.
* Creates the PreSecurity area and the EventCalendar.
* @param checkpoints number of checkpoints
* @param numPassengers number of passengers
* @param percentageTrusted percentage of Trusted Travelers
* @param percentageFast percentage of Fast 
* @param percentageRegular integer five
* @throws IllegalArgumentException if percents don't sum up to 100
* @throws IllegalArgumentException if no passengers is less than 1
* @throws IllegalArgumentException if checkpoints are less than three or greater than 17
* @throws IllegalArgumentException if percents are negative
*/
public Simulator(int checkpoints, int numPassengers, int percentageTrusted, int percentageFast, int percentageRegular) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Checks the parameters
* @param one integer one
* @param two integer two
* @param three integer three
* @param four integer four
*/
//private void checkParameters(int one, int two, int three, int four) {

/**
* Sets up the simulator
* @param one integer one
* @param two integer two
* @param three integer three
*/
// private void setUp(int one, int two, int three) {

/** 
* gets the reporter
* @return Reporter
*/
public Reporter getReporter() {

/**
* step() goes through the action of the next passenger from the EventCalendar. 
* If the passenger is from the ticketing area, this call to step() implements [UC5]. 
* If the passenger is from the security area, this call to step() implements [UC6]. 
* In each case, the passenger is removed from the corresponding area. 
* If there is no next passenger, an IllegalStateException is thrown.
*/
public void step() {

/**
* Determines if there are more steps
* @return false if there is no more steps
*/
public boolean moreSteps() {

/**
* getCurrentIndex() returns the index of the security checkpoint line for currentPassenger.
* @return current index
*/
public int getCurrentIndex() {

/**
* Gets the passenger color
* @return color
*/
public Color getCurrentPassengerColor() {

/**
* passengerClearedSecurity() is true if currentPassenger just cleared a security checkpoint 
* and finished processing and false otherwise, (including when currentPassenger is null).
* @return false if currentPassenger did not clear the security checkpoint
*/
public boolean passengerClearedSecurity() {


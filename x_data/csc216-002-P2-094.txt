---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class that facilitates the processing of incidents
*
*/
public class Command {

/** Code when awaiting caller */
public static final String OH_CALLER = "Awaiting Caller";

/** Code when awaiting a change to be made */
public static final String OH_CHANGE = "Awaiting Change";

/** Code when awaiting vendor */
public static final String OH_VENDOR = "Awaiting Vendor";

/** Code when an incident has been permanently solved */
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** Code when an incident has been worked around */
public static final String RC_WORKAROUND = "Workaround";

/** Code when an incident has not been solved */
public static final String RC_NOT_SOLVED = "Not Solved";

/** Code when an incident has been closed by the caller */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** Code when an incident is a duplicate */
public static final String CC_DUPLICATE = "Duplicate";

/** Code when an incident is unnecessary */
public static final String CC_UNNECESSARY = "Unnecessary";

/** Code when an incident does not qualify as an incident */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** The owner of the incident */
private String ownerId;

/** The note that describes the incident */
private String note;

/** Possible values of the command value */
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/** Possible reasons for an incident to be on hold */
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/** Possible codes for an incident to be resolved */
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/** Possible codes for an incident to be cancelled */
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT }
/** The command value of the command */
private CommandValue c;

/** Hold reason of the command */
private OnHoldReason onHoldReason;

/** Resolution code of the command */
private ResolutionCode resolutionCode;

/** Cancellation code of the command */
private CancellationCode cancellationCode;

/**
* Constructor for command objects
* @param v CommandValue of the command object
* @param id The id of the incident owner
* @param o Reason for the incident to be put on hold
* @param r Code for the incident's resolution
* @param c Code for the incident's cancellation
* @param note Note describing the incident
* @throws IllegalArgumentException if given invalid parameters
*/
public Command(CommandValue v, String id, OnHoldReason o, ResolutionCode r, CancellationCode c, String note) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the command value of the Command object
* @return the action the command object is taking
*/
public CommandValue getCommand() {

/**
* Gets the id of the command owner
* @return the owner's id
*/
public String getOwnerId() {

/**
* Gets the code to resolve the incident
* @return resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the not associated with the incident
* @return work note
*/
public String getWorkNote() {

/**
* Gets the reason the incident is on hold
* @return on hold reason
*/
public OnHoldReason getOnHoldReason() {

/**
* Gets the reason the incident is being cancelled
* @return cancellation code
*/
public CancellationCode getCancellationCode() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class that describes the instances and behaviors of Managed Incidents
*
*/
public class ManagedIncident {

/** Category of Inquiry incidents */
public static final String C_INQUIRY = "Inquiry";

/** Category of Software incidents */
public static final String C_SOFTWARE = "Software";

/** Category of Hardware incidents */
public static final String C_HARDWARE = "Hardware";

/** Category of Database incidents */
public static final String C_DATABASE = "Database";

/** Category of Network incidents */
public static final String C_NETWORK = "Network";

/** Urgent priority string */
public static final String P_URGENT = "Urgent";

/** High priority string */
public static final String P_HIGH = "High";

/** Medium priority string */
public static final String P_MEDIUM = "Medium";

/** Low priority string */
public static final String P_LOW = "Low";

/** The incident's id */
private int incidentId;

/** The caller of the incident */
private String caller;

/** The owner of the incident */
private String owner;

/** The name of the incident */
private String name;

/** The request to change the incident */
private String changeRequest;

/** The notes associated with the incident */
private ArrayList<String> notes;

/** New state string */
public static final String NEW_NAME = "New";

/** In progress state string */
public static final String IN_PROGRESS_NAME = "In Progress";

/** On hold state string */
public static final String ON_HOLD_NAME = "On Hold";

/** Resolved state string */
public static final String RESOLVED_NAME = "Resolved";

/** Closed state string */
public static final String CLOSED_NAME = "Closed";

/** Canceled state string */
public static final String CANCELED_NAME = "Canceled";

/** Counter to keep track of incidents */
private static int counter = 0;

/** All possible priorities */
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
/** All possible categories */
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/** New state */
private IncidentState newState;

/** Current state */
private IncidentState state;

/** Resolved state */
private IncidentState resolvedState;

/** On hold state */
private IncidentState onHoldState;

/** Closed state */
private IncidentState closedState;

/** In progress state */
private IncidentState inProgressState;

/** Canceled state */
private IncidentState canceledState;

/** Priority of the incident */
private Priority priority;

/** Cancellation code of the incident */
private CancellationCode cancellationCode;

/** Resolution code of the incident */
private ResolutionCode resolutionCode;

/** On hold reason of the incident */
private OnHoldReason onHoldReason;

/** Category of the incident */
private Category category;

/**
* ManagedIncident constructor from parameters
* @param caller the person calling in the incident
* @param c the category the incident falls under
* @param p the priority level of the incident
* @param name the name of the incident
* @param workNote the note describing the incident
* @throws IllegalArgumentException if given invalid parameters
*/
public ManagedIncident(String caller, Category c, Priority p, String name, String workNote) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructor for Managed incidents when given an incident
* @param i the incident to create as a managed incident
*/
public ManagedIncident(Incident i) {

/**
* Increments the counter variable
*/
public static void incrementCounter() {

/**
* Gets the id of this instance of incident
* @return incident id
*/
public int getIncidentId() {

/**
* Get the change string that the incident is attempting
* @return the change that is being requested for the incident
*/
public String getChangeRequest() {

/**
* Get the incident's category
* @return the incident's category
*/
public Category getCategory() {

/**
* Gets the incident's category as a string
* @return the incident's category
*/
public String getCategoryString() {

/**
* Sets the incident's category
* @param s the category to set 
*/
private void setCategory(String s) {

/**
* Gets the incident's priority as a string
* @return the incident's priority
*/
public String getPriorityString() {

/**
* Sets the incident's priority
* @param s the priority to set to
*/
private void setPriority(String s) {

/**
* Gets the incident's on hold reason as a string
* @return the incident's on hold reason
*/
public String getOnHoldReasonString() {

/**
* Sets the incident's on hold reason
* @param s the on hold reason to set
*/
private void setOnHoldReason(String s) {

/**
* Gets the incident's cancellation code as a string
* @return the incident's cancellation code
*/
public String getCancellationCodeString() {

/**
* Sets the incident's cancellation code
* @param s the cancellation code to set
*/
private void setCancellationCode(String s) {

/**
* Gets the current state of the incident
* @return the incident's current state
*/
public IncidentState getState() {

/**
* Sets the incident's state
* @param s the state to set to
*/
private void setState(String s) {

/**
* Gets the incident's resolution code
* @return the incident's resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the incident's resolution code as a string
* @return the incident's resolution code
*/
public  String getResolutionCodeString() {

/**
* Sets the incident's resolution code
* @param s the resolution code to set
*/
private void setResolutionCode(String s) {

/**
* Gets the incident's owner
* @return the incident's owner
*/
public String getOwner() {

/**
* Gets the incident's name
* @return the incident's name
*/
public String getName() {

/**
* Gets the incident's caller
* @return the incident's caller
*/
public String getCaller() {

/**
* Gets the notes associated with the incident
* @return the incident's notes
*/
public ArrayList<String> getNotes(){

/**
* Gets the incident's notes as a string
* @return the incident's notes
*/
public String getNotesString() {

/**
* Updates the incident with the given command
* @param c the command to execute
*/
public void update(Command c) {

/**
* Gets an incident from xml data
* @return an unmanaged incident
*/
public Incident getXMLIncident() {

/**
* Sets the counter to the specified value
* @param i the value to set the counter to
*/
public static void setCounter(int i ) {

/**
* Helper method that converts strings to enums
* @param s string to convert
* @return the enum result
* @throws IllegalArgumentException if the string coincides with no enum
*/
private Category convertCategory(String s) {

 - Throws: IllegalArgumentException

/**
* Helper method that converts strings to enums
* @param s string to convert
* @return the enum result
* @throws IllegalArgumentException if the string coincides with no enum
*/
private Priority convertPriority(String s) {

 - Throws: IllegalArgumentException

/**
* Helper method that converts strings to enums
* @param s string to convert
* @return the enum result
* @throws IllegalArgumentException if the string coincides with no enum
*/
private OnHoldReason convertOnHoldReason(String s) {

/**
* Helper method that converts strings to enums
* @param s string to convert
* @return the enum result
* @throws IllegalArgumentException if the string coincides with no enum
*/
private ResolutionCode convertResolutionCode(String s) {

/**
* Helper method that converts strings to enums
* @param s string to convert
* @return the enum result
* @throws IllegalArgumentException if the string coincides with no enum
*/
private CancellationCode convertCancellationCode(String s) {

/**
* Helper method that converts strings to incident states
* @param s the string to convert
* @return the state result
* @throws IllegalArgumentException if the string coincides with no state
*/
private IncidentState convertState(String s) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* The state of an incident on hold
*
*/
public class OnHoldState implements IncidentState {

/**
* Changes the current state based on the provided command
* @param command the command that's going to change the state
* @throws UnsupportedOperationException if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* The state of an incident that has been resolved
*
*/
public class ResolvedState implements IncidentState {

/**
* Changes the current state based on the provided command
* @param command the command that's going to change the state
* @throws UnsupportedOperationException if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* The state of a brand new incident
*
*/
public class NewState implements IncidentState {

/**
* Changes the current state based on the provided command
* @param command the command that's going to change the state
* @throws UnsupportedOperationException if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* The state of an incident in progress
*
*/
public class InProgressState implements IncidentState {

/**
* Changes the current state based on the provided command
* @param command the command that's going to change the state
* @throws UnsupportedOperationException if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* The state of an incident that has been closed
*
*/
public class ClosedState implements IncidentState {

/**
* Changes the current state based on the provided command
* @param command the command that's going to change the state
* @throws UnsupportedOperationException if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* The state of an incident that has been canceled
*
*/
public class CanceledState implements IncidentState {

/**
* Changes the current state based on the provided command
* @param command the command that's going to change the state
* @throws UnsupportedOperationException if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class that manages all of the incidents and facilitates program execution
*
*/
public class IncidentManager {

/** Single instance of the incident manager */
private static IncidentManager singleton;

/** The list of managed incidents that are being worked with */
private ManagedIncidentList incidentList;

/**
* Constructor of the incident manager
*/
private IncidentManager() {

/**
* Gets an instance of IncidentManager to work with
* @return an IncidentManager instance
*/
public static IncidentManager getInstance() {

/**
* Saves all of the incidents on the list to an external file 
* @param fileName name of the file to save to
* @throws IllegalArgumentException for invalid file names
*/
public void saveManagedIncidentsToFile(String fileName)  {

 - Throws: IllegalArgumentException

/**
* Loads in incidents from a file to the list
* @param fileName name of the file to load in from
* @throws IllegalArgumentException for invalid file names
*/
public void loadManagedIncidentsFromFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Creates an entirely new list of managed incidents
*/
public void createNewManagedIncidentList() {

/**
* Returns the list of managed incidents as an array
* @return array of managed incidents
*/
public String[][] getManagedIncidentsAsArray(){

/**
* Returns a specific category of managed incidents as an array                
* @param c the category of incidents that are being searched for
* @return array of managed incidents of the specified category
* @throws IllegalArgumentException if given a null category
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category c){

 - Throws: IllegalArgumentException

/**
* Gets a specified incident from the list
* @param id the id of the incident that is being searched for
* @return the specified incident
*/
public ManagedIncident getManagedIncidentById(int id) {

/**
* Executes a command on a managed incident
* @param id the id of the incident to modify
* @param c the command to execute on the specified incident
*/
public void executeCommand(int id, Command c) {

/**
* Deletes a specified incident from the list
* @param id the id of the incident to remove
*/
public void deleteManagedIncidentById(int id) {

/**
* Adds an incident with the specified parameters to the list
* @param caller the person calling in the incident
* @param c the category the incident falls under
* @param p the priority level of the incident
* @param name the name of the incident
* @param workNote the note describing the incident
*/
public void addManagedIncidentToList(String caller, Category c, Priority p, String name, String workNote) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class that describes a list of managed incidents 
*
*/
public class ManagedIncidentList {

/** A list of incidents */
private List<ManagedIncident> incidents;

/**
* ManagedIncidentList constructor 
*/
public ManagedIncidentList() {

/**
* Adds an incident with the specified parameters to the list
* @param caller the person calling in the incident
* @param c the category the incident falls under
* @param p the priority level of the incident
* @param name the name of the incident
* @param workNote the note describing the incident
* @return the index in the list the added incident was assigned
*/
public int addIncident(String caller, Category c, Priority p, String name, String workNote) {

/**
* Adds incidents to the list from an xml file
* @param list the collection of incidents stored in the xml file
*/
public void addXMLIncidents(List<Incident> list) {

/**
* Get the list of managed incidents
* @return the list of managed incidents
*/
public List<ManagedIncident> getManagedIncidents(){

/**
* Gets a list of managed incidents of the specified category
* @param c the category that is being searched for
* @return a list of managed incidents that fall under the provided category
* @throws IllegalArgumentException if given a null category
*/
public List<ManagedIncident> getIncidentsByCategory(Category c){

 - Throws: IllegalArgumentException

/**
* Gets a specific incident from the list
* @param id the id of the incident that is being searched for
* @return the incident matching the provided id
*/
public ManagedIncident getIncidentById(int id) {

/**
* Executes the specified command on the specified incident 
* @param id the id of the incident to modify
* @param c the command that is going to be enacted on the incident
*/
public void executeCommand(int id, Command c) {

/**
* Removes the specified incident from the list
* @param id the id of the incident to remove
*/
public void deleteIncidentById(int id) {


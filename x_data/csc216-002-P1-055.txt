---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class for the Pre Security area, which is made up of passengers
* who have not yet joined a security checkpoint line. Implements the transitGroup
* interface which uses methods to delegate which passengers to deal with and how
*
*/
public class PreSecurity implements TransitGroup{

/**Passenger queue of passengers waiting to leave the PreSecurity area and join lines*/
private PassengerQueue outsideSecurity = new PassengerQueue();

/**
* Pre security constructor which generates a specified number of passengers
* by successive calls to the ticketing class, adding each to its PassengerQueue
* @param numPassenger number of passengers in the pre security area
* @param reporter reporter object used to report statistics
* @throws throws an IllegalArgumentException
*/
public PreSecurity(int numPassenger, Reporter reporter) {

 - Throws: IllegalArgumentException

/**
* Method which returns the departure time of the next person in line
* @return returns the departure time of the next person in line as an int
*/
public int departTimeNext() {

/**
* Method which returns a passenger object that is the next to leave
* @return passenger object that is next to leave the pre security area
*/
public Passenger nextToGo() {

/**
* Method which determines if there are passengers waiting to leave the pre security area
* @return returns boolean function depending on whether or not there are extra passengers 
* 		   waiting to leave
*/
public boolean hasNext() {

/**
* method which removes the next passenger in line to leave the pre security area
* @return returns the passenger which is set to leave the pre security area
*/
public Passenger removeNext() {	
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class representing a security checkpoint and its line. It delegates stirct
* queue activities to its instance variable line which is a PassengerQueue object
*
*/
public class CheckPoint {

/**Line of passenger objects for a particular security checkpoint*/
private PassengerQueue line;

/**The time that the last passenger in line will clear security and leave the line*/
private int timeWhenAvailable;

/**
* Checkpoint constructor class which generates a security checkpoint and its line
* which is initially empty
*/
public CheckPoint() {

/**
* method which returns the size of the security checkpoint line
* @return returns an int describing the size of the security checkpoint line
*/
public int size() {

/**
* Method used to remove a passenger from the security checkpoint line, updating
* timeWhenAvailable and size
* @return returns a passenger object that is set to leave the security checkpoint line
*/
public Passenger removeFromLine() {

/**
* method which checks whether the security checkpoint has a passenger in line
* @return returns a boolean value depending on whether or not there are 
* 		   passenger objects in line
*/
public boolean hasNext() {

/**
* method which returns the amount of time before the next passenger will clear security
* @return returns an int amount of time before next passenger clears security
*/
public int departTimeNext() {

/**
* method which checks what passenger is at the front of the line
* @return returns the passenger that is at the front of the line
*/
public Passenger nextToGo() {

/**
* adds the passed passenger to the end of the line, setting their waitTime according to
* timeWhenAvailable and their arrivalTime and processTime, which in turn updates
* timeWhenAvailable
* @param passenger passenger to be added to the security checkpoint line
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class SecurityArea implements TransitGroup {

/**Upper bound of possible checkpoints*/
public static final int MAX_CHECKPOINTS = 17;

/**Lower bounds of possible checkpoints*/
public static final int MIN_CHECKPOINTS = 3;

/**Error message to be given if checkpoint amount is invalid*/
public static final String ERROR_CHECKPOINTS = "Number of checkpoints must be" +
/**Error message to be given if index is out of bounds*/
public static final String ERROR_INDEX = "Index out of range for this security area.";

/**largest index for  lines reserved for Fast Track passengers*/
private int largestFastIndex;

/**index of the line reserved for TSA PreCheck/Trusted Travelers*/
private int tsaPreIndex;

/**array list of all checkpoints*/
private ArrayList<CheckPoint> check = new ArrayList<CheckPoint>();

/**
* Security area constructor which is dependent on one parameter and throws 
* an exception if that parameter is invalid
* @param numGates number of gates to be created in the security area
* @throws throws an IllegalArgumentException if numGates is out of bounds
*/
public SecurityArea(int numGates) {

 - Throws: IllegalArgumentException

/**
* Helper method to check and make sure the number of gates passed to the constructor is 
* valid
* @param numGates number of gates to be checked for validity
* @return returns a boolean dependent on validity of passed number of gates
*/
private boolean numGatesOK(int numGates) {

/**
* adds a passenger object to the line of the given index
* @param index index of line the passenger is to be added to
* @param passenger passenger object to be added to given line
*/
public void addToLine(int index, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
* method which determines the shortest line regular passengers can join
* @return returns index of the shortest line a regular passenger can join as an int
*/
public int shortestRegularLine() {

/**
* method which determines the shortest line fast track passengers can join
* @return returns index of the shortest line a fast track passenger can join as an int
*/
public int shortestFastTrackLine() {

/**
* method which determines the shortest line precheck passengers can join
* @return returns index of the shortest line a precheck passenger can join as an int
*/
public int shortestTSAPreLine() {

/**
* method which returns the length of a given line
* @param idx index of line to be checked
* @return returns an int equal to the length of the given line
*/
public int lengthOfLine(int idx) {

/**
* method which returns time next passenger will clear security
* @return returns the time as an int that the next passenger will finish clearing security
*/
public int departTimeNext() {

/**
* method which checks which passenger is next to leave security
* @return returns passenger object that will leave security next
*/
public Passenger nextToGo() {

/**
* method which removes and returns the next passenger to clear security
* @return returns the passenger next to clear security
*/
public Passenger removeNext() {

/**
* returns the shortest line within a range of given indexes
* @param min minimum index
* @param max maximum index
* @return returns an int that is the index of the shortest line within the given range
*/
private int shortestLineInRange(int min, int max) {

/**
* method which checks which line is next to clear someone through security
* @return returns the index of the line with the next person to clear security
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class of the FastTrackPassenger object. Provides necessary class methods
* for the behaviors of the fast track passenger object and extends the passenger
* class
*
*/
public class FastTrackPassenger extends Passenger{

/**Max process time of a Fast Track passenger*/
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**Color of passenger based on time*/
private Color color;

/**
* Constructor for Fast Track Passenger object, passes parameters to super
* @param arrivalTime arrival time of passenger at the airport
* @param processTime process time of the passenger
* @param reporter reporter object used to log passenger data 
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter reporter) {

/**
* Method for return color value of given passenger
* @return returns Color of passenger as a color object
*/
public Color getColor() {

/**
* Method for choosing the appropriate line for passenger to join based
* on the size and length of other lines and type of passenger
* @param transitGroup the list of security lines in the airport
*/
public void getInLine(TransitGroup transitGroup) {

/**
* Method which selects the appropriate checkpoint waiting line for the passenger to join
* @param transitGroup list of security checkpoint lines in the security area
* @return returns an int of the index of security checkpoint line to join
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class of the OrdinaryPassenger object. Provides necessary class methods
* for the behaviors of the ordinary passenger object and extends the passenger
* class
*
*/
public class OrdinaryPassenger extends Passenger {

/**Max process time of a Ordinary passenger*/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**Color of passenger based on time*/
private Color color;

/**
* Constructor for Fast Track Passenger object, passes parameters to super
* @param arrivalTime arrival time of passenger at the airport
* @param processTime process time of the passenger
* @param reporter reporter object used to log passenger data
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter reporter) {

/**
* Method for return color value of given passenger
* @return returns Color of passenger as a color object
*/
public Color getColor() {

/**
* Method for choosing the appropriate line for passenger to join based
* on the size and length of other lines and type of passenger
* @param transitGroup the list of security lines in the airport
*/
public void getInLine(TransitGroup transitGroup) {

/**
* Helper method which selects the appropriate checkpoint waiting line for the passenger to 
* join
* @param transitGroup list of security checkpoint lines in the security area
* @return returns an int of the index of security checkpoint line to join
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public abstract class Passenger {

/**Minimum process time for every type of passenger*/
public static final int MIN_PROCESS_TIME = 20;

/**Arrival time of each individual passenger*/
private int arrivalTime;

/**Wait time each of each individual passenger*/
private int waitTime;

/**Process time of each individual passenger*/
private int processTime;

/**Index of the security line the passenger chooses*/
private int lineIndex;

/**True/false switch for status of passenger*/
private boolean waitingProcessing;

/**Passengers log to record data*/
private Reporter log;

/**
* Constructor class for Passenger object
* @param arrivalTime arrival time of passenger at the airport
* @param processTime process time of the passenger
* @param reporter reporter object used to log passenger data
*/
public Passenger (int arrivalTime, int processTime, Reporter reporter) {

/**
* Setter method for arrivalTime
* @param time arrival time of passenger
*/
private void setArrivalTime(int time) {

/**
* Setter method for ProcessTime
* @param time process time of passenger
*/
private void setProcessTime(int time) {

 - Throws: IllegalArgumentException

/**
* Method for returning arrival time of passenger
* @return returns an int of the passengers arrival time
*/
public int getArrivalTime() {

/**
* Method for returning the wait time of the passenger
* @return returns an int of the passengers wait time
*/
public int getWaitTime() {	
/**
* sets the wait time of the passenger object
* @param time int amount of time to set as the wait time
*/
public void setWaitTime(int time) {

/**
* method for returning the process time of the passenger
* @return returns an int of the passengers process time
*/
public int getProcessTime() {

/**
* method for returning the index of the line chosen by the passenger object
* @return returns the index of the chosen security line
*/
public int getLineIndex() {

/**
* Method which checks the status of whether or not the passenger is waiting in a security
* check point line or not
* @return returns true if the passenger is waiting in a security checkpoint line and
* 		   returns false if the passenger is not
*/
public boolean isWaitingInSecurityLine() {

/**
* method which removes the passenger from their waiting line
*/
public void clearSecurity() {

/**
* Set the index of the line the passenger chooses to join
* @param index index of line to be set to Line Index
*/
protected void setLineIndex(int index) {

/**
* Abstract method to be overridden which chooses the  appropriate line to join 
* based on passenger type and given list of security checkpoint lines
* @param transitGroup list of security lines in the airport
*/
public abstract void getInLine(TransitGroup transitGroup);

/**
* Abstract method to be overridden which returns a color object based on passenger type
* and process time associated with that passenger 
* @return returns a color object of passenger 
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class of the TrustedTraveler object. Provides necessary class methods
* for the behaviors of the trusted traveler object and extends the passenger
* class
*
*/
public class TrustedTraveler extends Passenger {

/**Max process time of a Trusted Traveler passenger*/
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**Color of passenger based on time*/
private Color color;

/**
* Constructor for Fast Track Passenger object, passes parameters to super
* @param arrivalTime arrival time of passenger at the airport
* @param processTime process time of the passenger
* @param reporter reporter object used to log passenger data
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter reporter) {

/**
* Method for return color value of given passenger
* @return returns Color of passenger as a color object
*/
public Color getColor() {

/**
* Method for choosing the appropriate line for passenger to join based
* on the size and length of other lines and type of passenger
* @param transitGroup the list of security lines in the airport
*/
public void getInLine(TransitGroup transitGroup) {

/**
* Method which selects the appropriate checkpoint waiting line for the passenger to join
* @param transitGroup list of security checkpoint lines in the security area
* @return returns an int of the index of security checkpoint line to join
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* EventCalendar class which handles the decision making process for the simulation
*
*/
public class EventCalendar {

/**High priority transit group to select passengers from*/
private TransitGroup highPriority;

/**Low priority transit group to select passengers from*/
private TransitGroup lowPriority;

/**
* Eventcaldar constructor which takes two parameters, the first having priority over
* the second when it comes to decision making
* @param highPriority the transitgroup passed with high priority 
* @param lowPriority the transitgroup passed with low priority
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* method which determines what passenger will be the next to exit the group
* or line they are currently at the front of
* @return returns passenger that is next to act
*/
public Passenger nextToAct() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class Log implements Reporter{

private int numCompleted;

private int totalWaitTime;

private int totalProcessTime;

public Log() {

public int getNumCompleted() {

public void logData(Passenger passenger) {

public double averageWaitTime() {

public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Simulator class which implements the back end process for setting up the simulation
* and running it
*
*/
public class Simulator {

/**Number of passengers in simulation*/
private int numPassengers;

/**Passengers who have not yet entered a checkpoint security line*/
private TransitGroup inTicketing;

/**Passengers who are in a checkpoint security line*/
private TransitGroup inSecurity;

/**Event calendar for simulation*/
private EventCalendar myCalendar;

/**Log for entire simulation*/
private Reporter log;

/**Current passenger to be passed through the simulation*/
private Passenger currentPassenger;

/**
* Constructor for the simulation which begins the simulation and is in charge of 
* checking all parameters (and throwing exceptions appropriately), initializing a log,
* setting passenger type distribution, initializing security area, initializing the ticketing
* area, creating preSecurity, as well as the eventCalendar.
* @param numGates number of gates to create security area with
* @param numPassengers number of passengers to run simulation with
* @param pctTrusted percentage of trusted travelers
* @param pctFastTrack percentage of fast track travelers
* @param pctOrdinary percentage of ordinary travelers
*/
public Simulator(int numGates, int numPassengers, int pctTrusted, int pctFastTrack,  
/**
* helper method to check the parameters passed to the constructor method
* @param numPassengers number of gates
* @param pctTrusted percent of trusted travelers
* @param pctFastTrack percent of fast track travelers
* @param pctOrdinary percent of ordinary travelers
*/
private void checkParameters (int numPassengers, int pctTrusted, int pctFastTrack, int pctOrdinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Helper method to set up ticketing area
* @param numPassengers number of passengers
* @param pctTrusted percentage of trusted travelers
* @param pctFastTrack percentage of fast track travelers
*/
private void setUp(int numPassengers, int pctTrusted, int pctFastTack) {

//set private field of passengers to num passed
//create inTicketing private field as well as creates preSecurity area
/**
* Method which returns reporter 
* @return returns reporter object of simulation
*/
public Reporter getReporter() {

/**
* Method which steps through the simulation once
* @throws throws an IllegalArgumentException if there is no passenger to step through
*/
public void step() {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Method used to determine if there are further steps to be taken
* @return returns true if there are, returns false if there are not
*/
public boolean moreSteps() {

/**
* returns the index of the security checkpoint line for currentPassenger
* @return returns an int representing the security checkpoint line the current passenger is in
*/
public int getCurrentIndex() {

/**
* returns the color object of the current passenger
* @return color object of the current passenger
*/
public Color getCurrentPassengerColor() {

/**
* determines if the current passenger has cleared through security
* @return returns a boolean depending on the security status of the current passenger
*/
public boolean passengerClearedSecurity() {


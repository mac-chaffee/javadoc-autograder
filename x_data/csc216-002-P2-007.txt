---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class creates objects that encapsulate
* user actions(or transitions) that cause the state of the 
* ManagedIncident to update
*/
public class Command {

/** On Hold Reason - Awaiting Caller*/
public static final String OH_CALLER = "Awaiting Caller";

/** On Hold Reason - Awaiting Change*/
public static final String OH_CHANGE = "Awaiting Change";

/** On Hold Reason - Awaiting vendor */
public static final String OH_VENDOR = "Awaiting Vendor";

/** Resolution Code - Permanently Solved */
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** Resolution Code - Workaround */
public static final String RC_WORKAROUND = "Workaround";

/** Resolution Code - Not Solved */ 
public static final String RC_NOT_SOLVED = "Not Solved";

/** Resolution Code - Caller Closed */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** Cancellation Code - Duplicate */ 
public static final String CC_DUPLICATE = "Duplicate";

/** Cancellation Code - Unnecessary */
public static final String CC_UNNECESSARY = "Unnecessary";

/** Cancellation Code - Not an Incident */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** Owner Id */
private String ownerId;

/** Note for the incident */
private String note;

/** Cancellation Code Enum Object */
/** Resolution Code Enum Object */
/** On Hold Reason Enum Object */
/**Command Value Object */
/**
* This the constructor of the class Command that constructs the variables
* command, ownerId, holdReason, resolutionCode, cancellationCode, and note
* @param command the command that the user gives
* @param ownerId the Id of the owner of the Incident
* @param onHoldReason the On Hold reason for the Incident 
* @param resolutionCode the resolution Code for the Incident
* @param cancellationCode the Cancellation codefor the Incident
* @param note the Work Note for the Incident
*/
public Command(CommandValue command, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode, CancellationCode cancellationCode, String note) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* returns the Command value of the user
* @return the Command Value of the user
*/
public CommandValue getCommand() {

/**
* Returns the Owner Id of the person that made the Incident
* @return the Owner Id
*/
public String getOwnerId() {

/**
* returns the Resolution Code of the Incident 
* @return the Resolution Code
*/
public ResolutionCode getResolutionCode() {

/**
* Returns the Work Note of the Incident 
* @return the Work Note
*/
public String getWorkNote() {

/**
* Returns the On Hold Reason for the Incident
* @return the On Hold reason
*/
public OnHoldReason getOnHoldReason() {

/**
* returns the Cancellation Code for the Incident
* @return the Cancellation Code 
*/
public CancellationCode getCancellationCode() {

/**
* Represents one of the six possible 
* commands that a user can make for 
* the Incident Management FSM.
*/
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/**
* Represents the three possible on hold codes.
*/
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/**
* Represents the four possible Resolution Codes
*/
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/**
* Represents the three possible cancellation codes.
*/
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT };

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class keeps track of all Incident information including the current state.
* This state is updated when a Command encapsulating a transition 
* is given to this class  
*/
@SuppressWarnings("unused")
public class ManagedIncident {

/** Category - Inquiry */
public static final String C_INQUIRY = "Inquiry";

/** Category - Software */
public static final String C_SOFTWARE = "Software";

/** Category - Hardware */
public static final String C_HARDWARE = "Hardware";

/** Category - Network */
public static final String C_NETWORK = "Network";

/** Category - Database */
public static final String C_DATABASE = "Database";

/** Priority - Urgent */
public static final String P_URGENT = "Urgent";

/** Priority - High */
public static final String P_HIGH = "High";

/** Priority - Medium */
public static final String P_MEDIUM = "Medium";

/** Priority - Low */
public static final String P_LOW = "Low";

/** Unique Id for the Incident */
private int incidentId;

/** User Id of the person who reported the Incident */
private String caller;

/** User Id of the Incident Owner */
private String owner;

/** Incident's name information for when the Incident was created */
private String name;

/** Change Request for the Incident */
private String changeRequest;

/** An ArrayList of notes */
private ArrayList<String> notes;

/** New Name */
public static final String NEW_NAME = "New";

/** In Progress Name */
public static final String IN_PROGRESS_NAME = "In Progress";

/** On Hold Name */
public static final String ON_HOLD_NAME = "On Hold";

/** Resolved Name */
public static final String RESOLVED_NAME = "Resolved";

/** Closed Name */
public static final String CLOSED_NAME = "Closed";

/** Cancelled Name */
public static final String CANCELED_NAME = "Canceled";

/**  A static field that keeps track of the id value that should be given to the next ManagedIncident created. */
private static int counter = 0;

/** Priority of the Incident */
private Priority priority;

/** Category of the Incident*/
private Category category;

/** State of the Incident */
/** On Hold State Object of the of the Incident */
private final IncidentState onHoldState = new OnHoldState();

/** Resolved State Object of the of the Incident */
private final IncidentState resolvedState = new ResolvedState();

/** New State Object of the of the Incident */
private final IncidentState newState = new NewState();

/** Closed State Object of the of the Incident */
private final IncidentState closedState = new ClosedState();

/** In Progress State Object of the of the Incident */
private final IncidentState inProgressState = new InProgressState();

/** Canceled State Object of the of the Incident */
private final IncidentState canceledState = new CanceledState();

/** Cancellation Code of the Incident */
/** Resolution Code of the Incident */
/** On Hold Reason of the Incident */
/**
* This is the constructor of the managedIncident Class, it constructs the 
* variables caller, category, Priority, name, and workNote of te class.
* @param caller caller of the Incident
* @param category category of the Incident
* @param priority Priority of the Incident 
* @param name Name of the Incident 
* @param workNote Work Note of the Incident
*/
public ManagedIncident(String caller, Category category, Priority priority, String name, String workNote) {

 - Throws: IllegalArgumentException

/**
* Constructs the Incident variable 
* @param incident the incident in the Management Incident
*/
public ManagedIncident(Incident incident) {		
 - Throws: IllegalArgumentException

/**
* Manages the Number of Incidents
*/
public static void incrementCounter() {

/**
* Returns the Incident 
* @return the Incident
*/
public int getIncidentId() {

/** 
* Returns the Change Request
* @return the Change Request
*/
public String getChangeRequest() {

/**
* Returns the Category
* @return the Category
*/
public Category getCategory() {

/**
* Returns the Category String 
* @return the Category String 
*/
public String getCategoryString() {

/**
* Sets the Category of the Incident
* @param category the category of the Incident
*/
private void setCategory(String category) {

 - Throws: IllegalArgumentException

/** 
* Returns the Priority String
* @return the Priority String
*/
public String getPriorityString() {

/**
* Sets the Priority of the Incident
* @param priority the priority of the Incident          
*/
private void setPriority(String priority) {

 - Throws: IllegalArgumentException

/**
* Returns the On Hold reason 
* @return the On Hold Reason
*/
public String getOnHoldReasonString() {

/**
* Sets the On Hold reason of the Incident
* @param holdReason Reason for Hold
*/
private void setOnHoldReason(String onHoldReason) {

 - Throws: IllegalArgumentException

/**
* Returns the String of the Cancellation Code
* @return the String of Cancellation Code
*/
public String getCancellationCodeString() {

/**
* Sets the Cancellation Code for the Incident 
* @param cancelCode cancellation Code for the Incident
*/ 
private void setCancellationCode(String cancelCode) {

/**
* Returns the State of the Incident 
* @return the State of Incident
*/
public IncidentState getState() {

/** 
* Sets the State of the Incident
* @param state the state of the Incident
*/
private void setState(String state) {

 - Throws: IllegalArgumentException

/** 
* Returns the Resolution Code
* @return the resolution Code
*/
public ResolutionCode getResolutionCode() {

/** 
* Returns the String of the Resolution Code
* @return the String of the Resolution Code
*/
public String getResolutionCodeString() {

/**
* Sets the Resolution Code of the Incident
* @param resolutionCode Resolution Code of the INcident
*/
private void setResolutionCode(String resolutionCode) {

 - Throws: IllegalArgumentException

/** 
* Returns the Owner of the Incident 
* @return the owner of Incident
*/
public String getOwner() {

/**
* Returns the Name of the Incident
* @return the name of the Incident
*/
public String getName() {

/**
* Returns the Caller 
* @return the Caller
*/
public String getCaller() {

/**
* Returns the "more Information" or note that the user 
* provides for the Incident
* @return the note for the Incident
*/
public ArrayList<String> getNotes() {

/**
* Returns the Notes of the Incident
* @return notes of the Incident
*/
public String getNotesString() {		//check this
/**
* Returns the Command that the user givs 
* @param command that the user gives
*/
public void update(Command command) {  //check the UnsupportedOperationException
 - Throws: IllegalArgumentException

/**
* Returns the Incident 
* @return the incident
*/
public Incident getXMLIncident() {

/**
* Sets the Counter, which is the Incident count
* @param ncounter the number of Incidents
*/
public static void setCounter(int ncounter) {

/**
* Represents the five possible categories of incidents 
*/
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/**
* Represents the four possible priorities of ManagedIncident
*/
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
/**
* This class represents the new state of the 
* Incident Management FSM
*/
public class NewState implements IncidentState {

/**
* Updates the state of the Incident
* @param command the Command that the user gives for the Incident
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the State Name of the Incident 
* @return the State name of the Incident
*/
public String getStateName() {

/**
* This class represents the on Hold State
* of the Incident Management FSM 
*/
public class OnHoldState implements IncidentState {

/**
* Updates the state of the Incident
* @param command the Command that the user gives for the Incident
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the State Name of the Incident 
* @return the State name of the Incident
*/
public String getStateName() {

/**
* This class represents the Resolved State of the
* Incident Management FSM
*/
public class ResolvedState implements IncidentState {

/**
* Updates the state of the Incident
* @param command the Command that the user gives for the Incident
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the State Name of the Incident 
* @return the State name of the Incident
*/
public String getStateName() {

/**
* This class represents the In Progress State 
* of the Incident Management FSM
*/
public class InProgressState implements IncidentState {

/**
* Updates the state of the Incident
* @param command the Command that the user gives for the Incident
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the State Name of the Incident 
* @return the State name of the Incident
*/
public String getStateName() {

/**
* This class represents the Closed State of the
* Incident Management FSM
*/
public class ClosedState implements IncidentState {

/**
* Updates the state of the Incident
* @param command the Command that the user gives for the Incident
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the State Name of the Incident 
* @return the State name of the Incident
*/
public String getStateName() {

/**
* This class represents the Canceled State of the 
* Incident Management FSM
*/
public class CanceledState implements IncidentState {

/**
* Updates the state of the Incident
* @param command the Command that the user gives for the Incident
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the State Name of the Incident 
* @return the State name of the Incident
*/
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class maintains a current List of ManagedIncidents 
* in the Incident Management System
*/
public class IncidentManager {

private ManagedIncidentList incidentList;

private static IncidentManager incident;

/**
* This is the constructor of the IncidentManager Class
*/
private IncidentManager() {

/**
* Returns the only instance of this class
* @return the only instance
*/
public static IncidentManager getInstance() {

/**
* Saves the Managed incidents to the File
* @param fileName the file that the Incidents are saved to
*/
public void saveManagedIncidentsToFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Loads the Managed Incidents from the File
* @param fileName the file from where the Incidents are loaded
*/
public void loadManagedIncidentsFromFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Creates new Managed Incident List
*/
public void createNewManagedIncidentList() {

/**
* Returns the managed incidents as a 2D array
* @return the Array of the incidents
*/
public String[][] getManagedIncidentsAsArray(){		//check this again
/**
* Returns the managed incidents as a 2D array according to category
* @param category the category according to which the Incidents are listed
* @return the Array of the incidents by Category
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category){

 - Throws: IllegalArgumentException

/**
* returns the Incident with the given Id
* @param id the id of the Incident 
* @return the Incident with that Id
*/
public ManagedIncident getManagedIncidentById(int id) {

/**
* Executes the Command from the User
* @param id of the Incident
* @param command from the User
*/
public void executeCommand(int id, Command command) {

/** 
* Deletes the Managed Incident of that particular Id
* @param id the Id of the incident that has to be deleted
*/
public void deleteManagedIncidentById(int id) {

/**
* Adds the Incident to the List
* @param caller of the Incident
* @param category Category of the Incident
* @param priority Priority of the Incident
* @param name of the Incident
* @param workNote Note or extra Information of the Incident
*/
public void addManagedIncidentToList(String caller, Category category, Priority priority, String name, String workNote) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class represents and maintains 
* the current List of Managed Incidents 
* in the Incident Management System 
*/
public class ManagedIncidentList {

/** List of all the Incidents */
private ArrayList<ManagedIncident> managedIncidents;

/**
* Constructor of the ManagedIncidentList class
*/
public ManagedIncidentList() {						
/**
* Adds a Managed Incident to the List
* @param caller of the Incident
* @param category of the Incident
* @param priority of the Incident
* @param name of the Incident
* @param workNote extra Information of the Incident
* @return the Id of the Incident
*/
public int addIncident(String caller, Category category, Priority priority, String name, String workNote) {

/**
* Adds a list of Incidents from the XML Library to the List
* @param incident the incidents from the XML Library
*/
public void addXMLIncidents(List<Incident> incident) {

/**
* Returns the entire List of Managed Incidents
* @return all the Managed Incidents 
*/
public ArrayList<ManagedIncident> getManagedIncidents() {   //check the return type
/**
* Returns the entire List of Managed Incidents by Category
* @param category the category of Incidents that are returned
* @return the Incidents with the particular Category
*/
public ArrayList<ManagedIncident> getIncidentsByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Searches and gets a Managed Incident in the 
* List by Id
* @param id the id of the Incident Returned
* @return the Incident with the given Id
*/
public ManagedIncident getIncidentById(int id) {

/**
* Updates a Managed Incident in the 
* List through execution of a Command
* @param id of the Incident
* @param command from the User
*/
public void executeCommand(int id, Command command) {

/**
* Deletes the Incident having the given Id
* @param id the Id of the incident that has be deleted
*/
public void deleteIncidentById(int id) {


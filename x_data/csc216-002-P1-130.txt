---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* implements TransitGroup and represents passengers have not join 
* 
*
*/
public class PreSecurity implements TransitGroup {

/**
* the queue in the preSecurity
*/
/**
* constructor of PreSecurity, use provided time and report the create a new
* PreSecurity
* 
* @param passengers
*            passengers number have not join a check point
* @param report
*            report of the PreSecurity
*/
public PreSecurity(int passengers, Reporter report) {

 - Throws: IllegalArgumentException

/**
* Show the depart time of next passenger
* 
* @return the time of next passenger depart time
*/
public int departTimeNext() {

/**
* Show the next depart passenger
* 
* @return Passenger who will depart next
*/
public Passenger nextToGo() {

/**
* Show whether there is next passenger waiting to leave
* 
* @return true if there is next passenger waiting to depart
*/
public boolean hasNext() {

/**
* return the next passenger and then delete from queue
* 
* @return Passenger who has been remove from queue successfully
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
* 
*/
public class Ticketing {

/**
* Absolute time for passengers created for the simulation. The time starts at
* zero and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger
* created.
*/
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a Fast Track passenger should be created */
private static double pctExpedite = .50; // initialize with default value
/**
* Percentage of time a trusted traveler/TSA PreCheck passenger should be
* created
*/
private static double pctTrust = .05; // initialize with default value
/**
* constructor of ticketing
*/
public Ticketing() {

/**
* Set the proportions of fast track, trusted traveler, and (by inference)
* ordinary passengers that should be generated. Proportion of ordinary
* passengers is 1 - (pctFast + pctTrusted).
* 
* @param pctTrusted
*            - proportion of passengers that are TrustedTravelers
* @param pctFast
*            - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments.
* 
* @param log
*            - where the passenger will log his/her data
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory. You can use this for testing ONLY! Do not use it in your
* simulator.
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class show one check point
* 
*
*/
public class CheckPoint {

/** the time available for last passenger */
private int timeWhenAvailable;

/** the line of passenger of the checkPoint */
private PassengerQueue line;

/**
* constructor of a checkPoint
*/
public CheckPoint() {

/**
* Returns the number of Passengers in the queue.
* 
* @return the number of Passengers
*/
public int size() {

/**
* remove a passenger in the line
* 
* @return passenger passenger will be removed from the line
*/
public Passenger removeFromLine() {

/**
* Show whether there is next passenger waiting to leave in the line
* 
* @return true if there is next passenger waiting to depart in the line
*/
public boolean hasNext() {

/**
* Show the depart time of next passenger in the line
* 
* @return the time of next passenger depart time in the line
*/
public int departTimeNext() {

/**
* Show the next depart passenger in the line
* 
* @return Passenger who will depart next in the line
*/
public Passenger nextToGo() {

/**
* add the given passenger to the line
* 
* @param passenger
*            passenger need to add to the line list
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class used to show the passengers had enter the security area
* 
*
*/
public class SecurityArea implements TransitGroup {

/** max number of checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** min number of checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** error message will come out when the checkpoints number is wrong */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** error message will come out when index is wrong */
private static final String ERROR_INDEX = "Index out of range for this security area.";

/** largest index for the fast passenger */
private int largestFastIndex;

/** pre index for tsa passenger */
private int tsaPreIndex;

/** list of the checkpoint lines */
private CheckPoint[] check;

/**
* constructor for the security area
* 
* @param numberCheckPoints
*            number of check points
*/
public SecurityArea(int numberCheckPoints) {

 - Throws: IllegalArgumentException

/**
* check out whether the gates number is right
* 
* @param gatesNumber
*            given number to check when it is right
* @return true if the gates number is right
*/
private boolean numGatesOK(int gatesNumber) {

/**
* add the given passenger to the line in the selected checkPoint
* 
* @param passenger
*            passenger need to add to the line list
* @param index
*            index of line need to add
*/
public void addToLine(int index, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
* Ordinary line with shortest length
* 
* @return length length of the regular line with shortest size
*/
public int shortestRegularLine() {

/**
* Fast line with shortest length
* 
* @return length length of the fast line with shortest size
*/
public int shortestFastTrackLine() {

/**
* TSA line with shortest length
* 
* @return length length of the TSA line with shortest size
*/
public int shortestTSAPreLine() {

/**
* show length of the chosen line
* 
* @param index
*            index of the line chosen to show length
* @return the length of the chosen length
*/
public int lengthOfLine(int index) {

 - Throws: IllegalArgumentException

/**
* Show the depart time of next passenger
* 
* @return the time of next passenger depart time
*/
public int departTimeNext() {

/**
* Show the next depart passenger
* 
* @return Passenger who will depart next
*/
public Passenger nextToGo() {

/**
* return the next passenger and then delete from queue
* 
* @return Passenger who has been remove from queue successfully
*/
public Passenger removeNext() {

/**
* show the shortest line number
* 
* @param largestFastIndex
*            index of fast line
* @param tsaPreIndex
*            index of tsa line
* @return number number of the shortest line
*/
private int shortestLineInRange(int leftRange, int rightRange) {

 - Throws: IllegalArgumentException

/**
* method to show the next line need to clean
* 
* @return number number of the next line need to clean
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* class used to show information of Fast track passenger
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** color of the Fast track passenger */
private Color color;

/** max expected process time of a fast track passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** number to determine what color this passenger is */
private int colorLevel;

/**
* constructor of FastTrackPassenger
* 
* @param arrivalTime
*            arrival time of fast track passenger
* @param processTime
*            process time of fast track passenger
* @param report
*            report information of the fast track passenger
*/
@SuppressWarnings("static-access")
public FastTrackPassenger(int arrivalTime, int processTime, Reporter report) {

/**
* show the color of Fast Track Passenger
* 
* @return the color
*/
public Color getColor() {

/**
* find a line by given transitGroup
* 
* @param transitGroup
*            transitGroup used to find the line
*/
public void getInLine(TransitGroup transitGroup) {

/**
* pick a line number of the Fast track passenger
* 
* @param transitGroup
*            transitGroup used to find the line
* @return the number of the line
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class show information of an ordinary passenger
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** color of the ordinary track passenger */
private Color color;

/** max expected process time of a ordinary track passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** number to determine what color this passenger is */
private int colorLevel;

/**
* constructor of an ordinary passenger
* 
* @param arrivalTime
*            arrival time of an ordinary passenger
* @param processTime
*            process time of an ordinary passenger
* @param report
*            report with information of an ordinary passenger
*/
@SuppressWarnings("static-access")
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter report) {

/**
* show the color of ordinary Passenger
* 
* @return the color
*/
public Color getColor() {

/**
* find a line by given transitGroup
* 
* @param transitGroup
*            transitGroup used to find the line
*/
public void getInLine(TransitGroup transitGroup) {

/**
* pick a line number of the ordinary passenger
* 
* @param transitGroup
*            transitGroup used to find the line
* @return the number of the line
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class shows one passenger information
* 
*
*/
public abstract class Passenger {

/** min process time every passenger will use */
public static final int MIN_PROCESS_TIME = 20;

/** arrival time of the passenger */
private int arrivalTime;

/** wait time of the passenger */
private int waitTime;

/** process time of the passenger */
private int processTime;

/** index of the line, which the passenger stand in */
private int lineIndex;

/** waiting process of the passenger */
private boolean waitingProcess;

/** Log information of the passenger */
private Log myLog = new Log();

/**
* constructor of the Passenger
* 
* @param arrivalTime
*            arrival time of the passenger
* @param processTime
*            process time of the passenger
* @param report
*            report information of the passenger
*/
public Passenger(int arrivalTime, int processTime, Reporter report) {

 - Throws: IllegalArgumentException

/**
* return the wait time of the passenger
* 
* @return the waitTime wait time of the passenger
*/
public int getWaitTime() {

/**
* set the wait time by the given wait time
* 
* @param waitTime
*            the waitTime to set
*/
public void setWaitTime(int waitTime) {

/**
* return the line index of the passenger
* 
* @return the lineIndex
*/
public int getLineIndex() {

/**
* set the line index by the given lineIndex
* 
* @param lineIndex
*            the lineIndex to set
*/
protected void setLineIndex(int lineIndex) {

/**
* return the arrival time of the passenger
* 
* @return the arrivalTime arrival time of the passenger
*/
public int getArrivalTime() {

/**
* return the processTime of the passenger
* 
* @return the processTime process time of the passenger
*/
public int getProcessTime() {

/**
* return whether the passenger has finished waiting
* 
* @return the waitingProcess waiting process of the passenger
*/
public boolean isWaitingInSecurityLine() {

/**
* clear passenger in security
*/
public void clearSecurity() {

/**
* change the waitingProcess to true
*/
public void castInLine() {

/**
* get a transit group in the line
* 
* @param transit
*            the transitGroup in the line
*/
public abstract void getInLine(TransitGroup transit);

/**
* show color of the passenger
* 
* @return color color of the passenger
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
* 
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* 
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* 
* @param person
*            the Passenger to add
*/
public void add(Passenger person) {

/**
* Removes and returns the front Passenger from the queue.
* 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException
*             if the queue is empty
*/
public Passenger remove() throws NoSuchElementException {

/**
* Gets the front Passenger of the queue without removing it, or null if the
* queue is empty. Does not remove the Passenger from the queue.
* 
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* 
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class showed information of a TrustedTraveler
* 
*
*/
public class TrustedTraveler extends Passenger {

/** color of the Fast track passenger */
private Color color;

/** max expected process time of a fast track passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** number to determine what color this passenger is */
private int colorLevel;

/**
* Constructor of the trusted traveler
* 
* @param arrivalTime
*            arrival time of the trusted traveler
* @param processTime
*            process time of the trusted traveler
* @param report
*            report with given information of the traveler
*/
@SuppressWarnings("static-access")
public TrustedTraveler(int arrivalTime, int processTime, Reporter report) {

/**
* show the color of trusted Passenger
* 
* @return the color
*/
public Color getColor() {

/**
* find a line by given transitGroup
* 
* @param transitGroup
*            transitGroup used to find the line
*/
public void getInLine(TransitGroup transitGroup) {

/**
* pick a line number of the trusted passenger
* 
* @param transitGroup
*            transitGroup used to find the line
* @return the number of the line
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class shows the event calendar has two transitGroup
* 
*
*/
public class EventCalendar {

/** transitGroup of high priority */
private TransitGroup highPriority;

/** transitGroup of low priority */
private TransitGroup lowPriority;

/**
* constructor of the EventCalender
* 
* @param highPriority
*            TransitGroup shows high priority
* @param lowPriority
*            TransiGroup shows low priority
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* show the next passenger need to act
* 
* @return Passenger passenger who will act next
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class used to log every passenger information
* 
*
*/
public class Log implements Reporter {

/** total number of passengers */
private int numCompleted;

/** total wait time */
private int totalWaitTime;

/** total process time */
private int totalProcessTime;

/**
* the constructor of log.
*/
public Log() {

/**
* show total number of passengers
* 
* @return numCompleted total number of passengers
*/
@Override
public int getNumCompleted() {

/**
* log the given passenger information to the log
*/
@Override
public void logData(Passenger p) {

/**
* show the average of total wait time
* 
* @return averageWaitTime the average of wait time
*/
@Override
public double averageWaitTime() {

/**
* show the average of total process time
* 
* @return averageWaitTime the average of process time
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group
* activities.
* 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* 
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* 
* @param p
*            - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* 
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* 
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Simulator control the whole program
* 
*
*/
public class Simulator {

/** max number of checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** min number of checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** number of passengers */
private int numPassengers;

/** the event calendar */
private EventCalendar myCalendar;

/** passenger log */
private Log log;

/** Passengers have not enter security area */
private TransitGroup inTicketing;

/** Passengers in security area */
private TransitGroup inSecurity;

/** current passenger */
private Passenger currentPassenger = null;

/**
* constructor of the simulator by given checkpoints number and passengers
* number and percentage
* 
* @param numCheckPoints
*            number of check points in security area
* @param numPassengers
*            number of passenger in preSecurity area
* @param numTSA
*            percentage of TSA passenger
* @param numFastTrack
*            percentage of Fast track passenger
* @param numOrdinary
*            percentage of ordinary passenger
*/
public Simulator(int numCheckPoints, int numPassengers, int numTSA, int numFastTrack, int numOrdinary) {

 - Throws: IllegalArgumentException

/**
* check the given number of check points and the percentages of tsa, fast and
* ordinary passengers
* 
* @param numCheckPoints
*            number of check points
* @param numTSA
*            percentage of tsa passenger
* @param numFastTrack
*            percentage of fast track passenger
* @param numOrdinary
*            percentage of ordinary passenger
*/
private void checkParameters(int numCheckPoints, int numTSA, int numFastTrack, int numOrdinary) {

 - Throws: IllegalArgumentException

/**
* check the passenger percentage
* 
* @param numTSA
*            percent of TSA passenger
* @param numFastTrack
*            percent of Fast track passenger
* @param numOrdinary
*            percent of ordinary passenger
* @throws IllegalArgumentException
*             if anyone of the percentage is negative
* @throws IllegalArgumentException
*             if the sum of the percents is not 100
*/
private void setUp(int numTSA, int numFastTrack, int numOrdinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* show the reporter contains information of every passenger
* 
* @return log reporter contains all passenger information
*/
public Reporter getReporter() {

/**
* step used to act the next passenger in myCalender
* 
* @throws IllegalArgumentException
*             if there is no more passenger in the Calendar
*/
public void step() {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* check out whether there is more steps can be played
* 
* @return false if there are no more passenger in myCalendar
*/
public boolean moreSteps() {

/**
* show the line index of the currentPassenger
* 
* @return line index of current passenger
*/
public int getCurrentIndex() {

/**
* show the color of the current passenger
* 
* @return color of current passenger
*/
public Color getCurrentPassengerColor() {

/**
* show whether the current passenger has finished the security
* 
* @return true if the passenger just finished the security
*/
public boolean passengerClearedSecurity() {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Creates a PreSecurity object, which represents the passengers in the terminal who have not joined a security line yet.
* It creates a passenger queue of all of these passengers, and has the ability to add and remove passengers to the queue, determine 
* which passenger is at the front of the pre-security line and what their departure time is.
*
*/
public class PreSecurity implements TransitGroup {

/** PassengerQueue that holds the passengers in the pre-security area */
private PassengerQueue outsideSecurity;

/**
* Constructs the PreSecurity object using the number of passengers and the log of passenger information
* It creates the passengers by calling Ticketing.generatePassenger() and adding each passenger
* to the PassengerQueue. If an invalid amount of Passengers is passed in, an IllegalArgumentException will be thrown.
*  
* @param numPassengers number of passengers 
* @param log log where information about all Passengers is stored
* @throws IllegalArgumentException if input is invalid 
*/
public PreSecurity(int numPassengers, Reporter log) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the next passenger in the PassengerQueue that represents the pre-security area
* @return The next passenger in the queue
*/
public Passenger nextToGo() {

/**
* Determines if the PassengerQueue that represents the pre-security are has a next element 
* @return true if the queue representing the Pre-security area has a next element 
* and false if it does not 
*/
public boolean hasNext() {

/** Removes the next Passenger from the PassengerQueue that represents the area
* before security
* @return the Passenger that is removed from the queue
*/
public Passenger removeNext() {

/**
* Returns the departure time of the next passenger in the PassengerQueue that represents the pre-security area
* The departure time at the pre-security area is the arrival time of the front passenger 
* @return the departure time of the next passenger
*/
@Override
public int departTimeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Creates the Checkpoint object, which represents the security checkpoints in the terminal
*
*/
public class CheckPoint {

/** the time that the last passenger in line will clear security and leave the line */
private int timeWhenAvailable;

/** passenger queue that holds the passengers in line */
private PassengerQueue line;

/**
* constructs the checkpoint object, which is represented by a PssengerQueue. The object is initialized to have no passengers in it, because the Passengers
* start in the ticketing area
*/
public CheckPoint() {

/**
* Returns the size of the checkpoint, which represents how many Passengers are in the line
* @return integer of the size of the line
*/
public int size() {

/**
* Removes the first passenger from the security checkpoint queue after they have finished undergoing security 
* @return returns the Passenger that is removed from the line 
*/
public Passenger removeFromLine() {

/**
* Returns whether or not there is another element in the PassengerQueue that represents the checkpoint
* @return true if there is another element in the queue and false if there is not another element in the queue
*/
public boolean hasNext() {

/**
* Returns the amount of time before the the next passenger will clear security, returns Integer.MAX_VALUE
* if the line is empty
* @return amount of time before next passenger gets through security
*/
public int departTimeNext() {

/**
* Returns the Passenger at the front of the PassengerQueue that represents the security checkpoint
* @return the Passenger at the front of the queue
*/
public Passenger nextToGo() {

/**
* Adds the Passenger passed in to the end of the line, sets their waitTime according to timeWhenAvailable 
* and their arrivalTime and processTime. It also updates timeWhenArrival to reflect another passenger being in the line
* @param passenger that is being added to the line
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Creates the SecurityArea class, which represents the area where the security checkpoints are in the terminal. A Passenger moves to 
* this area once they have been moved out of the pre-security area. A passenger enters this area and is placed in an appropriate checkpoint line
* based on their passenger type. Once a Passenger reaches the front of their line, they are processed and then leave the simulation.
*
*/
public class SecurityArea implements TransitGroup {

/** maximum number of checkpoints in the terminal*/
private static final int MAX_CHECKPOINTS = 17;

/** minimum amount of checkpoints in the terminal */
private static final int MIN_CHECKPOINTS = 3;

/** error message for the wrong number of checkpoints inputed */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** error message for any attempt to add a security checkpoint line with an improper index */ 
private static final String ERROR_INDEX = "Index out of range for this security area";

/** largest index for lines that are FastTrack checkpoints */
private int largestFastIndex;

/** index of the TSA checkpoint line */
private int tsaPreIndex;

/** ArrayList of checkpoints that represents the security area */
private ArrayList<CheckPoint> secArea;

/**
* Constructs the SecurityArea, which represents all of the checkpoints in the security area and their waiting lines.
* The constructor takes in the total number of checkpoints in the security area and throws an exception if that value is out of range. 
* After the ArrayList of checkpoints is constructed, the method will add the number of checkpoints to the array list based on the number of checkpoints passed in.
* It also sets the index of the TSA checkpoint, which is always the last index, and the number of checkpoints that are FastTrack, which is always (1/3) of the total checkpoints.    
* @param numCheckpoints number of Checkpoints 
* @throws IllegalArgumentException if number of checkpoints is out of range
*/
public SecurityArea(int numCheckpoints) {

 - Throws: IllegalArgumentException

/**
* private helper method that determines if the number of checkpoints is in range 
* @return true if the number of checkpoints is valid and false if it is not 
*/
private boolean numGatesOK(int numCheckpoints) {

/**
* Returns the index of the FastTrack checkpoint with the shortest line by using shortest line in range as a helper method. It passes shortest line in range the indexes of the checkpoint type
* and gets the shortest line in that range back.
* @return index of the shortest FastTrack Line
*/
public int shortestFastTrackLine() {

/**
* Returns the index of the Regular checkpoint with the shortest line by using shortest line in range as a helper method. It passes shortest line in range the indexes of the checkpoint type
* and gets the shortest line in that range back.
* @return index of the shortest Regular Line
*/
public int shortestRegularLine() {

/**
* Returns the index of the shortest TSA pre-check line, which is just the index of the TSA pre-check line, as 
* there is only one TSA precheck line
* @return index of the shortest TSA pre-check line 
*/
public int shortestTSAPreLine() {

/**
* Returns the length of the given checkpoint line at the index that is passed in. 
* An exception is thrown if the value passed in is out of range 
* @param lineIndex index of line
* @return length of line
* @throws IllegalArgumentException if the index is out of range
*/
public int lengthOfLine(int lineIndex) {

 - Throws: IllegalArgumentException

/**
* Returns the time that the next passenger will finish clearing security.
* Integer.Max_VALUE is returned if all of the lines are empty
* @return time the next passenger will finish security
*/
public int departTimeNext() {

/**
* Returns the next passenger to clear security or null if there is none
* @return the next Passenger 
*/
public Passenger nextToGo() {

/**
* removes the next passenger to clear security and returns the passenger 
* @return the next passenger to clear security
*/
public Passenger removeNext() {

/**
* Adds the passenger to the security line with the index passed in to the method
* @param index of the line that the passenger is being added to
* @param passenger Passenger that is being added to the line
* @throws IllegalArgumentException if the index inputed is out of range for the security area
*/
public void addToLine(int index, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
* Helper method that returns the shortest line in a range of indexes passed in. This method will help return the shortest
* line of each checkpoint type, by finding the shortest line in each section of checkpoints. 
* @return index of the shortest line in the range
*/
private int shortestLineInRange(int upper , int lower) {

/**
* Returns the index of the line that contains the passenger in the first index that 
* will clear security first 
* @return the index of the line 
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the FastTrackPassenger object, a subclass of Passenger. FastTrackPassenger holds all of the same information about a Passenger that the 
* Passenger class does, plus it holds information about what color each Passenger is. FastTrackPassengers are passengers that can use either 
* the FastTrack checkpoints or the ordinary checkpoints.
*
*/
public class FastTrackPassenger extends Passenger {

/** Maximum process time for a FastTrackPassenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** color of each passenger in the simulation, all FastTrackPassengers are blue, and their 
* shade depends on their process time.
*/
private Color color;

/**
* Constructs the FastTrackPassenger object using their arrival time and process time.
* Uses the super class, Passenger, constructor. It also sets the passenger's color by calling the getColor() method.
* All FastTrackPassenger are shades of blue. 
* @param arrivalTime time that a passenger joins a security line
* @param processTime time that a passenger will spend undergoing the security check
* @param log log of passenger activities 
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* This method uses the FastTrackPassenger's process time to assign a color to the passenger.
* FastTrackPassengers are all blue. They are light blue if their process time is in the lower half of the range of possible
* process times. They are a saturated blue if their process time is in the upper half of possible values.
*/
@Override
public Color getColor() {

/**
* Adds the passenger to the correct security line. 
* The method calls the pickLine method to select the correct line for the FastTrackPassenger, and then puts them into that line
* @param securityArea list of security lines in the terminal
*/
@Override
public void getInLine(TransitGroup securityArea) {

/**
* Private helper method that determines which line a FastTrackPassenger should choose.
* The FastTrackPassenger will chose the security checkpoint with the shortest line that is not the TSA checkpoint. If there
* are multiple shortest lines, they will choose the one with the smallest index
* @param securityArea list of security lines in the terminal
* @return integer index of the line that was chosen
*/
private int pickLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the OrdinaryPassenger object, a subclass of Passenger. OrdinaryPassenger holds all of the same information about a Passenger that the 
* Passenger class does, plus it holds information about what color each Passenger is. OrdinaryPassengers are passengers that can only use the unrestricted
* regular checkpoints
*
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum process time for an Ordinary Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** color of each passenger in the simulation, all Ordinary Passengers are red, and their 
* shade depends on their process time.
*/
private Color color;

/**
* Constructs the OrdinaryPassenger object using their arrival time and process time.
* Uses the super class, Passenger, constructor. It also sets the passenger's color by calling the getColor() method.
* All ordinary passengers are shades of red. 
* @param arrivalTime time that a passenger joins a security line
* @param processTime time that a passenger will spend undergoing the security check
* @param log log of passenger activities 
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* Adds the passenger to the correct security line. 
* The method calls the pickLine method to select the correct line for the OrdinaryPassenger, and then puts them into that line
* @param securityArea list of security lines in the terminal
*/	
@Override
public void getInLine(TransitGroup securityArea) {

/**
* This method uses the Ordinary Passenger's process time to assign a color to the passenger.
* Ordinary Passengers are all red. They are light red if their process time is in the lower half of the range of possible
* process time. They are a saturated red if their process time is in the upper half of possible values.
*/
@Override
public Color getColor() {

/**
* Private helper method that determines which line a OrdinaryPassenger should choose.
* The OrdinaryPassenger will chose the non-restricted checkpoint with the shortest line. If there are multiple 
* shortest lines, they will chose the one with the shortest index. 
* @param securityArea list of security lines in the terminal
* @return integer index of the line that was chosen
*/
private int pickLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the Passenger objects, which move through the simulation. The object stores information about each passenger, including
* their arrival time to the terminal, how long they have waited to be processed, the time it will take for them to be processed,
* what place they are in their line and if they are waiting to be processed. Each passenger will have a color that represents
* what kind of passenger they are. Red for ordinary, blue for FastTrack and green for TSA pre check passengers.
*
*/
public abstract class Passenger {

/** minimum process time it takes to process any passenger*/
public static final int MIN_PROCESS_TIME = 20;

/** time that the passenger joins a security line*/
private int arrivalTime;

/** time passenger spends in a security line */
private int waitTime;

/** time passenger spends undergoing the security check*/
private int processTime;

/** index of the checkpoint line they are in, before they join a line, it is -1 */
private int lineIndex;

/** True when a passenger selects and joins a line, false if they have not*/
private boolean waitingProcessing;

/** log that passengers log their info in */
private Log log;

/** Selects the correct checkpoint line for the passenger, places them in that line, and updates their line index 
* @param lines security checkpoint lines in the security area
*/
public abstract void getInLine(TransitGroup lines);

/** Returns the color of a passenger that depending on their passenger time and process time
* @return Color of the passenger
*/
public abstract Color getColor();

/**
* Constructs the Passenger class using their arrival time, process time, and the interface reporter. The Passenger's line index is set to -1 because
* a Passenger start in the ticketing area, and their waitTime is set to -1 because they have not joined a line, so they do not have a wait time yet.
* @param arrivalTime time that a passenger joins a security line
* @param processTime time that a passenger will spend undergoing the security check
* @param log log of passenger activities 
* @throws IllegalArgumentException if process time is invalid
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

 - Throws: IllegalArgumentException

/**
* Returns the Passenger's wait time 
* @return the amount of time that a passenger will spend in a security line
*/
public int getWaitTime() {

/**
* Sets a passenger's wait time. If the wait time is less than 0, an IllegalArgumentException is thrown. 
* @param waitTime the waitTime to set
* @throws IllegalArgumentException if the wait time is less than 0
*/
public void setWaitTime(int waitTime) {

 - Throws: IllegalArgumentException

/**
* Returns the index of the passenger in their security checkpoint line. If it is -1, the passenger is not in a security line yet 
* @return the lineIndex
*/
public int getLineIndex() {

/**
* Sets the lineIndex, which is the lineIndex of the checkpoint that the passenger is in. If the passenger is not in a line, it is set to -1.
* If the parameter passed in is less than -1, an IllegalArgumentException is thrown 
* @param lineIndex the lineIndex to set
* @throws IllegalArgumentException if a value less than -1 is passed in 
*/
protected void setLineIndex(int lineIndex) {

 - Throws: IllegalArgumentException

/**
* Returns the arrivalTime
* @return the time that the passenger joins a security checkpoint line
*/
public int getArrivalTime() {

/**
* Returns the processTime
* @return the time that a passenger will spend undergoing the security check
*/
public int getProcessTime() {

/**
* Returns whether or not a passenger is waiting to be processed. If a passenger is in a security line, they are waiting to be processed and true is returned, else false is returned.
* @return returns true if the passenger is in a security checkpoint line waiting to be processed and 
* false if they have not yet selected and joined a line
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from their security line once they finish their security check
*/
public void clearSecurity() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the TrustedTraveler object, a subclass of Passenger. Trusted Traveler holds all of the same information about a Passenger that the 
* Passenger class does, and also hold information about what color each Passenger is. TrustedTraveler Passengers are passengers that can use either 
* the TSA pre-check checkpoints or the ordinary checkpoints.
*
*/
public class TrustedTraveler extends Passenger {

/** Maximum process time for a TrustedTraveler */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** color of each passenger in the simulation, all TrustedTravlers are green, and their 
* shade depends on their process time.
*/
private Color color;

/**
* Constructs the TrustedTraveler object using their arrival time and process time.
* Uses the super class, Passenger, constructor. It also sets the passenger's color by calling the getColor() method.
* All trusted traveler passengers are shades of green. 
* @param arrivalTime time that a passenger joins a security line
* @param processTime time that a passenger will spend undergoing the security check
* @param log log of passenger activities 
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter log) {

/**
* Adds the passenger to the correct security line. 
* The method calls the pickLine method to select the correct line for the TrustedTraveler, and then puts them into that line
* @param securityArea list of security lines in the terminal
*/
public void getInLine(TransitGroup securityArea) {

/**
* This method uses the TrustedTravelers process time to assign a color to the passenger.
* TrustedTravelers are all green. They are light green if their process time is in the lower half of the range of possible
* process time. They are a saturated green if their process time is in the upper half of possible values.
*/
@Override
public Color getColor() {

/**
* Private helper method that determines which line a TrustedTravler should choose.
* The TrustedTraveler chooses the TSA checkpoint unless it is not empty and more than twice the length of the shortest
* ordinary line. If that is an option, the TrustedTravler will choose the shortest ordinary line with the smallest index
* @param lines list of security lines in the terminal
* @return integer index of the line that was chosen
*/
private int pickLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Creates the EventCalender object which determines which passenger will be processed next out of the 
* passengers that are left in the simulation. The passengers can be processed from the ticketing area and the security area, the 
* first passenger in each area has a departure time that determines when they will be processed. Whichever passenger has the earliest departure time will
* be processed next. If there is a tie, the passenger in the ticketing area will be processed first. If there is a tie and all Passengers are in the security area,
* the passenger in the line with the lowest index will be processed first.
*/
public class EventCalendar {

/** higher priority group to be processed */
private TransitGroup hp;

/**lower priority group to be processed */
private TransitGroup lp;

/**
* Constructs the EventCalender Object which determines which Passenger will be processed next. Each parameter group passed in represents an area of passengers.
* The object has a highPriority group that will be processed over the lowPriority group in the event of a tie
* @param highPriority the group that has a higher priority and will be processed first
* @param lowPriority the group that has a lower priority and will be processed second 
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority){

/**
* Returns the passenger from highPriority and lowPriority who will be the next to exit the area they are in the front of.
* If there's a tie for next to exit, the passenger from the highPriority group will be processed over the passenger from the lowPriority group.
* If both groups are empty, an exception is thrown
* @return the next Passenger to be processed
* @throws IllegalArgumentException if both TransitGroups are empty
*/
public Passenger nextToAct() {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the log object where the Passengers will log their information about their wait and process times.
* Passengers will log information after they have gone through security and before they leave the simulation.
*
*/
public class Log implements Reporter {

/** number of Passengers who have gone through security and left the simulation */
private int numCompleted;

/** the total wait time for all of the Passengers */
private int totalWaitTime;

/** the total process time for all of the passengers */
private int totalProcessTime;

/**
* Constructs the log object, where the passengers will store their information.
*/
public Log() {

/**
* Returns the number of passengers that have gone through security, logged their data, and left the simulation
* @return the number of passengers that have completed security
*/
@Override
public int getNumCompleted() {

/**
* Adds the Passenger's wait time and process time to the total wait time and the total process time so that the average of these variables 
* can be calculated once the simulation is complete. A passengers data is logged after they go through security and before they leave the simulation
* @param passenger Passenger who has finished security and is logging their data
*/
@Override
public void logData(Passenger passenger) {

/**
* Calculates the average wait time for all of the passengers who have completed the simulation. The value is divided by 60 to convert it into minutes
* This is calculated once the simulation is complete and all of the passengers have logged their data and exited.
* @return average wait time 
*/
@Override
public double averageWaitTime() {

/**
* Calculates the average process time for all of the passengers who have completed the simulation. The value is divided by 60 to convert it into minutes
* This is calculated once the simulation is complete and all of the passengers have logged their data and exited.
* @return average process time 
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the Simulator object that provides the logic and back end for the simulation user interface to run
* The Simulator object will accept the parameters that the user inputs and pass them to the user interface so that 
* the simulation can be shown to the user. The object will also allow the simulation to be erased after each run so that a new
* simulation can be ran.
*
*/
public class Simulator {

/** TransitGroup that represents passengers who are not in a security checkpoint line */
private TransitGroup inTicketing;

/** TransitGroup that represents the passengers who are in a security checkpoint line */
private TransitGroup inSecurity;

/** the passenger most recently returned by myCalender.nextToAct().*/
private Passenger currentPassenger;

/** log that passengers will report their data to when they exit the simulation */
private Log log;

/** Event Calendar */
private EventCalendar myCalender;

/** number of Passengers in the simulation */
private int numPass;

/**
* Constructs the Simulator object. The method checks to make sure that the parameters passed in by the user are valid, if 
* they are not valid, an IllegalArgumentException is thrown. This method uses the checkParameters helper method to check the parameters
* The method calls the setUp method to set up the simulator
* @param numCheckpoints number of checkpoints
* @param numPassengers number of passengers
* @param trustPct percent of trusted passengers
* @param fastPct percent of FastTrack passengers 
* @param ordPct percent of ordinary passengers 
*/
public Simulator(int numCheckpoints, int numPassengers, int trustPct, int fastPct, int ordPct) {

/**
* Private helper method that checks to make sure that the inputs are valid. If the percents do not sum up to 100, a
* negative integer is inputed, the checkpoint number is not in the range, or an integer is not inputed, an exception is thrown.
* 
* @param numPassengers number of passengers 
* @param trustPct percent of trusted passengers 
* @param fastPct percent of FastTrack passengers
* @param OrdPct percent of ordinary passengers 
*/
private void checkParameters(int numPassengers, int trustPct, int fastPct, int ordPct) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* private helper method that sets up the simulator user interface. 
* It sets the passenger type distribution for the Ticketing object
* @param pctTrusted percent of trusted passengers 
* @param pctFastTrack percent of FastTrack passengers
* @param numPassengers number of passengers
*/
private void setUp(int pctTrusted, int pctFastTrack, int numPassengers) {

/**
* Returns the log where all of the passengers log their information once they are through undergoing security
* @return returns the log
*/
public Reporter getReporter() {

/**
* Takes the nextPassenger through its events according to EventCalender. If the Passenger is in the ticketing area, 
* the Passenger will leave the ticketing area, select an appropriate checkpoint line, and join that line.
* If the Passenger is in the security area, they will leave the security area, be removed from their checkpoint line and 
* log their information to the log. An exception is thrown if there is no next passenger
* @throws IllegalArgumentException if there is no next passenger
*/
public void step() {

/**
* Determines if there are more events to step through
* @return true if there are more events and false if there are no more events 
*/
public boolean moreSteps() {

/**
* Returns the index of the security checkpoint line for currentPassenger
* @return the index of the checkpoint of currentPassenger
*/
public int getCurrentIndex() {

/**
* Returns the color of currentPassenger
* @return color of currentPassenger
*/
public Color getCurrentPassengerColor() {

/** Determines if currentPassenger has cleared security or not. Is false when currentPassenger is null
* @return true is currentPassenger has cleared a security checkpoint and finished processing and false if otherwise
*/
public boolean passengerClearedSecurity() {


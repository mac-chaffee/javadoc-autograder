---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class to provide command to incidentState in manager
* 
*
*/
public class Command {

/** on hold caller */
public static final String OH_CALLER = "Awaiting caller";

/** on hold change */
public static final String OH_CHANGE = "Awaiting change";

/** on hold vendor */
public static final String OH_VENDOR = "Awaiting vendor";

/** string for permanently solved */
public static final String RC_PERMANENTLY_SOLVED = "Permanently solved";

/** String for workaround */
public static final String RC_WORKAROUND = "Workaround";

/** String for not solved state */
public static final String RC_NOT_SOLVED = "Not Solved";

/** String for caller closed */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** String for duplicate reminder */
public static final String CC_DUPLICATE = "Duplicate";

/** String for unnecessary reminder */
public static final String CC_UNNECESSARY = "Unnecessary";

/** String for Not an incident reminder */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** owner ID string */
private String ownerId;

/** note for on incident */
private String note;

/** command value of this command */
private CommandValue commandValue;

/** reason for the command on hold */
private OnHoldReason onHoldReason;

/** resolution code */
private ResolutionCode resolutionCode;

/** cancellation code */
private CancellationCode cancellationCode;

/**
* constructor of command
* 
* @param commandvalue
* @param ownerId
* @param onHoldReason
* @param resolutionCode
* @param cancellationCode
* @param note
*/
public Command(CommandValue commandValue, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode,
 - Throws: IllegalArgumentException

/**
* show the OwnerId of this command
* 
* @return the ownerId owner ID of this command
*/
public String getOwnerId() {

/**
* show the note in this command
* 
* @return the note the work note in the command
*/
public String getWorkNote() {

/**
* show the command value of this command
* 
* @return the commandValue command value of this command
*/
public CommandValue getCommand() {

/**
* show the reason for on hold of this command
* 
* @return the onHoldReason on hold reason of this command
*/
public OnHoldReason getOnHoldReason() {

/**
* show the resolution code of this command
* 
* @return the resolutionCode resolution code of this command
*/
public ResolutionCode getResolutionCode() {

/**
* show cancellation code of this command
* 
* @return the cancelationCode cancellation code of this command
*/
public CancellationCode getCancellationCode() {

/**
* command value used to create a command
* 
*
*/
public enum CommandValue {

/** investigate command value */
/** hold command value */
/** resolve command value */
/** confirm command value */
/** reopen command value */
/** cancel command value */
/**
* reason for command on hold
* 
*
*/
public enum OnHoldReason {

/** reason about caller */
/** reason about wait to change */
/** reson about vendor */
/**
* code of the cancelation
* 
*
*/
public enum CancellationCode {

/** duplicate cause cancel */
/** cancel because unnecessary */
/** cancel because not an incident */
/**
* code consist resolution
* 
*
*/
public enum ResolutionCode {

/** solve permanently */
/** caller workaround */
/** not solved */
/** closed the caller */
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern. All concrete
* incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command
*            Command describing the action that will update the
*            ManagedIncident's state.
* @throws UnsupportedOperationException
*             if the CommandValue is not a valid action for the given state.
*/
/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* the management of all incidents
* 
*
*/
public class ManagedIncident {

/** inquiry category */
public static final String C_INQUIRY = "Inquiry";

/** software category */
public static final String C_SOFTWARE = "Software";

/** hardware category */
public static final String C_HARDWARE = "Hardware";

/** network category */
public static final String C_NETWORK = "Network";

/** database category */
public static final String C_DATABASE = "Database";

/** urgent priority */
public static final String P_URGENT = "Urgent";

/** high priority */
public static final String P_HIGH = "HIGH";

/** medium priority */
public static final String P_MEDIUM = "MEDIUM";

/** low priority */
public static final String P_LOW = "LOW";

/** incident id number */
private int incidentId;

/** caller of incident */
private String caller;

/** owner of incident */
private String owner;

/** name of the incident */
private String name;

/** request for change incident */
private String changeRequest;

/** arrayList of notes in incident */
private ArrayList<String> notes;

/** New state name */
public static final String NEW_NAME = "New";

/** in progress name */
public static final String IN_PROGRESS_NAME = "InProgress";

/** on hold state name */
public static final String ON_HOLD_NAME = "OnHold";

/** resolved state name */
public static final String RESOLVED_NAME = "Resolved";

/** closed state name */
public static final String CLOSED_NAME = "Closed";

/** on hold caller */
public static final String CANCELED_NAME = "Canceled";

/** counter of the state */
private static int counter = 0;

/** category */
private Category category;

/** current state */
private IncidentState state;

/** priority command */
private Priority priority;

/** cancellation code */
private CancellationCode cancellationCode;

/** resolution code */
private ResolutionCode resolutionCode;

/** on hold reason */
private OnHoldReason onHoldReason;

/** new State */
private final IncidentState newState;

/** in progress state */
private final IncidentState inProgressState;

/** closed state */
private final IncidentState closedState;

/** canceled state */
private final IncidentState canceledState;

/** resolved state */
private final IncidentState resolvedState;

/** on hold state */
private final IncidentState onHoldState;

/** XMLincident */
private Incident incident;

/**
* constructor of managedIncident by give caller category priority owner and
* name
* 
* @param caller
*            caller of this managed incident
* @param category
*            category of this managed incident
* @param priority
*            priority of this incident
* @param owner
*            owner of this incident
* @param name
*            name of this incident
*/
public ManagedIncident(String caller, Category category, Priority priority, String owner, String name) {

 - Throws: IllegalArgumentException

/**
* constructor of one managedIncident by given incident
* 
* @param incident
*            given XMLIncident
*/
public ManagedIncident(Incident incident) {

/**
* increase the number of counter
*/
public static void incrementCounter() {

/**
* return the request for change the state
* 
* @return the changeRequest
*/
public String getChangeRequest() {

/**
* set a new change request to the state
* 
* @param changeRequest
*            the changeRequest to set
*/
private void setChangeRequest(String changeRequest) {

 - Throws: IllegalArgumentException

/**
* get the category of the state
* 
* @return the category the category of this state
*/
public Category getCategory() {

/**
* get the string of the category of this state
* 
* @return categoryString String of the category
*/
public String getCategoryString() {

/**
* set a new category to this state by string of new state
* 
* @param category
*            the category to set
*/
private void setCategory(String categoryString) {

 - Throws: IllegalArgumentException

/**
* get the priority of this state
* 
* @return the priority priority of this state
*/
public String getPriorityString() {

/**
* set a new priority to this state
* 
* @param priority
*            the priority to set
*/
private void setPriority(String priorityString) {

 - Throws: IllegalArgumentException

/**
* get the cancellation code String of this state
* 
* @return cancellationCode string of the code to cancellation in the state
*/
public String getCancellationCodeString() {

/**
* set a new cancellation code to this state
* 
* @param cancellationCode
*            the cancellationCode to set
*/
private void setCancellationCode(String cancellationCodeString) {

 - Throws: IllegalArgumentException

/**
* get the current state of this managed state
* 
* @return currentState the current state of this state
*/
public IncidentState getState() {

/**
* set the current state to another state
* 
* @param incidentState
*            the incident type want to be set
*/
public void setState(String incidentState) {

/**
* get the resolution of the code
* 
* @return resolutionCode the resolution code of this state
*/
public ResolutionCode getResolutionCode() {

/**
* get the string of the resolution code in this state
* 
* @return the resolutionCode the string of the resolution code
*/
public String getResolutionCodeString() {

/**
* set a new resolution code to the state
* 
* @param resolutionCode
*            the resolutionCode to set
*/
private void setResolutionCode(String resolutionCodeString) {

 - Throws: IllegalArgumentException

/**
* get the string of the on hold command
* 
* @return the onHoldReason the string of the onHoldReason
*/
public String getOnHoldReasonString() {

/**
* set a new onHoldReason to this state
* 
* @param onHoldReason
*            the onHoldReason to set
*/
private void setOnHoldReason(String onHoldReasonString) {

 - Throws: IllegalArgumentException

/**
* get the incident ID of this state
* 
* @return the incidentId
*/
public int getIncidentId() {

/**
* get the caller of this incident
* 
* @return the caller caller of this incident
*/
public String getCaller() {

/**
* get Owner of this incident
* 
* @return the owner owner of this incident
*/
public String getOwner() {

/**
* name of this incident
* 
* @return name name of this incident
*/
public String getName() {

/**
* notes list of this incident
* 
* @return notes notes array in the incident
*/
public ArrayList<String> getNotes() {

/**
* return the notes in the incident by a whole String
* 
* @return notes the notes ready to be return
*/
public String getNotesString() {

/**
* update the state by given command
* 
* @param command
*            command used to update the incidentState
*/
public void update(Command command) {

/**
* get XML incident in the managed incident
* 
* @return xmlIncident CMLIncident of the managed incident
*/
public Incident getXMLIncident() {

/**
* set a new number to the counter
* 
* @param counter
*            the counter to set
*/
public static void setCounter(int counter) {

/**
* priority enumeration for incidents
* 
*
*/
public enum Priority {

/** urgent priority */
/** high priority */
/** medium priority */
/** low priority */
/**
* Category enumeration for incidents
* 
*
*/
public enum Category {

/** inquiry category */
/** software category */
/** hardware category */
/** network category */
/** database category */
/**
* a new IncidentState has been generated
* 
*
*/
public class NewState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command
*            Command describing the action that will update the
*            ManagedIncident's state.
* @throws UnsupportedOperationException
*             if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* state is in progress
* 
*
*/
public class InProgressState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command
*            Command describing the action that will update the
*            ManagedIncident's state.
* @throws UnsupportedOperationException
*             if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* incidentState is on hold
* 
*
*/
public class OnHoldState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command
*            Command describing the action that will update the
*            ManagedIncident's state.
* @throws UnsupportedOperationException
*             if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* incidentState has been resolved
* 
*
*/
public class ResolvedState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command
*            Command describing the action that will update the
*            ManagedIncident's state.
* @throws UnsupportedOperationException
*             if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* incident canceled by command
* 
*
*/
public class CanceledState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command
*            Command describing the action that will update the
*            ManagedIncident's state.
* @throws UnsupportedOperationException
*             if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

/**
* incidentState when closed
* 
*
*/
public class ClosedState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command
*            Command describing the action that will update the
*            ManagedIncident's state.
* @throws UnsupportedOperationException
*             if the CommandValue is not a valid action for the given state.
*/
@Override
public void updateState(Command command) {

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
@Override
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*
*/
public class IncidentManager {

private ManagedIncidentList incidentList;

private static IncidentManager singleton;

private IncidentManager() {

/**
* finds the current instance of IncidentManager
* 
* @return current instance
*/
public static IncidentManager getInstance() {

public void saveManagedIncidentsToFile(String filename) {

public void loadManagedIncidentsFromFile(String filename) {

public void createNewManagedIncidentList() {

public String[][] getManagedIncidentsAsArray() {

public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

public ManagedIncident getManagedIncidentById(int id) {

public void executeCommand(int id, Command command) {

public void deleteManagedIncidentById(int id) {

public void addManagedIncidentToList(String caller, Category category, Priority priority, String owner,
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* List of managedIncidents, can add and remove incidents
* 
*
*/
public class ManagedIncidentList {

/** list of managedIncidents */
private ArrayList<ManagedIncident> incidents;

/**
* constructor of ManagedIncident list
*/
public ManagedIncidentList() {

/**
* add one incident to this list
* 
* @param caller
*            caller of the incident
* @param category
*            category of the incident
* @param priority
*            priority of the incident
* @param owner
*            owner of the incident
* @param name
*            name of the incident
* @return 1 if add successful
* 
* @throws IllegalArgumentException
*             if fail to add the new incident
*/
public int addIncident(String caller, Category category, Priority priority, String owner, String name) {

 - Throws: IllegalArgumentException

/**
* Add a list of incidents to the ManagedIncidets list
* 
* @param listIncident
*            list contains incidents
*/
public void addXMLIncidents(List<Incident> listIncidents) {

/**
* return the list contains managedIncidents
* 
* @return the incidents the list contains incidents
*/
public List<ManagedIncident> getManagedIncidents() {

/**
* get the list of incident by category
* 
* @param category
*            category selected to be filter out
* @return
*/
public List<ManagedIncident> getIncidentsByCategory(Category category) {

/**
* get one incident by given id number
* 
* @param id
*            incident id of the incident
* @return the selected incident
*/
public ManagedIncident getIncidentById(int id) {

/**
* run the command to the selected incident
* 
* @param id
*            given id to find out the incident
* @param command
*            command need to be execute
*/
public void executeCommand(int id, Command command) {

/**
* remove one incident by given id
* 
* @param id
*            id need to delete from the incidents
*/
public void deleteIncidentById(int id) {


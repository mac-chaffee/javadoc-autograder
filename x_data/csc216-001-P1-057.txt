---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* PreSecurity implements TransitGroup and represents passengers in the terminal
* who have not yet joined a security checkpoint line.
* 
*
*/
public class PreSecurity implements TransitGroup {

/** Initiates outsideSecurity as a PassengerQueue */
/**
* The PreSecurity constructor has two parameters: the number of passengers and
* a logging Reporter, and it can throw an IllegalArgumentException [UC1,E2].
* The constructor creates the specified number of passengers by successive
* calls Ticketing.generatePassenger(), adding each to its PassengerQueue
* 
* @param numPassengers the number (int) of Passengers
* @param log           a logging Reporter
* @throws IllegalArgumentException if number of passengers is not positive
*/
public PreSecurity(int numPassengers, Reporter log) {

 - Throws: IllegalArgumentException

/**
* Gets the depart time of the next Passenger
* 
* @return int of the depart time of the next Passenger
*/
public int departTimeNext() {

/**
* Gets the next Passenger to go in Security
* 
* @return the next Passenger to go in Security
*/
public Passenger nextToGo() {

/**
* Are there any Passengers to go next in security?
* 
* @return boolean true if there is another Passenger to go next, false
*         otherwise
*/
public boolean hasNext() {

/**
* Removes next Passenger through Security
* 
* @return the next Passenger through security
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The CheckPoint class is a Java object with a constructor for a single check
* point or line in the SecurityArea. The class also contains methods to
* retrieve information about the line's length, adding/ removing Passengers, or
* getting information about the next Passenger in line.
* 
*
*/
public class CheckPoint {

/** The PassengerQueue of Passengers that make up a CheckPoint line. */
private PassengerQueue line;

/** Time when Checkpoint is available for next Passenger's processing */
private int timeWhenAvailable;

/**
* Constructor for CheckPoint which is a single line in the SecurityArea.
*/
public CheckPoint() {

/**
* Returns the size (number of people in line) at the CheckPoint
* 
* @return an int of the size/ number of people in line at the CheckPoint
*/
public int size() {

/**
* Removes a Passenger from the CheckPoint line
* 
* @return Passenger removed from the line
*/
public Passenger removeFromLine() {

/**
* Tells whether or not the CheckPoint has a next Passenger
* 
* @return boolean true if the line has a next Passenger, false otherwise
*/
public boolean hasNext() {

/**
* Tells the depart time of the next Passenger in line at the Ticketing Area
* 
* @return int of the depart time of the next Passenger in line at the
*         CheckPoint or Integer.MAX_VALUE if empty
*/
public int departTimeNext() {

/**
* Adds a Passenger to the CheckPoint line
* 
* @param p a Passenger to add to the CheckPoint line
*/
public void addToLine(Passenger p) {

/**
* Returns the next Passenger in the CheckPoint line to go
* 
* @return Passenger next in CheckPoint line to go
*/
public Passenger nextToGo() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The SecurityArea class contains a constructor for an airport security area.
* The class includes appropriate getter, setters, and other methods to modify
* the security area.
* 
*
*/
public class SecurityArea implements TransitGroup {

/** The maximum number of security checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** The minimum number of security checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** Error message for invalid number of checkpoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** Error message for invalid index */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** Largest index of checkpoint for Fast Track ckeckpoint */
private int largestFastIndex;

/** Index of TSA and prechecked Passengers */
private int tsaPreIndex;

/** The array of CheckPoints that make up Security. */
private CheckPoint[] check;

/**
* Constructor for SecurityArea
* 
* @param numGates the number of security checkpoint lines
*/
public SecurityArea(int numGates) {

 - Throws: IllegalArgumentException

/**
* Is the number of security checkpoint lines OK?
* 
* @param numGates number (int) of security gates
* @return boolean true if number of security gates id OK, false otherwise
*/
private boolean numGatesOK(int numGates) {

/**
* Adds a passenger to a checkpoint line
* 
* @param lineIndex of line in security area
* @param p    Passenger to add to line
* @throws IllegalArgumentException if line index is invalid
*/
public void addToLine(int lineIndex, Passenger p) {

 - Throws: IllegalArgumentException

/**
* Which line is the shortest Ordinary line?
* 
* @return index of shortest line that allows Ordinary Passengers
*/
public int shortestRegularLine() {

/**
* Which line is the shortest Fast Track line?
* 
* @return index of shortest line for a Fast Track Passenger
*/
public int shortestFastTrackLine() {

/**
* Which line is the shortest TSA/ prechecked line?
* 
* @return index of shortest line for a TSA/ prechecked Passenger
*/
public int shortestTSAPreLine() {

/**
* Gives the length of a given line
* 
* @param lineIndex to determine its length
* @return int length of line
*/
public int lengthOfLine(int lineIndex) {

 - Throws: IllegalArgumentException

/**
* When will the next passenger leave the group?
* 
* @return departure time of the next passenger or Integer.MAX_VALUE if the
*         group is empty
*/
@Override
public int departTimeNext() {

/**
* Returns next Passenger to clear security.
* 
* @return the next passenger to clear Security or null if there are no
*         Passengers
*
*/
@Override
public Passenger nextToGo() {

/**
* Removes the next passenger to leave the group.
* 
* @return the passenger who is removed
*/
@Override
public Passenger removeNext() {

/**
* What is the shortest line within a given range?
* 
* @param startLine first index of line in given range
* @param endLine   end index of line in given range
* @return index of the shortest line within range
*/
private int shortestLineInRange(int startLine, int endLine) {

/**
* Returns the index of the CheckPoint with the Passenger that is next to clear
* 
* @return index of line with the Passenger that is next to clear
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* FastTrackPassenger is a child class and extends Passenger. The
* FastTrackPassenger class constructs a Fast Track Passenger object for Fast
* Track Passengers
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** Maximum Process time for an Fast Track Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** Private field for color of Passenger */
private Color color;

/**
* Constructor for FastTrackPassenger calls on super class Passenger and sets
* color
* 
* @param arrivalTime int for arrival time of Passenger
* @param processTime int for process time of Passenger
* @param myLog       a Reporter Interface
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Places Passenger within a security line
* 
* @param group a TransitGroup interface
*/
@Override
public void getInLine(TransitGroup group) {

/**
* Getter for color of Passenger.
* 
* @return Color of Passenger
*/
@Override
public Color getColor() {

/**
* Picks the line the Passenger will chose to wait in based on length and type
* of Passenger clearance.
* 
* @param group the TransitGroup interface
* @return int of the security line chosen by the Passenger
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* OrdinaryPassenger is a child class and extends Passenger. The
* OrdinaryPassenger class constructs a Ordinary Passenger object for Passengers
* without special clearance PreCheck Passengers.
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum Process time for an Ordinary Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** Private field for color of Passenger */
private Color color;

/**
* Constructor for OrdinaryPassenger calls on super class Passenger and sets
* color
* 
* @param arrivalTime int for arrival time of Passenger
* @param processTime int for process time of Passenger
* @param myLog       a Reporter Interface
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Places Passenger within a security line
* 
* @param group a TransitGroup interface
*/
@Override
public void getInLine(TransitGroup group) {

/**
* Getter for color of Passenger.
* 
* @return Color of Passenger
*/
@Override
public Color getColor() {

/**
* Picks the line the Passenger will chose to wait in based on length and type
* of Passenger clearance.
* 
* @param group the TransitGroup interface
* @return int of the security line chosen by the Passenger
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Passenger is an abstract class that contains a constructor for the Passenger
* object The class contains the appropriate getter, setter, and other modifying
* methods for the Passenger object. Passenger also implements the Reporter
* Interface.
* 
*
*/
public abstract class Passenger {

/** Minimum process time a Passenger can take (20 seconds) */
public static final int MIN_PROCESS_TIME = 20;

/** Field for Passenger arrival time */
private int arrivalTime;

/** Field for Passenger wait time */
private int waitTime;

/** Field for Passenger process time */
private int processTime;

/** Field for Passenger line index */
private int lineIndex;

/** Field for if Passenger is waiting Processing */
private boolean waitingProcessing;

/** Field for myLog */
private Reporter myLog;

/**
* Constructor for a Passenger object.
* 
* @param arrivalTime int for arrival time of Passenger
* @param processTime int for process time of Passenger
* @param log       a Reporter Interface
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

 - Throws: IllegalArgumentException

/**
* Getter for Passenger's line index
* 
* @return lineIndex int of line index for Passenger
* 
*/
public int getLineIndex() {

/**
* Setter for Passenger line index.
* 
* @param lineIndex int of line index of Passenger
*/
protected void setLineIndex(int lineIndex) {

/**
* Getter for Passenger's arrival time
* 
* @return arrivalTime int of arrival time for Passenger
* 
*/
public int getArrivalTime() {

/**
* Getter for Passenger's wait time
* 
* @return waitTime int of wait time for Passenger
* 
*/
public int getWaitTime() {

/**
* Setter for Passenger wait time.
* 
* @param waitTime int of wait time for Passenger
*/
public void setWaitTime(int waitTime) {

/**
* Getter for Passenger's process time
* 
* @return processTime int of process time for Passenger
* 
*/
public int getProcessTime() {

/**
* Getter for Passenger's state if they are currently waiting in security line
* 
* @return boolean true if waiting in security line, false if not
* 
*/
public boolean isWaitingInSecurityLine() {

/**
* Clears security
* 
*/
public void clearSecurity() {

/**
* Passenger gets in line and updates Security Area with their presence
* 
* @param group a TransitGroup object
*/
abstract public void getInLine(TransitGroup group);

/**
* Returns color of Passenger that simulation will depict. Light blue for Fast
* Track Passengers with Process Time less than Halfway point in the range Blue
* for Fast Track Passengers with Process Time greater than Halfway point in the
* range Light green for Trusted Traveler Passengers with Process Time less than
* Halfway point in the range Green for Trusted Traveler Passengers with Process
* Time greater than Halfway point in the range Light red for Ordinary
* Passengers with Process Time less than Halfway point in the range Red for
* Ordinary Passengers with Process Time greater Halfway point in the range
* 
* @return Color the color the Passenger will be displayed as on the simulation
*/
abstract public Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TrustedTraveler is a child class and extends Passenger. The TrustedTraveler
* class constructs a Trusted Traveler object for Trusted Travelers and TSA
* PreCheck Passengers.
* 
*
*/
public class TrustedTraveler extends Passenger {

/** Maximum Process time for Trusted Traveler */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** Private field for color of Passenger */
private Color color;

/**
* Constructor for TrustedTraveler calls on super class Passenger and sets color
* 
* @param arrivalTime int for arrival time of Passenger
* @param processTime int for process time of Passenger
* @param myLog       a Reporter Interface
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter myLog) {

/**
* Places Passenger within a security line
* 
* @param group a TransitGroup interface
*/
@Override
public void getInLine(TransitGroup group) {

/**
* Getter for color of Passenger.
* 
* @return Color of Passenger
*/
@Override
public Color getColor() {

/**
* Picks the line the Passenger will chose to wait in based on length and type
* of Passenger clearance.
* 
* @param group the TransitGroup interface
* @return int of the security line chosen by the Passenger
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* EventClaender is a class that monitors Passenger events when they act at the
* the ticketing area and the security checkpoints
* 
*
*/
public class EventCalendar {

/** highPriority TransitGroup */
/** lowPriority TransitGroup */
/**
* Compares two groups of Passengers (TransitGroups) to construct an
* EventCalender. The TransitGroups are a SecurityArea (lowPriority) and
* PreSecurity (highPriority).
* 
* @param inTicketing TransitGroup of PreSecurity
* @param inSecurity  another ThransitGroup interface
*/
public EventCalendar(TransitGroup inTicketing, TransitGroup inSecurity) {

/**
* Returns the next Passenger to act either at the ticketing area or the
* security checkpoints
* 
* @return Passenger the next Passenger to take action
* @throws IllegalStateException if both Transit Groups are null both depart
*                               times are Integer.MAX_VALUE
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The Log Class is an object with a constructor of the data log used to keep
* track of Passengers completed with security. The class contains the
* appropriate getters for the given Log data fields, and an updater to add a
* newly completed Passenger's data.
* 
*
*/
public class Log implements Reporter {

/** Number of Passengers with all activities completed */
private int numCompleted;

/** Total of all Passenger wait times */
private int totalWaitTime;

/** Total of all Passenger processing times */
private int totalProcessTime;

/** The constructor for the Log object */
public Log() {

/**
* A getter that returns the number of Passengers that have went completely
* through security.
* 
* @return numCompleted an integer for the total number of Passenger who have
*         completed all their activities
*/
@Override
public int getNumCompleted() {

/**
* A Log updater to add a Passenger's data to the Log data.
* 
* @param p a Passenger object that has just completed security
*/
@Override
public void logData(Passenger p) {

/**
* Calculates average wait time of Passenger's through security
* 
* @return double of average wait time of Passenger's through security
*/
@Override
public double averageWaitTime() {

/**
* Calculates average process time of Passenger's through security
* 
* @return double of average process time of Passenger's through security
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The Simulator class are methods employed by the GUI to allow the simulation
* of the Airport Security to look as it does in the simulation.
* 
*
*/
public class Simulator {

/** Number of Passengers in simulator */
private int numPassengers;

/** Number of Passengers in Security */
private TransitGroup inSecurity;

/** Number of Passengers in Ticketing */
private TransitGroup inTicketing;

/** Reporter log */
private Reporter log;

/** Number of Passengers in simulator */
private Passenger currentPassenger;

/** Initializes myCalendar an EventCalendar object */
private EventCalendar myCalendar;

/**
* Constructor for Simulator class. Requires user input from GUI.
* 
* @param numberOfCheckpoints int number of check points lines in simulation
* @param numberOfPassengers  int number of passengers in simulation
* @param trustPct            int percentage of Trusted Travelers in simulation
* @param fastPct             int percentage of Fast Track Passengers in
*                            simulation
* @param ordPct              int percentage of Ordinary Passengers in
*                            simulation
*/
public Simulator(int numberOfCheckpoints, int numberOfPassengers, int trustPct, int fastPct, int ordPct) {

/**
* Checks parameters of simulation to make sure they fall within guidelines.
* Number of lines is between 3 and 17 inclusive. Percents of each type of
* Passenger adds up to 100.
* 
* @param numLines        int number of security Check Point lines in simulation
* @param percentTrusted  int percentage of Trusted Travelers in simulation
* @param percentFast     int percentage of Fast Track Passengers in simulation
* @param percentOrdinary int percentage of Ordinary Passengers in simulation
* @throws IllegalArgumentException if parameters are invalid
*/
private void checkParameters(int numberOfPassengers, int percentTrusted, int percentFast, int percentOrdinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Getter for the Reporter interface for the Simulation.
* 
* @return Reporter interface for the simulation
*/
public Reporter getReporter() {

/**
* Takes a step in the simulation. One action by the Passengers occurs within
* the Security Area or PreSecurity
*/
public void step() {

 - Throws: IllegalStateException

/**
* Are there any more steps left in the simulation?
* 
* @return boolean true if there are more steps in the simulation, false
*         otherwise
*/
public boolean moreSteps() {

/**
* Gets index of the current Passenger's security line
* 
* @return int index of Passenger's security line
*/
public int getCurrentIndex() {

/**
* Gets color of current Passenger
* 
* @return Color of current Passenger
*/
public Color getCurrentPassengerColor() {

/**
* Has current Passenger cleared(left) security?
* 
* @return boolean true if Passenger has cleared security, false otherwise
*/
public boolean passengerClearedSecurity() {


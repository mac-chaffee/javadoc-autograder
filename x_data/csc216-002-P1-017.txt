---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class that creates the PreSecurity Object
*
*/
public class PreSecurity implements TransitGroup {

private PassengerQueue outsideSecurity;

/**
* constructor for the presecurity object
* @param numPassengers number of passengers that have not entered checkpoint lines
* @param log reporter being used in the construction of the object
*/
public PreSecurity(int numPassengers, Reporter log){

 - Throws: IllegalArgumentException

/**
* returns the depart time of the next passenger from the line
* @return the depart time of the next passenger from the line
*/
public int departTimeNext() {

/**
* returns the next passenger to go
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* returns boolean of whether or not the line has another passenger
* @return boolean if there is a next passenger
*/
public boolean hasNext() {

/**
* returns the passenger being removed next
* @return the passenger being removed
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* class that creates the CheckPoint Object
*
*/
public class CheckPoint {

/** time when checkpoint is available */
public int timeWhenAvailable;

private PassengerQueue line;

/**
* constructor for the CheckPoint object
*/
public CheckPoint() {

/**
* returns the size of the checkpoint
* @return the size of the checkpoint
*/
public int size() {

/**
* returns the passenger being removed from line array
* @return passenger object
*/
public Passenger removeFromLine() {

/**
* returns boolean of whether or not the line has another passenger
* @return boolean if there is a next passenger
*/
public boolean hasNext() {

/**
* returns next depart time
* @return next depart time
*/
public int departTimeNext() {

/**
* returns the next passenger to go
* @return the next passenger to go
*/
public Passenger nextToGo() {

/**
* adds the next passenger to line
* @param p the next passenger to be added
*/
public void addtoLine(Passenger p) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class that creates the SecurityArea Object
*
*/
public class SecurityArea implements TransitGroup {

/** Maximum checkpoints allowed */
public static final int MAX_CHECKPOINTS = 17;

/** Minimum checkpoints allowed */
public static final int MIN_CHECKPOINTS = 3;

/** Error for invalid checkpoint input */
public static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17";

/** Error for improper index of a security line */
public static final String ERROR_INDEX = "Index out of range for this security area";

/** largest Fast track checkpoint index*/
public int largestFastIndex;

/** TSA Pre-check index*/
public int tsaPreIndex;

private ArrayList<CheckPoint> allCheckPoints;

private int numFastTrack;

private int numOrdinary;

/**
* constructor for the security area
* @param numOfCheckpoints user declared number of checkpoints
*/
public SecurityArea(int numOfCheckpoints) {

 - Throws: IllegalArgumentException

/**
* returns whether or not the number of checkpoints is okay
* @param numOfCheckpoints user declared number of checkpoints
* @return true or false whether or not the number of checkpoints is allowed
*/
private boolean numGatesOK(int numOfCheckpoints) {

/**
* adds passenger to line
* @param line the line the passenger is added to
* @param passenger passenger being added to a line
*/
public void addToLine(int line, Passenger passenger) {

/**
* returns the shortest regular line
* @return shortest regular line
*/
public int shortestRegularLine() {

/**
* returns the shortest fast track line
* @return shortest fast track line
*/
public int shortestFastTrackLine() {

/**
* returns the shortest TSA precheck line
* @return shortest TSA precheck line
*/
public int shortestTSAPreLine() {

/**
* returns the length of specified line
* @param line the line for which we are checking
* @return length of line
*/
public int lengthOfLine(int line) {

 - Throws: IllegalArgumentException

/**
* returns the depart time of next passenger from a checkpoint
* @return the depart time of next passenger from a checkpoint
*/
public int departTimeNext() {

/**
* returns the next passenger to go
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* returns the next passenger to act being removed from the line
* @return the passenger being removed from the line
*/
public Passenger removeNext() {

/**
* returns the shortest line in range
* @param line shortest line
* @param range range
* @return the shortest line in range
*/
@SuppressWarnings("unused")
private int shortestLineInRange(int line, int range) {

/**
* returns the line with the next passenger to clear
* @return the line with the next passenger to clear
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* class that creates the fast track passenger object
*
*/
public class FastTrackPassenger extends Passenger {

/** Max process time for the passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* Constructor for the Fast Track Passenger Object
* @param arrivalTime first int used in construction of FTP
* @param processTime second int used in construction of FTP
* @param c Reporter object used in construction of FTP
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter c) {

/**
* returns color of passenger object
* @return Color color
*/
public Color getColor() {

/**
* method that chooses the line the passenger will be in
* @param transitGroup transit group of the passenger
*/
public void getInLine(TransitGroup transitGroup) {

/**
* picks the line that the passenger will be in
* @param transitGroup the transit group of the passenger
* @return the index of the transit value for the passenger
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class that creates the Ordinary passenger object
*
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* Constructor for the Ordinary Passenger Object
* @param arrivalTime arrival Time of the passenger
* @param processTime process time of the passenger
* @param c Reporter object used in logging data of the passenger
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter c) {

/**
* returns color of passenger object
* @return Color color
*/
public Color getColor() {

/**
* method that chooses the line the passenger will be in
* @param transitGroup transit group of the passenger
*/
public void getInLine(TransitGroup transitGroup) {

/**
* picks the line that the passenger will be in
* @param transitGroup the transit group of the passenger
* @return the index of the transit value for the passenger
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Parent Class that creates the basic passenger object
*/
public abstract class Passenger {

/** Max process time for the passenger */
public static final int MIN_PROCESS_TIME = 0;

private int arrivalTime;

private int waitTime;

private int processTime;

private int lineIndex = -1;

private boolean waitingProcessing = false;

private Reporter myLog;

/**
* Constructor for the Fast Track Passenger Object
* @param arrivalTime arrival time of the passenger
* @param processTime process time of the passenger
* @param myLog Reporter object used in construction of Passenger and logging of passenger data
*/
public Passenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* returns arrival time of passenger
* @return arrival time of passenger
*/
public int getArrivalTime() {

/**
* returns wait time of passenger
* @return wait time of passenger
*/
public int getWaitTime() {

/**
* sets wait time of passenger
* @param waitTime wait time of passenger
*/
public void setWaitTime(int waitTime) {

/**
* returns process time of passenger
* @return process time of passenger
*/
public int getProcessTime() {

/**
* returns line index of passenger
* @return line index of passenger
*/
public int getLineIndex() {

/**
* returns boolean of whether or not a passenger is waiting
* @return whether or not the passenger is waiting in a security line
*/
public boolean isWaitingInSecurityLine() {

/**
* clears the passenger of security checks
*/
public void clearSecurity() {

/**
* sets the line index of the passenger
* @param lineIndex line index of the passenger
*/
protected void setLineIndex(int lineIndex) {

/**
* puts the passenger in a waiting line array of other passengers
* @param tG Transit group of the passenger
*/
public abstract void getInLine(TransitGroup tG);

/**
* returns the color of the passenger object
* @return Color color of the passenger
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* class that creates the TrustedTraveler object
*
*/
public class TrustedTraveler extends Passenger {

/** Max process time for the passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Constructor for the Trusted Traveler Object
* @param arrivalTime first int used in construction of TT
* @param b second int used in construction of TT
* @param c Reporter object used in construction of TT
*/
public TrustedTraveler(int arrivalTime, int b, Reporter c) {

/**
* returns color of passenger object
* @return Color color
*/
public Color getColor() {

/**
* method that chooses the line the passenger will be in
* @param transitGroup transit group of the passenger
*/
public void getInLine(TransitGroup transitGroup) {

/**
* picks the line that the passenger will be in
* @param transitGroup the transit group of the passenger
* @return the index of the transit value for the passenger
*/
private int pickLine(TransitGroup transitGroup) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class that creates the EventCalendar Object
*
*/
public class EventCalendar {

private TransitGroup highPriority;

private TransitGroup lowPriority;

/**
* constructor for the EventCalendar object
* @param highPriority first TransitGroup for event calendar
* @param lowPriority second TransitGroup for the event calendar
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* returns the next passenger to act
* @return the next passenger to act
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class that creates a log for the simulation
*
*/
public class Log implements Reporter {

private int numCompleted;

private int totalWaitTime;

private int totalProcessTime;

/**
* constructor for the log object
*/
public Log() {

/**
* returns the number of checkpoints completed
* @return number of checkpoints completed
*/
public int getNumCompleted() {

/**
* logs data of passenger
* @param p passenger object being logged
*/
public void logData(Passenger p) {

 - Throws: IllegalArgumentException

/**
* returns the average wait time
* @return average wait time
*/
public double averageWaitTime() {

/**
* returns the average process time
* @return average process time
*/
public double averageProcessTime () {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* class that creates the simulator object
*
*/
public class Simulator {

private int numberOfCheckpoints;

private int numPassengers;

private int trustPct;

private int fastPct;

@SuppressWarnings("unused")
private int ordPct;

private Passenger currentPassenger = null;

private Reporter log;

private EventCalendar calendar;

private TransitGroup inSecurity;

private TransitGroup inTicketing;

/**
* constructor for the simulator object
* @param numberOfCheckpoints number of checkpoints user declared
* @param numberOfPassengers number of passengers user declared
* @param trustPct percent of TrustedTravelerPassengers user declared
* @param fastPct percent of FastTrackPassengers user declared
* @param ordPct percent of OrdinaryPassengers user declared
*/
public Simulator(int numberOfCheckpoints, int numberOfPassengers, int trustPct, int fastPct, int ordPct) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* returns current index
* @return current index
*/
public int getCurrentIndex() {

/**
* returns color of the current passenger
* @return color of passenger
*/
public Color getCurrentPassengerColor() {

/**
* returns reporter object
* @return reporter object
*/
public Reporter getReporter() {

/**
* returns boolean of whether there are more steps or not in the simulation
* @return true/false of more steps or not
*/
public boolean moreSteps() {

/**
* returns whether or not a passenger cleared security
* @return boolean of clearance of passenger
*/
public boolean passengerClearedSecurity() {

/**
* steps forward into the simulation
*/
public void step() {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*
*/
public class Command {

public static final String CC_DUPLICATE = null;

public static final String CC_UNNECESSARY = null;

public static final String CC_NOT_AN_INCIDENT = null;

public static final String RC_PERMANENTLY_SOLVED = null;

public static final String RC_WORKAROUND = null;

public static final String RC_NOT_SOLVED = null;

public static final String RC_CALLER_CLOSED = null;

public static final String OH_CALLER = null;

public static final String OH_CHANGE = null;

public static final String OH_VENDOR = null;

private String ownerId;

private String note;

private CommandValue c;

private CancellationCode cancellationCode;

private ResolutionCode resolutionCode;

private OnHoldReason onHoldReason;

public Command(CommandValue reopen, String object, OnHoldReason object2, ResolutionCode object3, CancellationCode object4, String note) {

public CommandValue getCommand() {

/**
* @return the ownerId
*/
public String getOwnerId() {

/**
* @return the note
*/
public String getWorkNote() {

public ResolutionCode getResolutionCode() {

public OnHoldReason getOnHoldReason() {

public CancellationCode getCancellationCode() {

/**
*
*/
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/**
*
*/
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/**
*
*/
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT }
/**
*
*/
public enum CommandValue { RESOLVE, REOPEN, CANCEL, HOLD, CONFIRM, INVESTIGATE }
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*
*/
public class ManagedIncident {

public static final String C_INQUIRY = null;

public static final String C_SOFTWARE = null;

public static final String C_HARDWARE = null;

public static final String C_NETWORK = null;

public static final String C_DATABASE = null;

public static final String ON_HOLD_NAME = null;

public static final String NEW_NAME = null;

public static final String IN_PROGRESS_NAME = null;

public static final String RESOLVED_NAME = null;

public static final String CLOSED_NAME = null;

public static final String CANCELED_NAME = null;

public static final String P_URGENT = null;

public static final String P_HIGH = null;

public static final String P_MEDIUM = null;

public static final String P_LOW = null;

private int incidentId;

private String caller;

private String owner;

private String name;

private String changeRequest;

private ArrayList<String> notes;

private static int counter = 0;

private Priority priority;

private CancellationCode cancellationCode;

private ResolutionCode resolutionCode;

private OnHoldReason onHoldReason;

private Category category;

private IncidentState state;

private IncidentState newState;

private IncidentState onHoldState;

private IncidentState closedState;

private IncidentState canceledState;

private IncidentState inProgressState;

private IncidentState resolvedState;

public ManagedIncident(String caller, Category category, Priority priority, String name,
public ManagedIncident(Incident incident) {

public static void incrementCounter() {

public int getIncidentId() {

public String getCaller() {

public Category getCategory() {

public String getCategoryString() {

private void setCategory(String cat) {

public IncidentState getState() {

private void setState(String state) {

public String getPriorityString() {

private void setPriority(String priority) {

public String getOwner() {

public String getName() {

public String getOnHoldReasonString() {

private void setOnHoldReason(String reason) {

public String getChangeRequest() {

public ResolutionCode getResolutionCode() {

public String getResolutionCodeString() {

private void setResolutionCode(String code) {

public String getCancellationCodeString() {

private void setCancellationCode(String code) {

public String getNotesString() {

public void update(Command command) {

public Incident getXMLIncident() {

public static void setCounter(int counter) {

/**
*
*/
public enum Priority {

/**
*
*/
public enum Category {

private class InProgressState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

private class CanceledState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

private class ClosedState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

private class ResolvedState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

private class NewState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

private class OnHoldState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*
*/
public class IncidentManager {

private ManagedIncidentList incidentList;

private IncidentManager singleton;

private IncidentManager() {

public void saveManagedIncidentsToFile(String fileName) {

public static IncidentManager getInstance() {

public void deleteManagedIncidentById(int incidentId) {

public ManagedIncident getManagedIncidentById(int incident) {

public String[][] getManagedIncidentsAsArray() {

public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

public void executeCommand(int incidentId, Command c) {

public void addManagedIncidentToList(String caller, Category category, Priority priority, String name,
public void loadManagedIncidentsFromFile(String fileName) {

public void createNewManagedIncidentList() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*
*/
public class ManagedIncidentList {

private ArrayList<ManagedIncident> incidents;

public int addIncident(String caller, Category category, Priority priority, String name,
public void addXMLIncidents(List<Incident> list) {

public List<ManagedIncident> getManagedIncidents() {

public List<ManagedIncident> getIncidentsByCategory(Category cat) {

public ManagedIncident getIncidentById(int id) {

public void executeCommand(int value, Command command) {

public void deleteIncidentById(int id) {


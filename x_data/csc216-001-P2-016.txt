---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates objects that encapsulate user actions (or transitions) that cause the state of a ManagedIncident to update. 
* Concrete States in the ManagedIncident class rely heavily on this class.
* 
*/
public class Command {

/** Command type specified by the user */
public enum CommandValue {

/**
* Investigate an incident, which will result in a transition to the in progress state.
*/
/** Put an incident on hold */
/** Resolve an incident */
/** Confirm the resolution of an incident, which will result in a transition to the closed state */
/** Reopen an incident */
/** Cancel an incident */
/**
* Predetermined constant on hold reason values.
*/
public enum OnHoldReason {

/**
* String saying that the incident is waiting for a caller to be investigated
*/
/** String saying that the incident is waiting for a change */
/**
* String describing that the incident is waiting for the vendor to be
* investigated
*/
/**
* Predetermined constant resolution code values.
*/
public enum ResolutionCode {

/** String signifying that the incident is permanently solved */
/** Means the incident was resolved using a workaround - not permanent fix */
/** String meaning that the incident was not solved */
/** String meaning that the caller has decided to close the incident */
/** 
* Predetermined constant cancellation code values.
*/
public enum CancellationCode {

/** String to assign if the incident is a duplicate */
/** String to assign if the incident is unnecessarily reported */
/** String to assign if the incident shouldn't be considered an incident */
/** Awaiting Caller string to be displayed in the drop down list of on hold reasons */
public static final String OH_CALLER = "Awaiting Caller";

/** Awaiting Change string to be displayed in the drop down list of on hold reasons */
public static final String OH_CHANGE = "Awaiting Change";

/** Awaiting Vendor string to be displayed in the drop down list of on hold reasons */
public static final String OH_VENDOR = "Awaiting Vendor";

/** Permanently solved string to be displayed in the drop down list of resolution codes */
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** Workaround string to be displayed in the drop down list of resolution codes */
public static final String RC_WORKAROUND = "Workaround";

/** Not solved string to be displayed in the drop down list of resolution codes */
public static final String RC_NOT_SOLVED = "Not Solved";

/** Caller closed string to be displayed in the drop down list of resolution codes */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** Duplicate string to be displayed in the drop down list of resolution codes */
public static final String CC_DUPLICATE = "Duplicate";

/** Unnecessary string to be displayed in the drop down list of resolution codes */
public static final String CC_UNNECESSARY = "Unnecessary";

/** Not an incident string to be displayed in the drop down list of resolution codes */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** Id of the user who will investigate the incident */
private String ownerId;

/** Notes written by the person making the command, not the caller */
private String note;

/** Cancellation code that the client passes in to be assigned */
private CancellationCode cancellationCode;

/** OnHoldCode that the client passes in to be assigned */
private OnHoldReason onHoldReason;

/** Resolution code that the client passes in to be assigned */
private ResolutionCode resolutionCode;

/** Command value that the client passes in to be assigned */
private CommandValue c;

/** 
* Constructs a Command which represents how the user can interact with an incident. 
* Not all parameters are required for each CommandValue. Unneeded values should be passed as null or ignored
* when the Command is used by the ManagedIncidentFSM.
* 
* @param c the Command Value that will interact with the predetermined enumerations
* @param ownerId the id of the investigating user, if there is one
* @param onHoldReason reason why the incident is/was put on hold, will interact with the appropriate enumeration
* @param resolutionCode reason why the incident is/was resolved, will interact with the appropriate enumeration
* @param cancellationCode reason why the incident is being canceled, will interact with the appropriate enumeration
* @param note follow up info entered by the owner
* @throws IllegalArgumentException when parameter values are invalid 
*/
public Command(CommandValue c, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode, CancellationCode cancellationCode, String note) {

/**
* Checks that the parameters passed through the Command constructor are valid
* before assigning variables.
* 
* @param c                the command type - button press. What action we want
*                         to apply to this incident
* @param ownerId          the id of the person who has applied the command
* @param onHoldReason     the reason why the incident is put on hold if the
*                         command value is CommandValue.HOLD
* @param resolutionCode   the reason why the incident is resolved if the
*                         command value is CommandValue.RESOLVE
* @param cancellationCode the reason why the incident is cancelled if the
*                         command value is CommandValue.CANCEL
* @param note             follow up work note entered by the owner of the incident
* @throws IllegalArgumentException if c is null, note is null or empty, or if a
*                                  command of a certain type has a null reason
*/
private void checkParameters(CommandValue c, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode, CancellationCode cancellationCode, String note) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Get the Command Value for this particular instance of interaction.
* 
* @return either investigate, on hold, resolved, confirm, cancel, or reopen
*/
public CommandValue getCommand() {

/**
* Get the id of the user who is performing this command.
* 
* @return the owner's unity id
*/
public String getOwnerId() {

/**
* Get the resolution code for an incident.
* 
* @return the resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Get the work note (written by owner) of this incident.
* 
* @return the note
*/
public String getWorkNote() {

/**
* Get the on hold reason for the incident.
* 
* @return the on hold reason
*/
public OnHoldReason getOnHoldReason() {

/**
* Get the cancellation reason for the incident.
* 
* @return the cancellation code
*/
public CancellationCode getCancellationCode() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Tracks an incident as well as an incident's fields, behaviors, and state. Contains the functionality to transition between the states.
* Keeps track of everything specific to one particular incident, except for the counter field which is shared for all ManagedIncidents
* until the setCounter is reset.
* 
*/
public class ManagedIncident {

/** The State string that is displayed in the incident list when the an incident is newly created */
public static final String NEW_NAME = "New";

/** The State string that is displayed in the incident list when the an incident is inProgress */
public static final String IN_PROGRESS_NAME = "In Progress";

/** The State string that is displayed in the incident list when the an incident is put On Hold */
public static final String ON_HOLD_NAME = "On Hold";

/** The State string that is displayed in the incident list when the an incident is Resolved */
public static final String RESOLVED_NAME = "Resolved";

/** The State string that is displayed in the incident list when the an incident is Closed */
public static final String CLOSED_NAME = "Closed";

/** The State string that is displayed in the incident list when the an incident is Canceled */
public static final String CANCELED_NAME = "Canceled";

/**
* Represents a newly made incident, its name and ability to change state. 
* 
*/
public class NewState implements IncidentState {

/**
* Updates the state of the new incident. If the command value is anything besides investigate or cancel then an exception is thrown.
* When changing state, the code should also be updated from the respective type.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state which is provided in the class constants.
* @return the state name in string form
*/
@Override
public String getStateName() {

/**
* Represents an in progress incident, its name and ability to change state. 
* 
*/
public class InProgressState implements IncidentState {

/**
* Updates the state of the incident. Incidents in progress can only transition to on hold, resolved, and canceled.
* Otherwise, an exception is thrown.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state which is provided in the class constants.
* @return the state name in string form
*/
@Override
public String getStateName() {

/**
* Represents an incident on hold, it's name and ability to change state. 
* 
*/
public class OnHoldState implements IncidentState {

/**
* Updates the state of the incident. Incidents on hold only have the ability to transition to reopened, resolved, and cancelled.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state which is provided in the class constants.
* @return the state name in string form
*/
@Override
public String getStateName() {

/**
* Represents a resolved incident, it's name and ability to change state. 
* 
*/
public class ResolvedState implements IncidentState {

/**
* Updates the state of the incident. Valid state transitions are reopen, put on hold, cancel, and closed.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state which is provided in the class constants.
* @return the state name in string form
*/
@Override
public String getStateName() {

/**
* Represents a closed incident, it's name and ability to change state. 
* 
*/
public class ClosedState implements IncidentState {

/**
* Updates the state of the incident.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state which is provided in the class constants.
* @return the state name
*/
@Override
public String getStateName() {

/**
* Represents a canceled incident, it's name and ability to change state. 
* 
*/
public class CanceledState implements IncidentState {

/**
* Updates the state of the incident.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state which is provided in the class constants.
* @return the state name
*/
@Override
public String getStateName() {

/** All of the possible Category types for an incident */
public enum Category {

/** Inquiry category */
/** Software category */
/** Hardware category */
/** Network category */
/** Database category */
/** All of the possible Priority levels for an incident */
public enum Priority {

/** Urgent priority */
/** High priority */
/** Medium priority */
/** Low priority */
/** The Category string that is displayed in the incident list when the Category is Inquiry */
public static final String C_INQUIRY = "Inquiry";

/** The Category string that is displayed in the incident list when the Category is Software */
public static final String C_SOFTWARE = "Software";

/** The Category string that is displayed in the incident list when the Category is Hardware */
public static final String C_HARDWARE = "Hardware";

/** The Category string that is displayed in the incident list when the Category is Network */
public static final String C_NETWORK = "Network";

/** The Category string that is displayed in the incident list when the Category is Database */
public static final String C_DATABASE = "Database";

/** The Priority string that is displayed in the incident list when the Priority is Urgent */
public static final String P_URGENT = "Urgent";

/** The Priority string that is displayed in the incident list when the Priority is High */
public static final String P_HIGH = "High";

/** The Priority string that is displayed in the incident list when the Priority is Medium */
public static final String P_MEDIUM = "Medium";

/** The Priority string that is displayed in the incident list when the Priority is Low */
public static final String P_LOW = "Low";

/** Unique id for an incident */
private int incidentId;

/** User id of person who reported the incident */
private String caller;

/**
* Category of the incident. One of the values in the Category Enumeration class
*/
private Category category;

/** Current state for the incident. Uses the IncidentState interface. */
private IncidentState state;

/**
* The priority of the incident. One of the values in the Priority Enumeration
* class
*/
private Priority priority;

/** User id of the incident owner or null if there is not an assigned owner */
private String owner;

/** Incident's name information from when the incident is created. */
private String name;

/**
* The onHoldReason for the incident. One of the values from the OnHoldReason
* Enumeration or null if the incident isn't in the HoldState
*/
private OnHoldReason onHoldReason;

/**
* Change request information for the incident. Can be null if no change request
* has been made.
*/
private String changeRequest;

/**
* Resolution code for the incident. One of the values from ResolutionCode
* Enumeration or null if the incident isn't the ResolvedState or ClosedState
*/
private ResolutionCode resolutionCode;

/**
* Cancellation code for the incident. One of the values from CancellationCode
* or null if the incident isn't in the CanceledState
*/
private CancellationCode cancellationCode;

/** An ArrayList of incident notes */
private ArrayList<String> notes;

/**
* A static field that keeps track of the id value that should be given to the
* next ManagedIncident created
*/
private static int counter = 0;

/**
* Final instance of the NewState inner class.
*/
private final NewState newState = new NewState();

/**
* Final instance of the InProgressState inner class.
*/
private final InProgressState inProgressState = new InProgressState();

/**
* Final instance of the OnHoldState inner class.
*/
private final OnHoldState onHoldState = new OnHoldState();

/**
* Final instance of the ResolvedState inner class.
*/
private final ResolvedState resolvedState = new ResolvedState();

/**
* Final instance of the ClosedState inner class.
*/
private final ClosedState closedState = new ClosedState();

/**
* Final instance of the CanceledState inner class.
*/
private final CanceledState canceledState = new CanceledState();

/**
* Constructs a ManagedIncident from the provided parameters. If any of the
* parameters are null or empty strings, they are considered invalid.
* 
* @param caller the user who created the incident 
* @param category describes what type of IT problem the incident pertains to
* @param priority how severe the the problem is and how quickly it needs to be addressed
* @param name the name of the incident
* @param workNote notes about the incidents
* @throws IllegalArgumentException for invalid parameters
*/
public ManagedIncident(String caller, Category category, Priority priority, String name, String workNote) {

/**
* Checks that the fields passed into the constructor are valid.
* 
* @param caller the person who reported the incident
* @param category the type of incident 
* @param priority the urgency level of the incident
* @param name the name of the incident
* @param workNote any relevant notes about the incident
* @throws IllegalArgumentException if any of the fields are null or empty strings (if strings)
*/
private void checkParameters(String caller, Category category, Priority priority, String name, String workNote) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Checks that the fields passed into the constructor are valid.
* @param caller the person who reported the incident
* @param category the type of incident 
* @param priority the urgency level of the incident
* @param name the name of the incident
* @param workNote any relevant notes about the incident in the form of an ArrayList<String>
* @throws IllegalArgumentException if any of the fields are null or empty strings (if strings)
*/
private void checkParameters(String caller, String name, ArrayList<String> workNote) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* The fields of the ManagedIncident are set to the values from the Incident.
* You might find it useful to create private helper methods to transform the
* string category, state, priority, onHoldReason, resolutionCode, and
* cancellationCode from Incident into the object or enumeration types needed
* for a ManagedIncident. If the string cannot be converted to an appropriate
* object or enumeration type, then an IllegalArgumentException should be thrown
* for the required values. Remember that onHoldReason, resolutionCode, and
* cancellationCode can be null.
* 
* @param i the incident object we are using to pull field data from
* @throws IllegalArgumentException if the string cannot be converted to an
*                                  appropriate object or enumeration type
*/
public ManagedIncident(Incident i) {

/**
* Transforms the category string to a category enum.
* 
* @param c the string representing the category enumeration we want to determine our Category enum from
* @return c in enum form
*/
private Category convertCategory(String c) {

 - Throws: IllegalArgumentException

/**
* Transforms the priority string to a priority enum.
* 
* @param p the string representing the priory enumeration we want to determine our Priority enum from
* @return p in enum form
*/
private Priority convertPriority(String p) {

 - Throws: IllegalArgumentException

/**
* Transforms the enumeration to string form.
* 
* @param h on hold reason enum to transform
* @return the string version of the enum
*/
private String convertOnHoldReasonToString(OnHoldReason h) {

 - Throws: IllegalArgumentException

/**
* Transforms the resolution enumeration to string form.
* 
* @param r the enum to use to find the string version of itself
* @return the string version of the resolution code enum
*/
private String convertResolutionCodeToString(ResolutionCode r) {

 - Throws: IllegalArgumentException

/**
* Transforms the cancellation code to string form.
* 
* @param c the cancellation code enum to use to find the string version of itself
* @return the string version of the cancellation code enum
*/
private String convertCancelationCodeToString(CancellationCode c) {

 - Throws: IllegalArgumentException

/**
* Increments the counter for each incident that is created/reported.
*/
public static void incrementCounter() {

/**
* Collects the ID of the incident for use.
* 
* @return the incidentID
*/
public int getIncidentId() {

/**
* Collects the field changeRequest variable.
* 
* @return changeRequest value
*/
public String getChangeRequest() {

/**
* Collects the category of the incident.
* 
* @return category value
*/
public Category getCategory() {

/**
* Collects the category in string form (for xml use when loading from file).
* 
* @return the string form of the category
*/
public String getCategoryString() {

/**
* Sets the category field by making sure it is valid.
* 
* @param category the what IT department the incident pertains to
*/
private void setCategory(String category) {

 - Throws: IllegalArgumentException

/**
* Collects the priority type in string form.
* 
* @return the string form of the priority
*/
public String getPriorityString() {

/**
* Sets the priority field by making sure it is valid. 
* 
* @param priority of the incident
*/
private void setPriority(String priority) {

 - Throws: IllegalArgumentException

/**
* Collects the onHold reason in string form for an incident in the onHold state.
* 
* @return the onHold reason
*/
public String getOnHoldReasonString() {

/**
* Sets the onHoldReason field.
*/
private void setOnHoldReason(String code) {

 - Throws: IllegalArgumentException

/**
* Collects the code for an incident in the canceled state.
* 
* @return the cancellation code in string form.
*/
public String getCancellationCodeString() {

/**
* Sets the cancellation code by making sure it is valid.
* 
* @param code the code to set
*/
private void setCancellationCode(String code) {

 - Throws: IllegalArgumentException

/**
* Collect the current state of the incident.
* 
* @return the State object representing the incident's state
*/
public IncidentState getState() {

/**
* Sets the state based on the string determined by the state that was transitioned to.
* 
* @param s the state string name
*/
private void setState(String s) {

 - Throws: IllegalArgumentException

/**
* Collects the resolution code of type ResolutionCode.
* 
* @return the resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Collects the resolutionCode in string form. 
* 
* @return the string form of the resolution code
*/
public String getResolutionCodeString() {

/**
* Sets the resolutionCode to the string after converting it to the right type.
* 
* @param code the code to set 
*/
private void setResolutionCode(String code) {

 - Throws: IllegalArgumentException

/**
* Collects the owner's id and null if an owner has not been assigned to the incident. 
* 
* @return the unity id of the owner of the incident
*/
public String getOwner() {

/**
* Collects the name of the incident. 
* 
* @return the incident's name
*/
public String getName() {

/**
* Collect the unity id of the person who reported the incident.
* 
* @return the caller's id
*/
public String getCaller() {

/**
* Collects the notes for an incident in an ArrayList.
* 
* @return a collection of incident notes
*/
public ArrayList<String> getNotes() {

/**
* Convert notes in string form to be used for the GUI.
* 
* @return the notes in string form
*/
public String getNotesString() {

/** 
* Drives the FSM by delegating the Command to the current state and if successful adding non-null notes to the notes list. 
* 
* @param c Command object 
* @throws UnsupportedOperationException if the current state determines that the current transition is not appropriate for the FSM
*/
public void update(Command c) {

/**
* Parses the xml file and returns the current incident.
* 
* @return an incident from the load file
*/
public Incident getXMLIncident() {

/**
* Sets the counter that will be used to determine incidentId.
* 
* @param updatedCount resets the counter for when a new ManagedIncidentList is created or incidents are loaded from an XML file
*/
public static void setCounter(int updatedCount) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Implements the Singleton Design Pattern. This means only one instance of the IncidentManager can ever be created 
* Controls the creation and modification of (potentially) many ManagedIncidentsLists.
* Catch and throw a new IllegalArgumentException when an IncidentIOException is thrown from the IO classes in IncidentXML.jar
* 
*/
public class IncidentManager {

/**The list of managed Incidents created by ManagedIncidentList class*/
private ManagedIncidentList incidentList;

/** Single instance */
private static IncidentManager singleton;

/**
* Constructs a single instance.
*/
private IncidentManager() {

/**
* Gets an instance of IncidentManager.
* 
* @return an IncidentManager object
*/
public static IncidentManager getInstance() {

/**
* Saves Incidents to an xml file.
* 
* @param fileName the file to save to
*/
public void saveManagedIncidentsToFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Loads Incidents from an xml file (and adds them to a list?)
* 
* @param fileName the file to load incidents from
*/
public void loadManagedIncidentsFromFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Creates a new ManagnedIncidentList. This makes the count in ManagedIncidentList restart to 0.
*/
public void createNewManagedIncidentList() {

/**
* Creates a 2D array of all incidents in the list with 1 row for each incident.
* Each column contains the ID number, Category, State name, Priority as a String, and name.
* For methods similar to managedincident methods, just make the managedincidentlist use it's method for doing the same thing.
* @return a 2D Array of multiple Incident's information
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Creates a 2D array of all incidents in the list with 1 row for each incident. Specialized for 
* incident's of a specific category. 
* Each column contains the ID number, Category, State name, Priority as a String, and name.
* 
* @param c Desired category 
* @return a 2D Array of multiple Incident information
* @throws IllegalArgumentException if the Category passed is null
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category c) {

/**
* Gets an incident by looking for the incident with the corresponding ID.
* 
* @param incidentId the incident with the ID to look for
* @return the desired incident 
*/
public ManagedIncident getManagedIncidentById(int incidentId) {

/**
* Applies a command to an incident.
* 
* @param num probably incidentId, the number of the id of the incident we want to run a command on
* @param c the command to perform of the desired incident
*/
public void executeCommand(int num, Command c) {

/**
* Removes the incident with the passed ID.
* 
* @param incidentId the ID of the incident to remove
*/
public void deleteManagedIncidentById(int incidentId) {

/**
* Manually add the incident by creating a ManagedIncident with the passed fields.
* 
* @param caller the person who reported the incident
* @param c category of the ManagedIncident
* @param p urgency level of the ManagedIncident
* @param name of the ManagedIncident
* @param workNote of the ManagedIncident recorded when it was reported
*/
public void addManagedIncidentToList(String caller, Category c, Priority p, String name, String workNote) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Maintains a List of ManagedIncidents (ArrayList). This class can
* add a ManagedIncident to the list, add a list of Incidents from the XML library to the list
* remove a ManagedIncident from the list, search and get a ManagedIncident in the list by id,
* update a ManagedIncident in the list through Command functionality, and return the entire list
* or sublist of itself(e.g. list of ManagedIncidents filtered by Category).
* 
*/
public class ManagedIncidentList {

/** List that maintains ManagedIncidents in the application*/
private ArrayList<ManagedIncident> incidentsList;

/**
* Creates a ManagedIncidentList.
* Reset the ManagedIncident's counter to 0 when a new MIL is created.
* When creating a MIL from an XML file, set the managedIncident's counter to the maxId in the list of XML incidents plus 1.
*/
public ManagedIncidentList() {

/**
* Adds an incident to the list using the given parameters.
* @param caller the person who reported the incident 
* @param c department type of the incident
* @param p urgency level of the incident
* @param name of the incident
* @param workNote notes relevant to the incident made at the time of the report
* @return the id of the new incident
*/
public int addIncident(String caller, Category c, Priority p, String name, String workNote) {

/**
* Add a list of incidents collected from an XML file and adds it to the list field. 
* 
* @param l the list of incident grabbed from the loaded XML file
*/
public void addXMLIncidents(List<Incident> l) {

/**
* Grabs the list field.
* 
* @return a list of all managed incidents
*/
public List<ManagedIncident> getManagedIncidents() {

/**
* Collects a list of incidents organized by category.
* 
* @param c desired incident category that we want to display
* @return a list of managed incidents based on the category type
*/
public List<ManagedIncident> getIncidentsByCategory(Category c) {

 - Throws: IllegalArgumentException

/**
* Grabs an incident from the list that has an id that matches the one passed through the parameter.
* Return NULL if the passed ID is does not exist in the list. 
* @param incidentId the id to search for
* @return the desired managedIncident
*/
public ManagedIncident getIncidentById(int incidentId) {

/**
* Executes a command.
* 
* @param incidentId of the managedIncident we want to run a command on
* @param c command that we want to run on the incident
*/
public void executeCommand(int incidentId, Command c) {

/**
* Removes a particular incident from the list field.
* 
* @param incidentId ID of the incident to remove from the list
*/
public void deleteIncidentById(int incidentId) {


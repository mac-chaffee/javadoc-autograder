---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* 
* Command class holds the information about a user command that would lead to a transition.
* Causes the state of ManagedIncident to update. 
* Contains four inner enumerations.
* 
*
*/
public class Command {

/** Represents one of the six possible commands that a user can make. */
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/** Represents one of the three possible on hold codes. */
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/** Represents one of the four possible resolution codes. */
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/** Represents one of the four possible cancellation codes. */
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT }
/** String value for CancelationCode enum duplicate */
public static final String CC_DUPLICATE = "Duplicate";

/** String value for CancelationCode enum not an incident */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** String value for CancelationCode enum unnecessary */
public static final String CC_UNNECESSARY = "Unnecessary";

/** String value for OnHoldReason enum awaiting caller */
public static final String OH_CALLER = "Awaiting Caller";

/** String value for OnHoldReason enum awaiting change */
public static final String OH_CHANGE = "Awaiting Change";

/** String value for OnHoldReason enum awaiting vendor */
public static final String OH_VENDOR = "Awaiting Vendor";

/** String value for ResolutionCode enum caller closed */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** String value for ResolutionCode enum not solved */
public static final String RC_NOT_SOLVED = "Not Solved";

/** String value for ResolutionCode enum permanently solved*/
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** String value for ResolutionCode enum workaround */
public static final String RC_WORKAROUND = "Workaround";

/** User ID of the current owner of the command */
private String ownerId;

/** Note added to the incident by the user */
private String note;

/** Field containing current value of CancellationCode enumeration */
private CancellationCode cancellationCode;

/** Field containing current value of ResolutionCode enumeration */
private ResolutionCode resolutionCode;

/** Field containing current value of OnHoldReason enumeration */
private OnHoldReason onHoldReason;

/** Field containing current value of CommandValue enumeration */
private CommandValue c;

/**
* Construct a command object
* Used by ManagedIncident to transition between states.
* 
* @param c Command Value - must be present
* @param ownerId Owner ID of current command owner or null
* @param onHoldReason On Hold Reason for the command
* @param resolutionCode Resolution Code for the command
* @param cancellationCode Cancelation Code for the command
* @param note current note being added to the command
*/
public Command(CommandValue c, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode,
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Get the ID of the incident owner
* 
* @return ownerId current owner of the command
*/
public String getOwnerId() {

/**
* Get the value of the current work note.
* 
* @return the note
*/
public String getWorkNote() {

/**
* Get the cancellation code for the command.
* 
* @return the cancellationCode
*/
public CancellationCode getCancellationCode() {

/**
* Get the resolution code for the command.
* 
* @return the resolutionCode
*/
public ResolutionCode getResolutionCode() {

/**
* Get the onhold reason for the command.
* 
* @return the onHoldReason
*/
public OnHoldReason getOnHoldReason() {

/**
* Get the current Command.
* 
* @return the c the current Command from the user.
*/
public CommandValue getCommand() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* ManagedIncident is the context for the Finite State Machine(FSM).
* ManagedIncident represents an incident that is being tracked through the system.
* Contains 6 inner states that represent the 6 possible states of the FSM.
* 
*
*/
public class ManagedIncident {

/** Represents one of the five possible categories of incident. */
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/** Represents one of the four possible priorities of incident. */
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
/** String value for Category enum database */
public static final String C_DATABASE = "Database";

/** String value for Category enum hardware */
public static final String C_HARDWARE = "Hardware";

/** String value for Category enum inquiry */
public static final String C_INQUIRY = "Inquiry";

/** String value for Category enum network */
public static final String C_NETWORK = "Network";

/** String value for Category enum software */
public static final String C_SOFTWARE = "Software";

/** String value for canceled name */
public static final String CANCELED_NAME = "Canceled";

/** String value for closed name */
public static final String CLOSED_NAME = "Closed";

/** String value for in progress name */
public static final String IN_PROGRESS_NAME = "In Progress";

/** String value for new name */
public static final String NEW_NAME = "New";

/** String value for on hold name */
public static final String ON_HOLD_NAME = "On Hold";

/** String value for resolved name */
public static final String RESOLVED_NAME = "Resolved";

/** String value for Priority enum high */
public static final String P_HIGH = "High";

/** String value for Priority enum low */
public static final String P_LOW = "Low";

/** String value for Priority enum medium */
public static final String P_MEDIUM = "Medium";

/** String value for Priority enum urgent */
public static final String P_URGENT = "Urgent";

/** Unique id for an incident. */
private int incidentId;

/** User id of person who reported the incident. */
private String caller;

/** User id of the incident owner or null if there is not an assigned owner. */
private String owner;

/** Incidents name information from when the incident is created. */
private String name;

/** Change request information for the incident. */
private String changeRequest;

/** An ArrayList of notes. */
private ArrayList<String> notes = new ArrayList<String>();

/** Keeps track of the id value that should be given to the next ManagedIncident created*/
private static int counter = 0;

/** Current state for the incident of type IncidentState. */
private IncidentState state;

/** Instance of the NewState inner class */
private final IncidentState onHoldState = new OnHoldState();

/** Instance of the NewState inner class */
private final IncidentState resolvedState = new ResolvedState();

/** Instance of the NewState inner class */
private final IncidentState newState = new NewState();

/** Instance of the NewState inner class */
private final IncidentState closedState = new ClosedState();

/** Instance of the NewState inner class */
private final IncidentState inProgressState = new InProgressState();

/** Instance of the NewState inner class */
private final IncidentState canceledState = new CanceledState();

/** Cancellation code for the incident.  */
private CancellationCode cancellationCode;

/**  Resolution code for the incident. */
private ResolutionCode resolutionCode;

/** On hold reason for the incident. */
private OnHoldReason onHoldReason;

/** Priority of the incident. One of the 4 Priority values. */
private Priority priority;

/** Category of the incident. One of the 5 Category values. */ 
private Category category;

/**
* Constructs a ManagedIncident.
* All parameters must be populated or an IllegalArgumentException is thrown.
* All fields are set by the parameters with the exception of counter which is 
* set to ManagedIncident.counter.
* 
* @param caller User id of the incident owner 
* @param name Incidents name information
* @param notes Worknotes
* @param priority Priority of the incident
* @param category Category of the incident
*/
public ManagedIncident(String caller, Category category, Priority priority, 
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Construct a ManagedIncident from an Incident in the XML file.
* 
* @param incident from the XML file.
*/
public ManagedIncident(Incident incident) {

/**
* Set the counter to use as the incidentId
*
* @param counter the counter to set
*/
public static void setCounter(int counter) {

/**
* Static method to increment counter to be used as Incident ID.
*/
public static void incrementCounter() {

/**
* Delegate the command to the current state. Update notes.
* Throws an UnsupportedOperationException if the current state determines 
* that the current transition is not appropriate for the FSM.
* 
* @param command received from the GUI
*/
public void update(Command command) throws UnsupportedOperationException {

 - Throws: UnsupportedOperationException

/**
* Generate an Incident for writing to XML file.
* 
* @return incident in XML form
*/
public Incident getXMLIncident() {

/**
* Get the User ID of person who reported incident.
* 
* @return caller User ID of person who originated incident
*/
public String getCaller() {

/**
* Get the cancellation code as a string.
* 
* @return cancellationCode String cancellation code
*/
public String getCancellationCodeString() {

/**
* Get the category as a string.
* 
* @return category the category for the incident
*/
public String getCategoryString() {

/**
* Get the Change request information for the incident.
* 
* @return changeRequest String change request
*/
public String getChangeRequest() {

/**
* Get the Unique id for an incident.
* 
* @return incidentId incident ID
*/
public int getIncidentId() {

/**
* Get Incidents name information
* 
* @return name String incident name
*/
public String getName() {

/**
* Get the incident notes as a String.
* 
* @return notes incident notes
*/
public String getNotesString() {

/**
* Get the on hold reason as a String.
* 
* @return onHoldReason for incident
*/
public String getOnHoldReasonString() {

/**
* Get the owner of the incident.
* 
* @return owner of the incident
*/
public String getOwner() {

/**
* Get the incident priority.
* 
* @return priority for the incident.
*/
public String getPriorityString() {

/**
* Get the resolution code as a String
* 
* @return resolutionCode for the incident
*/
public String getResolutionCodeString() {

/**
* Get the current incident state.
* 
* @return state the current state of the incident
*/
public IncidentState getState() {

/**
* Get all the notes for the incident in an ArrayList.
* 
* @return the notes as an ArrayList
*/
public ArrayList<String> getNotes() {

/**
* Set the cancellation code when the incident is canceled.
* 
* @param c cancellationCode the cancellationCode to set
*/
private void setCancellationCode(String c) {

 - Throws: IllegalArgumentException

/**
* Get the resolution code of the incident.
* 
* @return resolutionCode of the incident 
*/
public ResolutionCode getResolutionCode() {

/**
* Set the resolution code when the incident is resolved. 
* 
* @param r resolutionCode the resolutionCode to set
*/
private void setResolutionCode(String r) {

 - Throws: IllegalArgumentException

/**
* Set the on hold reason when the incident is put on hold.
* 
* @param onHoldReason the onHoldReason to set
*/
private void setOnHoldReason(String o) {

 - Throws: IllegalArgumentException

/**
* Set the priority of the incident.
* 
* @param priority the priority to set
*/
private void setPriority(String p) {

 - Throws: IllegalArgumentException

/**
* Get the incident's category.
* 
* @return category for the incident.
*/
public Category getCategory() {

/**
* Set the category for the incident.
* 
* @param category the category to set
*/
private void setCategory(String c) {

 - Throws: IllegalArgumentException

/**
* Set the current state for the incident.
* 
* @param state the state to set
*/
private void setState(String s) {

 - Throws: IllegalArgumentException

/**
* OnHoldState concrete class processes incidents that are on hold.  
* Based on the command received the user, OnHoldState incidents can be:
* Reopened: state updated to InProgress
* Resolved: state set to Resolved
* Cancel: state set to Canceled
* 
*
*/
public class OnHoldState implements IncidentState {

/**
* The current state of the incident is updated according to the command received
* from the user.  
* 
* @param command the command received from the user via the GUI.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Get the name of the current state
* 
* @return the name of the state
*/
@Override
public String getStateName() {

/**
* NewState concrete class processes incidents that are new.  
* Based on the command received the user, NewState incidents can be:
* Assigned: state updated to InProgress
* Cancel: state set to Canceled
* 
*
*/	
public class NewState implements IncidentState {

/**
* The current state of the incident is updated according to the command received
* from the user.  
* 
* @param command the command received from the user via the GUI.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Get the name of the current state
* 
* @return the name of the state
*/
@Override
public String getStateName() {

/**
* ResolvedState concrete class processes incidents that are resolved.  
* Based on the command received the user, ResolvedState incidents can be:
* Reopen: state updated to InProgress
* Confirm: state updated to Closed
* Cancel: state set to Canceled
* Hold: state set to OnHold
* 
*
*/
public class ResolvedState implements IncidentState {

/**
* The current state of the incident is updated according to the command received
* from the user.  
* 
* @param command the command received from the user via the GUI.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Get the name of the current state
* 
* @return the name of the state
*/
@Override
public String getStateName() {

/**
* InProgressState concrete class processes incidents that are in progress.  
* Based on the command received the user, InProgressState incidents can be:
* Resolve: state set to Resolved
* Cancel: state set to Canceled
* Hold: state set to OnHold
* 
*
*/
public class InProgressState implements IncidentState {

/**
* The current state of the incident is updated according to the command received
* from the user.  
* 
* @param command the command received from the user via the GUI.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Get the name of the current state
* 
* @return the name of the state
*/
@Override
public String getStateName() {

/**
* ClosedState concrete class processes incidents that are closed.  
* Based on the command received the user, ClosedState incidents can be:
* Reopen: state updated to InProgress
* 
*
*/
public class ClosedState implements IncidentState {

/**
* The current state of the incident is updated according to the command received
* from the user.  
* 
* @param command the command received from the user via the GUI.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Get the name of the current state
* 
* @return the name of the state
*/
@Override
public String getStateName() {

/**
* Canceled concrete class processes incidents that are canceled.
* Incidents that are canceled cannot change their state.
* 
*
*/	
public class CanceledState implements IncidentState {

/**
* The current state of the incident is updated according to the command received
* from the user.  
* 
* @param command the command received from the user via the GUI.
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Get the name of the current state
* 
* @return the name of the state
*/
@Override
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* IncidentManager controls the creation and modification of 
* ManagedIncidentLists. IncidentManager implements the singleton design 
* pattern so only one instance of IncidentManager can ever be created.
* 
*
*/
public class IncidentManager {

/** Instance of ManagedIncidentList - list of ManagedIncidents */
private ManagedIncidentList incidentList;

/** Singleton instance of IncidentManager */
private static IncidentManager singleton;

/**
* Create a new IncidentManager
*/
private IncidentManager() {

/**
* Provides a static method for the GUI to access this instance of IncidentManager.
* 
* @return IncidentManager this instance of IncidentManager
*/
public static IncidentManager getInstance() {

/**
* Create a new ManagedIncidentList
*/
public void createNewManagedIncidentList() {

/**
* Add an incident to the ManagedIncidentList
* 
* @param caller incident originator
* @param category of incident
* @param priority of incident
* @param name of incident
* @param workNote notes
*/
public void addManagedIncidentToList(String caller, Category category, 
/**
* Delete a ManagedIncident by incidentID
* 
* @param incidentId of incident to delete
*/
public void deleteManagedIncidentById(int incidentId) {

/**
* Execute a command to update or view an incident.
* 
* @param incidentId of incident to update
* @param command to perform on incident
*/
public void executeCommand(int incidentId, Command command) {

/**
* Get a ManagedIncident by its incidentID.
* 
* @param incidentID of incident to return
* @return ManagedIncident selected by ID.
*/
public ManagedIncident getManagedIncidentById(int incidentID) {

/**
* Returns a 2D array of all incidents that is used by the GUI to populate tables. 
* ManagedIncidents id number
* ManagedIncidents category as a string
* ManagedIncidents state name
* ManagedIncidents priority as a string
* ManagedIncidents name
* 
* @return String 2D array of fields for GUI
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Returns a 2D array filtered by category of incidents that is used by 
* the GUI to populate tables. 
* ManagedIncidents id number
* ManagedIncidents category as a string
* ManagedIncidents state name
* ManagedIncidents priority as a string
* ManagedIncidents name
* 
* @return String 2D array of fields for GUI
* @param category of incidents to select
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Save ManagedIncidents to XML file
* 
* @param fileName name of file to save
*/
public void saveManagedIncidentsToFile(String fileName) {

/**
* Load ManagedIncidents from XML file.
* 
* @param fileName name of file open
*/
public void loadManagedIncidentsFromFile(String fileName) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Maintains a list of ManagedIncidents.  Provides list functions,
* such as add, remove, update, and search.
* 
*
*/
public class ManagedIncidentList {

/** List of managed incidents  */
private ArrayList<ManagedIncident> incidents;

/**
* Construct a new empty list of incidents and set the counter to 0
*/
public ManagedIncidentList() {

/**
* Add a new incident to the list.
* 
* @param caller the person who originated the incident
* @param category the category of the incident
* @param priority the priority of the incident
* @param name information from when the incident is created
* @param notes the incident's worknotes.
* 
* @return incidentID of the new incident.
*/
public int addIncident(String caller, Category category, Priority priority, 
/**
* Move the incidents from the xml source to the list
* 
* @param list of incidents from the XML
*/
public void addXMLIncidents(List<Incident> list) {

/**
* Get list of all ManagedIncidents
* 
* @return List of ManagedIncidents
*/
public List<ManagedIncident> getManagedIncidents() {

/**
* Get a list of ManagedIncidents filtered by category.
* 
* @param category the category to filter by
* @return list of ManagedIncidents
*/
public List<ManagedIncident> getIncidentsByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Get an incident by its incidentId.
* 
* @param incidentId of the incident to select
* @return incident selected by ID.
*/
public ManagedIncident getIncidentById(int incidentId) {

/**
* Execute a command to update or view an incident.
* 
* @param incidentId the incident to process
* @param command the command to perform
*/
public void executeCommand(int incidentId, Command command) {

/**
* Delete and incident from the list by incidentId
* 
* @param incidentId the ID of the incident to delete.
*/
public void deleteIncidentById(int incidentId) {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class is designed to contain and handle all of the passengers who have not yet 
* entered one of the security lines
* 
*
*/
public class PreSecurity implements TransitGroup {

/** The number of passengers */
@SuppressWarnings("unused")
private int numPass;

/** The reporter to be used */
@SuppressWarnings("unused")
private Reporter rep;

/** The list of passengers in the preSecurity line */
/**
* Constructor for the PreSecurity class
* @param numPass the number of passengers to be added
* @param rep a reporter to facilitate movement
* @throws IllegalArgumentException if the user enters an invalid number of passengers
*/
public PreSecurity(int numPass, Reporter rep) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

/**
* Finds the next departing time, i.e. the next person to leave the line
* @return time the next departing time
*/
public int departTimeNext() {

/**
* Finds the Passenger who is next to leave the line, based on departure time
* @return pass the next passenger to depart
*/
public Passenger nextToGo() {

/**
* Determines whether or not there are more passengers in line
* @return true if there are more passengers, and false if not
*/
public boolean hasNext() {

/**
* Removes the next Passenger from the list once they have been determined as the next to depart
* @return pass the passenger to be removed
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class is designed to represent a security checkpoint and its line
* 
*
*/
public class CheckPoint {

/** The time that the last passenger in line will clear security */
private int timeWhenAvailable = 0;

/** A passenger queue representing the security line */
/**
* The Constructor for CheckPoint
*/
public CheckPoint() {

/**
* Gives the current number of people in line
* @return s the size of the list (number of people in line)
*/
public int size() {

/**
* Return timeWhenAvailable
* @return timeWhenAvialable the last person's departure time
*/
public int getTimeWhenAvailable() {

/**
* Removes a passenger from the line once they have cleared security
* @return pass the Passenger that has just cleared security
*/
public Passenger removeFromLine() {

/**
* Determines if there are still more people in line
* @return true if there are people, false if not
*/
public boolean hasNext() {

/**
* Determines the time of the next person to depart
* @return time the next departure time
*/
public int departTimeNext() {

/**
* Determines the next person in line to go through security
* @return pass the next passenger in line
*/
public Passenger nextToGo() {

/**
* Adds a Passenger to the security line
* @param pass the passenger to be added
*/
public void addToLine(Passenger pass) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class holds a collection of security checkpoints
* 
*
*/
public class SecurityArea implements TransitGroup {

/** the maximum number of checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** the minimum number of checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** The error message given when the user enters an invalid number of checkpoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least " + MIN_CHECKPOINTS + " and at most " + MAX_CHECKPOINTS + ".";

/** The error message when the index given is out of the allowable range */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** The maximum index for FastTrack indexes (lower 1/3) */
private int largestFastIndex;

/** The number of security checkpoints */
private int numCheckpoints;

/** The index of the TSA checkpoint (number minus 1) */
private int tsaPreIndex;

/** A list of Array lists, representing all of the security lines */
public CheckPoint[] securityCheckpoints;

/**
* The constructor for the SecurityArea class
* @param numCheckpoints the number of checkpoints provided by the user
* @throws IllegalArgumentException if the number of checkpoints is out of range
*/
public SecurityArea(int numCheckpoints) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

/**
* Sets the maximum index of the FastTrack gates, which is the lower third rounded up
* @return the highestFastTrackIndex
*/
public int setFastTrackIndex() {

/**
* Checks to see if the number of gates is in the acceptable range
* @param numCheckpoints the number of checkpoints provided
* @return true if the number provided is okay
*/
private boolean numGatesOK(int numCheckpoints) {

/**
* Adds the Passenger to the security line with the given index
* @param idx the index of the security line to be added
* @param pass the Passenger to be added to that security line
*/
public void addToLine(int idx, Passenger pass) {

/**
* Determines the shortest regular line
* @return idxS the index of the shortest line
*/
public int shortestRegularLine() {

/**
* Determines the shortest FastTrack line (includes regular lines)
* @return idxS the index of the shortest line
*/
public int shortestFastTrackLine() {

/**
* Determines the shortest TSA line (includes all lines)
* @return idxS the index of the shortest line
*/
public int shortestTSAPreLine() {		
/**
* Determines the number of Passengers in a line with the given index
* @param idx the index of the line to be checked
* @return numPass the number of Passengers in line
* @throws IllegalArgumentException if the index is out of range
*/
public int lengthOfLine(int idx) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

/**
* Determines the next departure time
* @return time the next departure time
*/
public int departTimeNext() {

/**
* Determines the next Passenger in line to go
* @return pass the next Passenger
*/
public Passenger nextToGo() {

/**
* Removes the passenger from a line once they have cleared security
* @return pass the Passenger to be removed
*/
public Passenger removeNext() {

/**
* Determines the shortest line
* @param lowerBound the lower bound of the target range
* @param upperBound the upper bound of the target range
* @return idxS the shortest line in range
*/
@SuppressWarnings("unused")
private int shortestLineInRange(int lowerBound, int upperBound) {

/**
* Determines the next line that will clear out
* @return idxC the next line to clear
*/
@SuppressWarnings("unused")
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The class for the FastTrackPassenger type
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** The maximum process time for a fast track passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** The color assigned to a FastTrack passenger */
private Color color;

/**
* The constructor for the FastTrackPassenger
* @param arrivalTime the generated arrival time
* @param processTime the generated process time
* @param rep the Reporter to be used
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter rep) {

 - Throws: IllegalArgumentException

/**
* Abstract method that gets the color for this passenger
* @return c the Color of the Passenger
*/
@Override
public Color getColor() {

/**
* Puts the Passenger in line
* @param group the list of security lines
*/
@Override
public void getInLine(TransitGroup group) {

/**
* Determines which line is shortest
* @param group the list of security lines
* @return idxB the best line to join
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Contains information specific to an Ordinary Passenger
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** The maximum process time for a regular passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** The color assigned to a regular passenger */
private Color color;

/**
* The constructor for the OrdinaryPassenger
* @param arrivalTime the generated arrival time
* @param processTime the generated process time
* @param rep the Reporter to be used
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter rep) {

/**
* Abstract method that gets the color for this passenger
* @return c the Color of the Passenger
*/
@Override
public Color getColor() {

/**
* Puts the Passenger in line
* @param group the list of security lines
*/
@Override
public void getInLine(TransitGroup group) {

/**
* Determines which line is shortest
* @param group the list of security lines
* @return idxB the best line to join
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The abstract class that contains the other three concrete Passenger classes
* 
*
*/
public abstract class Passenger {

/** The minimum process time */
public static final int MIN_PROCESS_TIME = 20;

/** The arrival time */
private int arrivalTime;

/** The wait time */
private int waitTime;

/** The process Time */
private int processTime;

/** The security line index */
private int lineIndex = -1;

/** Whether or not the Passenger is in processing */
private boolean waitingProcessing = false;

/** The created Reporter class */
private Reporter rep;

/**
* The constructor for the Passenger class
* @param arrivalTime the generated arrival time
* @param processTime the generated process time
* @param rep the Reporter
*/
public Passenger(int arrivalTime, int processTime, Reporter rep) {

/** 
* Abstract method for getting the Color
* @return color the color
*/
public abstract Color getColor();

/** 
* Abstract method for the Passenger getting in Line
* @param group the group to work through
*/
public abstract void getInLine(TransitGroup group);

/**
* Gets the wait time
* @return the waitTime
*/
public int getWaitTime() {

/**
* Sets the waiting processing variable
* @param waitingProcessing whether or not the Passenger is in line
*/
public void setWaitingProcessing(boolean waitingProcessing) {

/**
* Sets the wait time
* @param waitTime the waitTime to set
*/
public void setWaitTime(int waitTime) {

/**
* Gets the arrival time
* @return the arrivalTime
*/
public int getArrivalTime() {

/**
* Gets the processTime
* @return the processTime
*/
public int getProcessTime() {

/**
* Gets the lineIndex
* @return the lineIndex
*/
public int getLineIndex() {

/**
* Determines whether or not the Passenger is in a security line
* @return true if they are waiting and false if they are not
*/
public boolean isWaitingInSecurityLine() {

/**
* Clears the security line
*/
public void clearSecurity() {

/**
* Sets the index of the security line
* @param idxL the new index of the security line
*/
protected void setLineIndex(int idxL) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() throws NoSuchElementException {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Contains information specific to a trusted traveler
* 
*
*/
public class TrustedTraveler extends Passenger {

/** The maximum process time for a trusted traveler */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** The color assigned to a TrustedTraveler */
private Color color;

/**
* The constructor for the TrustedTraveler
* @param arrivalTime the generated arrival time
* @param processTime the generated process time
* @param rep the Reporter to be used
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter rep) {

/**
* Abstract method that gets the color for this passenger
* @return c the Color of the Passenger
*/
@Override
public Color getColor() {

/**
* Puts the Passenger in line
* @param group the list of security lines
*/
@Override
public void getInLine(TransitGroup group) {

/**
* Determines which line is shortest
* @param group the list of security lines
* @return idxB the best line to join
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Determines which Passenger to process next
* 
*
*/
public class EventCalendar {

/** The first group to be observed */
private TransitGroup group1;

/** The second group to be observed */
private TransitGroup group2;

/**
* The constructor for EventCalendar
* @param group1 the first group
* @param group2 the second group
*/
public EventCalendar(TransitGroup group1, TransitGroup group2) {

/**
* Returns the Passenger who will be next to leave their line
* @return the Passenger from high and low priority
* @throws IllegalStateException if both groups are empty
*/
public Passenger nextToAct() throws IllegalStateException {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Stores much of the data used in determining the efficiency of the system
* 
*
*/
public class Log implements Reporter {

/** The Number of completed Passengers */
private int numCompleted;

/** The total wait time of all Passengers */
private int totalWaitTime = 0;

/** The total process time of all Passengers */
private int totalProcessTime = 0;

/**
* The constructor for Log
*/
public Log() {

/**
* Gets the number of completed Passengers
* @return numCompleted the number of completed Passengers 
*/
public int getNumCompleted() {

/**
* Logs the data for a Passenger
* @param pass the Passenger to be used
*/
public void logData(Passenger pass) {

/**
* Finds the average wait time
* @return time the average Wait Time
*/
public double averageWaitTime() {

/**
* Finds the average Process Time
* @return time the average Process Time
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Executes various other classes
* 
*
*/
public class Simulator {

/** Number of Passengers */
@SuppressWarnings("unused")
private int numPassengers;

/** Number of Checkpoints */
@SuppressWarnings("unused")
private int numCheckpoints;

/** Percent of TSA PreChecked Passengers */
@SuppressWarnings("unused")
private int percentTrust;

/** Percent of FastTrack Passengers */
@SuppressWarnings("unused")
private int percentFast;

/** Percent of Regular Passengers */
@SuppressWarnings("unused")
private int percentReg;

//	/** The group who have not yet entered a checkpoint security line */
//	private TransitGroup inTicketing;

//	/** The group who are in a checkpoint security checkpoint line */
//	private TransitGroup inSecurity;

/**
* Constructor for Simulator. Constructs other various classes.
* @param numCheckpoints the number of checkpoints
* @param numPassengers the number of Passengers
* @param percentTrust the percent of TSA PreChecked Passengers
* @param percentFast the percent of FastTrack Passengers
* @param percentReg the percent of Regular Passengers
* @throws IllegalArgumentException if any of the data entered is incorrect
*/
public Simulator(int numCheckpoints, int numPassengers, int percentTrust, 
/**
* Checks that all parameters are valid
* @param numPassengers the number of Passengers
* @param percentTrust percentage of TrustedTravelers
* @param percentFast percentage of FastTrack Passengers
* @param percentReg percent of Regular Passengers
*/
private void checkParameters(int numPassengers, int percentTrust, int percentFast, int percentReg) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

//			throw new IllegalArgumentException(e.getMessage());

/**
* Sets up the Passengers
* @param num1 PLACE.................................PLACE
* @param num2 PLACE.................................PLACE
* @param num3 PLACE.................................PLACE
*/
@SuppressWarnings("unused")
private void setUp(int num1, int num2, int num3) {

/**
* Gets the Reporter
* @return log the Reporter
*/
public Reporter getReporter() {

/**
* Goes through the Passengers in the Event Calendar
*/
public void step() throws IllegalStateException {

/**
* Checks to see if there are more Passengers to Step to
* @return true if there are more, and false if not
*/
public boolean moreSteps() {

@SuppressWarnings("unused")
/**
* Gets the index of the security checkpoint for the current Passenger
* @return idx the index of the checkpoint
*/
public int getCurrentIndex() {

/**
* Gets the color of the Current Passenger
* @return c the Color corresponding to the Passenger
*/
public Color getCurrentPassengerColor() {

/**
* Determines whether or not a Passenger just cleared a security checkpoint
* @return true if the Passenger cleared a Security checkpoint, and false if they
* did not or the Passenger is null.
*/
public boolean passengerClearedSecurity() {


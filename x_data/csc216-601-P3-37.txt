---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* WolfResultsManager is a singleton that maintains the RaceList, reads from, 
* and writes to the *.md files for the WolfResults application. 
* The WolfResultsManager has a RaceList , the last used filename, and if the list has changed.
* 
*
*/
public class WolfResultsManager extends Observable {

/** the filename */
private String filename;

/** returns true if object has changed */
private boolean changed;

/** list of races */
private RaceList list;

/** singleton instance of WolfResultsManager */
private WolfResultsManager instance;

/**
*  Returns the single WolfResultsManager instance. 
*  If the instance is null, a WolfResultsManager is constructed.
*  
* @return the single instance of the WolfResultsManager
*/
public static WolfResultsManager getInstance() {

/**
* Constructs the WolfResultsManager by creating an empty RaceList and registering as 
* an Observer of the list. The constructor is private.
*/
private WolfResultsManager() {

/** 
* Creates a new empty RaceList and registers the WolfResultsManager as an Observer. 
* The method notifies observers of the change if no exception is thrown.
*/
public void newList() {

/**
* Returns the value stored in changed.
* 
* @return true if object has changed, else false
*/
public boolean isChanged() {

/**
* Sets the value of changed
* 
* @param b the value of changed, true or false
*/
private void setChanged(boolean b) {

/**
* Gets the filename
* 
* @return the filename
*/
public String getFilename() {

/**
*  If the filename parameter is null, the empty string, or string with whitespace only, 
*  an IllegalArgumentException is thrown. Leading and trailing whitespace should be trimmed
*  from filename prior to assigning to field. Otherwise, the filename field is set. 
*  setFileName() doesnt need to notify observers since the state of the RaceList is not 
*  changed by setFilename(). 
* 
* @param filename the filename to set
*/
public void setFilename(String filename) {

/**
* Returns the RaceList. 
* Use this method to access the RaceList an all of its contents from the GUI.
* 
* @return the list
*/
public RaceList getRaceList() {

/**
* Reads the WolfResults from the given filename and adds the WolfResultsManager as an 
* observer to the created RaceList. The filename field is set to the parameter value 
* (if valid). Sets the changed flag to false. 
* The method notifies observers of the change if no exception is thrown.
* 
* @param filename the filename to read
*/
public void loadFile(String filename) {

/**
* Saves the RaceList to the given filename. Sets the changed flag to false.
* 
* @param filename the filename to save
*/
public void saveFile(String filename) {

/**
* If a RaceList observed by WolfResultsManager changes, the update() method is 
* automatically called. WolfResultsManager should propagate the notification of 
* the change to its Observers and changed should be updated to true.
* 
* @param obs the subject
* @param obj the object that is observing for change
*/
public void update(Observable obs, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class for reading Wolf Results files.
* 
*
*/
public class WolfResultsReader {

/**
* Constructs WolfResultsReader object
*/
public WolfResultsReader() {

/**
* Returns a RaceList from the given file. 
* An IllegalArgumentException is thrown on any error or incorrect formatting.
* 
* @param filename the name of the file to read.
* @return the RaceList from the file.
*/
public static RaceList readRaceListFile(String filename) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class for writing Wolf Results files.
* 
*
*/
public class WolfResultsWriter {

/**
* Constructs new WolfResultsWriter object
*/
public WolfResultsWriter() {

/**
* Writes the given RaceList to the given file.
* An IllegalArgumentException is thrown on any error.
* 
* @param filename the filename to write
* @param list the RaceList to write to the file
*/
public static void writeRaceFile(String filename, RaceList list) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A representation of one persons race result for a single race in the WolfResultsManager 
* application.   IndividualResult is an Observer in the Observer Pattern and implement the 
* Comparable interface. 
* 
*
*/
public class IndividualResult implements Comparable<IndividualResult>, Observer {

private Race race;

private String name;

private int age;

private RaceTime pace;

private RaceTime time;

/**
* The constructor should throw an IllegalArgumentException if 
* (a) race is null;

* (b) name is null, empty string, or all whitespace;

* (c) age is negative; or 
* (d) time is null. 
* name should be trimmed of leading and/or trailing whitespace. 
* The pace field should be set to the pace based on time and race distance. 
* An Observer should be added for race.
* 
* @param race the name of the race
* @param name the name of the racer
* @param age the age the of racer
* @param time the racer time
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* get the race.
* 
* @return the race
*/
public Race getRace() {

/**
* Get the racer's name.
* 
* @return the name
*/
public String getName() {

/**
* Get the racer's age.
* 
* @return the age
*/
public int getAge() {

/**
* Get the racer's pace.
* 
* @return the pace
*/
public RaceTime getPace() {

/**
* Get the racer's time.
* 
* @return the time
*/
public RaceTime getTime() {

/**
* Two IndividualResult objects are compared on their times. Delegate to RaceTimes compareTo method.
* 
* @param ir Individual Result
* @return 0 for equal, negative if less, positive if more
*/
public int compareTo(IndividualResult ir) {

/**
* The string representation of an IndividualResult is the name, age, time, and pace fields. 
* (IndividualResult [name=NAME, age=AGE, time=TIME, pace=PACE])
* 
* @return String representation of result
*/
public String toString() {

/**
* The pace is updated if the Race that the IndividualResult is observing notified its observers of a change.
* 
* @param obs the subject
* @param obj the observer
*/
public void update(Observable obs, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A representation of a Race in the WolfResultsManager application.
* 
*
*/
public class Race extends Observable {

private String name;

private double distance;

private LocalDate date;

private String location;

private RaceResultList results;

/**
* The constructor should throw an IllegalArgumentException if 
* (a) name is null, empty string, or all whitespace;

* (b) distance is non-positive;

* (c) date is null;

* (d) location is null; or 
* (e) results is null. 
* name and location should be trimmed of leading and/or trailing whitespace.
* 
* @param name the name of the race
* @param distance the distance of the race
* @param date the date of the race
* @param location the location of the race
* @param results the results of the race
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* The constructor should throw an IllegalArgumentException if 
* (a) name is null, empty string, or all whitespace;

* (b) distance is non-positive;

* (c) date is null;

* (d) location is null; or 
* name and location should be trimmed of leading and/or trailing whitespace.
* 
* @param name the name of the race
* @param distance the distance of the race
* @param date the date of the race
* @param location the location of the race
*/
public Race(String name, double distance, LocalDate date, String location) {

/**
* Add individual result to race
* 
* @param ir IndividualResult to add to race
*/
public void addIndividualResult(IndividualResult ir) {

//Sends a message to any Observer classes that the object has changed.
/**
* Get the race distance
* 
* @return the distance
*/
public double getDistance() {

/**
* Set the race distance
* Set distance field to the parameter. 
* The method throws IllegalArgumentException if distance is non-positive. 
* Observers of Race are notified of the change.
* 
* @param distance the distance to set
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* Get the race name
* 
* @return the name
*/
public String getName() {

/**
* Get the race date
* 
* @return the date
*/
public LocalDate getDate() {

/**
* Get the race location
* 
* @return the location
*/
public String getLocation() {

/**
* Get the race results.
* 
* @return the results
*/
public RaceResultList getResults() {

/**
* (int minAge, int maxAge, String minPace, String maxPace): 
* Throws IllegalArgumentException if minPace or maxPace is not a valid RaceTime. 
* Returns list of results such that runners age is between minAge and maxAge (inclusive) 
* and runners pace is between minPace and maxPace (inclusive).
* 
* @param minAge of the racers
* @param maxAge of the racers
* @param minPace of the racers
* @param maxPace of the racers
* @return result of the race
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

/**
* The string representation of an Race is the name, distance, date, and location fields. 
* (NAME (DISTANCE miles) on DATE at LOCATION)
* 
* @return the String representation of a race.
*/
@Override
public String toString() {

/**
* use by equals() method 
*/
@Override
public int hashCode() {

/**
* Compares races to see if they are equal
* based on the name, distance, date, and location fields.
* 
* @return true if races are equal, else false.
*/
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* RaceList has a ArrayList of Races.
* RaceList has an instance of an edu.ncsu.csc216.wolf_results.util.ArrayList 
* that will hold Race objects.
* 
*
*/
public class RaceList extends Observable implements Observer {

/** array list of races */
private ArrayList races;

/**
*  Constructs the edu.ncsu.csc216.wolf_results.util.ArrayList
*/
public RaceList() {

/**
* Throws IllegalArgumentException if race is null. 
* Adds race to list. 
* Observer is added for the race. 
* Observers of RaceList are notified of the change.
* 
* @param race to add to list
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

/**
* Constructs a Race using the provided parameters and 
* throws IllegalArgumentException if Race constructor throws exception. 
* Adds race to list. 
* Observer is added for the race. 
* Observers of RaceList are notified of the change.
* 
* @param name the name of the race
* @param distance the distance of the race
* @param date the date of the race
* @param location the location of the race
*/
public void addRace(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* Removes race at index idx. Observers of RaceList are notified of the change.
* 
* @param idx the index of the race to remove
*/
public void removeRace(int idx) {

/**
* Gets race at index idx. Throws IndexOutOfBoundsException if idx is out of bounds.
* 
* @param idx the index of the race to return
* @return the race at given index
*/
public Race getRace(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Returns number of races in list.
* 
* @return the number of races in the list
*/
public int size() {

/**
* The RaceList.update() method is called if the Race that the RaceList is observing notified 
* its observers of a change. If the Observable is an instance of Race, then the observers of 
* the RaceList should be updated. The current instance is passed to notifyObservers().
* 
* @param obs the subject
* @param obj the object that is observing
*/
public void update(Observable obs, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*  A SortedLinkedList of IndividualResult.
*  A RaceResultList has a SortedLinkedList for holding IndividualResult objects.
* 
*
*/
public class RaceResultList {

/** linked list of individual results */
private SortedLinkedList<IndividualResult> results;

/**
* Constructs the edu.ncsu.csc216.wolf_results.util.SortedLinkedList.
* 
*/
public RaceResultList() {

/**
* Add individual result to list
* The method throws IllegalArgumentException if result is null.
* 
* @param ir Individual Result to add to list.
*/
public void addResult(IndividualResult ir) {

 - Throws: IllegalArgumentException

/**
* Adds race result to list.
* The method constructs a IndividualResult using the provided parameters and 
* throws IllegalArgumentException if IndividualResult constructor throws exception.
* 
* @param race the race to add to the list
* @param name the name of the racer
* @param age the age of the racer
* @param time the time of the racer
*/
public void addResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* Gets result at index i. 
* Throws IndexOutOfBoundsException if i is out of bounds.
* 
* @param i the index of the IndiviudalResult to get
* @return the results
*/
public IndividualResult getResult(int i) {

 - Throws: IndexOutOfBoundsException

/**
* Returns number of results in list
* 
* @return the number of results in list
*/
public int size() {

/**
* Returns a 2D array of string representing results. 
* First dimension is the result/runner (in sorted order by time). 
* Second dimension are the different fields: 
* [0]: name, [1]: age, [2]: time, [3]: pace. 
* If the list is empty, an empty 2D array is returned.
* 
* @return 2D array of string results
*/
public String[][] getResultsAsArray() {

/**
* Throws IllegalArgumentException if minPace or maxPace is not a valid RaceTime. 
* Returns list of results such that runners age is between minAge and maxAge (inclusive) 
* and runners pace is between minPace and maxPace (inclusive).
* 
* @param minAge runner's minimum age
* @param maxAge runner's maximum age
* @param minPace runner's minimum pace
* @param maxPace runner's maximum pace
* 
* @return list of results based on parameters
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* An implementation of the List interface with an array data structure.
* The List interface contains 8 methods that must be implemented by ArrayList. 
* The Javadoc of the List interface describes what each method should do, 
* including when exceptions must be thrown.
* 
*
*/
public class ArrayList implements List {

private static final long serialVersionUID = 1;

private static final int RESIZE = 2;

private Object[] list;

private int size = 0;

/**
* Construct an array list of objects
* 
* @param capacity the size of that array
*/
public ArrayList(int capacity) {

/**
* Construct an array list of objects
*/
public ArrayList() {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
@Override
public int size() {

/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
@Override
public boolean isEmpty() {

/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
@Override
public boolean contains(Object o) {

/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true if equal
*/
@Override
public boolean add(Object o) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

 - Throws: IndexOutOfBoundsException

/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range 
*/
@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range
*/
@Override
public void add(int index, Object element) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

 - Throws: IndexOutOfBoundsException

 - Throws: IndexOutOfBoundsException

/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range
*      
*/      
@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
@Override
public int indexOf(Object o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true if add successful
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range 
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* RaceTime represents a race time in the following format: hh:mm:ss, where 
* (a) hours may be listed as one digit or many digits with leading zeros and 
* (b) minutes and seconds are always two digits between 0 and 59 (inclusive).
* 
*/
public class RaceTime {

/** number of hours */
private int hours;

/** number of minutes */
private int minutes;

/** number of seconds */
private int seconds;

/**
* The constructor should throw an IllegalArgumentException if 
* (a) hours is negative or 
* (b) minutes or seconds are not in between 0 and 59 (inclusive).
* 
* @param hours the number of hours in the race
* @param minutes the number of minutes in the race
* @param seconds the number of seconds in the race
*/
public RaceTime(int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
*  The constructor should throw an IllegalArgumentException if 
*  (a) time is not in the correct format (hh:mm:ss), 
*  (b) hh is negative, or 
*  (c) mm or ss are not in between 0 and 59 (inclusive)
*  
* @param time the time of the race
*/
public RaceTime(String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Get the number of hours
* 
* @return the hours
*/
public int getHours() {

/**
* Get the number of minutes
* 
* @return the minutes
*/
public int getMinutes() {

/**
* Get the number of seconds
* 
* @return the seconds
*/
public int getSeconds() {

/**
* Returns the total time in seconds. 
* 
* @return the total time in seconds.
*/
public int getTimeInSeconds() {

/**
* Returns the RaceTime as a string with the format hh:mm:ss, where 
* (a) hours may be listed as one digit or many digits with leading zeros and 
* (b) minutes and seconds are always two digits between 0 and 59 (inclusive).
* 
* @return string representation of race time.
*/
public String toString() {

/**
* RaceTimes are compared based on their total time.
* @param rt the race time to compare
* @return 0 for equal, negative if less, positive if more
*/
public int compareTo(RaceTime rt) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The SortedList E extends Comparable  interface contains 7 methods that must be implemented 
* by SortedLinkedList E extends Comparable. The Javadoc of the SortedList interface describes
* what each method should do, including when exceptions must be thrown.
* 
* @param <E> generic object
*
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/** the first node in the linked list */
private Node head;

/**
* Construct a new SortedLinkedList object
*/
public SortedLinkedList() {

/**
* returns a String representation of the list
* 
* @return list as a string
*/
public String toString() {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
@Override
public int size() {

/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
@Override
public boolean isEmpty() {

/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
@Override
public boolean contains(E e) {

/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true if add successful
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
@Override
public boolean add(E e) {

 - Throws: IllegalArgumentException

 - Throws: NullPointerException

/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range
*/
@Override
public E remove(int index) {

/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
@Override
public int indexOf(E e) {

* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

/**
* Creates node for SortedLinkedList class.
* 
*
*/
public class Node {

/** value of generic object to store in linked list */
public E value;

/**  location of next node */
public Node next;

/**
* Construct new node for linked list
* 
* @param value of object to store in list
* @param next location of next node
*/
public Node(E value, Node next) {

* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

@SuppressWarnings("unchecked")
//private SortedLinkedList getOuterType() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
* @param <E> generic object type
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true if add successful
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

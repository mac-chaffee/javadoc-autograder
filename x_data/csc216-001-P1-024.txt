---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This is the presecurity for the airport!
*
*/
public class PreSecurity implements TransitGroup {

/**
* The constructor for the class PreSecurity
* @param numberOfPassengers ,  an int
* @param report , a Reporter
*/
public PreSecurity(int numberOfPassengers, Reporter report) {

 - Throws: IllegalArgumentException

/**
* The method to determine depart time
* @return int , returns the time
*/
@Override
public int departTimeNext() {

/**
* Determines the next passenger to go
* @return Passenger , the passenger that is returned
*/
@Override
public Passenger nextToGo() {

/**
* Determines if there is another passenger in line
* @return boolean , true if there is another passenger
*/
public boolean hasNext() {

/**
* Determines which passenger to remove from the line
* @return Passenger, the passenger that has been removed
*/
@Override
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class that represents a security checkpoint ands it's waiting time
*
*/
public class CheckPoint {

/** Variable that stores the int value for determining when the line is open*/
private int timeWhenAvailable;

/** Our queue of passengers in the line */
/**
* Method that is the class constructor
*/
public CheckPoint() {

/**
* Method that returns the size of the line
* @return int , returns the amount of passengers waiting in line for the security checkpoint
*/
public int size() {

/**
* Method that removes a passenger from the security line
* @return Passenger, the passenger removed
*/
public Passenger removeFromLine() {

/**
* Method that determines if there is a passenger waiting to be processed
* @return boolean , true if there is a passenger waiting, false if there is no passenger waiting
*/
public boolean hasNext() {

/**
* Method that returns the time for the next passenger to depart the line
* @return int , the time returned
*/
public int departTimeNext() {

/**
* Method that returns the next passenger to be processed in the security line
* @return Passenger, the passenger to be processed next
*/
public Passenger nextToGo() {

/**
* Method that adds a passenger to the security line
* @param value , the passenger to be added
*/
public void addToLine(Passenger value) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* SecurityArea is the class responsible for managing the security area
*
*/
public class SecurityArea implements TransitGroup {

/** The maximum amount of checkpoints in the security area*/
private static final int MAX_CHECKPOINTS = 17;

/** The minimum amount of checkpoints in the security area*/
private static final int MIN_CHECKPOINTS = 3;

/** The amount of error checkpoints*/
//private static final int ERROR_CHECKPOINTS = 0;

/** The index of the errors*/
//private static final String ERROR_INDEX = "";

/** The index of the fast-pass passengers*/
private int largestFastIndex;

/** The index of the tsaPreCheck passengers*/
private int tsaPreIndex;

private CheckPoint[] check;

/**
* The constructor for the SecurityArea class
* @param value , number of security lines
*/
public SecurityArea(int value) {

 - Throws: IllegalArgumentException

/**
* Method that determines if a certain number of gates is allowed
* @param value , the number of gates suggested
* @return boolean , determines if its true or not
*/
private boolean numGatesOK(int value) {

/**
* Method to add passengers to a selected line
* @param value , which line to add too
* @param passenger , the passenger to be moved
*/
public void addToLine(int value, Passenger passenger) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Method to get the shortest regular line
* @return int , the line number returned
*/
public int shortestRegularLine() {

/**
* Method to return the shortest fastTrack line
* @return int , the line number returned
*/
public int shortestFastTrackLine() {

/**
* Method to return the shortest TSAPreCheck line
* @return int, the line number returned
*/
public int shortestTSAPreLine() {

/**
* Method to get the length of a selected line
* @param value , the line to get the length from
* @return int , the length of the line returned
*/
public int lengthOfLine(int value) {

 - Throws: IllegalArgumentException

* @see edu.ncsu.csc216.transit.airport.TransitGroup#departTimeNext()
*/
@Override
public int departTimeNext() {

* @see edu.ncsu.csc216.transit.airport.TransitGroup#departTimeNext()
*/
@Override
public Passenger nextToGo() {

* @see edu.ncsu.csc216.transit.airport.TransitGroup#departTimeNext()
*/
@Override
public Passenger removeNext() {

 - Throws: ArrayIndexOutOfBoundsException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class that represents a specific type of passenger classed a FastTrackPassenger
*
*/
public class FastTrackPassenger extends Passenger {

/** represents the color of the passenger*/
private Color color;

/** the maximum expected processing time*/
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* Method is a class constructor for FastTrackPassenger
* @param arrivalTime , an int
* @param processTime , an int
* @param report , a Reporter
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter report) {

/**
* Method that gets the color of a TrustedPassenger
* @return Color , the color returned
*/
public Color getColor() {

/**
* Method that put a TrustedTraveler into a security line
* @param value , a TransitGroup to determine which line to go into
*/
public void getInLine(TransitGroup value) {

/**
* Method that picks a line for the passenger
* @param value , the TransitGroup to determine which line to choose
* @return int , the line chosen
*/
private int pickLine(TransitGroup value) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class that represents a specific type of passenger, an OrdinaryPassenger
*
*/
public class OrdinaryPassenger extends Passenger {

/** represents the color of the passenger*/
private Color color;

/** the maximum expected processing time*/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* Method that is a class constructor for OrdinaryPassenger
* @param arrivalTime , an int
* @param processTime , and int
* @param reporter , a Reporter
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter reporter) {

/**
* Method that gets the color of a TrustedPassenger
* @return Color , the color returned
*/
public Color getColor() {

/**
* Method that put a TrustedTraveler into a security line
* @param value , a TransitGroup to determine which line to go into
*/
public void getInLine(TransitGroup value) {

/**
* Method that picks a line for the passenger
* @param value , the TransitGroup to determine which line to choose
* @return int , the line chosen
*/
private int pickLine(TransitGroup value) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Abstract Class that represents a passenger
*/
public abstract class Passenger {

/** The minimum processing time for a passenger*/
public static final int MIN_PROCESS_TIME = 20;

/** The arrival time for the passenger*/
private int arrivalTime;

/** The wait time for a passenger*/
private int waitTime;

/** The processing time for a passenger*/
private int processTime;

/** The location of the passenger in a security line*/
private int lineIndex;

/** Boolean value that shows if a passenger is being processed or not*/
private boolean waitingProcessing;

/** Holds the reporter object*/
private Reporter myLog;

/**
* The constructor for a passenger
* @param arrivalTime , arrival time
* @param processTime , the processing time for the passenger
* @param log , a Reporter
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

 - Throws: IllegalArgumentException

/**
* Method that is getter for arrival time
* @return the arrivalTime
*/
public int getArrivalTime() {

/**
* Method that is setter for arrival time
* @param arrivalTime the arrivalTime to set
*/
public void setArrivalTime(int arrivalTime) {

 - Throws: IllegalArgumentException

/**
* Method that gets the wait time
* @return the waitTime
*/
public int getWaitTime() {

/**
* Method that sets the wait time
* @param waitTime the waitTime to set
*/
public void setWaitTime(int waitTime) {

 - Throws: IllegalArgumentException

/**
* Method that gets the processing time
* @return the processTime
*/
public int getProcessTime() {

/**
* Method that sets the processing time
* @param processTime the processTime to set
*/
public void setProcessTime(int processTime) {

 - Throws: IllegalArgumentException

/**
* Method that gets the line index
* @return the lineIndex
*/
public int getLineIndex() {

/**
* Method that returns true if the passenger is waiting in security and false if the passenger is not waiting in security
* @return boolean , true if waiting and false if not waiting
*/
public boolean isWaitingInSecurityLine() {

/**
* Method that clears the security line
*/
public void clearSecurity() {

/**
* Method that sets the line index
* @param lineIndex the lineIndex to set
*/
public void setLineIndex(int lineIndex) {

 - Throws: IllegalArgumentException

/**
* Method that puts the passenger into a security line
* @param value , A TransitGroup object
*/
public abstract void getInLine(TransitGroup value);

/**
* Method that gets the color representing the passenger
* @return Color , the color returned
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. throws NoSuchElementException if the queue is empty
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class that represents a specific type of passenger, the Trusted Traveler
*
*/
public class TrustedTraveler extends Passenger {

/** represents the color of the passenger*/
private Color color;

/** the maximum expected processing time*/
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Method that is the constructor for TrustedTraveler
* @param arrivalTime , an int
* @param processTime , an int
* @param report , a Reporter
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter report) {

/**
* Method that gets the color of a TrustedPassenger
* @return Color , the color returned
*/
public Color getColor() {

/**
* Method that put a TrustedTraveler into a security line
* @param value , a TransitGroup to determine which line to go into
*/
public void getInLine(TransitGroup value) {

/**
* Method that picks a line for the passenger
* @param value , the TransitGroup to determine which line to choose
* @return int , the line chosen
*/
private int pickLine(TransitGroup value) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Method that represents an event calendar to help give priority to events in the simulation
*
*/
public class EventCalendar {

private TransitGroup high;

private TransitGroup low;

/**
* Method that is the constructor for EventCalendar
* @param high , a TransitGroup
* @param low , a TransitGroup
*/
public EventCalendar(TransitGroup high , TransitGroup low) {

/**
* Method that determines the next passenger to act
* @return Passenger , the passenger to act
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class that logs all the data of the simulation
*
*/
public class Log implements Reporter {

/** an int that has the number of passenger who went through the simulation*/
private int numCompleted;

/** an int that has the total waiting time for passengers in the simulation*/
private int totalWaitTime;

/** and int that has the total processing time for passengers in the simulation*/
private int totalProcessTime;

/**
* Method that is the class constructor for Log
*/
public Log() {

* @see edu.ncsu.csc216.transit.simulation_utils.Reporter#getNumCompleted()
*/
@Override
public int getNumCompleted() {

* @see edu.ncsu.csc216.transit.simulation_utils.Reporter#logData(edu.ncsu.csc216.transit.airport.travelers.Passenger)
*/
@Override
public void logData(Passenger value) {

* @see edu.ncsu.csc216.transit.simulation_utils.Reporter#averageWaitTime()
*/
@Override
public double averageWaitTime() {

* @see edu.ncsu.csc216.transit.simulation_utils.Reporter#averageProcessTime()
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class that is the main logic class in the airport simulator
*
*/
public class Simulator {

/** The total number of passengers*/
private int numPassengers;

private Reporter report;

private TransitGroup inTicketing;

private TransitGroup inSecurity;

private EventCalendar myCalendar;

private Passenger currentPassenger;

/**
* Method that is the class constructor for Simulator
* @param check , number of security checkpoints
* @param pass , total number of passengers
* @param tsa , percentage of trusted travelers
* @param fast , percentage of fast track passengers
* @param ordinary , percentage of ordinary passengers
*/
public Simulator(int check, int pass, int tsa, int fast, int ordinary) {

/**
* Method that checks the parameters to make sure they're in bounds
* @param check , checks number of checkpoints
* @param pass , checks total number of passengers
* @param tsa , checks percentage of trusted travelers
* @param fast , checks percentage of fast track passengers
* @param ordinary , checks percentage of ordinary passengers
*/
private void checkParameters(int check, int pass, int tsa, int fast, int ordinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Method that gets the simulation reporter
* @return Reporter ,  the reporter returned
*/
public Reporter getReporter() {

/**
* Method that does a step in the simulation
*/
public void step() {

 - Throws: IllegalStateException

/**
* A method that checks to see if more steps can be done in the simulation
* @return boolean , true if more steps can happen and false if no more steps can happen
*/
public boolean moreSteps() {

/**
* Method that returns the current step the simulation is on
* @return int , the step the simulation is on
*/
public int getCurrentIndex() {

/**
* Method that gets the color of the passenger
* @return Color , the color returned
*/
public Color getCurrentPassengerColor() {

/**
* Method that returns if a passenger has cleared security
* @return boolean , true if the passenger has cleared security and false if the passenger has not cleared security
*/
public boolean passengerClearedSecurity() {


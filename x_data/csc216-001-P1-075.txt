---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class PreSecurity implements TransitGroup {

private PassengerQueue outsideSecurity ;

public PreSecurity(int number, Reporter name) {

public int departTimeNext() {

public Passenger nextToGo() {

public boolean hasNext() {

public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class CheckPoint {

private int timeWhenAvailable ;

private PassengerQueue line = null;

public CheckPoint() {

public int size() {

public Passenger removeFromLine() {

public boolean hasNext() {

public int departTimeNext() {

public Passenger nextToGo() {

public void addToLine(Passenger name) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class SecurityArea implements TransitGroup{

private static final int MAX_CHECKPOINTS = 0;

private static final int MIN_CHECKPOINTS = 0 ;

private static final String ERROR_CHECKPOINTS = null ;

private static final String ERROR_INDEX = null;

private int largestFastIndex ;

private int tsaPreIndex ;

private ArrayList<CheckPoint> check = new ArrayList<CheckPoint>() ;

public SecurityArea(int number) {

private boolean numGatesOK(int number) {

public void addToLine(int number, Passenger name) {

public int shortestRegularLine() {

public int shortestFastTrackLine() {

public int shortestTSAPreLine() {

public int lengthOfLine(int number) {

public int departTimeNext() {

public Passenger nextToGo() {

public Passenger removeNext() {

private int shortestLineInRange(int number, int number1) {

private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class FastTrackPassenger extends Passenger {

public static final int MAX_EXPECTED_PROCESS_TIME = 330 ;

public static final int MIN_PROCESS_TIME = 20;

private Color color = null ;

public FastTrackPassenger(int number, int number2, Reporter name) {

public void getInLine(TransitGroup name) {

public Color getColor() {

private int pickLane(TransitGroup name) {

@Override
public void getColor(Color name) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class OrdinaryPassenger extends Passenger {

public static final int MAX_EXPECTED_PROCESS_TIME = 150 ;

public static final int MIN_PROCESS_TIME = 20 ;

private Color color = null ;

public OrdinaryPassenger(int number, int number2, Reporter name) {

public void getInLine(TransitGroup name) {

public Color getColor() {

private int pickLane(TransitGroup name) {

@Override
public void getColor(Color name) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public abstract class Passenger {

public static final int MIN_PROCESS_TIME = 0 ;

private int arrivalTime ;

private int waitTime ;

private int processTime ;

private int lineIndex ;

private boolean waitingProcessing ;

private Reporter myLog ;

public Passenger(int arrivalTime, int processTime, Reporter myLog) {

public int getArrivalTime() {

public int getWaitTime() {

public void setWaitTime(int number) {

public int getProcessTime() {

public int getLineIndex() {

public boolean isWaitingInSecurityLine() {

public void clearSecurity() {

protected void setLineIndex() {

public abstract void getInLine(TransitGroup name);

public abstract void getColor(Color name) ;

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class TrustedTraveler extends Passenger{

public static final int MAX_EXPECTED_PROCESS_TIME = 180 ;

public static final int MIN_PROCESS_TIME = 20 ;

private Color color = null ;

public TrustedTraveler(int number, int number2, Reporter name) {

public void getInLine(TransitGroup name) {

public Color getColor() {

private int pickLane(TransitGroup name) {

@Override
public void getColor(Color name) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class EventCalendar {

private TransitGroup highPriority = null ;

private TransitGroup lowPriority = null ;

public EventCalendar(TransitGroup name, TransitGroup name2) {

public Passenger nextToAct() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class Log implements Reporter {

private int numCompleted ;

private int totalWaitTime ;

private int totalProcessTime ;

public Log() {

public int getNumCompleted() {

public void logData(Passenger name) {

public double averageWaitTime() {

public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class Simulator {

private int numPassengers ;

private TransitGroup inTicketing = null ;

private TransitGroup inSecurity = null ;

private EventCalendar myCalender = null ;

private Passenger currentPassenger = null ;

private Reporter log = null ;

public Simulator(int num, int num1, int num2, int num3,int num4) {

private void checkParameters(int num, int num1, int num2, int num3) {

private void setUp(int num, int num1, int num2) {

public Reporter getReporter() {

public void step() {

public boolean moreSteps() {

public int getCurrentIndex() {

public Color getCurrentPassengerColor() {

public boolean passengerClearedSecurity() {


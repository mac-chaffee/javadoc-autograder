---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class created to deal with passengers that have been created 
* but have yet to join a queue for security.
* 
*
*/
public class PreSecurity implements TransitGroup {

private PassengerQueue outsideSecurity;

private Reporter reporter;

/**
* Constructs PreSecurity
* 
* @param numPassengers number of passengers in the simulation
* @param reporter reports data of the passenger
*/
public PreSecurity(int numPassengers, Reporter reporter) {

/**
* Gives the next departure time for the next passenger.
* 
* @return departing time of the passenger
*/
public int departTimeNext() {

/**
* Gives the passenger next to leave PreSecurity
* 
* @return passenger set to leave PreSecurity next
*/
public Passenger nextToGo() {

/**
* Returns if the preSecuirty line has next
* 
* @return true if PreSecurity has next
*/
public boolean hasNext() {

/**
* Removes the next passenger.
* 
* @return Passenger removes the next passenger
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The Checkpoint class
* 
*
*/
public class CheckPoint {

/** Time it will take until this Checkpoint is clear */
private int timeWhenAvailable;

/** The queue of passengers waiting to be processed */
private PassengerQueue line;

/**
* Stuff
*/
public CheckPoint() {

/**
* The size of the checkpoint line.
* 
* @return size of the line
*/
public int size() {

/**
* Removes the passenger from the line.
* 
* @param Passenger passenger removed from the line
* @return Passenger passenger
*/
public Passenger removeFromLine() {

/**
* Returns true if line is not empty.
* 
* 
* @return true if there is at least one passenger in line at this checkpoint
*/
public boolean hasNext() {

/**
* Gives the length of time until the passenger at the front of the line leaves
* the checkpoint area.
* 
* @return the depart time of the next passenger
* 
*/
public int departTimeNext() {

/**
* Returns the passenger in line with the shortest departure time.
* 
* @return passenger with shortest wait time
*/
public Passenger nextToGo() {

/**
* Adds passenger to given line
* 
* @param passenger added
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class SecurityArea implements TransitGroup {

private static final int MIN_CHECKPOINTS = 3;

private static final int MAX_CHECKPOINTS = 15;

private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

private static final String ERROR_INDEX = "Index out of range for this security area.";

private int largestFastIndex;

private int tsaPreIndex;

/**
* Constructs SecurityArea array which contains a checkpoint object in each index.
* 
* @throws IllegalArgumentException if numCheckpoints is out of range
* @param numCheckpoints number of checkpoints
*/
public SecurityArea(int numCheckpoints) {

 - Throws: IllegalArgumentException

private boolean numGatesOK(int numCheckpoints) {

public void addToLine(int idx, Passenger passenger) {

public int shortestRegularLine() {

public int shortestFastTrackLine() {

public int shortestTSAPreLine() {

public int lengthOfLine(int a) {

public int departTimeNext() {

public Passenger nextToGo() {

public Passenger removeNext() {

/**
* Gives the shortest line within a certain range of checkpoint indexes.
* 
* @param lowIndex lowest index
* @param highIndex highest index
* 
* @return integer
*/
private int shortestLineInRange(int lowIndex, int highIndex) {

/**
* Gives index of the next line that is empty
* 
* @return index of checkpoint
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The FastTrackPassenger subclass to Passenger. Creates a FastTrackPassenger object and 
* methods that interact with it.
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** Maximum expected process time for a Fast Track passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** Simulation color of a Fast Track Passenger */
private Color color = Color.RED;

/**
* Constructor of FastTrackPassenger.
* 
* @param arrivalTime arrival time of the passenger.
* @param processTime process time of the passenger.
* @param reporter assigned reporter of the passenger.
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter reporter) {

/**
* Prompts passenger to get in line.
* @param transitGroup the type of passenger 
*/
@Override
public void getInLine(TransitGroup transitGroup) {

/**
* Gives the color of the Passenger in the simulation based 
* on process time of the passenger.
* 
* @return color of the passenger in the simulation
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The OrdinaryPassenger subclass to Passenger. Creates a OrdinaryPassenger object and 
* methods that interact with it.
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum expected process time for a Ordinary passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** Simulation color of a Ordinary Passenger */
private Color color = Color.BLUE;

/**
* Constructor of OrdinaryPassenger.
* 
* @param arrivalTime arrival time of the passenger.
* @param processTime process time of the passenger.
* @param reporter assigned reporter of the passenger.
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter reporter) {

/**
* Prompts passenger to get in line.
* @param transitGroup the type of passenger 
*/
@Override
public void getInLine(TransitGroup transitGroup) {

/**
* Gives the color of the Passenger in the simulation based 
* on process time of the passenger.
* 
* @return color of the passenger in the simulation
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Abstract Passenger class that creates the passengers
*
*/
public abstract class Passenger {

/** The minimum time a passenger is processed. */
public static final int MIN_PROCESS_TIME = 0;

/** Arrival time of the passenger. */
private int arrivalTime;

/** Wait time of the passenger. */
private int waitTime;

/** Process time of the passenger. */
private int processTime;

/** The position in line that a passenger is currently. */
private int lineIndex;

/** Boolean that shows if a passenger is waiting to be processed. */
private boolean waitingProcess;

/**
* Creates the passenger object
* 
* @param arrivalTime time of arrival
* @param processTime process time
* @param reporter reporter assigned
*/
public Passenger(int arrivalTime, int processTime, Reporter reporter) {

/** 
* Assigns the passenger to a security line
* 
* @param transitGroup transit Group
*/
public abstract void getInLine(TransitGroup transitGroup);

/** 
* Returns the color of the passenger 
*
* @return Color color 
*/
public abstract Color getColor();

/**
* Boolean that shows if a passenger is being processed by security.
* 
* @return the waitingProcess
*/
public boolean isWaitingProcess() {

/**
* Boolean that shows if a passenger is waiting in line already.
* 
* @return true if the passenger is waiting
*/
public boolean isWaitingInSecurityLine() {

/**
* Clears the security area.
*/
public void clearSecurity() {

/**
* Gets the minimum process time.
* 
* @return the minProcessTime
*/
public static int getMinProcessTime() {

/**
* Gets the arrival time the passenger has.
* 
* @return the arrivalTime
*/
public int getArrivalTime() {

/**
* Gets the wait time the passenger waits for.
* 
* @return the waitTime
*/
public int getWaitTime() {

/**
* Gets the amount of time the passenger is processed for.
* 
* @return the processTime
*/
public int getProcessTime() {

/**
* Gets the position in line the passenger is at.
* 
* @return the lineIndex
*/
public int getLineIndex() {

/**
* Sets the arrival time.
* 
* @param arrivalTime the arrivalTime to set
*/
public void setArrivalTime(int arrivalTime) {

/**
* Sets the wait time.
* 
* @param waitTime the waitTime to set
*/
public void setWaitTime(int waitTime) {

/**
* Sets the process time.
* 
* @param processTime the processTime to set
*/
public void setProcessTime(int processTime) {

/**
* Sets the line index.
* 
* @param lineIndex the lineIndex to set
*/
protected void setLineIndex(int lineIndex) {

/**
* Sets whether a passenger is waiting or not.
* 
* @param waitingProcess the waitingProcess to set
*/
public void setWaitingProcess(boolean waitingProcess) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The TrustedTraveler subclass to Passenger. Creates a TrustedTraveler object and 
* methods that interact with it.
* 
*
*/
public class TrustedTraveler extends Passenger {

/** Maximum expected process time for a Trusted Traveler passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** Simulation color of a Fast Track Passenger */
private Color color = Color.GREEN;

/**
* Constructor of TrustedTraveler.
* 
* @param arrivalTime arrival time of the passenger.
* @param processTime process time of the passenger.
* @param reporter assigned reporter of the passenger.
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter reporter) {

/**
* Prompts passenger to get in line.
* @param transitGroup the type of passenger 
*/
@Override
public void getInLine(TransitGroup transitGroup) {

/**
* Has the passenger choose between the security lines.
* 
* @param transit the group the passenger is assigned to
* @return int
*/
private int pickLine(TransitGroup transit) {

/**
* Gives the color of the Passenger in the simulation based 
* on process time of the passenger.
* 
* @return color of the passenger in the simulation
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The Event Calendar
* 
*
*/
public class EventCalendar {

/** High priority transit group */
private TransitGroup highPriority;

/** Low priority transit group */
private TransitGroup lowPriority;

/**
* Constructor for EventCalendar
* 
* @param highPriority high priority
* @param lowPriority low priority
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Returns passengers who will be next to exit the group 
* both highPriority and lowPriority
* 
* @throws IllegalStateException if both highPriority and lowPriority = null
* @return Passenger passenger
*/
public Passenger nextToAct() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Logs Passenger info
* 
*
*/
public class Log implements Reporter {

/** Number of passengers cleared. */
private int numCompleted;

/** The total amount of time waiting spent by passengers. */
private int totalWaitTime;

/** The total amount of time passengers were processed. */
private int totalProcessTime;

/**
* Creates a log object
*/
public Log() {

/**
* Gets the number of completed passengers.
* 
* @return numCompleted the amount of completed passengers
*/
public int getNumCompleted() {

/**
* Logs the passenger's data for later use.
* 
* @param passenger passenger
*          
*/
public void logData(Passenger passenger) {

/**
* Calculates average wait time a passenger spent in line.
* 
* @return averageWaitTime of a passenger
*/
public double averageWaitTime() {

/**
* Calculates average process time a passenger spent in line.
* 
* @return averageProcessTime of a passenger
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class Simulator {

/** Minimum allowed Checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** Maximum allowed Checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** Maximum total percentage of Passengers allowed */
private static final int MAX_COMBINED_PERCENT = 100;

/** Number of Checkpoints in the simulation */
private int numCheckpoints;

/** Number of passengers in the simulation */
private int numPassengers;

/** Percentage amount of Trusted Traveler passengers */
private int trustPct;

/** Percentage amount of Fast Track passengers */
private int fastPct;

/** Percentage amount of Ordinary passengers */
private int ordPct;

/** Current passenger */
private Passenger currentPassenger;

/** Checkpoints */
private CheckPoint[] checkpoint;

/** Log */
private Log myLog;

/** Event Calendar */
private EventCalendar myCalendar;

/** If a passenger is in security */
private TransitGroup inSecurity;

/** If a passenger is in ticketing */
private TransitGroup inTicketing;

/**
* Creates a Simulator object
* 
* @param numCheckpoints Number of Checkpoints
* @param numPassengers Number of passengers
* @param trustPct Trusted Traveler Percentage of total passengers
* @param fastPct Fast Track Percentage of total passengers
* @param ordPct Ordinary Passenger Percentage of total passengers
*/
public Simulator(int numCheckpoints, int numPassengers, int trustPct, int fastPct, int ordPct) {

 - Throws: IllegalArgumentException

/**
* Checks Parameters of 
* 
* @param numberOfCheckpoints
* @param numberOfPassengers
* @param trustPct
* @param fastPct
* @param ordPct
*/
private void checkParameters(int numCheckpoints, int numPassengers, int trustPct, int fastPct) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets reporter
* 
* @return reporter
*/
public Reporter getReporter() {

/**
* Creates a step
*/
public void step() {

/**
* Checks to see if more steps are available
* 
* @return true if more steps can be taken
*/
public boolean moreSteps() {

/**
* Returns the index of the security checkpoint line for currentPassenger.
* 
* @return index of passenger
*/
public int getCurrentIndex() {

/**
* Gets passenger color
* 
* @return color of passenger
*/
public Color getCurrentPassengerColor() {

/**
* Sees if passenger is cleared security
* 
* @return true if passenger passed
*/
public boolean passengerClearedSecurity() {


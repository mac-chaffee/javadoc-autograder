---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Manager for holding the information that is then displayed to the GUI
* Utilizes Observer and Observable to notify the GUI and get notified by the contained RaceList
*
*/
public class WolfResultsManager extends Observable implements Observer {

/** File name that the manager reads from */
private String fileName;

/** Boolean for a recent change to the manager. True if recent change */
private boolean changed;

/** List of races for the manager to hold */
private RaceList list;

/** Instance of WolfResultsManager */
private static WolfResultsManager instance;

/**
* Instance getter to insure that only one WolfResultsManager may exist at once
* @return the instance of the WolfResultsManager
*/
public static WolfResultsManager getInstance() {

/**
* Constructor of a new WolfResultsManager
*/
private WolfResultsManager() {

/**
* Creates a new List of races
*/
public void newList() {

/**
* Determines if there has been a recent change to the system
* @return true if this is the case
*/
public boolean isChanged() {

/**
* Private method for changing setChanged
* @param now new state of changed
*/
private void setChanged(boolean now) {

/**
* Getter for the filename
* @return the filename that Manager is currently operating
*/
public String getFilename() {

/**
* Setter for the filename
* @param fileName the new file name
*/
public void setFilename(String fileName) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Loads the file into the manager, sets up the list for use
* @param file file for loading
*/
public void loadFile(String file) {

/**
* Saves the file to the given filename
* @param file the file that information is being saved to
*/
public void saveFile(String file) {

/**
* Getter for the race list that manager is operating
* @return the race list
*/
public RaceList getRaceList() {

/**
* Updates the manager with the given information
* @param observe observable for manipulating
* @param obj object for manipulation
*/
public void update(Observable observe, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Input class for the input/output package. Reads in races from a file
*
*/
public class WolfResultsReader {

/**
* Reads in races from a file, and outputs the races in a RaceList
* @param fileName file for reading in
* @return the RaceList of races
*/
public static RaceList readRaceListFile(String fileName) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Output for the input/output packaged. Outputs the a list of races into a file.
*
*/
public class WolfResultsWriter {

/**
* Takes a RaceList and outputs the list of races into the given file name
* @param fileName output file
* @param list list of races
*/
public static void writeRaceFile(String fileName, RaceList list) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates an individual result which
* is passed to the entire result list
*/
public class IndividualResult implements Comparable<IndividualResult>, Observer {

/** The racer's name */
private String name;

/** The racer's age */
private int age;

/** The racer's time */
private RaceTime time;

/** The racer's race */
private Race race;

/** The racer's pace */
private RaceTime pace;

/**
* Creates an individual result - or a racer
* with a specific race, name, age, and time. 
* The name is trimmed of any leading or trailing 
* whitespace and the pace is calculated. An
* observer is also added for the race
* Using String.trim() for trimming whitespace
* @param race the race the racer is racing
* @param name the racer's name
* @param age the racer's age
* @param time the racer's racing time
* @throws IllegalArgumentException for the following:
* race = null, name = null or "" or whitespace, 
* age less than 0, time = null
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

/**
* Error checking for parameters for an IndividualResult object
* @param r the race parameter
* @param n the name parameter
* @param a the age parameter
* @param t the time parameter
* @throws IllegalArgumentException for the following:
* race = null, name = null or "" or whitespace, 
* age less than 0, time = null
*/
private void errorCheck(Race r, String n, int a, RaceTime t) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Getter method for the racer's race
* @return the racer's race
*/
public Race getRace() {

/**
* Getter method for the racer's name
* @return the racer's name
*/
public String getName() {

/**
* Getter method for the racer's age
* @return the racer's age
*/
public int getAge() {

/**
* Getter method for the racer's racing time
* @return the racer's racing time
*/
public RaceTime getTime() {

/**
* Getter method for the racer's pace, 
* calculated by: Math.floor(time / distance)
* @return the racer's pace
*/
public RaceTime getPace() {

/**
* Setter method for the 
* individual result's pace
*/
private void setPace() {

/**
* Compares two individual results based on their
* times. Delegates to Racetime's compareTo method
* @param other the Individual Result to compare to
* @return an integer representation of the sizes for
* this.compareTo(other):
* less than 0: this smaller than other
* 0: this equal to other
* greater than 0: this bigger than other
*/
@Override
public int compareTo(IndividualResult other) {

/**
* Returns a string representation of the
* individual result as 
* IndividualResult [name=NAME, age=AGE, time=TIME, pace=PACE]
* @return a string of the individual result
*/
public String toString() {

/**
* Update the pace if the Race of this IndividualResult
* is changed in any way
* @param obvs the Observer to check for change
* @param obj the object to change the pace of
*/
public void update(Observable obvs, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A race object that contains racers
* and generic methods to return race
* information
*/
public class Race extends Observable {

/** The race name */
private String name;

/** The race distance */
private double distance;

/** The race date */
private LocalDate date;

/** The race location */
private String location;

/** The race results */
private RaceResultList results;

/**
* Creates a race object with the given parameters.
* Name and location have their whitespace trimmed
* @param name the race's name
* @param distance the race's distance
* @param date the race's date
* @param location the race's location
* @param results the race's results
* @throws IllegalArgumentException for the following:
* name is null, "", or whitespace; distance is less than 0;

* date is null; location is null; results is null;

*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

/**
* Provides error checking for race parameters
* @param name the race's name
* @param distance the race's distance
* @param date the race's date
* @param location the race's location
* @param results the race's results
* @throws IllegalArgumentException for the following:
* name is null, "", or whitespace; distance is less than 0;

* date is null; location is null; results is null;

*/
private void errorCheck(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Calls upon the previous constructor to create a race 
* object with the given parameters. A new RaceResultList
* is passed as the race has no runners yet
* @param name the race's name
* @param distance the race's distance
* @param date the race's date
* @param location the race's location
* @throws IllegalArgumentException with the above information
*/
public Race(String name, double distance, LocalDate date, String location) {

/**
* Getter method for the race's name
* @return the race's name
*/
public String getName() {

/**
* Getter method for race's distance
* @return the race's distance
*/
public double getDistance() {

/**
* Getter method for the race's date
* @return the race's date
*/
public LocalDate getDate() {

/**
* Getter method for the race's location
* @return the race's location
*/
public String getLocation() {

/**
* Getter method for the race's results
* @return the race's results
*/
public RaceResultList getResults() {

/**
* Adds the result to the list of results
* The race observer is notified of the change
* @param result the result to add to the list
*/
public void addIndividualResult(IndividualResult result) {

/**
* Sets the new distance. The race observer is
* notified of the change
* @param distance the new distance to set
* @throws IllegalArgumentException if the distance
* is less than 0
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* Return a list of results if the runner information
* is between the provided parameters
* @param minAge the runner's min age to include
* @param maxAge the runner's max age to include
* @param minPace the runner's min pace to include
* @param maxPace the runner's max pace to include
* @return a list of results with runners corresponding to 
* the above information
* @throws IllegalArgumentException if the minPace or maxPace
* is not a valid RaceTime
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

*/
/**
* Returns a string representation of a race
* @return a race as a string
*/
@Override
public String toString() {

/**
* Generates a hashcode for the race
*/
@Override
public int hashCode() {

/**
* Checks to see if two races are equal
* @return true if they are equal and 
* false if they are not equal
*/
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A list of races as an array list type. Contains
* generic methods for an array list
*/
public class RaceList extends Observable implements Observer {

/** Holds an array list of races */
private ArrayList races;

/**
* Creates a list of races in the form
* of an array list
*/
public RaceList() {

/**
* Adds a race to the list and creates
* an observer for the race. The RaceList 
* observers are notified of the change
* @param race the race to add to the list
* @throws IllegalArgumentException if the 
* race is null
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

/**
* Adds a race, created from the 
* parameters, to the list and creates
* an observer for the race. Passed to 
* the above constructor for error 
* checking and adding to the list
* @param name the race's name
* @param distance the race's distance
* @param date the race's date
* @param location the race's location
*/
public void addRace(String name, double distance, LocalDate date, String location) {

/**
* Removes the race at the given index. 
* RaceList observers are notified of change
* @param index the index to remove
*/
public void removeRace(int index) {

/**
* Getter method to return list size
* @return the number of races in the list
*/
public int size() {

/**
* Returns a race at the specific index
* @param index the index of the race in the list
* @return the race at that index
*/
public Race getRace(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Called if the Race has notified
* Observers of a change. If Observable is instance
* of Race, observers of RaceList are updated. Current
* instance is passed to notifyObservers()
* @param obvs the Observable to observe if any change
* is notified
* @param obj the Object to observe and update
*/
public void update(Observable obvs, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The list containing IndividualResult 
* objects with generic list methods
*/
public class RaceResultList {

/** The list of IndividualResults */
private SortedLinkedList<IndividualResult> list;

/**
* Creates a list of IndividualResults 
* as a sorted linked list
*/
public RaceResultList() {

/**
* Adds a result to the list
* @param result the IndividualResult to add 
* @throws IllegalArgumentException if the result is null
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* Adds a result to the list. Constructs the result
* from the parameters then is added. 
* @param race the racer's race
* @param name the racer's name
* @param age the racer's age
* @param time the racer's time
* @throws IllegalArgumentException if the IndividualResult
* constructor throws exception -- error checking is passed
* to IndividualResult
*/
public void addResult(Race race, String name, int age, RaceTime time) {

/**
* Returns the IndividualResult at the given index
* @param index the list index's data to return
* @return the IndividualResult at the index
* @throws IndexOutOfBoundsException if index is out of bounds
*/
public IndividualResult getResult(int index) {

/**
* Getter method for list size
* @return the number of elements in the list
*/
public int size() {

/**
* Return a 2D array of the results with the following info:
* index 0: name, index 1: age, index 2: time, index 3: pace
* @return the 2D array with the racer information
*/
public String[][] getResultsAsArray() {

/**
* Return a list of results if the runner information
* is between the provided parameters
* @param minAge the runner's min age to include
* @param maxAge the runner's max age to include
* @param minPace the runner's min pace to include
* @param maxPace the runner's max pace to include
* @return a list of results with runners corresponding to 
* the above information
* @throws IllegalArgumentException if the minPace or maxPace
* is not a valid RaceTime
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Custom ArrayList for use in the WolfResults project
*
*/
public class ArrayList implements List, Serializable {

/**
* Version ID for the ArrayList
*/
private static final long serialVersionUID = 1L;

/**
* If the list needs to be resized, this is the default value
*/
private static final int RESIZE = 10;

/**
* The Array for the ArrayList
*/
private Object[] list;

/**
* Size of the contained elements within the Array, not length of the Array itself
*/
private int size;

/**
* Constructor for ArrayList, with a default size
*/
public ArrayList() {

/**
* Constructor for the ArrayList, with a given max size
* @param size maximum size for the ArrayList
*/
public ArrayList(int size) {

 - Throws: IllegalArgumentException

/**
* Getter for the size of the array list
* @return size of the ArrayList
*/
@Override
public int size() {

/**
* Determines if the ArrayList is empty
* @return true if empty
*/
@Override
public boolean isEmpty() {

/**
* Determines if the ArrayList contains the given object
* @param o object for determination
* @return true if in list
*/
@Override
public boolean contains(Object o) {

/**
* Adds an object to the end of the list
* @param o object for adding
* @return true if the object has been added
*/
@Override
public boolean add(Object o) {

 - Throws: IllegalArgumentException

 - Throws: NullPointerException

/**
* Returns the object at the specified index
* @param index index of object
* @return the object in the index
*/
@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Adds an object at the specified index
* @param index index of new object
* @param element element to be added
*/
@Override
public void add(int index, Object element) {

 - Throws: IllegalArgumentException

 - Throws: NullPointerException

 - Throws: IndexOutOfBoundsException

/**
* Removes the object at the given index
* @param index index of removal
* @return the removed object
*/
@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the given object or -1 if not in the list
* @param o object in list
* @return the index of object
*/
@Override
public int indexOf(Object o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* see Collection
* see Set
* see ArrayList
* see LinkedList
* see Vector
* see Arrays#asList(Object[])
* see Collections#nCopies(int, Object)
* see Collections#EMPTY_LIST
* see AbstractList
* see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true (as specified by {link Collection#add})
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index less than 0
*             or index greater than and size())
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index less than 0
*             or index greater than and size())
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index less than 0
*             or index greater than and size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class for representing a Race time in the format of hh:mm:ss
*
*/
public class RaceTime implements Comparable<RaceTime> {

/**
* Hours for a given time
*/
private int hours;

/**
* Minutes for a given time
*/
private int minutes;

/**
* Seconds for a given time
*/
private int seconds;

/**
* Generates a race time given hours, minutes, and seconds as integers
* @param hrs hours
* @param min minutes
* @param sec seconds
*/
public RaceTime(int hrs, int min, int sec) {

 - Throws: IllegalArgumentException

/**
* Generates a race time using a specified String format
* @param time String for generating a time
*/
public RaceTime(String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Getter for hours
* @return hours
*/
public int getHours() {

/**
* Getter for minutes
* @return minutes
*/
public int getMinutes() {

/**
* Getter for seconds
* @return seconds
*/
public int getSeconds() {

/**
* Gets the total time in just seconds
* @return total time
*/
public int getTimeInSeconds() {

/**
* Returns a String of the time
* @return time in String form
*/
public String toString() {

/**
* Compares the given RaceTime to another RaceTime
* @param time RaceTime for comparison
* @return the result of the comparison
*/
public int compareTo(RaceTime time) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Custom SortedLinkedList for use within Project 3
*
* @param <E> type of object for the List
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/**
* Inner class representing a singular node within the linked list
*
* @param <E> type of object for the node to generate
*/
@SuppressWarnings("hiding")
public class Node<E extends Comparable<E>> {

/**
* What the node stores as information
*/
private E value;

/**
* The next node within the chain
*/
private Node<E> next;

/**
* Constructor of a new node within the sequence
* @param data what the node represents
* @param future the next node in the sequence
*/
public Node(E data, Node<E> future) {

/**
* Hash code override for the Node
* @return the hash code
*/
@Override
public int hashCode() {

/**
* Equals method for the Node
* @param obj node for comparison
* @return true if objects are equal
*/
@Override
public boolean equals(Object obj) {

@SuppressWarnings("unchecked")
/**
* Front node of the SortedLinkedList
*/
private Node<E> head;

/**
* Size of the list
*/
private int size;

/**
* Constructor of a new LinkedList, creates an initial front node
*/
public SortedLinkedList() {

/**
* Getter for the size of the LinkedList
* @return the size of the List
*/
@Override
public int size() {

*/
/**
* Determines if the list is empty or not
* @return true if the list is empty
*/
@Override
public boolean isEmpty() {

/**
* Determines if the current list contains the given element
* @param e element for the comparison
* @return true if the element is in the list
*/
@Override
public boolean contains(E e) {

/**
* Adds the element to the end of the list
* @param e element to be added
* @return true if added
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
@Override
public boolean add(E e) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Gets the element at the specified index
* @param index index of the desired element
* @return the element at the index
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Removes the element at the current index
* @param index index of the desired element
* @return the element that's been removed
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the given element
* @param e element to be found
* @return the index of the element
*/
@Override
public int indexOf(E e) {

/**
* Hash code generator for the SortedLinkedList
* @return the hash code
*/
@Override
public int hashCode() {

/**
* Determines if the given LinkedList is equal to another LinkedList
* @param obj List for comparison
* @return true if the given LinkedList is the same as this one
*/
@SuppressWarnings("unchecked")
@Override
public boolean equals(Object obj) {

/**
* Generates a string for the list
* @return String of the list
*/
@Override
public String toString() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
* 
* @param <E> the generic type for the sorted list
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

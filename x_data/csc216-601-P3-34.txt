---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*
*/
public class WolfResultsManager extends Observable implements Observer{

private static WolfResultsManager instance;

private RaceList list;

private String filename;

private boolean changed;

/**
* Constructs the WolfResultsManager by creating an empty RaceList and registering as an 
* Observer of the list.
*/
private WolfResultsManager() {

/**
* Returns the single WolfResultsManager instance. If the instance is null, a WolfResultsManager is constructed.
* @return the active instance of the WolfResultsManager.
*/
public static WolfResultsManager getInstance() {

/**
* Creates a new empty RaceList and registers the WolfResultsManager as an Observer. The method 
* notifies observers of the change if no exception is thrown.
*/
public void newList() {

/**
* Returns the RaceList. Use this method to access the RaceList an all of its contents from the GUI.
* @return the list
*/
public RaceList getRaceList() {

/**
* @param list the list to set
*/
public void setList(RaceList list) {

/**
* @return the filename
*/
public String getFilename() {

/**
* @param filename the filename to set
*/
public void setFilename(String filename) {

/**
* @return the changed
*/
public boolean isChanged() {

/**
* @param changed the changed to set
*/
private void setChanged(boolean changed) {

/**
*  If a RaceList observed by WolfResultsManager changes, the update() method is automatically 
*  called. WolfResultsManager should propagate the notification of the change to its Observers 
*  and changed should be updated to true.
*/
@Override
public void update(Observable o, Object arg) {

/**
* Reads the WolfResults from the given filename and adds the WolfResultsManager as an observer 
* to the created RaceList. The filename field is set to the parameter value (if valid). Sets 
* the changed flag to false. The method notifies observers of the change if no exception is 
* thrown
* @param filename
*/
public void loadFile(String filename) {

/**
* Saves the RaceList to the given filename. Sets the changed flag to false.
* @param filename the path to the file
*/
public void saveFile(String filename) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*
*/
public class WolfResultsReader {

private Race race;

private RaceList races;

/**
* Returns a RaceList from the given file. The file is processed as described in Use Case 2. An 
* IllegalArgumentException is thrown on any error or incorrect formatting.
* @param filename the path to the file containing the data
* @return the list of Race objects read from the file
* @throws FileNotFoundException if there is a problem reading the file
*/
public static RaceList readRaceListFile(String filename) {

private void processResult(String line) {

private void processRace(String nameLine, String distString, String dateString, String location) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Creates an data file of type md with the passed race informaiton.
*
*/
public class WolfResultsWriter {

/**
* writes the given RaceList to the given file. The file is created as described in Use Case 3. 
* An IllegalArgumentException is thrown on any error.
* @param filename the path to the file to save the race list to
* @param list the list of races to be saved
* @throws FileNotFoundException thrown whenever the method encounters a problem writing a file
*/
public static void writeRaceFile(String filename, RaceList list) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class is a representation of one persons race result for a single race in the
*  WolfResultsManager application.
*  
*
*/
public class IndividualResult implements Comparable<IndividualResult>, Observer {

/** the number of seconds in an minute */
private static final int SECONDS_PER_MINUTE = 60;

/** the number of minutes in an hour */
private static final int MINUTES_PER_HOUR = 60;

/** the Race this results is for */
private Race race;

/** the racer's name */
private String name;

/** the racers age */
private int age;

/** time per mile that the racer averaged during the Race */
private RaceTime pace;

/** the total time in which the racer completed the coruse */
private RaceTime time;

/**
* Creates a new IndividualResult object. The constructor should throw an 
* IllegalArgumentException if (a) race is null; (b) name is null, empty string, or all 
* whitespace; (c) age is negative; or (d) time is null. name should be trimmed of leading 
* and/or trailing whitespace. The pace field should be set to the pace based on time and race 
* distance. An Observer should be added for race.
* 
* @param race the Race object to watch with this result
* @param name the name of the racer
* @param age the age of the racer
* @param time the time of race was completed in
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

/**
* @return the race
*/
public Race getRace() {

/**
* Sets the Race this result is for.  Throw an IllegalArgumentException if the Race is null.
* @param race the race to set
*/
private void setRace(Race race) {

 - Throws: IllegalArgumentException

/**
* @return the name
*/
public String getName() {

/**
* Sets the name of the racer.  Throws an IllegalArgumentException if the name is null, empty
* string, or all whitespace.  Name will be trimmed of all leading and trailing whitespace
* @param name the name to set
*/
private void setName(String name) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* @return the age
*/
public int getAge() {

/**
* Sets the age of the racer.  Throws an IllegalArgumentException if the age is negative.
* 
* @param age the age to set
*/
private void setAge(int age) {

 - Throws: IllegalArgumentException

/**
* Returns the pace the racer performed during the last race
* @return the pace
*/
public RaceTime getPace() {

/**
* Calculates the pace from the Race time and distance information.
*
*/
private void setPace() {

/**
* @return the time
*/
public RaceTime getTime() {

/**
* Sets the time the user race that race in.  Throws an IllegalArgumentException if the time
* is null.
* 
* @param time the time to set
*/
private void setTime(RaceTime time) {

 - Throws: IllegalArgumentException

* @see java.lang.Object#toString()
*/
@Override
public String toString() {

@Override
public void update(Observable o, Object arg) {

/**
* Compares Two IndividualResult objects are using their times. This method will delegate to 
* RaceTimes compareTo method.
*/
@Override
public int compareTo(IndividualResult o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class is the representation of a Race in the WolfResultsManager application.
*
*/
public class Race extends Observable {

/** the title of the Race */
private String name;

/** the total length of the Race's course */
private double distance;

/** the date the Race took place */
private LocalDate date;

/** the place where the race occurred */
private String location;

/** list of results for all the runners */
private RaceResultList results;

/**
* Creates a new Race object with the passed information. The constructor should throw an 
* IllegalArgumentException if 
* (a) name is null, empty string, or all whitespace;

* (b) distance is non-positive;

* (c) date is null; or
* (d) location is null.
* name and location should be trimmed of leading and/or trailing whitespace.
* 
* @param name title of the Race
* @param distance the total length of the Race's course 
* @param date the date the Race took place
* @param location the place where the race occurred
*/
public Race(String name, double distance, LocalDate date, String location) {

/**
* Creates a new Race object with the passed information. The constructor should throw an 
* IllegalArgumentException if 
* (a) name is null, empty string, or all whitespace;

* (b) distance is non-positive;

* (c) date is null;

* (d) location is null; or 
* (e) results is null. 
* name and location should be trimmed of leading and/or trailing whitespace.
*  
* @param name title of the Race
* @param distance the total length of the Race's course 
* @param date the date the Race took place
* @param location the place where the race occurred
* @param results list of results for all the runners
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

notifyObservers(this); //Sends a message to any Observer classes that the object has changed.
* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

/**
* Compares two objects to find out if two objects are equal
* 
* @param obj the item to be compared to this Race
*/
@Override
public boolean equals(Object obj) {

/**
* Returns the string representation of an Race is the name, distance, date, and location fields.
* 
*  @return a string with the race details in a CSV
*/
@Override
public String toString() {

public void addIndividualResult(IndividualResult result) {

notifyObservers(this); //Sends a message to any Observer classes that the object has changed.
/**
* What is the name of the Race?
* @return the name
*/
public String getName() {

/**
* The constructor should throw an llegalArgumentException if 
* (a) name is null, empty string, or all whitespace
* name should be trimmed of leading and/or trailing whitespace.
* 
* @param name the name to set
*/
private void setName(String name) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* How long was the Race course?
* 
* @return the distance
*/
public double getDistance() {

/**
* Sets the distance for the race.  The constructor should throw an 
* IllegalArgumentException if 
* (b) distance is non-positive;

* Updates the observers when the value is changed
* 
* @param distance the distance to set
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

notifyObservers(this); //Sends a message to any Observer classes that the object has changed.
/**
* What was the date the Race occured?
* @return the date
*/
public LocalDate getDate() {

/**
* Sets the date with a new Race object with the passed information. The constructor should throw an 
* IllegalArgumentException if  
* (c) date is null;

* 
* @param date the date to set
*/
private void setDate(LocalDate date) {

 - Throws: IllegalArgumentException

/**
* Where did the Race happen?
* @return the location
*/
public String getLocation() {

/**
* Sets the location for a new Race object with the passed information. The constructor should throw an 
* IllegalArgumentException if 
* (d) location is null;

* location should be trimmed of leading and/or trailing whitespace.
* @param location the location to set
*/
private void setLocation(String location) {

 - Throws: IllegalArgumentException

/**
* @return the results
*/
public RaceResultList getResults() {

/**
* Sets the results for a new Race object with the passed information. The constructor should throw an 
* IllegalArgumentException if 
* (e) results is null.
* @param results the results to set
*/
private void setResults(RaceResultList results) {

 - Throws: IllegalArgumentException

/**
* Filters the returned race information based on the passed information.  Returns list 
* of results such that runners age is between minAge and maxAge (inclusive) and runners pace 
* is between minPace and maxPace (inclusive).
* 
* Throws IllegalArgumentException if minPace or maxPace is not a valid RaceTime. 
* 
* @param minAge
* @param maxAge
* @param minPace
* @param maxPace
* @return
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*
*/
public class RaceList extends Observable implements Observer {

/** the list of races that have been or will be run */
private ArrayList<Race> races;

/**
* Constructs the ArrayList object.
*/
public RaceList() {

/**
* Adds race to list. Observer is added for the race. Observers of RaceList are notified of the change.
* The method throws IllegalArgumentException if race is null.
* 
* @param race the Race to be added to the list
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

notifyObservers(this); //Sends a message to any Observer classes that the object has changed.
/**
* Adds race to list. Observer is added for the race. Observers of RaceList are notified of the change.
* The method constructs a Race using the provided parameters and throws IllegalArgumentException 
* if Race constructor throws exception.
* 
* @param name title of the Race
* @param distance the total length of the Race's course 
* @param date the date the Race took place
* @param location the place where the race occurred
*/
public void addRace(String name, double distance, LocalDate date, String location) {

/**
* Removes race at index idx. Observers of RaceList are notified of the change.
* @param idx the index of the item to remove
* @return the race that was removed
*/
public Race removeRace(int idx) {

/**
* Gets race at index idx. Throws IndexOutOfBoundsException if idx is out of bounds.
* @param idx the index of the item to return
* @return 
*/
public Race getRace(int idx) {

/**
* Returns number of races in list.
* @return the number of races in the lsit
*/
public int size() {

/**
* The RaceList.update() method is called if the Race that the RaceList is observing notified 
* its observers of a change. If the Observable is an instance of Race, then the observers of 
* the RaceKust should be updated. The current instance is passed to notifyObservers().
* 
* @param o the object that is be
*/
@Override
public void update(Observable o, Object arg) {

notifyObservers(this); //Sends a message to any Observer classes that the object has changed.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This represents a group of RaceResults that are stored in a list and sort based on time.
* 
*
*/
public class RaceResultList {

/** the number of elements to be put into the array */
private static final int ARRAY_SIZE = 4;

/** the sorted list of race results */
private SortedLinkedList<IndividualResult> results;

/**
* Constructs a new RaceResultList object.  This will instantiate a SortedLinkedList that will
* hold the IndividualResult objects.
*/
public RaceResultList() {

/**
* Adds a new IndividualResult to the list.  The method throws an IllegalArgumentException if the
* result is null.
* 
* @param result the IndividualResult to be added to the list
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* Adds the information to the list as a new IndividualResult.  The method constructs an 
* IndividualResult using the provided parameters and throws IllegalArgumentException if 
* IndividualResult constructor throws exception. Adds race to list.
* 
* @param race the Race object associated with the results
* @param name the name of the racer
* @param age the age of the racer
* @param time the time racer took to complete the course
*/
public void addResult(Race race, String name, int age, RaceTime time) {

public int size() {

public IndividualResult getResult(int index) {

/**
* Returns a 2D array of string representing results. First dimension is the result/runner 
* (in sorted order by time). Second dimension are the different fields: 
* [0]: name, [1]: age, [2]: time, [3]: pace. 
* If the list is empty, an empty 2D array is returned.
* 
* @return a 2D array of Strings representing the results in sorted by time
*/
public String[][] getResultsAsArray() {

public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class defines the behaviors for a custom version of a custom List interface.  The underlying
* foundation is based on an array
*
*/
public class ArrayList<E> implements List {

/**  ID used for serialization */
private static final long serialVersionUID = 1L;

/** the number of elements to add to the array when resizing is required */
private static final int RESIZE = 10;

/** the data structure that holds our array of Objects */
private Object[] list;

/** the number of objects in our list */
private int size;

/**
* Creates an empty List (e.g., size is zero) of Objects with a list of capacity RESIZE. 
*  
*/
public ArrayList() {

/**
* Creates an empty List (e.g., size is zero) of Objects with a list of capacity given by the
* size parameter.
* 
* @param size the initial capacity of the List
*/
public ArrayList(int size) {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
@Override
public int size() {

/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
@Override
public boolean isEmpty() {

/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
@Override
public boolean contains(Object o) {

/**
* Appends the specified element to the end of this list 
*
* @param o element to be appended to this list
* @return true 
*/
@Override
public boolean add(Object o) {

/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index less then 0
*             || index = size())
*/
@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index greater than size())
*/
@Override
public void add(int index, Object element) {

 - Throws: IndexOutOfBoundsException

/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index = size())
*/
@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
@Override
public int indexOf(Object o) {

/**
* Increases the size of the underlying list
*/
private void resize() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* @param o element to be appended to this list
* @return true
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index less than 0
*             || index greater than or equal to size())
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index &gt;= size())
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index &gt;= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class RaceTime implements Comparable<RaceTime> {

/** the number of seconds in an minute */
private static final int SECONDS_PER_MINUTE = 60;

/** the number of minutes in an hour */
private static final int MINUTES_PER_HOUR = 60;

private int hours;

private int minutes;

private int seconds;

public RaceTime(String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* @param hours
* @param minutes
* @param seconds
*/
public RaceTime(int hours, int minutes, int seconds) {

/**
* @return the hours
*/
public int getHours() {

/**
* Sets the number of hours in the object;  Throws an IllegalArgumentException if the value
* of hours is &lt; 0
* @param hours the hours to set
*/
private void setHours(int hours) {

 - Throws: IllegalArgumentException

/**
* returns the number of minutes in the object
* @return the minutes
*/
public int getMinutes() {

/**
* Sets the number of minutes in the object;  Throws an IllegalArgumentException if the value
* of hours is &lt; 0 or &gt;= 60
* @param minutes the minutes to set
*/
private void setMinutes(int minutes) {

 - Throws: IllegalArgumentException

/**
* @return the seconds
*/
public int getSeconds() {

/**
* Sets the number of minutes in the object;  Throws an IllegalArgumentException if the value
* of hours is &lt; 0 or &gt;= 60
* 
* @param seconds the seconds to set
*/
private void setSeconds(int seconds) {

 - Throws: IllegalArgumentException

public int getTimeInSeconds() {

/**
* RaceTimes are compared based on their total time.
* @param o the other RaceTime object to be compared to
* @return 
*/
@Override
public int compareTo(RaceTime o) {

 - Throws: NullPointerException

/**
* Returns the RaceTime as a string with the format hh:mm:ss, where (a) hours may be listed as 
* one digit or many digits with leading zeros and (b) minutes and seconds are always two digits
* between 0 and 59 (inclusive).
* @return the time as a string
*/
@Override
public String toString() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
*
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/** the number of items in the list */
private int size;

/** the first node in the list AKA node @ 0 */
private Node<E> head;

/**
* Creates an empty SortedLinkedList ready to store elements of the type specified
*/
public SortedLinkedList() {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
* */
@Override
public int size() {

/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
@Override
public boolean isEmpty() {

/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
@Override
public boolean contains(E e) {

/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
@Override
public boolean add(E e) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index &gt;= size())
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index &gt;= size())
*/
@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
@Override
public int indexOf(E e) {

/**
* Creates a new node for inclusion in the list
* 
*
*/
private class Node<E> {

private E value;

private Node next;

public Node(E value, Node<E> next) {

* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index &gt;= size())
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index &lt; 0
*             || index &gt;= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class representing passengers who have not
* yet entered security checkpoint lines
*
*/
public class PreSecurity implements TransitGroup {

/** The queue of passengers that are outside of security */
private PassengerQueue outsideSecurity;

/**
* Constructor for a PreSecurity object
* @param numPassengers the starting number of passengers
* @param log the Reporter to be used
*/
public PreSecurity(int numPassengers, Reporter log) {

 - Throws: IllegalArgumentException

/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
@Override
public int departTimeNext() {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
@Override
public Passenger nextToGo() {

/**
* Returns true if there is a next passenger to enter security checkpoint lines
* @return true if there is a next passenger to enter security checkpoint lines
*/
public boolean hasNext() {

/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
@Override
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class to represent a singular security Checkpoint 
*
*/
public class CheckPoint {

/** The time when this given checkpoint is available*/
private int timeWhenAvailable;

private PassengerQueue line;

/**
* Constructor method for a Checkpoint
*/
public CheckPoint() {

/**
* Returns the size of the line at this Checkpoint
* @return The size of the line at this Checkpoint
*/
public int size() {

/**
* Method to remove a passenger from the line
* @return The passenger removed from the line
*/
public Passenger removeFromLine() {

/**
* Returns true if the line at the Checkpoint has a next passenger
* @return True if the line at the Checkpoint has a next passenger
*/
public boolean hasNext() {

/**
* Returns the departure time of the next passenger
* @return the departure time of the next passenger
*/
public int departTimeNext() {

/**
* Returns the next passenger set to go through the Checkpoint
* @return the next passenger set to go through the Checkpoint
*/
public Passenger nextToGo() {

/**
* Adds a passenger to the line at this Checkpoint
* @param p The passenger to be added to the line at this Checkpoint
*/
public void addToLine(Passenger p) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class to represent the security area and
* all of the checkpoints in it
*/
public class SecurityArea implements TransitGroup {

/** The maximum number of checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** The minimum number of checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** The error message to be displayed when there is an error in the checkpoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** The error message to be displayed when there is an error in the index
*  of the checkpoints */
private static final String ERROR_INDEX = "Index out of range for this security area" ;

/** The largest index of a FastTrack line */
private int largestFastIndex;

/** The index of a TSA PreCheck line */
private int tsaPreIndex;

/** The collection of CheckPoints */
private ArrayList<CheckPoint> check;

/**
* Constructor of the SecurityArea
* @param numCheckpoints The number of checkpoints
*/
public SecurityArea(int numCheckpoints) {

 - Throws: IllegalArgumentException

/**
* Method to determine whether or not the given number of gates is OK
* @param gates the given number of gates
* @return true if the given number of gates is OK
*/
private boolean numGatesOK(int gates) {

/**
* Method to add a Passenger to a checkpoint line
* @param idx the index of the line to which to add the passenger
* @param pass the passenger to add
*/
public void addToLine(int idx, Passenger pass) {

/**
* Method to return the index of the shortest normal line
* @return the index of the shortest normal line
*/
public int shortestRegularLine() {

/**
* Method to return the index of the shortest FastTrack line
* @return the index of the shortest FastTrack line
*/
public int shortestFastTrackLine() {

/**
* Method to return the index of the shortest TSA PreCheck line
* @return the index of the shortest TSA PreCheck line
*/
public int shortestTSAPreLine() {

/**
* Method to return the length of a given line
* @param line the index of the given line
* @return the length of the given line
*/
public int lengthOfLine(int line) {

 - Throws: IllegalArgumentException

@Override
public int departTimeNext() {

@Override
public Passenger nextToGo() {

@Override
public Passenger removeNext() {

/**
* Method to return the shortest line in a given range
* @param min the minimum value of the given range
* @param max the maximum value of the given range
* @return the index of the shortest line in the given range
*/
private int shortestLineInRange(int min, int max) {

/**
* Method to return the index of the line with the next passenger to clear security
* @return the index of the line with the next passenger to clear security
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class to represent a FastTrackPassenger object
*
*/
public class FastTrackPassenger extends Passenger {

/** The maximum expected process time for a FastTrackPassenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** The color of the passenger in the simulation */
private Color color;

/**
* Constructor for a FastTrackPassenger object
* @param log the Reporter to be used for this object
* @param waitTime the wait time for this passenger
* @param processTime the generated process time for this passenger
* 
*/
public FastTrackPassenger(int processTime, int waitTime, Reporter log) {

/**
* Method to tell this passenger to get in line
* @param t the TransitGroup to be used for this passenger
*/
public void getInLine(TransitGroup t) {

/**
* Method to return the color for this passenger in the simulation
* @return the color for this passenger in the simulation
*/
public Color getColor() {

/**
* Method to choose a line for this passenger to enter
* @param t the TransitGroup to be used for this passenger
* @return the line chosen by the passenger
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class to represent an OrdinaryPassenger object
*
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum expected processing time for an ordinary passenger*/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** The color of the passenger in the simulation */
private Color color;

/**
* Constructor for an OrdinaryPassenger object
* @param log the Reporter to be used for this object
* @param waitTime the wait time for this passenger
* @param processTime the generated process time for this passenger
* 
*/
public OrdinaryPassenger(int processTime, int waitTime, Reporter log) {

/**
* Method to return the color for this passenger in the simulation
* @return the color for this passenger in the simulation
*/
public Color getColor() {

/**
* Method to tell this passenger to get in line
* @param t the TransitGroup to be used for this passenger
*/
public void getInLine(TransitGroup t) {

/**
* Method to choose a line for this passenger to enter
* @param t the TransitGroup to be used for this passenger
* @return the line chosen by the passenger
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Abstract class to represent a Passenger object
*
*/
public abstract class Passenger {

/** Minimum process time for all passengers*/
public static final int MIN_PROCESS_TIME = 0;

/** The arrival time for this passenger*/
private int arrivalTime;

/** The time that this passenger has to wait */
private int waitTime;

/** The generated process time for this passenger */
private int processTime;

/** The index of the line that this passenger is in */
private int lineIndex;

/** Boolean value to tell whether or not the passenger is currently waiting*/
private boolean waitingProcessing;

/** The Reporter to be used for this project */
private Reporter myLog;

/**
* Constructor for a Passenger object
* @param log the Reporter to be used for this object
* @param processTime the wait time for this passenger
* @param arrivalTime the generated process time for this passenger
* 
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

/**
* Getter method for the arrival time for this passenger
* @return the arrivalTime
*/
public int getArrivalTime() {

/**
* Getter method for the time that this passenger has to wait
* @return the waitTime
*/
public int getWaitTime() {

/**
* Setter method for the time that this passenger has to wait
* @param waitTime the waitTime to set
*/
public void setWaitTime(int waitTime) {

/**
* Getter method for the generated process time for this passenger
* @return the processTime
*/
public int getProcessTime() {

/**
* Getter method for the line that this passenger is in
* @return the lineIndex
*/
public int getLineIndex() {

/**
* Method to tell whether or not this passenger is currently waiting in
* the security line
* @return true if this passenger is currently waiting in the security line
*/
public boolean isWaitingInSecurityLine() {

/**
* Method to tell this passenger to clear security
*/
public void clearSecurity() {

/**
* Setter method for the line that this passenger is in
* @param lineIndex the lineIndex to set
*/
protected void setLineIndex(int lineIndex) {

/**
* Method to tell this passenger to get in line
* @param t the TransitGroup to be used for this passenger
*/
public abstract void getInLine(TransitGroup t);

/**
* Method to return the color for this passenger in the simulation
* @return the color for this passenger in the simulation
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class to represent a TrustedTraveler object
*
*/
public class TrustedTraveler extends Passenger {

/** Maximum expected processing time for a trusted traveler*/
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** The color of the passenger in the simulation */
private Color color;

/**
* Constructor for a TrustedTraveler object
* @param log the Reporter to be used for this object
* @param waitTime the wait time for this passenger
* @param processTime the generated process time for this passenger
* 
*/
public TrustedTraveler(int processTime, int waitTime, Reporter log) {

/**
* Method to return the color for this passenger in the simulation
* @return the color for this passenger in the simulation
*/
public Color getColor() {

/**
* Method to tell this passenger to get in line
* @param t the TransitGroup to be used for this passenger
*/
public void getInLine(TransitGroup t) {

/**
* Method to choose a line for this passenger to enter
* @param t the TransitGroup to be used for this passenger
* @return the line chosen by the passenger
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class to represent the event calendar,
* the class that decides which events happen when
*
*/
public class EventCalendar {

/** The TransitGroup with the higher priority */
private TransitGroup highPriority;

/** The TransitGroup with the lower priority */
private TransitGroup lowPriority;

/**
* Constructor method for the event calendar
* @param pre the TransitGroup to represent PreSecurity
* @param security the TransitGroup to represent the SecurityArea
*/
public EventCalendar(TransitGroup pre, TransitGroup security) {

/**
* Method to return the next passenger that will act
* @return the next passenger that will act
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Class to represent the log of transit
*
*/
public class Log implements Reporter {

/** The number of passengers that have cleared security*/
private int numCompleted;

/** The total accumulated wait time of all passengers */
private int totalWaitTime;

/** The total accumulated process time of all passengers*/
private int totalProcessTime;

/**
* Constructor for the Log
*/
public Log() {

@Override
public int getNumCompleted() {

@Override
public void logData(Passenger p) {

@Override
public double averageWaitTime() {

@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Class to represent the Simulator that actually runs the program
*
*/
public class Simulator {

/** The total number of passengers */
private int numPassengers;

/** The transitGroup to represent passengers that are not yet in the SecurityArea */
private TransitGroup inTicketing;

/** The transitGroup to represent passengers that are in the SecurityArea */
private TransitGroup inSecurity;

/** The current passenger */
private Passenger currentPassenger;

/** The log to be used for recording Passenger records */
private Log log;

/** The event calendar to be used for this simulation */
private EventCalendar myCalendar;

/**
* Constructor for the Simulator object
* @param numberOfCheckpoints the number of checkpoints specified by the user
* @param numberOfPassengers the number of passengers specified by the user
* @param trustPct the percentage of passengers that are trusted travelers
* @param fastPct the percentage of passengers that are fast track passengers
* @param ordPct the percentage of passengers that are ordinary passengers
*/
public Simulator(int numberOfCheckpoints, int numberOfPassengers, int trustPct, int fastPct, int ordPct) {

/**
* Method to check if all of the given parameters in terms of passengers are correct
* @param numberOfPassengers the number of passengers specified by the user
* @param trustPct the percentage of passengers that are trusted travelers
* @param fastPct percentage of passengers that are fast track passengers
* @param ordPct the percentage of passengers that are ordinary passengers
*/
private void checkParameters(int numberOfPassengers, int trustPct, int fastPct, int ordPct) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Method to set up the number of each type of passenger
* @param trustPct the percentage of passengers that are trusted travelers
* @param fastPct percentage of passengers that are fast track passengers
* @param ordPct the percentage of passengers that are ordinary passengers
*/
private void setUp(int trustPct, int fastPct, int ordPct) {

/**
* Method to return a reporter
* @return the reporter
*/
public Reporter getReporter() {

/**
* Method to execute a step
*/
public void step() {

/**
* Method to tell if there are more steps remaining
* @return true if steps are remaining
*/
public boolean moreSteps() {

/**
* Method to return the current index of the security checkpoint line for currentPassenger
* @return the current index of the security checkpoint line for currentPassenger
*/
public int getCurrentIndex() {

/**
* Method to return the color of the current passenger
* @return the color field of the current passenger
*/
public Color getCurrentPassengerColor() {

/**
* Method to tell whether or not the passenger has cleared security
* @return true if the passenger has cleared security
*/
public boolean passengerClearedSecurity() {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class representing passengers who have not yet entered security checkpoint lines.
* This class implements the TransitGroup interface.
*
*/
public class PreSecurity implements TransitGroup {

/** Instance of PassengerQueue */
private PassengerQueue outsideSecurity = new PassengerQueue();

/**
* Constructor for PreSecurity that creates the specified number of 
* Passengers by successive calls Ticketing.generatePassenger(), 
* adding each to its PassengerQueue.
* @param numOfPassengers the number of Passengers
* @param log Reporter object
* @throws IllegalArgumentException when the number of Passengers
*         is less than 1
*/
public PreSecurity(int numOfPassengers, Reporter log) {

 - Throws: IllegalArgumentException

/**
* Returns the amount of time before the next passenger will clear security
* @return Integer.MAX_VALUE if the line is empty, otherwise the arrival time of the
*         Passenger at the front of the line
*/
public int departTimeNext() {

/**
* Returns the Passenger at the front of the security line
* @return p the Passenger at the front of the security line
*/
public Passenger nextToGo() {

/**
* Returns if the line has another Passenger object
* @return true if the line has another Passenger object, false if it does not
*/
public boolean hasNext() {

/**
* Removes and returns the next passenger to clear security
* @return the next Passenger in line to clear security
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class representing a security checkpoint and its corresponding waiting line.
* This class delegates strict queue activities to its instance variable line.
*
*/
public class CheckPoint {

/** 
* The time that the last passenger in line will clear security and 
* leave the line 
*/
private int timeWhenAvailable;

/** PassengerQueue object */
private PassengerQueue line;

/**
* Constructor for CheckPoint.
*/
public CheckPoint(){

/**
* Returns the size of the PassengerQueue line
* @return the size of the PassengerQueue line
*/
public int size(){

/**
* Removes a Passenger from the PassengerQueue line
* @return the Passenger removed from the PassengerQueue line
*/
public Passenger removeFromLine(){

/**
* Returns true if the line has another Passenger.
* @return true if the line has another Passenger, false 
*         if it does not.
*/
public boolean hasNext(){

/**
* Returns the amount of time before the next passenger will clear security
* @return Integer.MAX_VALUE if the line is empty, otherwise the average of
*         the arrival time, wait time, and process time of the Passenger at
*         the front of the line
*/
public int departTimeNext(){

/**
* Returns the Passenger at the front of the security line
* @return p the Passenger at the front of the security line
*/
public Passenger nextToGo(){

/**
* Adds the Passenger parameter to the end of the line, sets waitTime
* according to timeWhenAvailable as well as their arrivalTime and 
* processTime, and updates timeWhenAvailable
* @param p the Passenger to be added to the end of the line
*/
public void addToLine(Passenger p){

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class representing the entire TSA security area, including all of the checkpoints and their lines.
*
*/
public class SecurityArea implements TransitGroup {

/** The maximum number of checkpoints. */
private static final int MAX_CHECKPOINTS = 17;

/** The minimum number of checkpoints. */
private static final int MIN_CHECKPOINTS = 3;

/** The error message for an invalid number of checkpoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must "
/** 
* The error message for any attempt to add to a security 
* checkpoint line with an improper index. 
*/
private static final String ERROR_INDEX = "Index out of range for this security area";

/** The largest index for lines reserved for Fast Track passengers */
private int largestFastIndex;

/** The index of the line reserved for TSA PreCheck/Trusted Travelers */
private int tsaPreIndex;

/** Instance of CheckPoint */
private CheckPoint[] check;

/**
* Constructor for SecurityArea.
* @param num number of security checkpoints
* @throws IllegalArgumentException if the number of security checkpoints
*         is invalid
*/
public SecurityArea(int num) {

 - Throws: IllegalArgumentException

/**
* Helper method that returns true if the number of gates is valid.
* @param num the number of gates
* @return true if the number of gates is valid, false if not
*/
private boolean numGatesOK(int num) {

/**
* Adds the passenger to the security line with the index given by the first parameter
* @param i index  of line where the Passenger should be added
* @param p the Passenger object
*/
public void addToLine(int i, Passenger p) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the shortest security line that ordinary passengers are 
* permitted to select
* @return the index of the shortest security line that ordinary passengers are 
* permitted to select
*/
public int shortestRegularLine() {

/**
* Returns the index of the shortest security line restricted to Fast Track passengers
* @return the index of the shortest security line restricted to Fast Track passengers
*/
public int shortestFastTrackLine() {

/**
* Returns the index of the shortest security line restricted to 
* TSA PreCheck/Trusted Traveler passengers
* @return the index of the shortest security line restricted to 
* TSA PreCheck/Trusted Traveler passengers
*/
public int shortestTSAPreLine() {

/**
* Returns the number of passengers in the line with the given index
* @param i index of the line
* @return the number of passengers in the line with the given index
*/
public int lengthOfLine(int i) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the amount of time before the next passenger will clear security
* @return Integer.MAX_VALUE if the line is empty, otherwise the average of
*         the arrival time, wait time, and process time of the Passenger at
*         the front of the line
*/
public int departTimeNext() {

/**
* Returns the next Passenger to clear security
* @return p the next Passenger to clear security, or null if there is none
*/
public Passenger nextToGo() {	
/**
* Removes and returns the next Passenger to clear security
* @return the next Passenger to clear security
*/
public Passenger removeNext() {

/**
* Returns the index of the CheckPoint with the Passenger who will clear security next
* @return index the index of the CheckPoint with the Passenger who will clear security next
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Concrete class representing a passenger with Fast Track status.
* This class extends the Passenger abstract class.
*/
public class FastTrackPassenger extends Passenger {

/** The maximum expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** Color of the FastTrackPassenger */
private Color color;

/**
* Constructor for FastTrackPassenger.
* @param arrivalTime arrival time
* @param processTime process time
* @param myLog Reporter object
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from 
* the parent class, and then places this passenger in the selected line
* @param t TransitGroup object
*/
@Override
public void getInLine(TransitGroup t) {

/**
* Helper method that selects the appropriate checkpoint waiting line
* @param t TransitGroup object
* @return the index of the line to be selected
*/
private int pickLine(TransitGroup t) {

/**
* Creates the color of the FastTrackPassenger
* @return color the color of the FastTrackPassenger
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Concrete class representing a passenger with neither Fast Track nor TSA PreCheck status.
* This class extends the Passenger abstract class.
*/
public class OrdinaryPassenger extends Passenger {

/** The maximum expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** Color of the OrdinaryPassenger */
private Color color;

/**
* Constructor for OrdinaryPassenger.
* @param arrivalTime arrival time
* @param processTime process time
* @param myLog Reporter object
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from 
* the parent class, and then places this passenger in the selected line
* @param t TransitGroup object
*/
@Override
public void getInLine(TransitGroup t) {

/**
* Helper method that selects the appropriate checkpoint waiting line
* @param t TransitGroup object
* @return regularLine the index of the regular line
*/
private int pickLine(TransitGroup t) {

/**
* Creates the color of the FastTrackPassenger
* @return color the color of the FastTrackPassenger
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Abstract base class that is extended by FastTrackPassenger, 
* OrdinaryPassenger, and TrustedTraveler.
*/
public abstract class Passenger {

/** Minimum process time */
public static final int MIN_PROCESS_TIME = 20;

/** The time that the passenger joins a security line */
private int arrivalTime;

/** The time the passenger will spend in a security line */
private int waitTime;

/** 
* The amount of time that the passenger spends at the front of the security line 
* actually undergoing the security check 
*/
private int processTime;

/** 
* The index of the security checkpoint line that the passenger selects and joins
* Before the passenger gets into line, the value should be -1
*/
private int lineIndex;

/** 
* False until the passenger selects and joins a security line, 
* when it becomes true 
*/
private boolean waitingProcessing;

/**
* Instance of Reporter
*/
/**
* Constructor for Passenger
* @param arrivalTime arrival time
* @param processTime process time
* @param myLog Reporter object
*/
 - Throws: IllegalArgumentException

/**
* Getter for arrival time
* @return arrivalTime the arrival time
*/
public int getArrivalTime(){

/**
* Getter for wait time
* @return waitTime the wait time
*/
public int getWaitTime(){

/**
* Setter for wait time
* @param waitTime the wait time
* 
*/
public void setWaitTime(int waitTime){

/**
* Getter for process time
* @return processTime process time
*/
public int getProcessTime(){

/**
* Getter for line index
* @return lineIndex line index
*/
public int getLineIndex(){

/**
* Setter for line index
* @param lineIndex index
*/
protected void setLineIndex(int lineIndex) {

/**
* Returns true if a Passenger is waiting in a security line
* @return true if a Passenger is waiting in a security line,
*         false if not
*/
public boolean isWaitingInSecurityLine(){

/**
* Removes the passenger from the waiting line when he/she 
* finishes security checks
*/
public void clearSecurity(){

/**
* An abstract method that adds the passenger to the appropriate security line
* @param t list of security lines in the airport
*/
public abstract void getInLine(TransitGroup t);

/**
* Abstract method that gets the color of a Passenger object
* @return Color of the Passenger object
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() throws NoSuchElementException {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Concrete class representing a passenger with TSA PreCheck status.
* This class extends the Passenger abstract class.
*
*/
public class TrustedTraveler extends Passenger {

/** Color of the TrustedTraveler */
private Color color;

/** The maximum expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Constructor for TrustedTraveler.
* @param arrivalTime arrival time
* @param processTime process time
* @param myLog Reporter object
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter myLog) {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from 
* the parent class, and then places this passenger in the selected line
* @param t TransitGroup object
*/
@Override
public void getInLine(TransitGroup t) {

/**
* Helper method that selects the appropriate checkpoint waiting line
* @param t TransitGroup object
* @return the index of the line to be selected
*/
private int pickLine(TransitGroup t) {

/**
* Creates the color of the FastTrackPassenger
* @return the color of the FastTrackPassenger
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Determines which Passenger is next to act.
*
*/
public class EventCalendar {

/** TransitGroup with high priority */
private TransitGroup highPriority;

/** TransitGroup with low priority */
private TransitGroup lowPriority;

/**
* Constructor for EventCalender
* @param highPriority TransitGroup with higher priority
* @param lowPriority TransitGroup with lower priority
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Returns the Passenger from highPriority and lowPriority who will be the 
* next to exit the group/line theyre currently at the front of. 
* In case of a tie for the next to exit, the passenger from highPriority is returned. 
* If both TransitGroups are empty (their next passengers are null), an IllegalStateException is thrown.
* @return the Passenger object that is next to exit the group/line they're currently at the front of.
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Logging mechanism for the simulation, with behaviors specified 
* by the interface it implements, Reporter.
*
*/
public class Log implements Reporter {

/** Number completed */
private int numCompleted;

/** Total wait time */
private int totalWaitTime;

/** Total process time */
private int totalProcessTime;

/**
* Constructor for log
*/
public Log() {

/**
* Getter for number completed
* @return number completed
*/
@Override
public int getNumCompleted() {

/**
* Logs data of a Passenger
* @param p Passenger object
*/
@Override
public void logData(Passenger p) {

/**
* Calculates the average wait time
* @return the average wait time 
*/
@Override
public double averageWaitTime() {

/**
* Calculates the average process time
* @return the average process time
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Runs the simulation, stepping through acts of each Passenger.
*
*/
public class Simulator {

/** The TransitGroup that represents passengers who have not yet entered a checkpoint security line */
private TransitGroup inTicketing;

/** The TransitGroup that represents passengers who are in a checkpoint security line */
private TransitGroup inSecurity;

/** The passenger most recently returned by myCalendar.nextToAct(). */
private Passenger currentPassenger;

/** Instance of Reporter */
private Reporter log;

/** Instance of EventCalendar */
private EventCalendar myCalendar;

/** Number of passengers */
private int numPassengers;

/**
* Constructor for Simulator...
* 
* It checks the parameters to make sure they are valid, throwing IllegalArgumentExceptions if they are not.
* It initializes log to record the passenger statistics.
* It sets the passenger type distribution for the Ticketing factory.
* It establishes the security area.
* It creates the PreSecurity area and the EventCalendar.
* 
* @param numOfCheckpoints number of CheckPoints
* @param numOfPassengers number of Passengers
* @param percentTSA percent of passengers who are TSA PreCheck/Trusted Travelers
* @param percentFastTrack percent of passengers who are Fast Track
* @param percentOrdinary percent of passengers who do not qualify for special security checkpoints
* @throws IllegalArgumentException if... 
*         The number of checkpoints is less than 3 or greater than 17
*         The number of passengers is less than 1
*         The percents do not add up to 100
*/
public Simulator(int numOfCheckpoints, int numOfPassengers, int percentTSA, int percentFastTrack, 
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

//	/**
//	private void checkParameters(int a, int b, int c, int d) {

//	/**
//	private void setUp(int a, int b, int c) {

/**
* Returns the Reporter object
* @return Reporter
*/
public Reporter getReporter( ) {

/**
* Goes through the action of the next passenger from the EventCalendar. 
* If the passenger is from the ticketing area, this call to step() implements [UC5]. 
* If the passenger is from the security area, this call to step() implements [UC6]. 
* In each case, the passenger is removed from the corresponding area. 
* If there is no next passenger, an IllegalStateException is thrown.
*/
public void step() {

//			throw new IllegalStateException();

 - Throws: IllegalStateException

 - Throws: IllegalStateException

/**
* Returns true if there are more steps
* @return true if there are more steps
*/
public boolean moreSteps() {

/**
* Returns the index of the security checkpoint line for currentPassenger
* @return the index of the security checkpoint line for currentPassenger
*/
public int getCurrentIndex() {

/**
* Getter for the color of the current Passenger
* @return color of the current Passenger
*/
public Color getCurrentPassengerColor() {

/**
* Returns true if currentPassenger just cleared a security checkpoint and finished 
* processing and false otherwise, including when currentPassenger is null.
* @return true if the Passenger cleared security
*/
public boolean passengerClearedSecurity() {


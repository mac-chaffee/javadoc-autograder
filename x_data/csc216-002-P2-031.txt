---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The Command class creates objects that encapsulate user actions (or transitions) that cause the state of a ManagedIncident to update.
*
*/
public class Command {

/** A string representing the Awaiting Caller on hold reason. */
public static final String OH_CALLER = "Awaiting Caller";

/** A string representing the  on hold Awaiting Change reason. */
public static final String OH_CHANGE  = "Awaiting Change";

/** A string representing the  on hold Awaiting Vendor reason. */
public static final String OH_VENDOR = "Awaiting Vendor";

/** A String representing the Permanently Solved resolution code */
public static final String RC_PERMANENTLY_SOLVED  = "Permantly Solved";

/** A String representing the Workaround resolution code */
public static final String RC_WORKAROUND  = "Workaround";

/** A String representing the Not Solved resolution code */
public static final String RC_NOT_SOLVED  = "Not Solved";

/** A String representing the Caller Closed resolution code */
public static final String RC_CALLER_CLOSED  = "Caller Closed";

/** A string representing the Duplicate cancellation code. */
public static final String CC_DUPLICATE = "Duplicate";

/** A string representing the Unnecessary cancellation code. */
public static final String CC_UNNECESSARY = "Unnecessary";

/** A string representing the Not an Incident cancellation code. */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** All of the values a command can take */
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/** All the reasons an incident can be put on hold. */
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/** All of the codes for resolving an incident. */
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/** All of the codes for canceling and incident. */
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT };

/** The incident's owner */
private String ownerId;

/** The incident's note */
private String note;

/** The Command's Cancellation Code */
private CancellationCode cancellationCode;

/** The Command's Cancellation Code */
private ResolutionCode resolutionCode;

/** The Command's onHoldReason Code */
private OnHoldReason onHoldReason;

/** The Command's command value */
private CommandValue c;

/**
* Constructs a Command with all of the given parameters. Irrelevant parameters should be passed in as null.
* @param c The command value of the command.
* @param ownerId The owner of the incident.
* @param onHoldReason The on-hold reason.
* @param resolutionCode The resolution code.
* @param cancellationCode The cancellation code.
* @param note The note.
*/
public Command(CommandValue c, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode, 
if(c == null) throw new IllegalArgumentException("Null command value.");

if(note == null || note.equals("")) throw new IllegalArgumentException("Null or empty note");

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/** 
* The command value 
*@return The command value.
*/
public CommandValue getCommand() { return this.c; }
/** The owner id 
*@return The owner id.
*/
public String getOwnerId() { return this.ownerId; }
/** The resolution code 
*@return The resolution code.
*/
public ResolutionCode getResolutionCode() { return this.resolutionCode; }
/** The work note 
*@return The work note.
*/
public String getWorkNote() { return this.note; }
/** The on hold reason 
*@return The on hold reason.
*/
public OnHoldReason getOnHoldReason() { return this.onHoldReason; }
/** The cancellation code 
*@return The cancellation code.
*/
public CancellationCode getCancellationCode() { return this.cancellationCode; }
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Concrete class representing the State Pattern context class. 
* A ManagedIncident keeps track of all incident information including the current state. 
* The state is updated when a Command encapsulating a transition is given to the ManagedIncident. 
*
*/
public class ManagedIncident {

/**Concrete class that represents the new state of the Incident Management FSM.*/
private class NewState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

/**Concrete class that represents the in progress state of the Incident Management FSM.*/
private class InProgressState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

/**Concrete class that represents the on hold state of the Incident Management FSM.*/
private class OnHoldState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

/**Concrete class that represents the resolved state of the Incident Management FSM.*/
private class ResolvedState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

/**Concrete class that represents the closed state of the Incident Management FSM*/
private class ClosedState implements IncidentState {

@Override
public void updateState(Command command) {

@Override
public String getStateName() {

/**Concrete class that represents the canceled state of the Incident Management FSM.*/
private class CanceledState implements IncidentState {

@Override
public void updateState(Command command) { System.out.print(""); }
@Override
public String getStateName() {

/**An enumeration contained in the ManagedIncident class. Represents the five possible categories of incidents.*/
public enum Priority { URGENT, HIGH, MEDIUM, LOW };

/**An enumeration contained in the ManagedIncident class. Represents the four possible priorities for ManagedIncidents.*/
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE };

/** A string representing the inquiry category. */
public static final String C_INQUIRY = "Inquiry";

/** A string representing the software category. */
public static final String C_SOFTWARE = "Software";

/** A string representing the hardware category. */
public static final String C_HARDWARE = "Hardware";

/** A string representing the network category. */
public static final String C_NETWORK = "Network";

/** A string representing the database category. */
public static final String C_DATABASE = "Database";

/** A string representing the urgent priority. */
public static final String P_URGENT = "Urgent";

/** A string representing the high priority. */
public static final String P_HIGH = "High";

/** A string representing the medium priority. */
public static final String P_MEDIUM = "Medium";

/** A string representing the low priority. */
public static final String P_LOW = "Low";

/** The incident's ID */
private int incidentId;

/** The caller's ID */
private String caller;

/** The incident's owner*/
private String owner;

/** The incident's name */
private String name;

/** The incident's change request */
private String changeRequest;

/** The incident's notes */
private List<String> notes = new ArrayList<String>();

/** The name of a new incident */
public static final String NEW_NAME = "New";

/** The name of an in-progress incident */
public static final String IN_PROGRESS_NAME = "In Progress";

/** The name of an on-hold incident */
public static final String ON_HOLD_NAME = "On Hold";

/** The name of a resolved incident */
public static final String RESOLVED_NAME = "Resolved";

/** The name of a closed incident */
public static final String CLOSED_NAME = "Closed";

/** The name of a canceled incident */
public static final String CANCELED_NAME = "Canceled";

/** A static field that keeps track of the id value that should be given to the next ManagedIncident created. */
private static int counter = 0;

/** The incident's priority */
private Priority priority;

/** The incidient's category */
private Category category;

/** The incident's Cancellation Code */ 
private CancellationCode cancellationCode;

/** The incident's Resolution Code */
private ResolutionCode resolutionCode;

/** The incidents OnHoldReason */
private OnHoldReason onHoldReason;

/** The incident's current state */
private IncidentState state;

/**
* Constructs a ManagedIncident from the provided parameters. The incident's ID will be generated automatically.
* 
* @param caller The incident's caller.
* @param category The incident's category.
* @param priority The incident's priority.
* @param name The incident's name.
* @param workNote The incident's work note.
* 
* @throws IllegalArgumentException if any of the parameters are null or empty strings.
* 
*/
public ManagedIncident(String caller, Category category, Priority priority, String name, String workNote) {

 - Throws: IllegalArgumentException

/**
* The fields of the ManagedIncident are set to the values from the Incident. 
* You may find it useful to create private helper methods to transform the string category, state, priority, onHoldReason, 
* resolutionCode, and cancellationCode from Incident into the object or enumeration types needed for a ManagedIncident. 
* If the string cannot be converted to an appropriate object or enumeration type, then an IllegalArgumentException should be 
* thrown for required values. 
* @param i The Incident.
*/
public ManagedIncident(Incident i) {

private Category parseCategory(String category) {

if(category == null) throw new IllegalArgumentException("XML Incident category String cannot be null");

 - Throws: IllegalArgumentException

private IncidentState parseState(String state) {

if(state == null) throw new IllegalArgumentException("XML Incident state String cannot be null");

 - Throws: IllegalArgumentException

private Priority parsePriority(String priority) {

if(priority == null) throw new IllegalArgumentException("XML Incident priority string cannont be null!");

 - Throws: IllegalArgumentException

private OnHoldReason parseOnHoldReason(String onHoldReason) {

 - Throws: IllegalArgumentException

private ResolutionCode parseResolutionCode(String code) {

 - Throws: IllegalArgumentException

private CancellationCode parseCancellationCode(String code) {

 - Throws: IllegalArgumentException

/** Increments the incident counter by 1. */
public static void incrementCounter() { counter++; }
/**
* Gets the incident's priority.
* @return the priority
*/
public Priority getPriority() {

/**
* Sets the counter to the given value.
* @param value The value.
*/
public static void setCounter(int value) { counter = value; };

/**
* Sets the priority to the given value.
* @param priority the priority to set
*/
private void setPriority(Priority priority) {

/**
* Gets the incident's category.
* @return the category
*/
public Category getCategory() {

/**
* Sets the incident's category.
* @param category the category to set
*/
private void setCategory(Category category) {

/** Gets the incident's cancellation code.
* @return the CancellationCode
*/
public CancellationCode getCancellationCode() {

/**
* Sets the incidents cancellation code.
* @param CancellationCode the CancellationCode to set
*/
private void setCancellationCode(CancellationCode cancellationCode) {

/**
* Gets the incident's resolution code.
* @return the resolutionCode
*/
public ResolutionCode getResolutionCode() {

/**
* Sets the incident's resolution code.
* @param resolutionCode the resolutionCode to set
*/
private void setResolutionCode(ResolutionCode resolutionCode) {

/**
* Gets the incident's on hold reason.
* @return the onHoldReason
*/
public OnHoldReason getOnHoldReason() {

/**
* Sets the incident's on hold reason.
* @param onHoldReason the onHoldReason to set
*/
private void setOnHoldReason(OnHoldReason onHoldReason) {

/**
* Gets the incident's state.
* @return the state
*/
public IncidentState getState() {

/**
* Sets the incident's state.
* @param state the state to set
*/
private void setState(IncidentState state) {

/**
* Gets the incident's id.
* @return the incidentID
*/
public int getIncidentId() {

/**
* Sets the incident's id.
* @return the caller
*/
public String getCaller() {

/**
* Gets the incident's owner.
* @return the owner
*/
public String getOwner() {

/**
* Gets the incident's name.
* @return the name
*/
public String getName() {

/**
* Gets the incident's change request.
* @return the changeRequest
*/
public String getChangeRequest() {

/**
* Gets the incident's notes.
* @return the notes
*/
public List<String> getNotes() {

/** 
* Gets the incident's category String 
* @return the String.
*/
public String getCategoryString() { return (category.toString().charAt(0) + category.toString().substring(1, category.toString().length()).toLowerCase()).replaceAll("_", " "); }
/** Gets the incident's priority String 
* @return the String.
*/
public String getPriorityString() { return (priority.toString().charAt(0) + priority.toString().substring(1, priority.toString().length()).toLowerCase()).replaceAll("_", " "); }
/** Gets the incident's resolution code string 
* @return the String.
*/
public String getResolutionCodeString() {

/** Gets the incident's notes String 
* @return the String.
*/
public String getNotesString() {

/**
* Executes the given command on the incident.
* @param command The command.
* @throws UnsupportedOperationException If the command is invalid for the current state.
*/
public void update(Command command) {

if(state.equals(prev)) throw new UnsupportedOperationException("Invalid transition");

/**
* Gets an XML incident version of the incident.
* @return the incident
*/
public Incident getXMLIncident() {

/**
* Gets the incidents cancellation code String.
* @return The string.
*/
public String getCancellationCodeString() {

/**
* Gets the incident's on hold reason String.
* @return the String.
*/
public String getOnHoldReasonString() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Concrete class that maintains the ManagedIncidentList and handles Commands from the GUI. 
* IncidentManager implements the Singleton Design Pattern.
*
*/
public class IncidentManager {

/** The list of incident */
private ManagedIncidentList incidentList = new ManagedIncidentList();

/** Singleton instance of IncidentManager*/
private static IncidentManager instance = new IncidentManager();

/** Private constructor to create the singleton IncidentManager */
private IncidentManager() { }
/**
* Gets an incident of the IncidentManager.
* @return The incident.
*/
public static IncidentManager getInstance() { return instance; }
/**
* Saves the incidents to the given file.
* @param fileName The name of the file.
*/
public void saveManagedIncidentsToFile(String fileName){

/**
* Loads the incidents from the given file.
* @param fileName The name of the file.
*/
public void loadManagedIncidentsFromFile(String fileName) {

/**
* Creates a new managed incident list.
*/
public void createNewManagedIncidentList() { incidentList = new ManagedIncidentList(); }
/**
* Gets a String[][] representing the incidents.
* @return The String[][]
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Gets a String[][] representing the incidents in the given category.
* @param category The category of the incidents.
* @return The String[][]
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

/**
* Gets the incident with the given id.
* @param id The incident's id.
* @return The incident.
*/
public ManagedIncident getManagedIncidentById(int id) { return incidentList.getIncidentById(id); }
/**
* Executes the given command on the incident with the given id.
* @param id The incident's id.
* @param command The command to be executed.
*/
public void executeCommand(int id, Command command) {

/**
* Deletes the incident with the given id.
* @param id The incident's id.
*/
public void deleteManagedIncidentById(int id) {

/**
* Adds an incident with all of the given parameters.
* @param caller The incident's caller.
* @param category The incdient's category.
* @param priority The incident's priority.
* @param name The incident's name.
* @param workNote The incident's work note.
*/
public void addManagedIncidentToList(String caller, Category category, Priority priority, String name, String workNote) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Concrete class that maintains a current list of ManagedIncidents in the Incident Management system.
*
*/
public class ManagedIncidentList {

private List<ManagedIncident> incidents = new ArrayList<ManagedIncident>();

/** Constructs a ManagedIncidentList with no parameters provided. */
public ManagedIncidentList() { ManagedIncident.setCounter(0); }
/**
* Adds an incident with the given parameters to the ManagedIncidentList
* @param caller The incident's caller..
* @param category The incident's category.
* @param priority The incident's priority.
* @param name The incident's name.
* @param workNote The incident's work note..
* @return The added incident's ID.
*/
public int addIncident(String caller, Category category, Priority priority, String name, String workNote) {

/**
* Adds the XML list of incidents to the ManagedIncidentList.
* @param list The list of incidents.
*/
public void addXMLIncidents(List<Incident> list) {

/**
*The list of managed incidents.
* @return the list of managed incidents.
*/
public List<ManagedIncident> getManagedIncidents() { return incidents; }
/**
* A list of all the ManagedIncidents in the given category.
* @param category The category.
* @return the list of ManagedIncidents.
*/
public List<ManagedIncident> getIncidentsByCategory(Category category) {

if(category == null) throw new IllegalArgumentException ("Null category.");

/**
* Gets the incident with the given id.
* @param id the id.
* @return the incident.
*/
public ManagedIncident getIncidentById(int id) {

/**
* Executes the given command on the incident with the given id.
* @param id The incident's id.
* @param command The command to be executed.
*/
public void executeCommand(int id, Command command) {

if(command == null) throw new IllegalArgumentException("Null command");

/**
* Deletes the incident with the given id.
* @param id The id.
*/
public void deleteIncidentById(int id) {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A class representing passengers who have not yet entered security checkpoint
* lines
* 
*
*/
public class PreSecurity implements TransitGroup {

private PassengerQueue outsideSecurity = new PassengerQueue();

/**
* The PreSecurity constructor. Calls ticketing to generate passengers.
* 
* TODO add illegalArgumentException
* 
* @param num the number of passengers in the pre security line
* @param rep the associated Reporter.
*/
public PreSecurity(int num, Reporter rep) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Checks if there is another person in line
* 
* @return true if there is another person, false if not
*/
public boolean hasNext() {

/**
* When will the next passenger leave the group?
* 
* @return 'arrival time' of the next passenger or Integer.MAX_VALUE if the
*         group is empty
*/
@Override
public int departTimeNext() {

/**
* Who will be the next passenger to leave the group?
* 
* @return the next passenger
*/
@Override
public Passenger nextToGo() {

/**
* Removes the next passenger to leave the group.
* 
* @return the passenger who is removed
*/
@Override
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The checkpoint class, located at each index of the security area, used to
* process passengers and ensure they are safe to ride their plane.
* 
*
*/
public class CheckPoint {

/**
* the time that the last passenger in line will clear security and leave the
* line
*/
private int timeWhenAvailable = 0;

/** the passenger queue representing people waiting in line for the simulator */
private PassengerQueue line;

/**
* The constructor for the checkpoint class
*/
public CheckPoint() {

/**
* Checks the length of the checkpoint's line
* 
* @return the length of the checkpoint's line
*/
public int size() {

/**
* Removes a passenger from the line
* 
* @return the removed passenger
*/
public Passenger removeFromLine() {

/**
* Who will be the next passenger to leave the group?
* 
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* Returns the time the next passenger will depart. According to [UC4][S1]:
* Departure time at a security checkpoint is the sum of the arrival time, wait
* time, and process time of the front passenger or Integer.MAX_VALUE if the
* line at the checkpoint is empty.
* 
* @return the depart time of the next passenger
*/
public int departTimeNext() {

/**
* Checks if there is another person in the line
* 
* @return true if there is another person, false if the line is empty
*/
public boolean hasNext() {

/**
* Adds a passenger to the line, and logs their various times.
* 
* @param p the passenger being added
*/
public void addToLine(Passenger p) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A class representing the entire TSA security area, including all of the
* checkpoints and their lines.
* 
*
*/
public class SecurityArea implements TransitGroup {

/** the maximum number of checkpoints allowed by the simulation */
private static final int MAX_CHECKPOINTS = 17;

/** the minimum number of checkpoints allowed by the simulation */
private static final int MIN_CHECKPOINTS = 3;

/** Error message thrown when checkpoints are too low/high */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least " + MIN_CHECKPOINTS
/** Error for a checkpoint with a bad index */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** the index of the fast track security area farthest from ticketing */
private int largestFastIndex;

/** the index of the TSA-Pre security area */
private int tsaPreIndex;

/** an arrayList of checkpoints in the security area */
private ArrayList<CheckPoint> check = new ArrayList<CheckPoint>();

/**
* The constructor for the SecurityArea class
* 
* @param length the number of security gates.
* @throws IllegalArgumentException if length is too large.
*/
public SecurityArea(int length) {

 - Throws: IllegalArgumentException

/**
* Checks if the number of security gates is within the simulations bounds
* 
* @param length the number of security gates
* @returns true of its within the bounds of the simulation, false if not.
*/
private boolean numGatesOK(int length) {

/**
* Adds a passenger to a specific security gate
* 
* @param i the index of the gate the passenger is being added to
* @param p the passenger being added to the line
* @throws IllegalArgumentException if the index parameter is illegal for the
*                                  type of passenger inputed.
*/
public void addToLine(int i, Passenger p) {

 - Throws: IllegalArgumentException

/**
* Gets the index of the shortest regular line available
* 
* @return the index of the shortest regular line available.
*/
public int shortestRegularLine() {

/**
* Gets the index of the shortest fast track line available
* 
* @return the index of the shortest fast track line available
*/
public int shortestFastTrackLine() {

/**
* Gets the index of the shortest tsa pre line available
* 
* @return the index of the shortest tsa pre line available
*/
public int shortestTSAPreLine() {

/**
* Gets the length of the line at the given index
* 
* @param idx the index of the line being checked
* @return the length of the line
* @throws IllegalArgumentException if index is out of range
*/
public int lengthOfLine(int idx) {

 - Throws: IllegalArgumentException

/**
* Returns the time that the next passenger will finish clearing security.
* 
* @return the time that the next passenger will finish clearing security. If
*         all lines are empty, returns a max-valued integer.
*/
@Override
public int departTimeNext() {

/**
* Who will be the next passenger to leave the group?
* 
* @return the next passenger
*/
@Override
public Passenger nextToGo() {

/**
* Removes the next passenger to leave the group.
* 
* @return the passenger who is removed
*/
@Override
public Passenger removeNext() {

/**
* Returns the index of the security gate with the shortest line within the
* given range of indexes
* 
* @param start the start index
* @param end   the end index
* @return the index of the shortest line in the range
*/
private int shortestLineInRange(int start, int end) {

//	/** commented out because I think this is redundant.
//	private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The FastTrackPassenger Class. Contains the constructor and various methods
* executable by a fast track passenger.
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** the max expected time it will take to process the passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** the passenger's color displayed by the simulation */
private Color color;

/**
* The fast track passenger's constructor method
* 
* @param arrivalTime the time the passenger arrives
* @param processTime the time it takes to process the passenger
* @param myLog       the passengers personal log
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Gets into the a line fitting for a fast track passenger.
*/
@Override
public void getInLine(TransitGroup t) {

/**
* Gets the fast track passenger's color for the simulation
*/
@Override
public Color getColor() {

/**
* Picks a line for the passenger to go to. Compares fast track lines with
* ordinary lines and selects whichever one is shortest & closest.
* 
* @param t the simulation's securityArea
* @return the index of the shortest fast track or regular line
* @throws IllegalArgumentException if t is not a SecurityArea
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The class for an ordinary passenger. Contains the constructor and methods
* associated with an ordinary passenger.
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** the max expected time it takes to process the passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** the passengers color displayed by the simulation */
private Color color;

/**
* the ordinary passenger's constructor method
* 
* @param arrivalTime the time the passenger arrives
* @param processTime the time it takes to process the passenger
* @param myLog       the passengers personal log
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Adds the passenger to a specific security line
* @param t the SecurityArea of the simulation
*/
@Override
public void getInLine(TransitGroup t) {

/**
* Gets the passenger's color.
*/
@Override
public Color getColor() {

/**
* Picks the shortest, closest regular line for the passenger to go to.
* 
* @param t the simulation's securityArea
* @return the index of the shortest closest regular line
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The passenger class details the various actors in the simulation and what
* they can do.
* 
*
*/
public abstract class Passenger {

/** The minimum process time of each passenger */
public static final int MIN_PROCESS_TIME = 20;

/** The arrival time of the passenger */
private int arrivalTime;

/** the time the passenger will spend in a security line. */
private int waitTime;

/** The time it takes to process the passenger */
private int processTime;

/** The the index of the line the passenger is in */
private int lineIndex = -1;

/** log for the passenger's different times */
private Log myLog;

/**
* false until the passenger selects and joins a security line, when it becomes
* true
*/
private boolean waitingProccessing = false;

/**
* The passenger's constructor method
* 
* @param arrivalTime the time the passenger arrives in a security line
* @param processTime the amount of time that the passenger spends at the front
*                    of the security line actually undergoing the security
*                    check
* @param myLog       the passenger's reporting mechanism
*/
public Passenger(int arrivalTime, int processTime, Reporter myLog) {

 - Throws: IllegalArgumentException

/**
* Fetches the time that the passenger joins a security line
* 
* @return arrivalTime the passenger's arrival time.
*/
public int getArrivalTime() {

/**
* Fetches the passenger's waitTime value
* 
* @return the waitTime
*/
public int getWaitTime() {

/**
* Sets the passenger's waitTime value. It cannot be computed until the
* passenger joins a security line
* 
* @param waitTime the waitTime to set
*/
public void setWaitTime(int waitTime) {

/**
* Fetches the amount of time that the passenger spends at the front of the
* security line actually undergoing the security check
* 
* @return the processTime
*/
public int getProcessTime() {

/**
* Fetches the passenger's LineIndex value
* 
* @return the lineIndex
*/
public int getLineIndex() {

/**
* Sets the passenger's lineIndex value
* 
* @param lineIndex the lineIndex to set
*/
protected void setLineIndex(int lineIndex) {

/**
* Checks if the passenger is waiting in a security line
* 
* @return true if they are waiting, false if they aren't
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when he/she finishes security
* checks
*/
public void clearSecurity() {

/**
* Abstract method that sends the passenger to a specific line. Further detailed
* by various passenger subclasses and their specific privileges.
* 
* @param t the transitGroup of the passenger
*/
public abstract void getInLine(TransitGroup t);

/**
* Gets the color of the passenger
* 
* @return Color the passenger's color
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
* 
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* 
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* 
* @param person the Passenger to add
*/
public void add(Passenger person) {

/**
* Removes and returns the front Passenger from the queue.
* 
* @return the Passenger at the front of the queue
*  throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null if the
* queue is empty. Does not remove the Passenger from the queue.
* 
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* 
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The trusted traveler class. Contains the constructor and methods available to
* a passenger with the privileges of a TSA trusted traveler
* 
*
*/
public class TrustedTraveler extends Passenger {

private Color color;

/** the maximum time its expected to process the fast track passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* The trustedTraveler passenger's constructor method
* 
* @param arrivalTime the time the passenger arrives
* @param processTime the time it takes to process the passenger
* @param myLog       the passengers personal log
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter myLog) {

/**
* Adds the passenger to a specific security line
* 
* @param t the SecurityArea of the simulation
*/
@Override
public void getInLine(TransitGroup t) {

/**
* Gets the passenger's color.
*/
@Override
public Color getColor() {

/**
* Picks a line for the passenger to go to. Compares TSA PRE lines with ordinary
* lines and decides which one to use via a specific algorithm.
* 
* @param t the simulation's securityArea
* @return the index of the tsa pre or regular line
* @throws IllegalArgumentException if t isn't a security area.
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The event calendar class. Handles decisions on what passenger should be
* processed next, since the simulation is technically stepping one passenger at
* a time.
* 
*
*/
public class EventCalendar {

/** the high priority transitGroup (preSecurity) */
private TransitGroup highPriority;

/** the low priority transitGroup (securityArea) */
private TransitGroup lowPriority;

/**
* The event calendar's constructor method
* 
* @param highPriority the more important transitGroup (preSecurity)
* @param lowPriority  the less important transitGroup (securityArea)
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Determines the next passenger to act in the system.
* 
* @return the selected passenger
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The log class. Stores and sums each passenger's individual details/data
* 
*
*/
public class Log implements Reporter {

/** the number of passengers who have passed security */
private int numCompleted;

/** the total wait time of all passengers */
private int totalWaitTime;

/** the total process time of all passengers */
private int totalProcessTime;

/**
* The Log class' constructor method.
*/
public Log() {

/**
* How many passengers have completed all their activities?
* 
* @return the number of passengers who have completed their activities
*/
@Override
public int getNumCompleted() {

/**
* Log the data for a passenger.
* 
* @param p - passenger whose data is to be logged
*/
@Override
public void logData(Passenger p) {

/**
* How long did passengers have to wait before completing processing?
* 
* @return average passenger waiting time (in minutes).
*/
@Override
public double averageWaitTime() {

/**
* How long did it take passengers to complete processing?
* 
* @return average processing time
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The physical simulator object that controls and simulates all of the
* different types of passengers.
* 
*
*/
public class Simulator {

/** the number of passengers in the simulator */
private int numPassengers;

/** The EventCalendar the simulator is using */
private EventCalendar myCalendar;

/** The simulator's log */
private Reporter log;

/** the current passenger being worked on by the simulator */
private Passenger currentPassenger;

/** the transit group for the security area */
private TransitGroup inSecurity;

/** the transit group for the ticketing/presecurity area. */
private TransitGroup inTicketing;

/**
* The constructor method for the simulator class
* 
* @param numCheckpoints the number of checkpoints
* @param numPassengers  the number of passengers
* @param pctPre         the percent of passengers who are TSA PreCheck/Trusted
*                       Travelers
* @param pctFast        the percent of passengers who are Fast Track
* @param pctNorm        the percent of passengers who do not qualify for
*                       special security checkpoints
*/
public Simulator(int numCheckpoints, int numPassengers, int pctPre, int pctFast, int pctNorm) {

/**
* Checks the various parameters to make sure they are legal
* 
* @param numPassengers the number of passengers
* @param pctPre        the percent of passengers who are TSA PreCheck/Trusted
*                      Travelers
* @param pctPre        the percent of passengers who are TSA PreCheck/Trusted
*                      Travelers
* @param pctFast       the percent of passengers who are Fast Track
* @param pctNorm       the percent of passengers who do not qualify for special
*                      security checkpoints
*/
private void checkParameters(int numPassengers, int pctPre, int pctFast, int pctNorm) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Prepares the simulator by setting the passenger distribution via ticketing.
* 
* @param pctTrusted the pct trusted passengers in the simulation
* @param pctFast    the pct fast passengers in the simulation
*/
private void setUp(int pctTrusted, int pctFast) {

/**
* Gets a reporter for the simulator
* 
* @return r the reporter/log
*/
public Reporter getReporter() {

/**
* Goes through the action of the next passenger from the EventCalendar. If the
* passenger is from the ticketing area, this call to step() implements [UC5].
* If the passenger is from the security area, this call to step() implements
* [UC6]. In each case, the passenger is removed from the corresponding area. If
* there is no next passenger, an IllegalStateException is thrown
* 
* @throws IllegalStateException if there is no next passenger.
*/
public void step() {

 - Throws: IllegalStateException

/**
* Checks if there are more steps to be made.
* 
* @return true if there are more, false if the simulator is finished
*/
public boolean moreSteps() {

/**
* Gets the index of the security checkpoint of the currentPassenger
* 
* @return the fetched index, or 0 if current passenger is null
*/
public int getCurrentIndex() {

/**
* Gets the color of a specific passenger
* 
* @return the passenger's color
*/
public Color getCurrentPassengerColor() {

/**
* Checks if a passenger has finished clearing security
* 
* @return true if they have, false if they haven't.
*/
public boolean passengerClearedSecurity() {


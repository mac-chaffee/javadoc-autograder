---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class implements TransitGroup and represents passengers in the terminal
* who have not yet joined a security checkpoint line
* 
*
*/
public final class PreSecurity implements TransitGroup {

/**
* Constructs an Trusted Traveler object with the number of passengers and a
* logging Reporter
* 
* @param passengers
* @param myLog
* @throws IllegalArgumentException
*/
public PreSecurity(int passengers, Reporter myLog) throws IllegalArgumentException {

/**
* Who will be the next passenger to leave the group?
* 
* @return the next passenger
*/
@Override
public Passenger nextToGo() {

/**
* When will the next passenger leave the group?
* 
* @return departure time of the next passenger or Integer.MAX_VALUE if the
*         group is empty
*/
@Override
public int departTimeNext() {

/**
* Removes the next passenger to leave the group.
* 
* @return the passenger who is removed
*/
@Override
public Passenger removeNext() {

/**
* Checks whether it has a next passenger
* 
* @return true if there is a next passenger
*/
public boolean hasNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
* 
*/
public class Ticketing {

/**
* Absolute time for passengers created for the simulation. The time starts at
* zero and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger
* created.
*/
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a Fast Track passenger should be created */
private static double pctExpedite = .50; // initialize with default value
/**
* Percentage of time a trusted traveler/TSA PreCheck passenger should be
* created
*/
private static double pctTrust = .05; // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference)
* ordinary passengers that should be generated. Proportion of ordinary
* passengers is 1 - (pctFast + pctTrusted).
* 
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast    - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments.
* 
* @param log - where the passenger will log his/her data
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory. You can use this for testing ONLY! Do not use it in your
* simulator.
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents a security checkpoint line and its line, where the
* front passenger is undergoing the actual security process; It delegates
* strict queue activities to its instance variable line;

* 
*
*/
public class CheckPoint {

/**
* The time that the last passenger in line will clear security and leave the
* line.
*/
private int timeWhenAvaliable = 0;

/**
* Constructs a CheckPoint object
*/
public CheckPoint() {

public int size() {

/**
* Removes passenger from line
* 
* @return passenger
*/
public Passenger removeFromLine() {

/**
* Checks whether it has a next passenger
* 
* @return true if there is a next passenger
*/
public boolean hasNext() {

/**
* Returns the amount of time before the next passenger will clear security
* 
* @return next depart time
*/
public int departTimeNext() {

/**
* Returns the passenger at the front of the security line
* 
* @return the next passenger to go
*/
public Passenger nextToGo() {

/**
* Adds the parameter to the end of the line
* 
* @param passenger
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class implements TransitGroup and represents the collection of security
* checkpoints and their waiting lines.
* 
*
*/
public class SecurityArea implements TransitGroup {

/** The maximum of checkpoints. */
private static final int MAX_CHECKPOINTS = 17;

/** The minimum of checkpoints. */
private static final int MIN_CHECKPOINTS = 3;

/** The error message when checkpoints are out of range. */
private static final String ERROR_CHECKPOINTS = "Number of checkpoint must be at least 3 and at most 17";

/** The error message when index is out of range. */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** The largest index for lines reserved for Fast Track passengers. */
private int largestFastIndex;

/** The index of the line reserved for TSA PreCheck/Trusted Travelers */
private int tsaPreIndex;

/**
* Constructs a SecurityArea object with the number of security checkpoints
* 
* @param tsaPreIndex
* @throws IllegalArgumentException
*/
public SecurityArea(int tsaPreIndex) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

private boolean numGatesOK(int gates) {

/**
* Adds the passenger to the security line with the index
* 
* @param index
* @param passenger
*/
public void addToLine(int index, Passenger passenger) {

/**
* Returns the index of the shortest security line that ordinary passengers are
* permitted to select
* 
* @return short Regular Line
*/
public int shortestRegularLine() {

/**
* Returns the index of the shortest security line restricted to Fast Track
* passengers
* 
* @return short Fast Track Line
*/
public int shortestFastTrackLine() {

/**
* Returns the index of the shortest security line restricted to TSA
* PreCheck/Trusted Traveler passengers
*/
public int shortestTSAPreLine() {

/**
* The number of passengers in the line with the given index
* 
* @param index
* @return length of line
* @throws IllegalArgumentException
*/
public int lengthOfLine(int index) throws IllegalArgumentException {

/**
* Returns the time that the next passenger will finish clearing security
* 
* @return next depart time
*/
public int departTimeNext() {

/**
* Returns the next passenger to clear security, or null if there is none
* 
* @return the next passenger to go
*/
public Passenger nextToGo() {

/**
* Removes and returns the next passenger to clear security
* 
* @return the next remove passneger
*/
public Passenger removeNext() {

private int shortesLineInRange(int index, int index1) {

private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in
* which there is a "next" passenger and a time when that passenger will leave
* the group.
* 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* 
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* 
* @return departure time of the next passenger or Integer.MAX_VALUE if the
*         group is empty
*/
/**
* Removes the next passenger to leave the group.
* 
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class is a sub-class of Passenger class to create Fast Track Passenger
* objects
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** The color of the fast track passenger. */
private Color color;

/** The maximum process time of fast track passenger. */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* Constructs a Fast Track Passenger object with values of arrivalTime,
* processTime and myLog.
* 
* @param arrivalTime
* @param processTime
* @param myLog
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Adds the passenger to the appropriate security line
*/
@Override
public void getInLine(TransitGroup fasttrackpassenger) {

/**
* Returns the color of Fast Track Passenger
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class is a sub-class of Passenger to create Ordinary Passenger
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** The maximum process time of Ordinary Passenger. */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* Constructs an Ordinary Passenger object with values of arrivalTime,
* processTime and myLog.
* 
* @param arrvialTime
* @param processTime
* @param myLog
*/
public OrdinaryPassenger(int arrvialTime, int processTime, Reporter myLog) {

/**
* Adds the passenger to the appropriate security line
*/
@Override
public void getInLine(TransitGroup ordinarypassenger) {

/**
* Returns the color of Ordinary Passenger
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class is a super class of FastTrackPassenger, OrdinaryPassenger and
* TrustedTraveler; This class creates the passenger objects of three types and
* the passenger objects are created by Ticketing class;

* 
*
*/
public abstract class Passenger {

/** The minimum process time of each passenger. */
public static final int MIN_PROCESS_TIME = 20;

/** The time that the passenger joins a security line. */
private int arrivalTime;

/** The time that the passenger will spend in a security line. */
private int waitTime;

/**
* The amount of time that the passenger spends at the front of the security
* line
*/
private int processTime;

/**
* The index of the security checkpoint line that the passenger selects and
* joins.
*/
private int lineindex = -1;

/** Checks the passenger whether selecting and joining a security line. */
private boolean waitingProcessing;

/**
* Constructs a Passenger object with values of arrivalTime, processTime and
* myLog.
* 
* @param arrivalTime
* @param processTime
* @param myLog
*/
public Passenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Returns the passenger's arrival time
* 
* @return the arrivalTime
*/
public int getArrivalTime() {

/**
* Returns the passenger's wait time
* 
* @return the waitTime
*/
public int getWaitTime() {

/**
* Sets the passenger's wait time
* 
* @param waitTime the waitTime to set
*/
public void setWaitTime(int waitTime) {

/**
* Returns the passenger's process time
* 
* @return the processTime
*/
public int getProcessTime() {

/**
* Returns the the index of line which passenger selects
* 
* @return the lineindex
*/
public int getLineIndex() {

/**
* Returns the passneger's waiting process
* 
* @return the waitingProcessing
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when he/she finishes security
* checks
* 
*/
public void clearSecurity() {

/**
* @param lineindex the lineindex to set
*/
protected void setLineindex(int lineindex) {

/**
* An abstract method that adds the passenger to the appropriate security line
* 
* @param passenger
*/
public abstract void getInLine(TransitGroup passenger);

/**
* An abstract method that returns the color of each passenger
* 
* @return
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
* 
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* 
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* 
* @param person the Passenger to add
*/
public void add(Passenger person) {

/**
* Removes and returns the front Passenger from the queue.
* 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null if the
* queue is empty. Does not remove the Passenger from the queue.
* 
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* 
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class is a sub-class of Passenger to create the Trusted Traveler
* 
*
*/
public class TrustedTraveler extends Passenger {

/** The maximum process time of Trusted Traveler. */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Constructs an Trusted Traveler object with values of arrivalTime, processTime
* and myLog.
* 
* @param arrivalTime
* @param processTime
* @param myLog
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter myLog) {

/**
* Adds the passenger to the appropriate security line
*/
@Override
public void getInLine(TransitGroup trustedtraveler) {

/**
* Returns the color of Trusted Traveler
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents the event calendar to choose which passenger to enter
* and leave the security checkpoints
* 
*
*/
public class EventCalendar {

/**
* Constructs an event calendar object with the values of TransitGroup
* 
* @param highPriority
* @param lowPriority
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Returns the passenger from highPriority and lowPriority who will be the next
* to exit the group/line
* 
* @return next passenger to act
* @throws IllegalStateException
*/
public Passenger nextToAct() throws IllegalStateException {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class Log implements Reporter {

private int numCompleted = 0;

private int totalWaitTime = 0;

private int totalProcessTime = 0;

public Log() {

public int getNumCompleted() {

public void logData(Passenger passenger) {

public double averageWaitTime() {

public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group
* activities.
* 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* 
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* 
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* 
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* 
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

public class Simulator {

private int numPassengers = 0;

public Simulator(int numberOfCheckpoints, int numberOfPassengers, int trustPct, int fastPct, int ordPct) {

private void checkParameters() {

private void setUp() {

public boolean moreSteps() {

public void step() {

public int getCurrentIndex() {

public boolean passengerClearedSecurity() {

public Color getCurrentPassengerColor() {

public Reporter getReporter() {


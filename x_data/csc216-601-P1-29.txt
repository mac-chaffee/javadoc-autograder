---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* PreSecurity implements TransitGroup and represents passengers in the terminal
* who have not yet joined a security checkpoint line.
* 
* @version 10/10/18
*
*/
public class PreSecurity implements TransitGroup {

/** Queue to hold Passengers in PreSecurity area */
private PassengerQueue outsideSecurity;

/**
* Constructor for PreSecurity. Uses the generatePassenger factory from
* Ticketing to build outsideSecurity. Throws an IllegalArgumentException if
* numPassengers less then 1.
* 
* @param numPassengers number of Passengers to be generated
* @param log           Passenger Reporter object to log data
* @throws IllegalArgumentException if numPassengers less then one
*/
public PreSecurity(int numPassengers, Reporter log) {

 - Throws: IllegalArgumentException

/**
* Determines the departure time of the next passenger. This would be the
* ArrivalTime of the Passenger at the front of the PassengerQueue, or if the
* PassengerQueue is empty will be set to Integer.MAX_VALUE.
* 
* @return The next Passengers arrivalTime or Integer.MAX_VALUE
*/
@Override
public int departTimeNext() {

/**
* Determines the next Passenger from outsideSecurity to leave the PreSecurity
* area. This will be the Passenger at the front of the PassengerQueue.
* 
* @return Passenger at front of PassengerQueue or null if empty.
*/
@Override
public Passenger nextToGo() {

/**
* Returns true if the PassengerQueue is not empty, otherwise returns false.
* 
* @return boolean for if PassengerQueue is empty
*/
public boolean hasNext() {

/**
* Removes the next passenger from PassengerQueue. This will throw a NoSuchElementException
* if outsideSecurity is empty.
* 
* @return the passenger who is removed
* @throws NoSuchElementException if line is empty
*/
@Override
public Passenger removeNext() {

 - Throws: NoSuchElementException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents a security checkpoint and its line. Where the front passenger is
* undergoing the actual security process and subsequent Passengers are in the
* line awaiting processing.
* 
* @version 10/10/18
*
*/
public class CheckPoint {

/**
* Time that the last passenger in line will clear security and leave the line.
* This is the departure time of the last passenger in line.
*/
private int timeWhenAvailable;

/** PassengerQueue for this CheckPoint */
private PassengerQueue line;

/**
* Constructor for CheckPoint initializes line and keeps default value for
* timeWhenAvailable.
*/
public CheckPoint() {

/**
* The number of Passengers in the line.
* 
* @return number of Passengers in the line.
*/
public int size() {

/**
* Removes the next Passenger from the line. This will return the Passenger at
* the front of the line and remove this Passenger from the line. If no
* Passenger in the line, throw a NoSuchElementException.
* 
* @return the Passenger at the front of the line
* @throws NoSuchElementException if line is empty
*/
public Passenger removeFromLine() {

 - Throws: NoSuchElementException

/**
* Returns true if the line is not empty, otherwise returns false.
* 
* @return boolean for if line is empty
*/
public boolean hasNext() {

/**
* Determines the departure time of the next passenger. This would be the amount
* of time before the next passenger will clear security, or Integer.MAX_VALUE
* if the line is empty. A Passengers departure time at a CheckPoint is the sum
* of the next Passenger's arrivalTime, waitTime, and processTime.
* 
* @return The next Passengers departure time or Integer.MAX_VALUE if line is
*         empty
*/
public int departTimeNext() {

/**
* Returns the passenger at the front of the security line.
* 
* @return Passenger at front of PassengerQueue or null if empty.
*/
public Passenger nextToGo() {

/**
* Adds the Passenger to the end of the line, sets the Passenger's waitTime, and
* update the timeWhenAvailable to reflect the new addition. The passengers
* waitTime to the time until he/she reaches front. This would be the difference
* between the last Passenger's departure time (timeWhenAvailable) and the
* passenger's departure time. If the passenger arrives after the last Passenger
* has cleared security then their waitTime is zero or if the line is empty,
* otherwise their waitTime is the difference.
* 
* @param passenger to be added to line
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* SecurityArea represents the collection of security checkpoints and their
* waiting lines. It implements the TransitGroup Interface. It is where
* Passengers line up to be processed by Security. They exit the simulation
* after processing.
* 
* 
* @version 10/10/18
*/
public class SecurityArea implements TransitGroup {

/** Constant for the maximum number of CheckPoints that can be simulated */
private static final int MAX_CHECKPOINTS = 17;

/** Constant for the minimum number of CheckPoints that can be simulated */
private static final int MIN_CHECKPOINTS = 3;

/** Error message for invalid number of CheckPoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/**
* Error message for any attempt to add to a security checkpoint line with an
* improper index.
*/
private static final String ERROR_INDEX = "Index out of range for this security area";

/** The largest index for lines reserved for Fast Track passengers */
private int largestFastIndex;

/** The largest index for lines reserved for SA PreCheck/Trusted Travelers */
private int tsaPreIndex;

/** Array of CheckPoints */
private CheckPoint[] check;

/**
* Constructor for SecurityArea. The numCheckPoints will determine the size of
* the check array which holds the CheckPoints. Throws a
* IllegalArgumentException if Parameter is out of acceptable range
* 
* @param numCheckPoints number of checkpoints to build
* @throws IllegalArgumentException if numCheckPoints greater then Max or less then Min
*/
public SecurityArea(int numCheckPoints) {

 - Throws: IllegalArgumentException

/**
* Helper method to determine if we have a valid number of check points.
* 
* @param numCheckPoints number of CheckPoints
* @return true if valid else false
*/
private boolean numGatesOK(int numCheckPoints) {

/**
* Adds the passenger to the security line with the index given by the first
* parameter.
* 
* @param passenger to be added to line at index in check
* @param index     is the index in check corresponding to the security line
*
*/
public void addToLine(int index, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
* Returns the index of the shortest security line that ordinary passengers are
* permitted to select. Looks at the CheckPoints between largestFastIndex and
* tsaPreIndex. If multiple lines are the shortest, the one with the smallest
* index is returned.
* 
* @return the index in check array for shortest regular CheckPoint
*/
public int shortestRegularLine() {

/**
* Returns the index of the shortest security line restricted to Fast Track
* passengers. Looks at the CheckPoints between index 0 and largestFastIndex in
* check. If multiple lines are the shortest, the one with the smallest index is
* returned.
* 
* @return the index in check array for shortest Fast Track CheckPoint
*/
public int shortestFastTrackLine() {

/**
* Returns the index of the shortest security line restricted to TSA
* PreCheck/Trusted Traveler passengers. This would be the last line (index) of
* check.
* 
* @return the last index of check
*/
public int shortestTSAPreLine() {

/**
* Returns the number of passengers in the line with the given index.
* 
* @param index CheckPoint location in check
* 
* @return index of CheckPoint in check
* @throws IllegalArgumentException if the index is out of range
*/
public int lengthOfLine(int index) {

 - Throws: IllegalArgumentException

/**
* Returns the next passenger to clear security, or null if there is none.
* 
* @return Passenger at the front of a CheckPoint to be processed
*/
@Override
public Passenger nextToGo() {

/**
* Returns the time that the next passenger will finish clearing security, or
* Integer.MAX_VALUE if all lines are empty
* 
* @return the departure time of the nextToGo Passenger
*/
@Override
public int departTimeNext() {

/**
* Removes and returns the next passenger to clear security.
* 
* @return the nextToGo Passenger and remove from CheckPoint
*/
@Override
public Passenger removeNext() {

/**
* Returns the shortest line in check between two indices. This helper method
* can be used to determine which line a Passenger should join.
* 
* @param l1 lower index
* @param l2 upper index
* 
* @return The index in check between two parameters that has the shortest line
*/
private int shortestLineInRange(int l1, int l2) {

/**
* Returns the line that is next to be processed. This would correspond to the
* Passenger with the smallest departure time at the front of each CheckPoint.
* 
* @return the line in check with the front passenger having the smallest
*         departure time
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Child class of Passenger. Represents a FastTrack Passenger who can only join
* FasdtTrack or Ordinary security checkpoint lines.
* 
* @version 10/11/18
*/
public class FastTrackPassenger extends Passenger {

/**
* Constant for the maximum amount of time a Passenger will take to be processed
*/
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* Color of the Passenger represents if they will wait a long or short time to
* be processed
*/
private Color color;

/**
* FastTrackPassenger Constructor calls Passenger parent. Initialize Color based
* on range of process time. If pTime is less then the halfway point of the
* range of possible processTime values then assign color a light shade else
* dark shade.
* 
* @param aTime Passenger arrivalTime
* @param pTime Passenger processTime
* @param log   Reporter object for Passenger to log info
*/
public FastTrackPassenger(int aTime, int pTime, Reporter log) {

/**
* Returns the Color of the Passenger which represents, roughly, if the
* Passenger will take less then or more then the halfway point in the range of
* possible Color values.
* 
* @return Color of the passenger.
*/
@Override
public Color getColor() {

/**
* Passenger picks a line based on the shortest line in Security Area that the
* passenger is allowed to join. Uses SecurityArea.shortestFastTrackLine() and
* SecurityArea.shortestRegularLine() to determine shortest lines in respective
* ranges. SecurityArea.lengthOfLine() used to get the actual length of each
* line. These values are compared to find which is the shortest. The line with
* a smaller value of length is returned, or if it's a tie then the line with
* smaller index is returned.
* 
* @param securityArea is the TransitGroup implemented in SecurityArea
* @return the index of the line chosen.
*/
private int pickLine(TransitGroup securityArea) {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the
* parent class, and then places this passenger in the selected line. Chosen line
* found from pickLine. 
* 
* @param securityArea is the TransitGroup implemented in SecurityArea
*/
@Override
public void getInLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Child class of Passenger. Represents an Ordinary Passenger who can only join
* Ordinary security checkpoint lines.
* 
* @version 10/11/18
*/
public class OrdinaryPassenger extends Passenger {

/**
* Constant for the maximum amount of time a Passenger will take to be processed
*/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* Color of the Passenger represents if they will wait a long or short time to
* be processed
*/
private Color color;

/**
* OrdinaryPassenger Constructor calls Passenger parent. Initialize Color based
* on range of process time. If pTime is less then the halfway point of the
* range of possible processTime values then assign color a light shade else
* dark shade.
* 
* @param aTime Passenger arrivalTime
* @param pTime Passenger processTime
* @param log   Reporter object for Passenger to log info
*/
public OrdinaryPassenger(int aTime, int pTime, Reporter log) {

/**
* Returns the Color of the Passenger which represents, roughly, if the
* Passenger will take less then or more then the halfway point in the range of
* possible Color values.
* 
* @return Color of the passenger.
*/
@Override
public Color getColor() {

/**
* Passenger picks a line based on the shortest line in Security Area that the
* passenger is allowed to join. Uses SecurityArea.shortestRegularLine() to
* determine shortest line.
* 
* @param securityArea is the TransitGroup implemented in SecurityArea
* @return the index of the line chosen.
*/
private int pickLine(TransitGroup securityArea) {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the
* parent class, and then places this passenger in the selected line. Chosen line
* found from pickLine. 
* 
* @param securityArea is the TransitGroup implemented in SecurityArea
*/
@Override
public void getInLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Abstract base class for certain types of Passengers. Created by the Ticketing
* factory.
* 
* @version 10/11/18
*
*/
public abstract class Passenger {

/** Constant for the minimum process time for all Passengers */
public static final int MIN_PROCESS_TIME = 20;

/** Time that the passenger joins a security line */
private int arrivalTime;

/** Time the passenger will spend in a security line */
private int waitTime;

/**
* Amount of time that the passenger spends at the front of the security line
* actually undergoing the security check
*/
private int processTime;

/**
* Index of the security checkpoint line that the passenger selects and joins
*/
private int lineIndex;

/** Boolean describing if the passenger is currently in security line */
private boolean waitingProcessing;

/** Reporting mechanism for Passenger */
private Reporter myLog;

/**
* Constructor for Passenger which uses Ticketing to generate a Passenger. This
* class is abstract so cannot be directly constructed, will be used in child
* classes to initialize state.
* 
* @param aTime arrival time of the passenger
* @param pTime process time of the passenger
* @param log   Passenger Reporter object to log data
*/
public Passenger(int aTime, int pTime, Reporter log) {

 - Throws: IllegalArgumentException

/**
* Getter for Passenger arrivalTime
* 
* @return int for the this Passengers arrivalTime
*/
public int getArrivalTime() {

/**
* Getter for Passenger waitTime
* 
* @return int for the this Passengers waitTime
*/
public int getWaitTime() {

/**
* Setter for Passenger waitTime. Computed when Passenger joins a security line.
* This will also set the waiting processing to true as this is calculated once
* the Passenger enters a Security CheckPoint. 
* 
* @param wTime the time the Passenger will spend in line until they reach
*              front.
*/
public void setWaitTime(int wTime) {

/**
* Getter for Passenger processTime
* 
* @return int for the this Passengers prcoessTime
*/
public int getProcessTime() {

/**
* Getter for Passenger lineIndex. The index of the security checkpoint line in
* security area.
* 
* @return int for the this Passengers lineIndex
*/
public int getLineIndex() {

/**
* Determines if the Passenger has entered a Security line.
* 
* @return boolean true if Passenger in line else false
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when he/she finishes security
* checks. A Passenger will have to log their data and then indicate they are no longer
* waiting in the security line. 
*/
public void clearSecurity() {

/**
* Setter for Passenger waitTime. Computed when Passenger joins a security line.
* 
* @param index line index represents position of checkpoint in security area
*/
protected void setLineIndex(int index) {

/**
* Abstract method that adds the passenger to the appropriate security line.
* 
* @param checkPoints is the list of security lines in the airport
*/
public abstract void getInLine(TransitGroup checkPoints);

/**
* Abstract method that returns the Color the of the Passenger. This is
* dependent on which Child class is being used.
* 
* @return Color of the Passenger
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Child class of Passenger. Represents a TrustedTraveler Passenger who can only
* join Precheck or Ordinary security checkpoint lines.
* 
* @version 10/11/18
*/
public class TrustedTraveler extends Passenger {

/**
* Constant for the maximum amount of time a Passenger will take to be processed
*/
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Color of the Passenger represents if they will wait a long or short time to
* be processed
*/
private Color color;

/**
* TrustedTraveler Passenger Constructor calls Passenger parent. Initialize
* Color based on range of process time. If pTime is less then the halfway point
* of the range of possible processTime values then assign color a light shade
* else dark shade.
* 
* @param aTime Passenger arrivalTime
* @param pTime Passenger processTime
* @param log   Reporter object for Passenger to log info
*/
public TrustedTraveler(int aTime, int pTime, Reporter log) {

/**
* Returns the Color of the Passenger which represents, roughly, if the
* Passenger will take less then or more then the halfway point in the range of
* possible Color values.
* 
* @return Color of the passenger.
*/
@Override
public Color getColor() {

/**
* Passenger picks a line based on the shortest line in Security Area that the
* passenger is allowed to join. Uses SecurityArea.shortestTSAPreLine() and
* SecurityArea.shortestRegularLine() to determine shortest lines in respective
* ranges. SecurityArea.lengthOfLine() used to get the actual length of each
* line. These values are compared to find which is the correct line to join.
* The TSA line is always chosen unless its length exceeds 2 times the length of
* the shortest regular line.
* 
* @param securityArea is the TransitGroup implemented in SecurityArea
* @return the index of the line chosen.
*/
private int pickLine(TransitGroup securityArea) {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the
* parent class, and then places this passenger in the selected line. Chosen
* line found from pickLine.
* 
* @param securityArea is the TransitGroup implemented in SecurityArea
*/
@Override
public void getInLine(TransitGroup securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* EventCalendar determines which passenger to process next. It makes this
* decision based on which Passenger has the smallest departure time.
* 
* @version 10/11/18
*
*/
public class EventCalendar {

/** TransitGroup with a higher priority */
private TransitGroup highPriority;

/** TransitGroup with a lower priority */
private TransitGroup lowPriority;

/**
* Constructor for EventCalender. The first parameter is used to initialize the
* highPriority TransitGroup while the second is used for lowPriority.
* 
* @param tGroup1 TransitGroup with higher priority
* @param tGroup2 TransitGroup with lower priority
*/
public EventCalendar(TransitGroup tGroup1, TransitGroup tGroup2) {

/**
* This method determines which Passenger should be acted on next. Meaning it
* looks at the departure times of both highPriority and lowPriority Passengers
* and returns the one with the smallest depertureTime. In the case of a tie the
* highPriority passenger is returned. This method will throw an
* IllegalStateException if if both TransitGroups are empty.
* 
* @return Passenger to be acted on
* @throws IllegalStateException if TransitGroups are empty (null)
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Log implements the Reporter interface which describes reporting methods for
* groups of passengers. It keeps track of the number of passengers processed
* and helps to calculate the average values for waitTime and processTime.
* 
*
*/
public class Log implements Reporter {

/** Number of passengers completed processing */
private int numCompleted;

/** total time spent in line for all passengers */
private int totalWaitTime;

/** total time spent in processing for all passengers */
private int totalProcessTime;

/**
* Constructor for Log. Default values used to initialize fields. 
*/
public Log() {

/**
* This will return the total number of Passengers that made it through a security 
* checkpoint.
* 
* @return Number of Passengers that have completed processing.
*/
@Override
public int getNumCompleted() {

/**
* Log the data for a passenger. This data would be the wait time and processing time 
* and should be added to the total values. 
* 
* @param p - passenger whose data is to be logged
*/
@Override
public void logData(Passenger p) {

/**
* How long did passengers have to wait before completing processing?
* totalWaitTime/totalPassengers
* @return average passenger waiting time (in minutes).
*/
@Override
public double averageWaitTime() {

/**
* How long did it take passengers to complete processing?
* totalProcessingTime/total Passengers
* 
* @return average processing time
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The simulator class controls the progression of the simulation. It will
* advance the simulation for each event and also hold data for reporting
* purposes.
* 
* @version 10/11/18
*
*/
public class Simulator {

/** number of Passengers in the simulation */
private int numPassengers;

/** Current Passenger is the one returned from EventCalender */
private Passenger currentPassenger;

/** Reporter that collects data for the simulation */
private Reporter log;

/** Group of Passengers in SecurityArea */
private TransitGroup inSecurity;

/** Group of Passengers in PreSecurityArea */
private TransitGroup inTicketing;

/** Calendar instance that tells the simulation who to act on */
private EventCalendar myCalender;

/**
* Constructor for Simulator which checks input for validity (used the
* checkParameters method) and then establishes state for log, the distribution
* of Passenger types for the Ticketing factory, builds the SecurityArea,
* PreSecurity Area, and an EventCalender. Parameters used come from user input.
* 
* @param nCheckpoints     number of checkpoints to simulate
* @param nPassengers      total number of passengers
* @param percentTrusted   percent of the total passengers that are Trusted
*                         members
* @param percentFastTrack percent of the total passengers that are FastTrack
*                         members
* @param percentOrdinary  percent of the total passengers that are Ordinary
*                         members
*/
public Simulator(int nCheckpoints, int nPassengers, int percentTrusted, int percentFastTrack, int percentOrdinary) {

/**
* Helper method used to check for valid input values.
* 
* @param nCheckpoints     number of checkpoints to simulate
* @param nPassengers      total number of passengers
* @param percentTrusted   percent of the total passengers that are Trusted
*                         members
* @param percentFastTrack percent of the total passengers that are FastTrack
*                         members
* @param percentOrdinary  percent of the total passengers that are Ordinary
*                         members
*/
private void checkParameters(int nCheckpoints, int nPassengers, int percentTrusted, int percentFastTrack,
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Helper method that calculates the distribution of Passenger types.
* 
* @param percentTrusted   percent of the total passengers that are Trusted
*                         members
* @param percentFastTrack percent of the total passengers that are FastTrack
*                         members
* @param percentOrdinary  percent of the total passengers that are Ordinary
*                         members
*/
private void setUp(int percentTrusted, int percentFastTrack, int percentOrdinary) {

/**
* Getter for log.
* 
* @return the Reporter log
*/
public Reporter getReporter() {

/**
* Goes through the action of the next passenger from the EventCalendar. If the
* passenger is from the ticketing area, this call to step() implements the
* process of moving currentPassenger to the SecurityArea. If the passenger is
* from the security area, this call to step() implements the process of sending
* a Passenger through Security processing and leaving simulation. In each case,
* the passenger is removed from the corresponding area. If there is no next
* passenger, an IllegalStateException is thrown.
* 
* @throws IllegalStateException if currentPassenger is null
*/
public void step() {

 - Throws: IllegalStateException

/**
* Tells the simulation if there are more passengers to move/process. True if
* the numPassengers equals the the number of completed passengers recorded in
* the log.
* 
* @return true if there are more Passengers to be processed/moved else false
*/
public boolean moreSteps() {

/**
* This method returns the index of the CheckPoint in SecurityArea that
* currentPassenger is either entering or leaving from.
* 
* @return the index of the checkpoint in Security the Passenger is in, will be
*         in, and will leave from.
*/
public int getCurrentIndex() {

/**
* Simple getter for currentPassengers Color.
* 
* @return currentPassenger color
*/
public Color getCurrentPassengerColor() {

/**
* Tells the simulation if the currentPassenger just cleared a Security
* Checkpoint and finished processing.
* 
* @return true if the Passenger has finished processing otherwise false
*         (including when currentPassenger is null)
*/
public boolean passengerClearedSecurity() {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This is a class use to set Pre security
*
*/
public class PreSecurity implements TransitGroup {

/** PassengerQueue outside security **/
private PassengerQueue outsideSecurity;

/**
* Constructor of PreSecurity
* @param p get a new passenger
* @param log get a new reporter
*/
public PreSecurity(int p, Reporter log) {

 - Throws: IllegalArgumentException

/**
* get depart time
* @return next depart time
*/
public int departTimeNext() {

/**
* Decide where passenger next to go
* @return next passenger
*/
public Passenger nextToGo() {

/**
* Check if has next
* @return false if PassengerQueue is empty
* 		   true if not
*/
public boolean hasNext() {

/**
* Remove next passenger
* @return remove passenger
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Use to get check point
*
*/
public class CheckPoint {

/** get the time when check point available**/
private int tWA;

/** PassengerQueue line**/
private PassengerQueue line;

/**
* Constructor of CheckPoint
*/
public CheckPoint() {

/**
* Get size of check point
* @return size of line
*/
public int size() {

/**
* Remove passenger from the line
* @return remove passenger
*/
public Passenger removeFromLine() {

/**
* Check if has next
* @return false if line is empty
* 		   true if not
*/
public boolean hasNext() {

/**
* Get depart time
* @return next depart time
*/
public int departTimeNext() {

/**
* Decide which check point passenger need to go
* @return next passenger
*/
public Passenger nextToGo() {

/**
* Add passenger to line
* @param p get a new passenger 
*/
public void addToLine(Passenger p) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Set the security areas
*
*/
public class SecurityArea implements TransitGroup {

/** max checkpoint number **/
public static final int MAX_CHECKPOINTS = 17;

/** min check point number **/
public static final int MIN_CHECKPOINT = 3;

/** message of error check points **/
public static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** message of error index **/
public static final String ERROR_INDEX = "Index out of range for this security area";

/** number of largest fast index **/
private int largestFIndex;

/** number of TSA Pre index **/
private int tsaPreIndex;

/** An array list of checkpoint **/
private ArrayList<CheckPoint> check;

/**
* Constructor of SecurityArea
* @param numOfCheckPoint number of check point
*/
public SecurityArea(int numOfCheckPoint) {

 - Throws: IllegalArgumentException

/**
* Check if number of gates is done
* @param numOfCheckPoint number of check point
* @return true if gates number is ok
* 	       false if not
*/
private boolean numGatesOK(int numOfCheckPoint) {

/**
* Add passenger to line
* @param i index of checkpoint
* @param p get a new passenger
*/
public void addToLine(int i, Passenger p) {

 - Throws: IllegalArgumentException

/**
* Get shortest regular line
* @return a shortest regular line
*/
public int shortestRegularLine(){

/**
* Get shortest Fast Track line
* @return a index of shortest Fast track line
*/
public int shortestFastTrackLine() {

/**
* Get shortest TSA Pre line
* @return index of shortest TSA pre line
*/
public int shortestTSAPreLine() {

/**
* Get the length of the line
* @param i line index
* @return length of line
*/
public int lengthOfLine(int i) {

 - Throws: IllegalArgumentException

/**
* Get depart time
* @return next depart time
*/
public int departTimeNext() {	
/**
* Get next passenger
* @return next passenger
*/
public Passenger nextToGo() {	
/**
* Remove next passenger
* @return p next passenger
*/
public Passenger removeNext() {	
/**
* Get the shortest line in range
* @param x lowest index
* @param y highest index
* @return i index of shortest line
*/
private int shortestLineInRange(int x, int y) {

/**
* Get the clear line number
* @return a line index
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Set one type of passenger
*
*/
public class FastTrackPassenger extends Passenger {

/** max process time for FastTrackPassenger**/
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** color of passenger**/
private Color color;

/**
* Constructor of Fast Track Passenger
* @param arrivalTime passenger arrival time
* @param processTime passenger process time
* @param reporter get a new reporter
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter reporter) {

@Override
public Color getColor() {

@Override
public void getInLine(TransitGroup tg) {

/**super.setLineIndex(pickLine(tg));**/
/**
* Set a line for Fast Track passengers
* @param tg get a new TransitGroup
* @return a if RegularLine is shorter
*         b if FastTrackLine is shorter
*/
private int pickLine(TransitGroup tg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Set one type of passenger
*
*/
public class OrdinaryPassenger extends Passenger {

/** max process time for ordinary passenger**/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** color of passenger **/
private Color color;

/**
* Constructor of ordinary passenger
* @param arrivalTime passenger arrival time
* @param processTime passenger process time
* @param reporter get a new reporter
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter reporter) {

@Override
public Color getColor() {

@Override
public void getInLine(TransitGroup tg) {

/**super.setLineIndex(pickLine(tg));**/
/**
* Set a line for ordinary passenger
* @param tg get a new TransitGroup
* @return shortestRegularLine
*/
private int pickLine(TransitGroup tg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Use to set passenger type and number
*
*/
public abstract class Passenger {

/**the min process time for every type of passenger **/
public static final int MIN_PROCESS_TIME = 20;

/** passenger arrival time**/
private int arrivalTime;

/** passenger wait time**/
private int waitTime;

/** passenger process time**/
private int processTime;

/** index of the line**/
private int lineIndex = -1;

/** if need to wait for processing**/
private boolean waitingProcessing;

/** Interface mylog **/
private Reporter mylog;

/**
* Constructor of Passenger
* @param arrivalTime passenger arrival time
* @param processTime passenger process time
* @param mylog get a new reporter
*/
public Passenger(int arrivalTime, int processTime, Reporter mylog) {

 - Throws: IllegalArgumentException

/**
* Get arrival time
* @return arrival time
*/
public int getArrivalTime() {

/**
* Get wait time
* @return waitTime
*/
public int getWaitTime() {

/**
* Set wait time
* @param waitTime passenger wait time
*/
public void setWaitTime(int waitTime) {

/**
* Get process time
* @return processTime
*/
public int getProcessTime() {

/**
* Get line index
* @return lineIndex
*/
public int getLineIndex() {

/**
* Check if need to wait in security line
* @return false if waiting processing is false or line index is -1
*         true if converse
*/
public boolean isWaitingInSecurityLine() {

/**
* Clear the security and log passengers data
*/
public void clearSecurity() {

/**
* Set line index
* @param a index of the line
*/
protected void setLineIndex(int a) {

/**
* Set the passenger in line
* @param tg get a new transitgroup
*/
public abstract void getInLine(TransitGroup tg);

/**
* Get the color
* @return passenger color
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Set one type of passenger
*
*/
public class TrustedTraveler extends Passenger {

/** the max process time for TrustedTraveler**/
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** color of passenger **/
private Color color;

/**
* Constructor of TrustedTraveler
* @param arrivalTime passenger arrival time
* @param processTime passenger process time
* @param reporter get a new reporter
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter reporter) {

@Override
public Color getColor() {

@Override
public void getInLine(TransitGroup tg) {

/**super.setLineIndex(pickLine(tg));**/
/**
* Set a line for passenger
* @param tg get a new TransitGroup
* @return a if TSAPreLine is 2 times longer than RegularLine and the length of TSAPreLine is not 0
*         b if TSAPreLine is not 2 times longer than RegularLine and the length of TSAPreLine is not 0
*/
private int pickLine(TransitGroup tg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Decide which group of people can go next
*
*/
public class EventCalendar {

/** TransitGroup with high priority **/
private TransitGroup highPriority;

/** TransitGroup with low priority **/
private TransitGroup lowPriority;

/**
* Constructor of EventCalendar
* @param highPriority get a new Transitgroup
* @param lowPriority get a new Transitgroup
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Decide which group of people can go next
* @return lowPriority if highPriority do not have next or highPriority depart time is bigger
* 	       highPriority if converse
* @throws IllegalStateException if both group do not have next
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* An implement class of Reporter
*/
public class Log implements Reporter {

/** get a complete number **/
private int numCompleted;

/** total wait time **/
private int totalWaitTime;

/** total process time**/
private int totalProcessTime;

/**
* Constructor of this class
*/
public Log() {

/**
* Get numCompleted
* @return numCompleted
*/
public int getNumCompleted() {

/**
* Get log data
* @param p get a new passenger
*/
public void logData(Passenger p) {

/**
* Get average wait time
* @return average wait time
* 		   0.0 if do not have completed passenger
*/
public double averageWaitTime() {

/**
* Get average process time
* @return average process time
*         0.0 if do not have completed passenger
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A simulator can get input data and get index and color information
*
*/
public class Simulator {

/** number of passengers **/
private int numberOfPassengers;

/** Event Calendar **/
private EventCalendar myCalendar;

/** Current Passenger **/
private Passenger currentPassenger;

/** TransitGroup in security **/
private TransitGroup inSecurity;

/** TransitGroup in ticketing **/
private TransitGroup inTicketing;

/** Reporter log **/
private Reporter log;

/** PreSecurity **/
private PreSecurity p;

/** Security Area **/
private SecurityArea s;

/**
* Constructor of Simulator
* @param numberOfCheckpoints number of checkpoint
* @param numberOfPassengers number of passengers
* @param percentOfTSA number of TSA passengers
* @param percentOfFT number of Fast Track passengers
* @param percentOfOP number of ordinary passengers
*/
public Simulator(int numberOfCheckpoints, int numberOfPassengers, int percentOfTSA, int percentOfFT, int percentOfOP){

/**
* Check parameters if they are valid
* @param numberOfPassengers number of passengers
* @param percentOfTSA number of TSA passengers
* @param percentOfFT number of Fast Track passengers
* @param percentOfOP number of ordinary passengers
*/
private void checkParameters(int numberOfPassengers, int percentOfTSA, int percentOfFT, int percentOfOP) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Set up the percent of passengers
* @param percentOfTSA percent of TSA passengers
* @param percentOfFT percent of Fast Track passengers
* @param percentOfOP percent of ordinary passengers
*/
private void setUp(int percentOfTSA, int percentOfFT, int percentOfOP) {

/**
* Get reporter
* @return Reporter log
*/
public Reporter getReporter() {

/**
* Decide which line passenger need to go next
*/
public void step() {

 - Throws: IllegalStateException

/**this.myCalendar = new EventCalendar(inTicketing, inSecurity);**/
/**this.myCalendar = new EventCalendar(inTicketing, inSecurity);**/
/**
* Check if have more steps
* @return true if number completed is smaller than number of passengers
* 	       false if converse
*/
public boolean moreSteps() {

/**
* Get current index
* @return current index
* 		-1 if current passenger is null
*/
public int getCurrentIndex() {

/**
* Get current passenger color
* @return current color
* 	       null if current passenger is null
*/
public Color getCurrentPassengerColor() {

/**
* Check if security has passengers
* @return false if current passenger is null or current passenger is waiting in line
*         true if converse
*/
public boolean passengerClearedSecurity() {


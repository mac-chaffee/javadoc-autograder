---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* this class represents passengers who have not yet entered a security checkpoint line
* 
*
*/
public class PreSecurity implements TransitGroup {

/** creates a passenger Queue somehow **/
private PassengerQueue outsideSecurity;

/**
* creates passengers and a reporter log and adds them
* to the queue
* 
* @param number
* 			the number of passengers to generate
* @param reporter
* 			the reporter log
* 
* @throws IllegalArgumentException
* 			throws and IAE if the number of passengers is negative
*/
public PreSecurity(int number, Reporter log) {

 - Throws: IllegalArgumentException

/**
* returns the arrival time of the passenger in the
* front of the line.
* 
* @return int
* 			the time of departure
*/
public int departTimeNext() {

/**
* gives the next passenger
* 
* @return Passenger
* 		The next passenger to go
*/
public Passenger nextToGo() {

/**
* Whether or not there is another Passenger
* @return boolean
* 			if there is another passenger 
*/
public boolean hasNext() {

/**
* removes the next passenger
* @return Passenger
* 		returns the passenger to be removed
*/
public Passenger removeNext() {

/**
* for checking if a queue was made in testing
* @return PassengerQueue
* 			the queue that was instantiated by 
* 			the constructor call
*/
protected PassengerQueue getOutSideSecurity() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* represents a security checkpoint line and its line, 
* where the front passenger is undergoing the actual security process.
* It delegates strict queue activities to its instance variable line.
* 
*
*/
public class CheckPoint {

/**the time a checkpoint is available**/
private int timeWhenAvailable;

/**a queue of passengers **/
private PassengerQueue line;

/**
* creates a checkpoint
*/
public CheckPoint() {

/**
* size of the line
* 
* @param int
* 		size of the line
*/
public int size() {

/**
* remove from the line 
* @return Passenger
* 		passenger that is removed from the line
*/
public Passenger removeFromLine() {

/**
* whether or not a checkpoint has a passenger or not
* 
* @return boolean
* 		if there is a passenger next in line or not
*/
public boolean hasNext() {

/**
* time of the next departure
* 
* @return int
* 		time a person is going to depart
*/
public int departTimeNext() {

/**
* selects the next passenger to go
* 
* @return Passenger
* 		the next passenger to go
*/
public Passenger nextToGo() {

/**
* adds a passenger to a line
* 
* @param Passenger
* 		The passenger to be added to a line
*/
public void addToLine(Passenger passenger) {

/**
* getter for timewhenavailabe
* @return int
* 		time when available
*/
public int getTimeWhenAvailable() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
* Represents the collection of security
* checkpoints and their waiting lines.
*
*/
public class SecurityArea implements TransitGroup {

/**maximum number of checkpoints**/
public static final int MAX_CHECKPOINTS = 17;

/**minimum number of checkpoints**/
public static final int MIN_CHECKPOINTS = 3;

/**Error message from incorrect number of checkpoints**/
public static final String ERROR_CHECKPOINTS = 
/**Error for index out of range**/
public static final String ERROR_INDEX = 
/**the largest index for lines reserved for Fast Track passengers **/
private int largestFastIndex;

/**the index of the line reserved for TSA PreCheck/Trusted Travelers**/
private int tsaPreIndex;

/**array of checkpoints**/
private CheckPoint check [];

/**
* creates a security checkpoints
* 
* @param numCheckpoints
* 		the number of checkpoints
* 
* @throws IllegalArgumentException
* 		IAE is thrown if the number of checkpoints
* 		is outside of acceptable range
*/
public SecurityArea (int numCheckpoints) {

 - Throws: IllegalArgumentException

/**
* there is not information about what this does
* 
* @param numberOK
* 		number of gates
* @return boolean
* 		there is not information about what this does
*/
private boolean numGatesOK(int numberOK) {

/**
* adds a passenger to a security line
* 
* @param index
* 	index of which checkpoint to add the passenger in the queue
* @param passenger
* 	The passenger to be added
*/
public void addToLine(int index, Passenger passenger) {

/**
* gives the shortest line that an ordinary
* passenger can access
* 
* @return int 
* 		returns the index of the shortest line
* 		for an ordinary passenger
*/
public int shortestRegularLine() {

/**
* gives the shortest line that TSA passengers 
* have access to
* 
* @return int
* 		the index of the shortest line that TSA passengers
* 		can access
*/
public int shortestTSAPreLine() {

/**
* gives the shortest line that FastTrack passengers 
* have access to
* 
* @return int
* 		the index of the shortest line that FastTrack passengers
* 		can access
*/
public int shortestFastTrackLine() {

/**
* gives size of the line of the checkpoint with
* the given index
* 
* @param index
* 		the index of the checkpoint
* @return int
* 		the length of the security line
*/
public int lengthOfLine(int index) {

/**
* gives the depart time of the next passenger
* 
* @return int
* 	the departure time
*/
public int departTimeNext() {

/**
* gives the passenger that is next to be processed
* 
* @return Passenger
* 		the passenger that is next to be processed
*/
public Passenger nextToGo() {

/**
* gives the next passenger to be processed and
* removed from the line
* 	  
* @return Passenger
* 	the passenger to be processed and removed
* 	from the line
*/
public Passenger removeNext() {

/**
* returns the checkpoint in question, used
* for testing (won't be called in actual program)
* 
* @param idx
* 			index of the checkpoint in the checkpoint
* 			array
* @return CheckPoint
* 			the checkpoint of the index from the array
*/
protected CheckPoint getCheckPoint(int idx) {

//	/**
//private int shortestLineInRange(int range1, int range2) {

//	/**
//	private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
* 
* -adapted into William's project
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* creates a FastTrackPassenger that extends passenger and knows its own color.
* 
*
*/
public class FastTrackPassenger extends Passenger {

/** maximim expected processing time **/
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** the color of the passenger **/
public Color color;

/**
* Creates a trusted traveler with a color of Green
* 
* @param waitTime
* 		The time a passenger waits
* @param arrivalTime
* 		the time that a passenger arrives into the airport
* @param reporter
* 		updates a log that each passenger has
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter reporter) {

/**
* finds to optimal line for a passenger to enter
* 
* @param transitgroup
* 			Transitgroup
* @return int
* 			int is the security line that the passenger should choose
*/
private int pickLine (TransitGroup transitgroup) {

/**
* puts the passenger in a given security line
*/
@Override
public void getInLine(TransitGroup transitgroup) {

/**
* sets the color of a passenger based on their kind and process time
* @return Color
* 			returns the color of the passengers based on its
* 			kind and process time.
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* creates an OrdinaryPassenger that extends passenger and knows its own color.
* 
*
*/
public class OrdinaryPassenger extends Passenger {

/** maximim expected processing time **/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** the color of the passenger **/
public Color color;

/**
* Creates a trusted traveler with a color of Red
* 
* @param waitTime
* 		The time a passenger waits
* @param arrivalTime
* 		the time that a passenger arrives into the airport
* @param reporter
* 		updates a log that each passenger has
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter reporter) {

/**
* finds to optimal line for a passenger to enter
* 
* @param transitgroup
* 			Transitgroup
* @return int
* 			int is the security line that the passenger should choose
*/
private int pickLine (TransitGroup transitgroup) {

/**
* puts the passenger in a given security line
*/
@Override
public void getInLine(TransitGroup transitgroup) {

/**
* gets the color of a passenger based on their kind and process time
* @return Color
* 			returns the color of the passengers based on its
* 			kind and process time.
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* an abstract method that provides functionality for passengers and
* manages arrival time, wait time, process time, line index and if 
* a passenger is waiting to be processed or not
* 
*
*/
public abstract class Passenger {

/** sets the minimum process time to 20 seconds **/
public static final int MIN_PROCESS_TIME = 20;

/** the arrival time of a passenger **/
private int arrivalTime;

/** the wait time of a passenger **/
private int waitTime;

/** the process time of a passenger **/
private  int processTime;

/** the spot in line of a passenger **/
private int lineIndex;

/** If a passenger is awaiting processing **/
private boolean waitingProcessing;

/** an instance of the log for each passenger **/
private Reporter myLog;

/** abstract method for choosing a line **/
public abstract void getInLine (TransitGroup transitgroup);

/** abstract method for finding a passenger's color **/
public abstract Color getColor();

/**
* creates a Passenger
* 
* @param waitTime
* 		sets the amount of time a passenger waits
* @param arrivalTime
* 		sets the arrival time of a passenger
* @param reporter
* 		creates a log for a passenger
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

/**
* provides the arrival time of a passenger
* @return int
* 			the time of arrival
*/
public int getArrivalTime() {

/**
* provides the wait time of a passenger
* 
* @return int
* 			the wait time of a passenger
*/
public int getWaitTime() {

/**
* sets the wait time of a passenger
* 
* @param waitTime
* 			the time that a passenger will wait
*/
public void setWaitTime(int waitTime) {

/**
* provides the process time of a passenger
* 
* @return int
* 		The time that a passenger will take to be processed
*/
public int getProcessTime() {

/**
* provides the spot a passenger is in line
* 
* @return int
* 			the location in line a passenger is
*/
public int getLineIndex() {

/**
* provides whether or not a passenger is in line
* 
* @return boolean
* 			whether or not a passenger is in line
*/
public boolean isWaitingInSecurityLine() {

/**
* sets waiting processing to false
* (no longer waiting to be processed)
*/
public void clearSecurity() {

/**
* sets the index in line for a passenger in a 
* security line
* @param number
* 			the number in line of a passenger
*/
protected void setLineIndex (int number) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
* 
* -Adapted for use by William Mitchell
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* creates a trustedTraveler that extends passenger and knows its own color.
* 
*
*/
public class TrustedTraveler extends Passenger {

/** maximim expected processing time **/
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** the color of the passenger **/
public Color color;

/**
* Creates a trusted traveler with a color of Blue
* 
* @param waitTime
* 		The time a passenger waits
* @param arrivalTime
* 		the time that a passenger arrives into the airport
* @param reporter
* 		updates a log that each passenger has
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter reporter) {

/**
* finds to optimal line for a passenger to enter
* 
* @param transitgroup
* 			Transitgroup
* @return int
* 			int is the security line that the passenger should choose
*/
private int pickLine (TransitGroup transitgroup) {

/**
* puts the passenger in a given security line
*/
@Override
public void getInLine(TransitGroup transitgroup) {

/**
* sets the color of a passenger based on their kind and process time
* @return Color
* 			returns the color of the passengers based on its
* 			kind and process time.
*/
@Override
public Color getColor() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* provides methods to determine which passengers to process next
* 
*
*/
public class EventCalendar {

/** creates a transit group of High Priority **/
private TransitGroup HighPriority;

/** creates a transit group of Low Priority **/
private TransitGroup LowPriority;

/**
* creates an Event Calendar from which to determine
* the next passenger to be processed
* @param transitGroup1
* 		the transit group representing the ticketing area
* @param transitGroup2
* 		the transit group representing the security area
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* determines which passenger needs to be processed next
* 
* @return Passenger
* 		the next passenger to be processed
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* log that keeps track of events
* 
*
*/
public class Log implements Reporter{

/**the number of steps completed**/
public int numCompleted;

/**the total wait time of all passengers**/
public int totalWaitTime;

/**the total process time of all passengers**/
public int totalProcessTime;

/**
* creates an instance of the log
*/
public Log() {

/**
* gives the number of steps completed 
* @return int
* 		the number of the steps completed
*/
public int getNumCompleted() {

/**
* logs a passenger's data
* @param passenger
* 		the passenger whom's data will be extracted
*/
public void logData(Passenger passenger) {

/**
* gives the average wait time
* 
* @return double
* 		the average wait time
*/
public double averageWaitTime() {

/**
* gives the average process time of all passengers
* 
* @return double
* 		the average process time of all passengers
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
* 
* -Adapted for use by William Mitchell
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The Simulator initializes and runs the 'simulation'
* 
*
*/
public class Simulator {

/** the number of passengers to be created for the simulation **/
private int numPassengers;

/** instance of transit group for security **/
private TransitGroup inSecurity;

/** instance of transit group for ticketing **/
private TransitGroup inTicketing;

/** the log **/
private Reporter log;

/** creates a instance of the current passenger **/
private Passenger currentPassenger;

/** creates the instance of eventCalendar **/
private EventCalendar myCalendar;

/**
* creates an instance of the simulator
* 
* @param numOfCheckpoints
* 		the number of checkpoints to be created
* @param numOfPassengers
* 		the total number of passengers to be created
* @param percentTSA
* 		the percent of the total passengers to be TSA
* @param percentFastTrack
* 		the percent of the total passengers to be fastTrack
* @param percentRegular
* 		the percent of the total passengers to be regular passengers
*/
public Simulator(int numOfCheckpoints, int numOfPassengers, 
/**
* checks the number of checkpoints, passengers and their percentages to
* ensure they fall within the correct parameters
* 
* @param numOfCheckpoints
* 		checks the total number of checkpoints
* @param numOfPassengers
* 		the total number of passengers to be created
* @param percentTSA
* 		the percent of the total passengers to be TSA
* @param percentFastTrack
* 		the percent of the total passengers to be fastTrack
* @param percentRegular
* 		the percent of the total passengers to be regular passengers
* @return 
*/
private void checkParameters (int numOfCheckpoints, int numOfPassengers, 
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* sets up Ticketing class
* 
* @param pctTrusted
* 		percent of trusted Traveler
* @param pctFast
* 		percent of fastTrack
*/
private void setUp (int pctTrusted, int pctFast) {

/**
* gives the reporter object
* 
* @return Reporter
* 		the reporter
*/
public Reporter getReporter() {

/**
* steps
*/
public void step() {

/**
* true if there are more steps to be completed
* false if all steps have been completed
* 
* @return boolean
* 		whether or not there are more steps
*/
public boolean moreSteps() {

/**
* gives the index of the current Passenger
* 
* @return int
* 		the index of the current passenger
*/
public int getCurrentIndex() {

public int getNumberOfPassengers() {

/**
* gives the color of the current passenger
* 
* @return Color
* 		the color of the current Passenger
*/
public Color getCurrentPassengerColor() {

/**
* is true if the current passenger cleared security
* is false otherwise
* 
* @return boolean
* 		whether or not a passenger has cleared security
*/
public boolean passengerClearedSecurity() {


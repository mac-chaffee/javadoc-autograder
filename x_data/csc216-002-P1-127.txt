---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls passengers who have not yet entered security checkpoint lines
*/
public class PreSecurity implements TransitGroup {

/** Contains all the passengers in presecurity */
private PassengerQueue outsideSecurity;

/**
* Constructs the passenger and adds them to the queue
* @param numPassengers number of passengers to be created
* @param log Reporter to add passenger data to
* @throws IllegalArgumentException if numPassengers is less than 1
*/
public PreSecurity(int numPassengers, Reporter log) {

 - Throws: IllegalArgumentException

/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
public int departTimeNext() {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* Returns whether there are any passengers left
* @return true or false if anyone is left
*/
public boolean hasNext() {

/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
public Passenger removeNext() {

 - Throws: NoSuchElementException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls a Checkpoint and all of its fields and methods
*/
public class CheckPoint {

/** Time when the last passenger in line will clear security */
private int timeWhenAvailable = 0;

/** Passenger Queue at checkpoint */
private PassengerQueue line;

/**
* Constructs a new checkpoint
*/
public CheckPoint() {

/**
* Returns the size of the checkpoint
* @return size as an integer
*/
public int size() {

/**
* Removes a passenger from the checkpoint line
* @return passenger that was removed
*/
public Passenger removeFromLine() {

 - Throws: NoSuchElementException

/**
* Returns whether there are any passengers left
* @return true or false if anyone is left
*/
public boolean hasNext() {

/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
public int departTimeNext() {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* Adds a passenger to the given security line
* @param p passenger to be added
*/
public void addToLine(Passenger p) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls the Security Area and all of its fields and methods
*/
public class SecurityArea implements TransitGroup {

/** Maximum number of possible checkpoints */
public static final int MAX_CHECKPOINTS = 17;

/** Minimum number of possible checkpoints */
public static final int MIN_CHECKPOINTS = 3;

/** Error message for invalid number of checkpoints */
public static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3"
/** Error message for invalid index of checkpoints */
public static final String ERROR_INDEX = "Index out of range for this security area";

/** Largest index of a FastTrack checkpoint */
private int largestFastIndex;

/** Index of line reserved for tsaPreCheck */
private int tsaPreIndex;

/** Array of checkpoints */
private CheckPoint[] check;

/**
* Constructs a security area with a certain number of checkpoints
* @param checkpoints number of checkpoints to be created
* @throws IllegalArgumentException if checkpoints is out of range
*/
public SecurityArea(int checkpoints) {

 - Throws: IllegalArgumentException

/**
* Checks to makes sure the number of checkpoints is okay
* @param num number of checkpoints
* @return true or false if the number is valid
*/
private boolean numGatesOK(int num) {

/**
* Adds a passenger to the given security line
* @param index index of the security line
* @param p passenger to be added
*/
public void addToLine(int index, Passenger p) {

 - Throws: IllegalArgumentException

/**
* Returns the shortest line that an ordinary passenger can use
* @return index of the shortest line
*/
public int shortestRegularLine() {

/**
* Returns the shortest of the FastTrack checkpoint lines
* @return index of the shortest line
*/
public int shortestFastTrackLine() {

/**
* Returns the short of the TSAPre checkpoint lines
* @return index of the shortest line
*/
public int shortestTSAPreLine() {

/**
* Returns the length of the line at a given index
* @param index of the line to be used
* @return length of the line as an integer
* @throws IllegalArgumentException if the index is out of range
*/
public int lengthOfLine(int index) {

 - Throws: IllegalArgumentException

/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
public int departTimeNext() {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
public Passenger removeNext() {

/**
* Returns the index of the shortest line between two indexes
* @param a first index to be used
* @param b second index to be used
* @return integer index of shortest line
*/
private int shortestLineInRange(int a, int b) {

/**
* Returns the index of the security line with
* the next passenger to clear security
* @return integer index of the line 
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls a FastTrack passenger and all of its fields and methods
*/
public class FastTrackPassenger extends Passenger {

/** Color of the passenger */
private Color color;

/** Maximum expected process time for a FastTrack Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* Constructs a FastTrack passenger given arrival and process time and a reporter
* @param arrival Arrival time of the passenger at a security line
* @param process Process time of the passenger at security
* @param r Reporter that records passenger data
*/
public FastTrackPassenger(int arrival, int process, Reporter r) {

/**
* Adds the passenger to the correct security line 
* given a TransitGroup
* @param g transit group to be used
*/
@Override
public void getInLine(TransitGroup g) {

/**
* Returns the color of the passenger
* @return color of the passenger as a color object
*/
@Override
public Color getColor() {

/**
* Picks the security line to go into
* @param g transit group to be used
* @return the security line index as an integer
*/
private int pickLine(TransitGroup g) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls an Ordinary passenger and all of its fields and methods
*/
public class OrdinaryPassenger extends Passenger {

/** Color of the passenger */
private Color color;

/** Maximum expected process time for an Ordinary Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* Constructs an Ordinary passenger given arrival and process time and a reporter
* @param arrival Arrival time of the passenger at a security line
* @param process Process time of the passenger at security
* @param r Reporter that records passenger data
*/
public OrdinaryPassenger(int arrival, int process, Reporter r) {

/**
* Adds the passenger to the correct security line 
* given a TransitGroup
* @param g transit group to be used
*/
@Override
public void getInLine(TransitGroup g) {

/**
* Returns the color of the passenger
* @return color of the passenger as a color object
*/
@Override
public Color getColor() {

/**
* Picks the security line to go into
* @param g transit group to be used
* @return the security line index as an integer
*/
private int pickLine(TransitGroup g) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls a passenger and all of its fields and methods
*/
public abstract class Passenger {

/** Minimum process time for any passenger */
public static final int MIN_PROCESS_TIME = 20;

/** Arrival time of the passenger at a security line */
private int arrivalTime;

/** Wait time of the passenger in security line */
private int waitTime;

/** Process time of the passenger at security */
private int processTime;

/** Passenger's security line index */
private int lineIndex;

/** Whether the passenger is waiting to be processed a security line*/
private boolean waitingProcessing = false;

/** Reporter Log **/
private Reporter myLog;

/**
* Constructs a passenger given arrival and process time and a reporter
* @param arrival Arrival time of the passenger at a security line
* @param process Process time of the passenger at security
* @param r Reporter that records passenger data
*/
public Passenger(int arrival, int process, Reporter r) {

 - Throws: IllegalArgumentException

/**
* Returns the time the passenger arrives at a security line
* @return arrival time as an integer
*/
public int getArrivalTime() {

/**
* Returns how long the passenger waits in line at security
* @return wait time as an integer
*/
public int getWaitTime() {

/**
* Sets the wait time of the passenger given an integer
* @param time time to set
*/
public void setWaitTime(int time) {

/**
* Returns the time the passenger spends being processed
* @return process time as an integer
*/
public int getProcessTime() {

/**
* Returns the index of the security line the passenger
* is in
* @return line index as an integer
*/
public int getLineIndex() {

/**
* Returns whether the passenger is waiting
* in line to be processed or not
* @return true or false if waiting
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the security line after being checked
*/
public void clearSecurity() {

/**
* Sets the security line index for the passenger given an integer
* @param index integer time to be used
*/
protected void setLineIndex(int index) {

/**
* Adds the passenger to the correct security line 
* given a TransitGroup
* @param g transit group to be used
*/
public abstract void getInLine(TransitGroup g);

/**
* Returns the color of the passenger
* @return color of the passenger as a color object
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls a FastTrack passenger and all of its fields and methods
*/
public class TrustedTraveler extends Passenger {

/** Color of the passenger */
private Color color;

/** Maximum expected process time for a Trusted Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Constructs a Trusted passenger given arrival and process time and a reporter
* @param arrival Arrival time of the passenger at a security line
* @param process Process time of the passenger at security
* @param r Reporter that records passenger data
*/
public TrustedTraveler(int arrival, int process, Reporter r) {

/**
* Adds the passenger to the correct security line 
* given a TransitGroup
* @param g transit group to be used
*/
@Override
public void getInLine(TransitGroup g) {

/**
* Returns the color of the passenger
* @return color of the passenger as a color object
*/
@Override
public Color getColor() {

/**
* Picks the security line to go into
* @param g transit group to be used
* @return the security line index as an integer
*/
private int pickLine(TransitGroup g) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Handles the schedule of actions for passengers
*/
public class EventCalendar {

/** High priority transit group */
private TransitGroup highPriority;

/** Low priority transit group */
private TransitGroup lowPriority;

/**
* Constructs an EventCalendar with two TransitGroups as parameters
* @param highPriority TransitGroup that has priority over the other
* @param lowPriority TransitGroup that always comes second in priority
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Determines which passengers will be used next between both groups
* @return the passenger to be used next
* @throws IllegalStateException if both transit groups are empty
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Log to hold passenger data including wait and process times
*/
public class Log implements Reporter {

/** Number of passengers that have been completely processed*/
private int numCompleted;

/** Total wait time for all passengers */
private int totalWaitTime;

/** Total process time for all passengers */
private int totalProcessTime;

/** Reporter to contain passenger data */
/**
* Constructs a new log to hold passenger data
*/
public Log() {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
public int getNumCompleted() {

/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
public void logData(Passenger p) {

/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
@Override
public double averageWaitTime() {

/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Constructs the simulation with checkpoints and passengers
*/
public class Simulator {

/** Number of passengers to be used in the simulation */
private int numPassengers;

/** Passengers who haven't entered a checkpoint */
private TransitGroup inTicketing;

/** Passengers who are in a security line */
private TransitGroup inSecurity;

/** The current passenger */
private Passenger currentPassenger;

/** Log to record passenger data */
private Log log;

/** Event calendar to handle actions */
private EventCalendar myCalendar;

/**
* Creates a simulator with specified checkpoints, passengers, and types
* @param numCheckpoints number of checkpoints to be created
* @param numPass number of passengers to be created
* @param percentTSA percent of trusted passengers
* @param percentFast percent of fast track passengers
* @param percentNorm percent of ordinary passengers
*/
public Simulator(int numCheckpoints, int numPass, int percentTSA, int percentFast, int percentNorm) {

/**
* Checks to make sure that the parameters are valid for the simulation
* @param numPass the number of passengers to be created
* @param percentTSA percent of trusted passengers
* @param percentFast percent of fast track passengers
* @param percentNorm percent of ordinary passengers
* @throws IllegalArgumentException if any parameters are invalid
*/
private void checkParameters(int numPass, int percentTSA, int percentFast, int percentNorm) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Sets up the simulator with the correct percentages of passengers
* @param percentTSA percent of trusted passengers
* @param percentFast percent of fast track passengers
* @param percentNorm percent of ordinary passengers
*/
private void setUp(int percentTSA, int percentFast, int percentNorm) {

/**
* Gets the data included in a reporter
* @return reporter data on passengers
*/
public Reporter getReporter() {

/**
* Moves the passenger through airport security
* i.e. leaving ticket area or security checkpoint
* @throws IllegalStateException if there is no next passenger
*/
public void step() {

 - Throws: IllegalStateException

/**
* Returns whether the simulator has more steps
* @return true or false if the simulator can step
*/
public boolean moreSteps() {

/**
* Returns the index of the security checkpoint line
* for the current passenger
* @return integer index of security checkpoint
*/
public int getCurrentIndex() {

/**
* Gets the color of the current passenger
* @return passenger's current color as a color object
*/
public Color getCurrentPassengerColor() {

/**
* Returns whether a passenger has cleared the checkpoint
* and finished processing
* False when the passenger is null
* @return true or false if passenger cleared security
*/
public boolean passengerClearedSecurity() {


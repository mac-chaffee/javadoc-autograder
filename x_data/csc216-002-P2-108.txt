---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Creates objects that encapsulate user actions
*
*/
public class Command {

/**Enumeration for the value of the command*/
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/**Enumeration for the reason for being on hold*/
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/**Enumeration for why the program was resolved*/
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/**Enumeration for why the program was cancelled*/
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT };

/**On hold reason string for awaiting caller*/
public static final String OH_CALLER = "Awaiting Caller";

/**On hold reason string for awaiting change*/
public static final String OH_CHANGE = "Awaiting Change";

/**On hold reason string for awaiting vendor*/
public static final String OH_VENDOR = "Awaiting Vendor";

/**Resoltion code reason string for solved*/
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/**Resoltion code reason string for workaround*/
public static final String RC_WORKAROUND = "Workaround";

/**Resoltion code reason string for workaround*/
public static final String RC_NOT_SOLVED = "Not Solved";

/**Resoltion code reason string for caller closed*/
public static final String RC_CALLER_CLOSED = "Caller Closed";

/**Cancellation code for duplicate*/
public static final String CC_DUPLICATE = "Duplicate";

/**Cancellation code for unnecessary*/
public static final String CC_UNNECESSARY = "Unnecessary";

/**Cancellation code for not an incident*/
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/**The cancellation code for the command*/
private CancellationCode cancellationCode = null;

/**The resolution code for the command*/
private ResolutionCode resolutionCode = null;

/**The reason for being on hold*/
private OnHoldReason onHoldReason = null;

/**The command value*/
private CommandValue commandValue = null;

/**The Id of the owner*/
private String ownerId;

/**The note of the command*/
private String note;

/**
* Constructs the command object
* @param commandValue command value
* @param ownerId the ID of the owner
* @param onHoldReason the reason for being on hold
* @param resolutionCode why the incident was resolved
* @param cancellationCode why the incident was cancelled
* @param note Note of the command
*/
public Command(CommandValue commandValue, String ownerId, OnHoldReason onHoldReason, ResolutionCode resolutionCode, CancellationCode cancellationCode, String note) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the command value
* @return the command value
*/
public CommandValue getCommand() {

/**
* Gets the owner id
* @return the owner id
*/
public String getOwnerId() {

/**
* Gets the resolution code
* @return the resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the work note
* @return the work note
*/
public String getWorkNote() {

/**
* Gets the reason for being on hold
* @return the reason for being on hold
*/
public OnHoldReason getOnHoldReason() {

/**
* Gets the cancellation code
* @return the cancellation code
*/
public CancellationCode getCancellationCode() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Incident tracked by the system
*/
public class ManagedIncident {

/**Enumeration for the priority*/
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
/**Enumeration for the category*/
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/**
* On hold state of an incident
*
*/
public class OnHoldState implements IncidentState {

/**
* updates the state
* @param command the given command
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* new state of an incident
*
*/
public class NewState implements IncidentState {

/**
* updates the state
* @param command the given command
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* resolved state of an incident
*
*/
public class ResolvedState implements IncidentState {

/**
* updates the state
* @param command the given command
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* closes the state of an incident
*
*/
public class ClosedState implements IncidentState {

/**
* updates the state
* @param command the given command
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* canceled state of an incident
*
*/
public class CanceledState implements IncidentState {

/**
* updates the state
* @param command the given command
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**
* progress state of an incident
*
*/
public class InProgressState implements IncidentState {

/**
* updates the state
* @param command the given command
*/
@Override
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Gets the name of the state
* @return the state name
*/
@Override
public String getStateName() {

/**String for category inquiry*/
public static final String C_INQUIRY = "Inquiry";

/**String for category software*/
public static final String C_SOFTWARE = "Software";

/**String for category hardware*/
public static final String C_HARDWARE = "Hardware";

/**String for category network*/
public static final String C_NETWORK = "Network";

/**String for category database*/
public static final String C_DATABASE = "Database";

/**String for priority urgent*/
public static final String P_URGENT = "Urgent";

/**String for priority high*/
public static final String P_HIGH = "High";

/**String for priority medium*/
public static final String P_MEDIUM = "Medium";

/**String for priority low*/
public static final String P_LOW = "Low";

/**String for name when on hold*/
public static final String ON_HOLD_NAME = "On Hold";

/**String for name when new*/
public static final String NEW_NAME = "New";

/**String for name when in progress*/
public static final String IN_PROGRESS_NAME = "In Progress";

/**String for name when resolved*/
public static final String RESOLVED_NAME = "Resolved";

/**String for name when closed*/
public static final String CLOSED_NAME = "Closed";

/**String for name when cancelled*/
public static final String CANCELED_NAME = "Canceled";

/**Unique id for an incident*/
private int incidentId;

/**User id of person who reported the incident*/
private String caller;

/**User id of the incident*/
private String owner;

/**The name of the incident*/
private String name;

/**The change request of the incident*/
private String changeRequest;

/**The notes of the incident*/
private ArrayList<String> notes;

/**The counter*/
private static int counter = 0;

/**The category of the managed incident*/
private Category category;

/**The priority of the managed incident*/
private Priority priority;

/**The cancellation code of the managed incident*/
private CancellationCode cancellationCode;

/**The resolution code of the managed incident*/
private ResolutionCode resolutionCode;

/**The on hold reason of the managed incident*/
private OnHoldReason onHoldReason;

/**The incident state*/
private IncidentState state;

/**On hold state of the managed incident*/
private final OnHoldState onHoldState = new OnHoldState();

/**New state of the incident*/
private final NewState newState = new NewState();

/**Resolved state of the incident*/
private final ResolvedState resolvedState = new ResolvedState();

/**Closed state of the incident*/
private final ClosedState closedState = new ClosedState();

/**Canceled state of the incident*/
private final CanceledState canceledState = new CanceledState();

/**In progress state of the incident*/
private final InProgressState inProgressState = new InProgressState();

/**
* The constructor for managed incident
* @param caller id of the caller
* @param category the category of the incident
* @param priority the priority of the incident
* @param name the name of the incident
* @param workNote the work note of the incident
*/
public ManagedIncident(String caller, Category category, Priority priority, String name, String workNote) {

 - Throws: IllegalArgumentException

/**
* Constructs a managed incident object from an existing Incident
* @param i the existing Incident
*/
public ManagedIncident(Incident i) {

/**
* Increments this.counter
*/
public static void incrementCounter() {

/**
* Gets the incident id
* @return int of the incident id
*/
public int getIncidentId() {

/**
* Gets the change request
* @return the change request
*/
public String getChangeRequest() {

/**
* Gets the category of the managed incident
* @return the category
*/
public Category getCategory() {

/**
* Gets the string representing the category
* @return string representing the category
*/
public String getCategoryString() {

/**
* Sets the category
* @param category the string for the category
*/
private void setCategory(String category) {

/**
* Converts a string to the correct category
* @param strCategory the category string
* @return category enumeration
*/
private Category toCategory(String strCategory) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the string for the priority
* @return the string for the priority
*/
public String getPriorityString() {

/**
* Sets the priority
* @param priority the string for the priority
*/
private void setPriority(String priority) {

/**
* Converts a string to the correct priority
* @param strPriority the priority string
* @return priority enumeration
*/
private Priority toPriority(String strPriority) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the reason for being on hold in a string
* @return the reason for being on hold
*/
public String getOnHoldReasonString() {

/**
* Sets the on hold reason
* @param onHoldReason the string for the on hold reason
*/
private void setOnHoldReason(String onHoldReason) {

/**
* Converts a string to the correct on hold reason
* @param strOnHoldReason the on hold reason string
* @return on hold reason enumeration
*/
private OnHoldReason toOnHoldReason(String strOnHoldReason) {

 - Throws: IllegalArgumentException

/**
* Gets the cancellation code in a string
* @return string of the cancellation code
*/
public String getCancellationCodeString() {

/**
* Sets the cancellationCode
* @param cancellationCode the string for the cancellation code
*/
private void setCancellationCode(String cancellationCode) {

/**
* Converts a string to the correct on CancellationCode
* @param strCancellationCode the CancellationCode string
* @return CancellationCode enumeration
*/
private CancellationCode toCancellationCode(String strCancellationCode) {

 - Throws: IllegalArgumentException

/**
* Gets the state of the incident
* @return state of the incident
*/
public IncidentState getState() { //not sure
/**
* Sets the IncidentState
* @param incidentState the string for the incident state
*/
private void setState(String incidentState) {

/**
* Converts a string to the correct on CancellationCode
* @param strCancellationCode the CancellationCode string
* @return CancellationCode enumeration
*/
private IncidentState toIncidentState(String strIncidentState) {

 - Throws: IllegalArgumentException

/**
* Gets the resolution code
* @return resolutionCode of the managed incident
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the string of the resolution code
* @return string of the resolution code
*/
public String getResolutionCodeString() {

/**
* Sets the resolution code
* @param resolutionCode the string for the resolutionCode
*/
private void setResolutionCode(String resolutionCode) {

/**
* Converts a string to the correct on ResolutionCode
* @param strResolutionCode the ResolutionCode string
* @return ResolutionCode enumeration
*/
private ResolutionCode toResolutionCode(String strResolutionCode) {

 - Throws: IllegalArgumentException

/**
* Gets the owner
* @return string for the owner
*/
public String getOwner() {

/**
* Gets the name
* @return string of the name
*/
public String getName() {

/**
* Gets the caller
* @return the string of the caller
*/
public String getCaller() {

/**
* Gets the notes
* @return ArrayList of the notes
*/
public ArrayList<String> getNotes(){

/**
* Gets the notes as a string
* @return the notes as a string
*/
public String getNotesString() {

/**
* Drives the finite state machine by delegating the Command to the current state and if 
* successful adds non-null notes to the notes list
* @param command the command 
*/
public void update(Command command) {

/**
* Makes XML incident into regular incident
* @return Incident the xml incident
*/
public Incident getXMLIncident() {

/**
* Gets the string corresponding to the current state
* @param state the IncidentState being evaluated
* @return a String representing the state in the class
*/
private String stateToName(IncidentState state) {

/**
* Sets the counter
* @param counter the number for the counter to be set to
*/
public static void setCounter(int counter) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Manages the incidents
*
*/
public class IncidentManager {

/**The list of incidents*/
private ManagedIncidentList incidentList;

/**The single instance of Incident Manager*/
private static IncidentManager singleton = new IncidentManager();

/**
* Constructs the incident manager
*/
private IncidentManager() {

/**
* Gets the instance
* @return instance the instance
*/
public static IncidentManager getInstance() {

/**
* Saves the managed incidents to a file
* @param fileName name of the file
*/
public void saveManagedIncidentsToFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Loads managed incidents from a file
* @param fileName name of the file to be loaded
*/
public void loadManagedIncidentsFromFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Makes a new managed incident list
*/
public void createNewManagedIncidentList() {

/**
* Gets the managed incidents as an array
* @return String array of managed incidents
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Gets the managed incidents as an array by category
* @param category the incidents must be under
* @return String array of managed incidents
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Gets a managed incident by the id
* @param id the id of the incident
* @return the managed incident
*/
public ManagedIncident getManagedIncidentById(int id) {

/**
* Executes the command
* @param id id of the incident
* @param command the command to be to be executed
*/
public void executeCommand(int id, Command command) {

/**
* Deletes the managed incident in the id
* @param id the id of the incident to be deleted
*/
public void deleteManagedIncidentById(int id) {

/**
* Adds an incident to the list
* @param caller id of the caller
* @param category the category of the incident
* @param priority the priority of the incident
* @param name the name of the incident
* @param workNote the work note of the incident
*/
public void addManagedIncidentToList(String caller, Category category, Priority priority, String name,
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Maintains a List of ManagedIncidents
*
*/
public class ManagedIncidentList {

/**List of managed incidents*/
private ArrayList<ManagedIncident> incidents;

/**
* Constructs a managed incident list object
*/
public ManagedIncidentList() {

/**
* Adds an incident to the list
* @param caller id of the caller
* @param category the category of the incident
* @param priority the priority of the incident
* @param name the name of the incident
* @param workNote the work note of the incident
* @return int the incident id
*/
public int addIncident(String caller, Category category, Priority priority, String name, String workNote) {

/**
* Adds list of incidents from xml
* @param incidents the list of incidents
*/
public void addXMLIncidents(List<Incident> incidents) {

/**
* Deletes the incident given the id
* @param id the id of the incident to be deleted
*/
public void deleteIncidentById(int id) {

/**
* Gets incident with the id
* @param id the id of the incident
* @return incident with specified id
*/
public ManagedIncident getIncidentById(int id) {

/**
* Executes the command
* @param id id
* @param command the command to be executed
*/
public void executeCommand(int id, Command command) {

/**
* Gets the list of managed incidents
* @return list of managed incidents
*/
public ArrayList<ManagedIncident> getManagedIncidents(){

/**
* Gets the incidents under a category
* @param category the category of the incidents
* @return the category of incidents
*/
public ArrayList<ManagedIncident> getIncidentsByCategory(Category category){

 - Throws: IllegalArgumentException


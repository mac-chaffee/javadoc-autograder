---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates objects that encapsulates the user's actions 
* that causes ManagedIncident to update
*/
public class Command {

/** On Hold state for caller */
public static final String OH_CALLER = "Awaiting Caller";

/** On Hold state for change */
public static final String OH_CHANGE = "Awaiting Change";

/** On Hold state for vendor */
public static final String OH_VENDOR = "Awaiting Vendor";

/** Resolution code for permanently solved */
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** Resolution code for workaround */
public static final String RC_WORKAROUND = "Workaround";

/** Resolution code for not solved */
public static final String RC_NOT_SOLVED = "Not Solved";

/** Resolution code for caller closed */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** Cancellation code for duplicate */
public static final String CC_DUPLICATE = "Duplicate";

/** Cancellation code for unnecessary */
public static final String CC_UNNECESSARY = "Unnecessary";

/** Cancellation code for not an incident */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** Owner's Id */
private String ownerId;

/** Command note */
private String note;

/** c as CommandValue */
private CommandValue c;

/** Command cancellation code */
private CancellationCode cancellationCode;

/** Command resolution code */
private ResolutionCode resolutionCode;

/** Command on hold reason */
private OnHoldReason onHoldReason;

/**
* Command constructor
* @param c as CommandValue
* @param ownerId as String
* @param onHoldReason OnHoldReason
* @param resolutionCode as ResolutionCode
* @param cancellationCode as CancellationCode
* @param note as String
*/
public Command(CommandValue c, String ownerId, OnHoldReason onHoldReason, 
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the command 
* @return c as CommandValue
*/
public CommandValue getCommand() {

/**
* Gets owner's Id
* @return owner's Id
*/
public String getOwnerId() {

/**
* Gets the resolution code
* @return resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the work notes
* @return work notes
*/
public String getWorkNote() {

/**
* Gets the on hold reason
* @return the on hold reason
*/
public OnHoldReason getOnHoldReason() {

/**
* Gets the cancellation code
* @return the cancellation code
*/
public CancellationCode getCancellationCode() {

/**
* Six actions for CommandValue
*/
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/**
* Three actions for OnHoldReason
*/
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/**
* Four actions for ResolutionCode
*/
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/**
* Three actions for CancellationCode
*/
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT };

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents the incidents that are tracked
*/
public class ManagedIncident {

/** Category as Inquiry */
public static final String C_INQUIRY = "Inquiry";

/** Category as Software */
public static final String C_SOFTWARE = "Software";

/** Category as Hardware */
public static final String C_HARDWARE = "Hardware";

/** Category as Network */
public static final String C_NETWORK = "Network";

/** Category as Database */
public static final String C_DATABASE = "Database";

/** Priority as Urgent */
public static final String P_URGENT = "Urgent";

/** Priority as High */
public static final String P_HIGH = "High";

/** Priority as Medium */
public static final String P_MEDIUM = "Medium";

/** Priority as Low */
public static final String P_LOW = "Low";

/** id of incident */
private int incidentId;

/** caller of incident */
private String caller;

/** owner of incident */
private String owner;

/** name of incident */
private String name;

/** requesting change in incident */
private String changeRequest;

/** notes for incident */
private ArrayList<String> notes;

/** Incident state as New */
public static final String NEW_NAME = "New";

/** Incident state as In Progress */
public static final String IN_PROGRESS_NAME = "In Progress";

/** Incident state as On Hold */
public static final String ON_HOLD_NAME = "On Hold";

/** Incident state as Resolved */
public static final String RESOLVED_NAME = "Resolved";

/** Incident state as Closed */
public static final String CLOSED_NAME = "Closed";

/** Incident state as Canceled */
public static final String CANCELED_NAME = "Canceled";

/** keeps track of id value given to the next incident */
private static int counter = 0;

/** priority of the incident */
private Priority priority;

/** category of the incident */
private Category category;

/** cancellation code of the incident */
private CancellationCode cancellationCode;

/** resolution code of the incident */
private ResolutionCode resolutionCode;

/** on hold reason for the incident */
private OnHoldReason onHoldReason;

/** current state of the incident */
private IncidentState state;

/** final instance of OnHoldState inner class */
private final IncidentState onHoldState = new OnHoldState();

/** final instance of ResolvedState inner class */
private final IncidentState resolvedState = new ResolvedState();

/** final instance of NewState inner class */
private final IncidentState newState = new NewState();

/** final instance of ClosedState inner class */
private final IncidentState closedState = new ClosedState();

/** final instance of InProgressState inner class */
private final IncidentState inProgressState = new InProgressState();

/** (Self-made) final instance of CanceledState inner class */
private final IncidentState canceledState = new CanceledState();

/**
* ManagedIncident constructor
* throw IllegalArgumentException if parameters are null or String parameters are
* empty String
* @param caller as String
* @param category as Category
* @param priority as Priority
* @param name as String
* @param workNote as String
*/
public ManagedIncident(String caller, Category category, Priority priority, 
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructor used to set the fields to the values from the Incident
* @param i as Incident
*/
public ManagedIncident(Incident i) {

/**
* Increments the counter
*/
public static void incrementCounter() {

/**
* Gets the incidentId
* @return the incident id
*/
public int getIncidentId() {

/**
* Gets the changed request
* @return the changed request
*/
public String getChangeRequest() {

/**
* Gets the incident category
* @return the incident category
*/
public Category getCategory() {

/**
* Gets the incident category as string
* @return the incident category as string
*/
public String getCategoryString() {

/**
* Sets the incident category
* @param s as String
*/
private void setCategory(String s) {

/**
* Gets the incident priority as string
* @return the incident priority as string
*/
public String getPriorityString() {

/**
* Sets the incident priority
* @param s as String
*/
private void setPriority(String s) {

/**
* Gets the incident on hold reason as string 
* @return the incident on hold reason as string
*/
public String getOnHoldReasonString() {

/**
* Sets the incident on hold reason as string
* @param s as String
*/
private void setOnHoldReason(String s) {

/**
* Gets the incident cancellation code as string
* @return the incident cancellation code as string
*/
public String getCancellationCodeString() {

/**
* Sets the incident cancellation code
* @param s as String
*/
private void setCancellationCode(String s) {

/**
* Gets the incident state
* @return the incident state
*/
public IncidentState getState() {

/**
* Sets the incident state
* @param s as String
*/
private void setState(String s) {

/**
* Gets the incident resolution code
* @return the incident resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Gets the incident resolution code as string 
* @return the incident resolution code as string
*/
public String getResolutionCodeString() {

/**
* Sets the incident resolution code as string
* @param s as String
*/
private void setResolutionCode(String s) {

/**
* Gets the incident owner
* @return the incident owner
*/
public String getOwner() {

/**
* Gets the incident name
* @return the incident name
*/
public String getName() {

/**
* Gets the incident caller
* @return the incident caller
*/
public String getCaller() {

/**
* Gets the incident notes
* @return the incident notes
*/
public ArrayList<String> getNotes() {

/**
* Gets the incident notes as string
* @return incident notes as string
*/
public String getNotesString() {

/**
* Delegates the Command to the current state
* @param c as Command
*/
public void update(Command c) {

/**
* Gets the XML incident
* @return the XML incident
*/
public Incident getXMLIncident() {

/**
* Sets the counter
* @param i as int
*/
public static void setCounter(int i) {

/**
* Five possible categories of Incident
*/
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/**
* Four possible priorities of Incident
*/
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
/**
* Represents the on hold state of Incident
*/
public class OnHoldState implements IncidentState {

/**
* Update the state
*/
@Override
public void updateState(Command c) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name
* @return name
*/
public String getStateName() {

/**
* Represents the new state of Incident
*/
public class NewState implements IncidentState {

/**
* Update the state
*/
@Override
public void updateState(Command c) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name
* @return name
*/
public String getStateName() {

/**
* Represents the resolved state of Incident
*/
public class ResolvedState implements IncidentState {

/**
* Update the state
*/
@Override
public void updateState(Command c) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name
* @return name
*/
public String getStateName() {

/**
* Represents the closed state of Incident
*/
public class ClosedState implements IncidentState {

/**
* Update the state
*/
@Override
public void updateState(Command c) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name
* @return name
*/
public String getStateName() {

/**
* Represents the in progress state of Incident
*/
public class InProgressState implements IncidentState {

/**
* Update the state
*/
@Override
public void updateState(Command c) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name
* @return name
*/
public String getStateName() {

/**
* Represents the canceled state of Incident
*/
public class CanceledState implements IncidentState {

/**
* Update the state
*/
@Override
public void updateState(Command c) {

 - Throws: UnsupportedOperationException

/**
* Gets the state name
* @return name
*/
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Controls the creation and modifications of ManagedIncidentList
*/
public class IncidentManager {

/** incidentList from ManagedIncidentList */
private ManagedIncidentList incidentList;

/** singleton as IncidentManager initially set to null */
private static IncidentManager singleton = null;

/**
* Creates one instance of IncidentManager
*/
private IncidentManager() {

/**
* Gets the instance of IncidentManger
* @return instance
*/
public static IncidentManager getInstance() {

/**
* Save the managed incidents to file
* try catch IncidentIOException and throws FileNotFoundException
* @param file as String 
*/
public void saveManagedIncidentsToFile(String file) {

 - Throws: IllegalArgumentException

/**
* Load the managed incidents from file
* try catch IncidentIOException and throws FileNotFoundException
* @param file as String
*/
public void loadManagedIncidentsFromFile(String file) {

 - Throws: IllegalArgumentException

/**
* Create new managed incident list
*/
public void createNewManagedIncidentList() {

/**
* Returns a 2D array to be used to populate IncidentTableModel with information
* @return string array
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Returns a 2D array to be used to populate IncidentTableModel 
* throws IllegalArgumentException if category is null
* with information by category
* @param category as Category
* @return string array
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

/**
* Gets the managed incident by id
* @param id as int
* @return the id of managed incident
*/
public ManagedIncident getManagedIncidentById(int id) {

/**
* Execute the command
* @param id as int 
* @param c as Command
*/
public void executeCommand(int id, Command c) {

/**
* Delegate the managed incident by id
* @param id as int
*/
public void deleteManagedIncidentById(int id) {

/**
* Add the managed incident to list
* @param id as String
* @param category as Category
* @param priority as Priority
* @param name as String
* @param note as String
*/
public void addManagedIncidentToList(String id, Category category, Priority priority, 
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Maintains a list of ManagedIncidents
*/
public class ManagedIncidentList {

/** incidents from ManagedIncident */
private ArrayList<ManagedIncident> incidents;

/**
* Constructor for ManagedIncidentList
*/
public ManagedIncidentList() {

/**
* Add a ManagedIncident to the list
* @param caller as String
* @param category as Category
* @param priority as Priority
* @param name as String
* @param workNote as String
* @return the number added
*/
public int addIncident(String caller, Category category, Priority priority, 
/**
* Add a list of Incidents from XML library to the list
* @param list as List type Incident
*/
public void addXMLIncidents(List<Incident> list) {

/**
* Gets the managed incidents
* @return the managed incidents
*/
public List<ManagedIncident> getManagedIncidents() {

/**
* Gets the incidents by category
* throws IllegalArgumentException if category is null
* @param category as Category
* @return the incidents by category
*/
public List<ManagedIncident> getIncidentsByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Gets the incident by id
* @param id as int
* @return the incident by id
*/
public ManagedIncident getIncidentById(int id) {

/**
* Executes the command
* @param id as int
* @param c as Command
*/
public void executeCommand(int id, Command c) {

/**
* Delete the incident by id
* @param id as int
*/
public void deleteIncidentById(int id) {


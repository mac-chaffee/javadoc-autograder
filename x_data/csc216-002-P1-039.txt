---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Pre Security Class that represents passengers in the terminal who have not yet joined a security checkpoint line.
*/
public class PreSecurity implements TransitGroup {

/** outside security is a Passenger Queue field */
private PassengerQueue outsideSecurity;

/**
* PreSecurity constructor
* @param passengers the number of passengers
* @param myLog Reporter parameter
* @throws IllegalArgumentException if number of passengers is 0.
*/
public PreSecurity(int passengers, Reporter myLog) {

 - Throws: IllegalArgumentException

/**
* Returns the depart time from the ticketing area
* @return value
*/
public int departTimeNext() {

/**
* Next passenger in line
* @return the next passenger to be in line
*/
public Passenger nextToGo() {

/**
* Method that checks if there is another passenger in line
* @return true if there is a passenger in line, false otherwise.
*/
public boolean hasNext() {

/**
* Method that removes the next passenger in the line
* @return the passenger to be removed
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* CheckPoint Class that represents a security checkpoint line and its line, 
* where the front passenger is undergoing the actual security process.
*/
public class CheckPoint {

/** time when the security checkpoint is available for the next passenger */
private int timeWhenAvailable;

/** Passenger Queue line*/
private PassengerQueue line;

/**
* CheckPoint constructor
*/
public CheckPoint() {

/**
* Method that gets the size of the security checkpoint line.
* @return the size of the line.
*/
public int size() {

/**
* Method that removes a passenger from a security checkpoint line
* @return passenger removed from line
*/
public Passenger removeFromLine() {

/**
* Checks if security has another passenger waiting in line
* @return true if there is another passenger waiting in line for security checkpoint, false otherwise.
*/
public boolean hasNext() {

/**
* Returns the time a passenger will clear security
* @return the time the passenger clears the security checkpoint
*/
public int departTimeNext() {

/**
* Returns the passenger at the front of the security line
* @return passenger in front of the line
*/
public Passenger nextToGo() {

/**
* Adds the passenger to the end of the line.
* Method explained by TA during office hours on 10/18.
* @param passenger next passenger
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Security Area Class
*
*/
public class SecurityArea implements TransitGroup {

/** Max number of checkpoints  */
public static final int MAX_CHECKPOINTS = 17;

/** Min number of checkpoints */
public static final int MIN_CHECKPOINTS = 3;

/** Checkpoints error message */
public static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** Index error message */
public static final String ERROR_INDEX = "Index out of range for this security area";

/** largest index for fast track */
private int largestFastIndex;

/** tsa pre security index */
private int tsaPreIndex;

/** CheckPoint Array*/
private CheckPoint[] check;

/**
* Constructor for security area
* @param checkpoints the number of security checkpoints
* @throws IllegalArgumentException if number of checkpoints is out of range.
*/
public SecurityArea(int checkpoints) {

 - Throws: IllegalArgumentException

/**
* Checks if number of gates are acceptable
* @param number index of the gate
* @return true if it is in between the range, and false otherwise
*/
private boolean numGatesOK(int number) {

/**
* Adds the passenger to the security line with the index given by the first parameter.
* @param index index of the passenger.
* @param passenger the passenger.
*/
public void addToLine(int index, Passenger passenger) {

/**
* Returns the index of the shortest security line that ordinary passengers are permitted to select.
* @return the index of the shortest line.
*/
public int shortestRegularLine() {

/**
* Returns the index of the shortest security line restricted to Fast Track passengers.
* @return the index of the shortest line.
*/
public int shortestFastTrackLine() {

/**
* Returns the index of the shortest security line restricted to TSA PreCheck/Trusted Traveler passengers.
* @return the index of the shortest line.
*/
public int shortestTSAPreLine() {

/**
* The number of passengers in the line with the given index.
* @param index number of the line.
* @return the length of the given line.
* @throws IllegalArgumentException if index is out of bounds.
*/
public int lengthOfLine(int index) {

 - Throws: IllegalArgumentException

/**
* Returns the time that the next passenger will finish clearing security.
* @return the time
*/
public int departTimeNext() {

/**
* Returns the next passenger to clear security.
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* Removes and returns the next passenger to clear security.
* @return the next passenger
*/
public Passenger removeNext() {

/**
* Returns the shortest line in range
* @param number index of first line to compare
* @param number2 index of second line to compare
* @return the shortest line
*/
private int shortestLineInRange(int number, int number2) {

/**
* Returns line with passenger about to clear security
* @return the line
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* FastTrackPassenger class 
*/
public class FastTrackPassenger extends Passenger {

/** Max expected process time constant */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** Color constant */
private Color color;

/**
* Fast TrackPassenger constructor with three different parameters that calls super constructor
* @param arrivalTime is the time that the passenger joins a security line.
* @param processTime processTime is the amount of time that the passenger spends at the front of 
* the security line actually undergoing the security check.
* @param myLog myLog is the reporting mechanism.
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* The passenger selects the security checkpoint with the shortest line that is not restricted
* to other kinds of passengers.
* @param group the transit group
*/
public void getInLine(TransitGroup group) {

/**
* Gets the color of the passenger
* @return the color of the passenger
*/
public Color getColor() {

/**
* Picks the line the passenger will go.
* @param group the transit group
* @return the line
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Ordinary Passenger Class
*
*/
public class OrdinaryPassenger extends Passenger {

/** Max expected process time constant */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** Color constant */
private Color color;

/**
* Ordinary Passenger constructor with three different parameters that calls super constructor
* @param arrivalTime is the time that the passenger joins a security line.
* @param processTime processTime is the amount of time that the passenger spends at the front of 
* the security line actually undergoing the security check.
* @param myLog myLog is the reporting mechanism.
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* The passenger selects the security checkpoint with the shortest line that is not restricted
* to other kinds of passengers.
* @param group the transit group
*/
public void getInLine(TransitGroup group) {

/**
* Gets the color of the passenger
* @return the color of the passenger
*/
public Color getColor() {

/**
* Picks the line the passenger will go.
* @param group the transit group
* @return the line
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Passenger Class
*/
public abstract class Passenger {

/** Minimum process time constant*/
public static final int MIN_PROCESS_TIME = 20;

/** is the time that the passenger joins a security line */
private int arrivalTime;

/** is the time the passenger will spend in a security line */
private int waitTime;

/** is the amount of time that the passenger spends at the front of the security line actually undergoing the security check.*/
private int processTime;

/** is the index of the security checkpoint line that the passenger selects and joins */
private int lineIndex = -1;

/** is false until the passenger selects and joins a security line, when it becomes true. */
private boolean waitingProcessing;

/** is the reporting mechanism initialized by the Reporter parameter in the constructor */
private Reporter myLog;

/**
* Passenger Constructor
* @param arrivalTime is the time that the passenger joins a security line.
* @param processTime is the amount of time that the passenger spends at the front of 
* the security line actually undergoing the security check.
* @param myLog is the reporting mechanism initialized by the Reporter parameter in the constructor.
*/
public Passenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Gets the time that the passenger joins a security line.
* @return the arrival time.
*/
public int getArrivalTime() {

/**
* Gets the time the passenger will spend in a security line.
* @return the wait time.
*/
public int getWaitTime() {

/**
* Sets the time the passenger will spend in a security line.
* @param waitTime the time to be set.
*/
public void setWaitTime(int waitTime) {

/**
* Gets the amount of time that the passenger spends at the front of the security line 
* actually undergoing the security check.
* @return the amount of process time.
*/
public int getProcessTime() {

/**
* Gets the line index.
* @return the index of the line. 
*/
public int getLineIndex() {

/**
* Checks if passenger is waiting in the security line.
* @return true if passenger is waiting, false otherwise.
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when he/she finishes security checks.
*/
public void clearSecurity() {

/**
* Set the line index number.
* @param lineIndex the line index.
*/
protected void setLineIndex(int lineIndex) {

/**
* Adds the passenger to the appropriate security line. The parameter is the list of security lines in the airport.
* @param group is the list of security lines in the airport.
*/
public abstract void getInLine(TransitGroup group);

/**
* Gets the color of the passenger.
* @return the color of the passenger.
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* TrustedTraveler Class
*/
public class TrustedTraveler extends Passenger {

/** Max expected process time constant */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** Color constant */
private Color color;

/**
* Trusted Traveler constructor with three different parameters that calls super constructor
* @param arrivalTime is the time that the passenger joins a security line.
* @param processTime processTime is the amount of time that the passenger spends at the front of 
* the security line actually undergoing the security check.
* @param myLog myLog is the reporting mechanism.
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter myLog) {

/**
* The passenger selects the security checkpoint with the shortest line that is not restricted
* to other kinds of passengers.
* @param group the transit group
*/
public void getInLine(TransitGroup group) {

/**
* Gets the color of the passenger
* @return the color of the passenger
*/
public Color getColor() {

/**
* Picks the line the passenger will go.
* @param group the transit group
* @return the line
*/
private int pickLine(TransitGroup group) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* The Event Calendar Class determines which passenger to process next while not all passengers have left the simulation.
*/
public class EventCalendar {

/** High Priority transit group */
private TransitGroup highPriority;

/** High Priority transit group */
private TransitGroup lowPriority;

/**
* Event Calendar constructor which takes two transit groups as parameters
* @param highPriority first transit group parameter
* @param lowPriority second transit group parameter
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Returns the passenger from highPriority and lowPriority who will be the next to exit the group/line
* theyre currently at the front
* @return the passenger who will be next to exit
* @throws IllegalStateException if both Transit Groups are empty.
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Log Class that logs passenger information into the reporter.
*/
public class Log implements Reporter {

/** number completed field */
private int numCompleted;

/** total wait time field */
private int totalWaitTime;

/** total process time field */
private int totalProcessTime;

/**
* Log constructor to initialize fields to 0.
*/
public Log() {

/**
* Gets the number of completed
* @return the number of completed checks
*/
public int getNumCompleted() {

/**
* Logs the passenger data
* @param passenger the passenger being analyzed 
*/
public void logData(Passenger passenger) {

/**
* Returns the average wait time in the line
* @return the average wait time
*/
public double averageWaitTime() {

/**
* Returns the average process time on line
* @return the average process time
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Simulator Class that implements the back end process for [UC1,S2] and [UC1, S2].
*
*/
public class Simulator {

/**  number of passenger field. */
private int numPassengers;

/** field to record passenger statistics */
private Reporter log;

/** is the passenger most recently returned by myCalendar.nextToAct() */
private Passenger currentPassenger;

/** Event Calendar field for use in simulator */
private EventCalendar myCalendar;

/** Transit Group field for use in simulator */
private TransitGroup inSecurity;

/** Transit Group field for use in simulator */
private TransitGroup inTicketing;

/**
* Simulator constructor with five parameters: the number of checkpoints, number of passengers,
* percent of passengers who are TSA PreCheck/Trusted Travelers, percent of passengers who are Fast Track, 
* and percent of passengers who do not qualify for special security checkpoints.
* @param checkpoints the number of checkpoints
* @param numPassengers the number of passengers
* @param tsaPct the percent of passengers TSA pre check/ trusted passengers
* @param fastPct percent of fast track passengers
* @param ordPct percent of passenger who do not qualify for special security checkpoints
* 
*/
public Simulator(int checkpoints, int numPassengers, int tsaPct, int fastPct, int ordPct) {

/**
* Checks the parameters for simulator constructor.
*
* @param numPassengers the number of passengers to check
* @param tsaPct the percent of passengers TSA pre check/ trusted passengers
* @param fastPct percent of fast track passengers
* @param ordPct percent of passenger who do not qualify for special security checkpoints
* @throws IllegalArgumentException if number of Passengers is lower then 1.
* @throws IllegalArgumentException if percents are passed as negative numbers.
* @throws IllegalArgumentException if percents do not sum up to 100.
*/
private void checkParameters(int checkpoints, int numPassengers, int tsaPct, int fastPct, int ordPct) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Sets up the parameters.
* @param tsaPer the percent of passengers TSA pre check/ trusted passengers
* @param fastPer percent of fast track passengers
* @param ordPer percent of passenger who do not qualify for special security checkpoints
*/
private void setUp(int tsaPct, int fastPct, int ordPct) {

/**
* Gets the reporter data.
*
* @return the report data
*/
public Reporter getReporter() {

/**
* Goes through the action of the next passenger from the EventCalendar. If the passenger is from the ticketing area,
* this call to step() implements [UC5]. If the passenger is from the security area, this call to step() implements [UC6].
* In each case, the passenger is removed from the corresponding area. If there is no next passenger, an IllegalStateException is thrown.
* @throws IllegalStateException if there is no next passenger.
*/
public void step() {

 - Throws: IllegalStateException

/**
* Checks if there is more steps to be taken.
*
* @return true if there is, false otherwise
*/
public boolean moreSteps() {

/**
* Returns the index of the security checkpoint line for currentPassenger.
* @return the current passenger index
*/
public int getCurrentIndex() {

/**
* Gets the current passenger color.
*
* @return the color of the current passenger
*/
public Color getCurrentPassengerColor() {

/**
* Checks if passenger cleared the security checkpoint.
*
* @return true if passenger cleared security checkpoint, false otherwise
*/
public boolean passengerClearedSecurity() {	

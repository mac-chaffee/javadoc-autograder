---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* declares WolfResultsManager class
* Observes RaceList, is observed by GUI.
*
*/
public class WolfResultsManager extends Observable implements Observer {

/** declares file name field */
private String filename;

/** declares changed field */
private boolean changed;

/** declares instance field */
private static WolfResultsManager instance;

/** declares list field */
private RaceList list;

/**
* gets instance of WolfResultsManager
* @return instance
*/
public static WolfResultsManager getInstance() {

/**
* WolfResultsManager constructor
*/
private WolfResultsManager() {

/**
* makes new list
*/
public void newList() {

/**
* checks if it's changed
* @return true if it's changed
*/
public boolean isChanged() {

/**
* sets the changed boolean
* @param changed boolean
*/
private void setChanged(boolean changed) {

/**
* gets file name
* @return file name
*/
public String getFilename() {

/**
* sets file name
* @param filename desired file name
* @throws IllegalArgumentException if filename is invalid
*/
public void setFilename(String filename) {

 - Throws: IllegalArgumentException

/**
* loads a RaceList from a file
* @param filename of file
*/
public void loadFile(String filename) {

/**
* saves file
* @param filename to save it to
*/
public void saveFile(String filename) {

/**
* gets race list
* @return race list
*/
public RaceList getRaceList() {

 - Throws: IllegalArgumentException

/**
* updates something
* @param observable to update
* @param o object
*/
public void update(Observable observable, Object o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* declares WolfResultsReader class
*
*/
public class WolfResultsReader {

/**
* WolfResultsReader constructor
*/
public WolfResultsReader() {

/**
* reads races from file
* @param filename of file
* @return raceList object with races
* @throws IllegalArgumentException if can't find file
*/
public static RaceList readRaceListFile(String filename) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* declares WolfResultsWriter class
*
*/
public class WolfResultsWriter {

/**
* WolfResultsWriter constructor
*/
public WolfResultsWriter() {

/**
* writes races to file
* @param filename of desired file
* @param list of races to write to file
*/
public static void writeRaceFile(String filename, RaceList list) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* declares IndividualResult class
*
*/
public class IndividualResult implements Comparable<IndividualResult>, Observer {

/** declares name field */
private String name;

/** declares age field */
private int age;

/** declares time field */
private RaceTime time;

/** declares pace field */
private RaceTime pace;

/** declares race field */
private Race race;

/**
* IndividualResult constructor
* @param race name
* @param name of runner
* @param age of runner
* @param time of runner
* @throws IllegalArgumentException if parameters are invalid
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* gets race
* @return race
*/
public Race getRace() {

/**
* gets name
* @return name of runner
*/
public String getName() {

/**
* gets runner age
* @return age
*/
public int getAge() {

/**
* returns time of runner
* @return time
*/
public RaceTime getTime() {

/**
* returns pace of runner
* @return pace
*/
public RaceTime getPace() {

/**
* compares to other runner's result
* @param other runner's time
* @return difference in time
*/
public int compareTo(IndividualResult other) {

/**
* returns individual result as string
* @return result as string
*/
public String toString() {

/**
* updates Observer
* @param observable to update
* @param arg object
*/
public void update(Observable observable, Object arg) {

/**
* calculates pace
* @param time total time
* @param distance of race
* @return time per mile
*/
private RaceTime calcPace(RaceTime time, double distance) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* declares Race class
*
*/
public class Race extends Observable {

/** declares name field */
private String name;

/** declares distance field */
private double distance;

/** declares date field */
private LocalDate date;

/** declares location field */
private String location;

/** declares results list field */
private RaceResultList results;

/**
* Race constructor
* @param name race name
* @param distance run in race
* @param date of race
* @param location of race
* @throws IllegalArgumentException if invalid parameters.
*/
public Race(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* Race constructor
* @param name race name
* @param distance run in race
* @param date of race
* @param location of race
* @param list of results
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList list) {

 - Throws: IllegalArgumentException

/**
* gets race name
* @return name
*/
public String getName() {

/**
* gets race distance
* @return distance
*/
public double getDistance() {

/**
* gets race date
* @return date
*/
public LocalDate getDate() {

/**
* gets race location
* @return location
*/
public String getLocation() {

/**
* get list of race race results
* @return race results
*/
public RaceResultList getResults() {

/**
* adds result to race
* @param result to add
*/
public void addIndividualResult(IndividualResult result) {

/**
* sets distance of race
* @param distance to set
* @throws IllegalArgumentException if distance is not positive
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* gets race as a string
* @return race as string
*/
public String toString() {

/**
* generates hashcode
* @return hash code
*/
@Override
public int hashCode() {

/** 
* checks if objects are equal
* @param obj to compare
* @return true if they're equal
*/
@Override
public boolean equals(Object obj) {

/**
* filters race by age and pace
* @param i min age
* @param i2 max age
* @param s1 min pace
* @param s2 max pace
* @return filtered list
*/
public RaceResultList filter(int i, int i2, String s1, String s2) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceList class
*
*/
public class RaceList extends Observable implements Observer {

/** declares races field */
private ArrayList races;

/**
* RaceList constructor
*/
public RaceList() {

/**
* adds race to list
* @param race to add
* @throws IllegalArgumentException if race is null
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

/**
* creates race and adds it to list
* @param name of race
* @param distance of race
* @param date of race
* @param location of race
* @throws IllegalArgumentException if parameters are invalid
*/
public void addRace(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* removes race at given index
* @param index of race to be removed
*/
public void removeRace(int index) {

/**
* gets race at given index
* @param index of desired race
* @return race at index
*/
public Race getRace(int index) {

 - Throws: IndexOutOfBoundsException

/**
* gets list size
* @return size of list
*/
public int size() {

/**
* updates something
* @param observe observable
* @param o object
*/
public void update(Observable observe, Object o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceResultList class
*
*/
public class RaceResultList {

/** declares results list field */
private SortedLinkedList<IndividualResult> results;

/** 
* RaceResultList constructor
*/
public RaceResultList() {

/**
* adds result to list
* @param result to add
* @throws IllegalArgumentException if result is null
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* adds result
* @param race race name
* @param name runner name
* @param age runner age
* @param time runner time
* @throws IllegalArgumentException if invalid parameters
*/
public void addResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* gets result at index
* @param index given
* @return result at that index
*/
public IndividualResult getResult(int index) {

 - Throws: IndexOutOfBoundsException

/**
* gets list size
* @return size
*/
public int size() {

/**
* gets results in 2D string array
* @return results in 2D string array
*/
public String[][] getResultsAsArray() {

/**
* filters list
* @param maxAge max age
* @param minAge min age
* @param minPace max pace
* @param maxPace min pace
* @return filtered list
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* ArrayList class
* 
*
*/
public class ArrayList implements List {

/** serialVersionUID field */
// private static final long serialVersionUID;

/** resize field */
private static final int RESIZE = 10;

/** list field */
private Object[] list;

/** size field */
private int size;

/**
* ArrayList Constructor
*/
public ArrayList() {

/**
* ArrayList Constructor
* 
* @param size size of list
*/
public ArrayList(int size) {

 - Throws: IllegalArgumentException

/**
* gets size
* 
* @return size
*/
@Override
public int size() {

/**
* determines if list is empty
* 
* @return true if empty
*/
@Override
public boolean isEmpty() {

/**
* checks if list contains an object
* 
* @return true if it does
*/
@Override
public boolean contains(Object o) {

/**
* Adds non-null Objects to the end of the list and returns true if it was added
* successfully. Returns false if there parameter is null.
* 
* @param o Object to append to the end of the list
* @return true If the element is successfully added. Returns false if the
*         parameter is null
* @throws NullPointerException if object is null
* @throws IllegalArgumentException if duplicate element
*/
@Override
public boolean add(Object o) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* gets an object at a given index as long as the index is Valid
* 
* @param index desired index
* @return object at that index
* @throws IndexOutOfBoundsException if parameter is less than 0 or greater than
*                                   or equals to size
*/
@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Shifts the parameter index and all following elements to one higher index and
* the adds the parameter element to the ArrayList at the desired index.
* Attempting to add null elements will throw a NullPointerException
* 
* @param index   desired index
* @param element to be added
* @throws IndexOutOfBoundsException if the parameter index to add to is greater
*                                   than size or less than 0
* @throws NullPointerException      if element to add is null
* @throws IllegalArgumentException if duplicate element
*/
@Override
public void add(int index, Object element) {

 - Throws: IndexOutOfBoundsException

 - Throws: IllegalArgumentException

 - Throws: NullPointerException

/**
* Removes an object from the ArrayList at an index and returns the object.
* Throws an exception if the parameter index is invalid
* 
* @param index to be removed
* @return object that's removed
* @throws IndexOutOfBoundsException if the parameter index to add to is greater
*                                   than size or less than 0
*/
@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the first occurrence of the specified element in this
* list, or -1 if this list does not contain the element
* 
* @param o object to check the existence of within the list
* @return index of that object in list
*/
@Override
public int indexOf(Object o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true (as specified by {@link Collection#add})
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index > size())
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceTime represents a race time in the following format: hh:mm:ss
*
*/
public class RaceTime {

/** Time in hours it took to finish the race */
private int hours;

/** The minutes in the time it took to finish the race */
private int minutes;

/** The seconds in the time it took to finish the race */
private int seconds;

/**
* Constructor for RaceTime when given the hours, minutes and seconds
* 
* @param hours The RaceTime hours
* @param minutes The RaceTime minutes
* @param seconds The RaceTime seconds
*/
public RaceTime(int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

/**
* Constructor for RaceTime with a string input to convert a time read in from a
* file in the format of "hh:mm:ss" to fill the hour, minute, and second field
* 
* @param time The string representation of time in the format of "hh:mm:ss"
*/
public RaceTime(String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the total time of this RaceTime in seconds
* 
* @return The seconds represented by the hours, minutes, and seconds field
*/
public int getTimeInSeconds() {

/**
* gets the number of hours
* @return hours
*/
public int getHours() {

/**
* gets the number of minutes
* @return minutes
*/
public int getMinutes() {

/**
* gets the number of seconds
* @return seconds
*/
public int getSeconds() {

/**
* Get this RaceTime as a string in the format of "hh:mm:ss"
* 
* @return The racetime as a string in the format of "hh:mm:ss"
*/
public String toString() {

/**
* Compares the total time of this RaceTime to the parameter raceTime. If this
* RaceTime is greater than the parameter, return a 1. Less than return a -1,
* and if they are equal a 0 is returned.
* 
* @param other The other RaceTime to compare to
* @return A 1 if this RaceTime is greater than other RaceTime, and -1 if this
*         RaceTime is less than other RaceTime, and a 0 if both RaceTime's are
*         equal
*/
public int compareTo(RaceTime other) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* SortedLinkedList has a type and stores a indefinitely long list of values
* using Nodes that contain a value and a next node. List cannot hold null
* values, or repeated values
* 
* @param <E> generic object
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/** declares head ListNode */
private Node<E> head;

/**
* SortedLinkedList constructor
*/
public SortedLinkedList() {

/** (non-Javadoc)
* @see java.lang.Object#toString()
*/
@Override
public String toString() {

//	public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

/**
* gets size of this List
* 
* @return size of the list as an integer
*/
@Override
public int size() {

/**
* determines if list is empty
* 
* @return true if empty
*/
@Override
public boolean isEmpty() {

/**
* checks if list contains a given object
* 
* @return true if it this SortedLinkedList contains parameter object
*/
@Override
public boolean contains(E e) {

/**
* adds an object to list in a sorted location as determined by elements
* compareTo method. Will not add null elements nor repeated elements
* 
* @param e Element to be sorted into the linked list
* @return true if it's added to the list successfully
* @throws NullPointerException if parameter element to be added is null
* @throw IllegalArgumentException if element already exists within the list
*/
@Override
public boolean add(E e) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* gets an object at a given index
* 
* @param index desired index
* @return object at that index
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* removes object at index
* 
* @param index to be removed
* @return object that's removed
* @throws IndexOutOfBoundsException if the index is less than 0 or greater than
*                                   or equals size
*/
@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

 - Throws: IndexOutOfBoundsException

/**
* gets index of a given object
* 
* @param e object
* @return index of that object in list
*/
@Override
public int indexOf(E e) {

/**
* Node Class that can store a value and a next node
* 
* @param <E> generic object
*/
@SuppressWarnings("hiding")
public class Node<E> {

/** declares next node */
private Node<E> next;

/** declared data value */
private E value;

/**
* Node constructor
* 
* @param e    data
* @param node next node
*/
public Node(E e, Node<E> node) {

/**
* Generates hash code for this Node
* 
* @return hashCode for this node using this nodes fields
*/
@Override
public int hashCode() {

/**
* Determines if this Node is equal to parameter node
* 
* @param obj Object to compare equity to
* @return True if this object equals the parameter Node
*/
@Override
public boolean equals(Object obj) {

@SuppressWarnings("unchecked")
@SuppressWarnings("rawtypes")
private SortedLinkedList getOuterType() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
* @param <E> element
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A class that handles the pre-security actions such as departure time and determining the next passenger to go through security.
*/
public class PreSecurity implements TransitGroup {

/** Instance variable that holds the number of passengers that are outside the security area */
private PassengerQueue outsideSecurity;

/**
* Generates the number of passengers and stores their total wait time and total process time in the log
* @param numberOfPassengers the number of passengers
* @param myLog the reporter object that logs the data if wait time, process time, etc
*/
public PreSecurity(int numberOfPassengers, Reporter myLog) {

 - Throws: IllegalArgumentException

/**
* Returns the departure time of the next passenger
* @return the departure time
*/
public int departTimeNext() {

/**
* Determines the next person that goes thru security
* @return the Passenger that is next to go through security
*/
public Passenger nextToGo() {

/**
* Determines if the pre-security area has another passenger in line
* @return true if there is another passenger in line and false otherwise
*/
public boolean hasNext() {

/**
* Removes the next Passenger
* @return the next passenger that is removed
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents the collection of security checkpoints and their waiting lines. 
*/
public class CheckPoint {

/** The time that the last passenger in line will clear security and leave the line */
private int timeWhenAvailable;

/** Instance variable that perform activities */
private PassengerQueue line;

/**
* The constructor
*/
public CheckPoint() {

/**
* Determines the size of the checkpoint
* @return the size of the checkpoint
*/
public int size() {

/**
* Removes a passenger from the checkpoint line
* @return the passenger that is removed from the checkpoint line
*/
public Passenger removeFromLine() {

/**
* Determines if the checkpoint has a next passenger
* @return a boolean depending on if the passengers has a next checkpoint
*/
public boolean hasNext() {

/**
* Returns the amount of time before the next passenger will clear security, returning Integer.MAX_VALUE if the line is empty
* @return the amount of time before the next passenger will clear security or Integer.MAX_VALUE if the line is empty
*/
public int departTimeNext() {

/**
* Returns the passenger at the front of the security line or null if the line is empty.
* @return the passenger at the front of the security line or null if the line is empty
*/
public Passenger nextToGo() {

/**
* Adds the parameter to the end of the line, setting their waitTime according to timeWhenAvailable as well as their arrivalTime and processTime, and updating timeWhenAvailable
* @param passenger the passenger in the checkpoint
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents the collection of security checkpoints and their waiting lines.
*/
public class SecurityArea implements TransitGroup {

/** Maximum number of security checkpoints */
private static final int MAX_CHECKPOINTS = 17;

/** Minimum number of security checkpoints */
private static final int MIN_CHECKPOINTS = 3;

/** Error message for invalid number of security checkpoints */
private static final String ERROR_CHEKCPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** Error message for any attempt to add to a security checkpoint line with an improper index */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** Largest index for lines reserved for Fast Track passengers */
private int largestFastIndex;

/** The index of the line reserved for TSA PreCheck/Trusted Travelers */
private int tsaPreIndex;

/** Array of security checkpoints */
private CheckPoint[] check;

/** Instance variable that refers to the Checkpoint class */
/**
* Determines the number of security checkpoints. If invalid number of security checkpoints,
* throw an IllegalArgumentException.
* @param numberOfSecurityCheckpoints the number of security checkpoints
* @throws IllegalArgumentException for invalid number of security checkpoints
*/
public SecurityArea(int numberOfSecurityCheckpoints) {

 - Throws: IllegalArgumentException

/**
* Determines if the number of security checkpoints is valid
* @param numberOfSecurityCheckpoints the number of security gates
* @return the boolean that relates to if the number of gates is valid
*/
private boolean numGatesOK(int numberOfSecurityCheckpoints) {

/**
* Adds the passenger to the security line with the index given by the first parameter
* @param line the line number that the passenger is added to
* @param passenger the passenger that is added to the security line with the index given by the first parameter
*/
public void addToLine(int line, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
*  Returns the index of the shortest security line that all passengers are permitted to select
*  @return the index of the shortest security line that all passengers are permitted to select
*/
public int shortestRegularLine() {

/**
*  Returns the index of the shortest security line restricted to Fast Track passengers
*  @return the index of the shortest security line restricted to Fast Track passengers
*/
public int shortestFastTrackLine() {

/**
*  Returns the index of the shortest security line restricted to TSA PreCheck/Trusted Traveler passengers
*  @return the index of the shortest security line restricted to TSA PreCheck/Trusted Traveler passengers
*/
public int shortestTSAPreLine() {

/**
* Returns the number of passengers in the line with the given index. 
* Throws an IllegalArgumentExcpetion if the index is out of range.
* @param length the length of the security checkpoint line
* @throws IllegalArgumentException if the index is out of range
* @return the number of passengers in the line with the given index
*/
public int lengthOfLine(int length) {

 - Throws: IllegalArgumentException

/**
* Returns the time that the next passenger will finish clearing security, or Integer.MAX_VALUE if all lines are empty
* @return the next time that the next passenger will finish clearing security
*/
@Override
public int departTimeNext() {

/**
* Returns the next passenger to clear security, or null if there is none
* @return the next passenger to clear security, or null if there is none
*/
@Override
public Passenger nextToGo() {

/**
* Removes and returns the next passenger to clear security
* @return the next passenger to clear security
*/
@Override
public Passenger removeNext() {

/**
* Return the index of the shortest line
* @param lowerRange the smallest index
* @param upperRange the largest index
* @return the shortest line in the security area
*/
private int shortestLineInRange(int lowerRange, int upperRange) {

/**
* Determines the line in the security area that is next to clear security and returns the index of the line
* @return the index of the line that is next to clear security
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Deals with the Fast Track passengers
* Extends the Passenger class
*/
public class FastTrackPassenger extends Passenger {

/** The color of the passenger in the animation */
private Color color;

/** Maximum expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* Calls the Passenger constructor to set values declared in Passenger
* @param arrivalTime the time that the passenger joins a security line
* @param processTime the amount of time that the passenger spends at the front of the security line actually undergoing the security check
* @param myLog the reporter object that logs the data of wait time, process time, etc
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter myLog) {

 - Throws: IllegalArgumentException

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the parent class, and then places this passenger in the selected line
* @param lines the list of security lines in the airport
*/
public void getInLine(TransitGroup lines) {

/**
* Gets the color of the passenger
* @return the color of the passenger
*/
public Color getColor() {

/**
* Picks a security checkpoint line and return the index of the line number the passenger will pick
* @param lines the list of security lines in the airport
* @return the line number that the passenger will pick
*/
//	private int pickLine(TransitGroup lines) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Deals with the ordinary passengers
* Extends the Passenger class
*/
public class OrdinaryPassenger extends Passenger {

/** The color of the passenger in the animation */
private Color color;

/** Maximum expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* Calls the Passenger constructor to set values declared in Passenger
* @param arrivalTime the time that the passenger joins a security line
* @param processTime the amount of time that the passenger spends at the front of the security line actually undergoing the security check
* @param myLog the reporter object that logs the data of wait time, process time, etc
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter myLog) {

 - Throws: IllegalArgumentException

/**
* Gets the color of the passenger
* @return the color of the passenger
*/
public Color getColor() {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the parent class, and then places this passenger in the selected line
* @param lines the list of security lines in the airport
*/
public void getInLine(TransitGroup lines) {

/**
* Picks a security checkpoint line and returns the index of the line number that the passenger will pick
* @param numberOfSecurityCheckpoints the list of security lines in the airport
* @return the line number that the passenger will pick
*/
//	private int pickLine(TransitGroup lines) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Abstract class that holds passenger objects
*/
public abstract class Passenger {

/** The minimum process time */
public static final int MIN_PROCESS_TIME = 20;

/** The arrival time of the passenger */
private int arrivalTime;

/** The time the passenger will spend in a security line */
private int waitTime;

/** The process time of the passenger */
private int processTime;

/** The line index */
private int lineIndex;

/** Waiting or processing */
private boolean waitingProcessing;

/** Logs the data in Reporter */
private Reporter myLog;

/**
* The constructor for the Passenger object
* @param arrivalTime the time that the passenger joins a security line
* @param processTime the amount of time that the passenger spends at the front of the security line actually undergoing the security check
* @param myLog the reporter object that logs the data of wait time, process time, etc
*/
public Passenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Gets the arrival time of the passenger
* @return the arrival time of the passenger
*/
public int getArrivalTime() {

/**
* Gets the waiting time of the passenger
* @return the waiting time of the passenger
*/
public int getWaitTime() {

/**
* Sets the wait time of the passenger
* @param waitTime the wait time of the passenger
*/
public void setWaitTime(int waitTime) {

/**
* Gets the process time of the passenger
* @return the process time of the passenger
*/
public int getProcessTime() {

/**
* Gets the line index of the security checkpoint line that the passenger selects and joins
* @return the line index of the passenger
*/
public int getLineIndex() {

/**
* Determines if the passenger is waiting in a security line
* @return true if the passenger selects and joins a security line, false until the passenger selects and joins a security line
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when he/she finishes security checks
*/
public void clearSecurity() {

/**
* Sets the index of the security checkpoint line that the passenger selects and joins
* @param lineIndex the index of the security checkpoint line that the passenger selects and joins
*/
protected void setLineIndex(int lineIndex) {

/**
* Adds the passenger to the appropriate security line
* @param lines the list of security lines in the airport
*/
public abstract void getInLine(TransitGroup lines);

/**
* Gets the color of the passenger
* @return the color of the passenger
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
* @throws NoSuchElementException if the queue is empty
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Deals with the TSA PreCheck and Trusted Traveler passengers
* Extends the Passenger class
*/
public class TrustedTraveler extends Passenger {

/** The color of the passenger in the animation */
private Color color;

/** Maximum expected process time */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Calls the Passenger constructor to set values declared in Passenger
* @param arrivalTime the time that the passenger joins a security line
* @param processTime the amount of time that the passenger spends at the front of the security line actually undergoing the security check
* @param myLog the reporter object that logs the data of wait time, process time, etc
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter myLog) {

 - Throws: IllegalArgumentException

/**
* Gets the color of the passenger
* @return the color of the passenger
*/
public Color getColor() {

/**
* selects the appropriate checkpoint waiting line, sets lineIndex from the parent class, and then places this passenger in the selected line
* @param lines the list of security lines in the airport
*/
public void getInLine(TransitGroup lines) {

/**
* Picks a security checkpoint line
* @param lines the list of security lines in the airport
* @return the line number that the passenger will pick
*/
//	private int pickLine(TransitGroup lines) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Determines which passenger to process next
*/
public class EventCalendar {

/** First instance variable that holds an instance of transit group */
private TransitGroup highPriority;

/** Second instance variable that holds an instance of transit group */
private TransitGroup lowPriority;

/**
* Takes in two TransitGroups
* @param highPriority the next passenger to leave the line they're currently at the front of
* @param lowPriority the next passenger to leave the line they're currently at the front of
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Returns the passenger from highPriority and lowPriority who will be the next to exit the group/line theyre currently at the front of
* @return the passenger from highPriority and lowPriority who will be the next to exit the group/line theyre currently at the front of
* the passenger from highPriority is returned in the case of a tie for the next to exit
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Logs important data
*/
public class Log implements Reporter {

/** number of passengers that have completed the process */
private int numCompleted;

/** total wait time for passenger */
private int totalWaitTime;

/** total process time for passenger */
private int totalProcessTime;

/**
* The parameterless constructor for the Log class
*/
public Log() {

/**
* Gets the number of passengers that have completed the whole ticketing and security process
* @return the number of passengers that have completed the whole ticketing and security process
*/
@Override
public int getNumCompleted() {

/**
* Logs the data for the passenger
*/
@Override
public void logData(Passenger p) {

/**
* Determine the average wait time for the passenger
* @return the average wait time for the passenger
*/
@Override
public double averageWaitTime() {

/**
* Determines the average process time for the passenger
* @return the average process time for the passenger
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements the back end process when results from any previous runs are erased and the security checkpoints are drawn.
*/
public class Simulator {

/** The number of passengers */
private int numPassengers;

/** Instance of TransitGroup for ticketing */
private TransitGroup inTicketing;

/** Intance of TransitGroup for security */
private TransitGroup inSecurity;

/** Instance of Passenger */
private Passenger currentPassenger;

/** Reporter object that logs the data */
private Reporter log;

/** EventCalendar object for EventCalendar */
private EventCalendar myCalendar;

/**
* The constructor for the Simulator class
* @param numberOfCheckpoints the number of checkpoints
* @param numPassengers the number of passengers
* @param trustedPercentage the percent of passengers who are TSA PreCheck/Trusted Travelers
* @param fastTrackPercentage the percent of passengers who are Fast Track
* @param ordinaryPercentage the percent of passengers who do not qualify for special security checkpoints
*/
public Simulator(int numberOfCheckpoints, int numPassengers, int trustedPercentage, int fastTrackPercentage, int ordinaryPercentage) {

 - Throws: IllegalArgumentException

/**
* Helper method that check the parameters of the constructor
* @param numPassengers the number of passengers
* @param trustedPercentage the percent of passengers who are TSA PreCheck/Trusted Travelers
* @param fastTrackPercentage the percent of passengers who are Fast Track
* @param ordinaryPercentage the percent of passengers who do not qualify for special security checkpoints
*/
private void checkParameters(int numPassengers, int trustedPercentage, int fastTrackPercentage, int ordinaryPercentage) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Sets up the Simulator
* @param numPassengers the number of passengers
* @param fastTrackPercentage the percent of passengers who are Fast Track
* @param ordinaryPercentage the percent of passengers who do not qualify for special security checkpoints
*/
private void setUp(int numberOfCheckpoints, int trustedPercentage, int fastTrackPercentage) {

/**
* Gets the reporter
* @return the Reporter object
*/
public Reporter getReporter() {

/**
* Goes through the action of the next passenger from the EventCalendar
*/
public void step() {

 - Throws: IllegalStateException

/**
* Determines if the passenger has more steps in the process
* @return true if the passenger has more steps to complete or false if they do not have any more steps
*/
public boolean moreSteps() {

/**
* Returns the index of the security checkpoint line for current passenger
* @return the index of the security checkpoint line for current passenger
*/
public int getCurrentIndex() {

/**
* Gets the color of the current passenger
* @return the color of the current passenger
*/
public Color getCurrentPassengerColor() {

/**
* Determines if the passenger just cleared a security checkpoint and finished processing
* @return true if the passenger has finished processing or false otherwise
*/
public boolean passengerClearedSecurity() {


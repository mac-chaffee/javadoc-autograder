---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*  This class is a singleton that maintains the RaceList, reads from, 
*  and writes to the *.md files for the WolfResults application
*
*/
public class WolfResultsManager extends Observable implements Observer {

/** The singleton */
private static WolfResultsManager instance;

/** The name of the file */
private String filename;

/** A boolean to check if the file has been changed */
private boolean changed = false;

/** The list of races */
private RaceList list;

/**
* Constructs the WolfResultsManager by creating an empty RaceList 
* and registering as an Observer of the list
*/
private WolfResultsManager() {

/**
* The singleton instance
* @return the single WolfResultsManager instance
*/
public static WolfResultsManager getInstance() {

/**
* Creates a new empty RaceList and registers the 
* WolfResultsManager as an Observer. 
*/
public void newList() {

/**
* Checks if the file has been changed
* @return the value stored in changed.
*/
public boolean isChanged() {

/**
* Change the value stored in changed
* @param changed
* 		the boolean value to change to
*/
/** private void setChanged(boolean changed) {

*/
/**
* Gets the filename
* @return a string of the filename
*/
public String getFilename() {

/**
* Sets the filename
* @param filename
* 		the name to set the filename to
* @throws IllegalArgumentException If the filename parameter is null, 
* 		the empty string, or string with whitespace only,
*/
public void setFilename(String filename) throws IllegalArgumentException{

 - Throws: IllegalArgumentException

/**
* Loads a file
* @param filename
* 		the name of the file to load
*/
public void loadFile(String filename) {

 - Throws: IllegalArgumentException

/**
* Saves a file
* @param filename
* 		the name of the file to save the file to
*/
public void saveFile(String filename) {

/**
* Gets the RaceList
* @return the RaceList
*/
public RaceList getRaceList() {

/**
* Update if a RaceList observed by WolfResultsManager changes
* @param o
* 		the observable
* @param arg
* 		the object to change to
*/
public void update(Observable o, Object arg) {

/**
* Adds the object as an observer
* @param obj
* 		the object to add 
*/
/**public void addObserver(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class reads a result from a file
* 
*
*/
public class WolfResultsReader {

/**
* The constructor for the class
*/
public WolfResultsReader() {

/**
* IO does not need a coded constructor
*/
/**
* This class is a private class in order to make the reader class more
* simple
* 
* @param line
*            line to scan
* @param race
*            Race to scan
* @return individualResult the results to return
*/
private static IndividualResult readIndividuals(String line, Race race) {

/**
* Reads the given RaceList to the given file.
* 
* @param filename
*            the name of the file to write to
* @throws IllegalArgumentException
*             if there is any error
* @return the RaceList
*/
public static RaceList readRaceListFile(String filename) {

 - Throws: IllegalArgumentException

// throw new IllegalArgumentException();

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class writes a result to a file
*
*/
public class WolfResultsWriter {

/**
* The constructor for the class
*/
public WolfResultsWriter() {

/**
* IO does not need a coded constructor
*/
/**
* Writes the given RaceList to the given file.
* @param filename
* 		the name of the file to write to
* @param list
* 		the RaceList to write to the file
* @throws IllegalArgumentException if there is any error
*/
public static void writeRaceFile(String filename, RaceList list) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class makes an individual list for each runner
*
*/
public class IndividualResult implements Comparable<IndividualResult>, Observer {

/** The name of the runner in the list */
private String name;

/** The age of the runner */
private int age;

/** The race */
private Race race;

/** The Runner's pace */
private RaceTime pace;

/** The Runner's time */
private RaceTime time;

/**
* The constructor for the class
* @param race
* 		the race which is an instance of Race
* @param name
*		the name of the runner
* @param age
* 		the age of the runner
* @param time
* 		the time of the runner
* @throws IllegalArgumentException
* 		if (a) race is null; (b) name is null, empty string, 
* 		or all whitespace; (c) age is negative; or (d) time is null.
*/
public IndividualResult(Race race, String name, int age, RaceTime time) throws  IllegalArgumentException {

 - Throws: IllegalArgumentException

/**
* Gets the race 
* @return the race, an instance of race
*/
public Race getRace() {

/**
* Gets the name of the runner
* @return the name of the runner
*/
public String getName() {

/**
* Gets the age of the runner
* @return the age of the runner
*/
public int getAge() {

/** 
* Gets the time of the runner
* @return the time the runner finished
*/
public RaceTime getTime() {

/** 
* Gets the pace of the runner
* @return	the pace
*/
public RaceTime getPace() {

/**
* Two IndividualResult objects are compared on their times. 
* @param other
* 		the object to be compared
* @return the time
*/
public int compareTo(IndividualResult other) {

/**
* The string representation of an IndividualResult is the name, age, 
* time, and pace fields. 
* @return a string representation
*/
public String toString() {

/**
* The pace is updated if the Race that the IndividualResult is observing 
* notified its observers of a change.
* @param o
* 		the observable
* @param arg
* 		the object to change
*/
public void update(Observable o, Object arg) {

private void calculatePace() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class has an instance of a LocalDate that will hold the date of the
* race.
* 
*
*/
public class Race extends Observable {

/** The name of the race */
private String name;

/** The distance of the race */
private double distance;

/** The date of the race */
private LocalDate date;

/** The location of the race */
private String location;

/** The race results */
private RaceResultList results;

/**
* A constructor for the class
* 
* @param name
*            the name of the race
* @param distance
*            the distance of the race
* @param date
*            the date of the race
* @param location
*            the location of the race
* @throws IllegalArgumentException
*             if any of the parameters is null
*/
public Race(String name, double distance, LocalDate date, String location) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* A constructor for the class
* 
* @param name
*            the name of the race
* @param distance
*            the distance of the race
* @param date
*            the date of the race
* @param location
*            the location of the race
* @param results
*            the results of the race
* @throws IllegalArgumentException
*             if any of the parameters is null
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

/**
* Gets the name of the race
* 
* @return the name of the race
*/
public String getName() {

/**
* Gets the distance of the race
* 
* @return the distance as a double
*/
public double getDistance() {

/**
* Gets the date of the race
* 
* @return the date as a LocalDate instance
*/
public LocalDate getDate() {

/**
* Gets the location of the race
* 
* @return the location of the race
*/
public String getLocation() {

/**
* Gets the result of the race
* 
* @return the results as an instance of RaceResultList
*/
public RaceResultList getResults() {

/**
* Add result to results
* 
* @param indResult
*            the result to add to the Results list
*/
public void addIndividualResult(IndividualResult indResult) {

/**
* Set distance field to the parameter.
* 
* @param distance
*            the distance to set the race distance
*/
public void setDistance(double distance) {

/**
* The string representation of an Race is the name, distance, date, and
* location fields
* 
* @return the string representation
*/
public String toString() {

/**
* The hashcode
* 
* @return an integer value
*/
@Override
public int hashCode() {

/**
* Checks if two objects are the same
* 
* @param obj
*            the object to compare
* @return a boolean value
*/
@Override
public boolean equals(Object obj) {

/**
* Filters the RaceResultList
* 
* @param minAge
*            the minimum age in the list
* @param maxAge
*            the maximum age in the list
* @param minPace
*            the minimum pace in the list
* @param maxPace
*            the maximum pace in the list
* @throws IllegalArgumentException
*             if minPace or maxPace is not a valid RaceTime
* @return a filtered RaceResultList
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace)
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class has an instance ArrayList that will hold Race objects.
* 
*
*/
public class RaceList extends Observable implements Observer {

/** A list of the races */
/**
* Constructs the ArrayList.
*/
public RaceList() {

/**
* Adds a race to the list
* 
* @param race
*            the race to be added to the list
* @throws IllegalArgumentException
*             if race is null
*/
public void addRace(Race race) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

/**
* Adds a race to the list
* 
* @param name
*            the name of the race
* @param distance
*            the distance of the race
* @param date
*            the date of the race
* @param location
*            the location of the race
* @throws IllegalArgumentException
*             if any of the parameters is null
*/
public void addRace(String name, double distance, LocalDate date, String location) throws IllegalArgumentException {

//			throw new IllegalArgumentException();

//			throw new IllegalArgumentException();

/**
* Removes race at a particular index
* 
* @param idx
*            the index of the race to be removed
*/
public void removeRace(int idx) {

/**
* Gets a race at the particular index in the array list
* 
* @param idx
*            the index of the race
* @return the race at the index
*/
public Race getRace(int idx) {

/**
* The size of the list
* 
* @return an integer representation of the size
*/
public int size() {

/**
* Checks if the Race that the RaceList is observing notified its observers
* of a change.
* 
* @param o
*            an instance of Race
* @param arg
*            the object to update the race to
*/
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* An ParkList that has a SortedLinkedList of IndividualResult.
* 
*/
public class RaceResultList {

private SortedLinkedList<IndividualResult> results;

/**
* The constructor for the class
*/
public RaceResultList() {

/**
* Add a result to the RaceResultList
* 
* @param racerResult
*            the results for the racer
* @throws IllegalArgumentException
*             if racerResult is null
*/
public void addResult(IndividualResult racerResult) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

/**
* Adds a result to the RaceResultList
* 
* @param race
*            the race to add the result to
* @param name
*            the name of the runner
* @param age
*            the age of the runner
* @param timer
*            the time the runner finished the race
* @throws IllegalArgumentException
*             if any of the parameters is null
*/
public void addResult(Race race, String name, int age, RaceTime timer) throws IllegalArgumentException {

/**
* Gets an individual result from the list
* 
* @param idx
*            the index of the result in the list
* @return an IndividualResult in the list
* @throws IndexOutOfBoundsException
*             if idx is out of bounds.
*/
public IndividualResult getResult(int idx) throws IndexOutOfBoundsException {

/**
* The number of elements in the list
* 
* @return an integer representation of the size
*/
public int size() {

/**
* Returns a 2D array of string representing results. First dimension is the
* result/runner (in sorted order by time). Second dimension are the
* different fields: name, age, time, and pace
* 
* @return the 2D array. If the list is empty an empty array
*/
public String[][] getResultsAsArray() {

/**
* Filters the RaceResultList
* 
* @param minAge
*            the minimum age in the list
* @param maxAge
*            the maximum age in the list
* @param minPace
*            the minimum pace
* @param maxPace
*            the maximum pace
* @return list of results such that runners age is between minAge and
*         maxAge (inclusive) and runners pace is between minPace and
*         maxPace (inclusive).
* @throws IllegalArgumentException
*             if minPace or maxPace is not a valid RaceTime
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace)
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Creates the ArrayList class with methods to add and remove items, get an item
* at an index, replace an item at an index, and find the ArrayList size.
* 
*/
public class ArrayList implements List {

/** The objects in the list */
private Object[] list;

/** The size of the list */
private int size;

/**
* The ArrayList constructor with no capacity
*/
public ArrayList() {

/**
* The ArrayList constructor with a capacity
* 
* @param capacity
*            the capacity of the list
*/
public ArrayList(int capacity) {

 - Throws: IllegalArgumentException

/**
* Adds an element to the array.
* 
* @param obj
*            the object to be added to the list
* @throws IllegalArgumentException
*             if the new element is a duplicate of an item already on the
*             list
* @throws NullPointerException
*             if the element is null
* @return a boolean value
* 
*/
public boolean add(Object obj) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Adds an element to a particular index in the array.
* 
* @param idx
*            index of the array where the element will be added
* @param obj
*            generic object type E that is the object being added
* @throws IndexOutOfBoundsException
*             if the index is outside the current size of the array or less
*             than zero
* @throws IllegalArgumentException
*             if the new element is a duplicate of an item already on the
*             list
* @throws NullPointerException
*             if the element is null
* 
*/
public void add(int idx, Object obj) {

 - Throws: NullPointerException

 - Throws: IndexOutOfBoundsException

// throw new IllegalArgumentException();

 - Throws: IllegalArgumentException

/**
* Checks if the list contains a specified object
* 
* @param obj
*            the element to check for in the list
* @return a boolean value
*/
public boolean contains(Object obj) {

/**
* Gets an element at a specified index in the array
* 
* @param index
*            the index of the element to return
* @return the element at the specified index
* @throws IndexOutOfBoundsException
*             if the index is less than zero or greater than the list size
*/
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Gets the index of an object in the list
* 
* @param obj
*            the element being searched for in the list
* @return the index of the object
*/
public int indexOf(Object obj) {

/**
* Checks if there are any element in the array
* 
* @return a boolean value
*/
public boolean isEmpty() {

/**
* Removes and returns an object in the list
* 
* @param index
*            the index of the element to remove
* @return the element to remove
*/
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Gets the size of the array, i.e, the number of elements in the array that
* are not null
* 
* @return an integer number for the size of the array
*/
public int size() {

/**
* This method doubles the capacity of the array when the number of elements
* in the array is equal to capacity
*/
private void growArray() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true (as specified by {@link Collection#add})
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index > size())
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A class that represents a race time hh:mm:ss.
* 
*
*/
public class RaceTime {

/** The time in hours */
private int hours;

/** The time in minutes */
private int minutes;

/** The time in seconds */
private int seconds;

/**
* The constructor for the class that creates the time the race was finished
* 
* @param hours
*            the hour the race was finished
* @param minutes
*            the minute the race was finished
* @param seconds
*            the second the race was finished
* @throws IllegalArgumentException
*             hours is negative or minutes or seconds are not in between 0
*             and 59 (inclusive).
*/
public RaceTime(int hours, int minutes, int seconds) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructor for the class that accepts the time as a string
* 
* @param time
*            the time the race was finished as a string
* @throws IllegalArgumentException
*             if time is not in the correct format (hh:mm:ss), hh is
*             negative, or mm or ss are not in between 0 and 59 (inclusive)
*/
public RaceTime(String time) throws IllegalArgumentException {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the hour the race was finished
* 
* @return the hour the race was finished
*/
public int getHours() {

/**
* Gets the minute the race was finished
* 
* @return the minute the race was finished
*/
public int getMinutes() {

/**
* Gets the second the hour the race was finished
* 
* @return the second the race was finished
*/
public int getSeconds() {

/**
* Converts the time the race was finished to seconds
* 
* @return the time in seconds
*/
public int getTimeInSeconds() {

/**
* Provides a string representation of the time
* 
* @return a string representation of the time
*/
public String toString() {

/**
* Compares the race time to another race time
* 
* @param timex
*            the time to be compared
* @return a negative integer, zero, or a positive integer as this object is
*         less than, equal to, or greater than the specified object.
*/
public int compareTo(RaceTime timex) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* An implementation of the SortedList interface with a data structure of linked
* Nodes
* 
* @param <E>
*            the object type
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/** The node */
private Node<E> head;

/** The size of the list */
private int size;

/**
* The SortedLinkedList constructor
*/
public SortedLinkedList() {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
public int size() {

/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
public boolean isEmpty() {

/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param obj
*            element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
public boolean contains(E obj) {

/**
* Adds the specified element to list in sorted order
*
* @param obj
*            element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException
*             if e is null
* @throws IllegalArgumentException
*             if list already contains obj
*/
public boolean add(E obj) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Returns the element at the specified position in this list.
*
* @param idx
*            index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException
*             if the index is out of range (index < 0 || index >= size())
*/
public E get(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param idx
*            the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException
*             if the index is out of range (index < 0 || index >= size())
*/
public E remove(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param element
*            element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
public int indexOf(E element) {

/**
* The hashcode for the class
* 
* @return an integer value for the generated hashcode
*/
@Override
public int hashCode() {

/**
* Checks if an object is the same as another object
* 
* @param obj
*            the object to be compared
* @return a boolean value
*/
@Override
public boolean equals(Object obj) {

/**
* Makes a string representation of the list
*
* @return a string representation of the list
*/
public String toString() {

/**
* This class makes the nodes to be used in the linked list
* 
*
* @param <E>
*            The object in the node
*/
public class Node<E> {

/** A value in the node */
/** The next node */
public Node<E> next;

/**
* The node constructor
* 
* @param value
*            the value at the data point of the node
* @param nodes
*            the next node
*/
public Node(E value, Node<E> nodes) {

/**
* The hashcode for the node
* 
* @return an integer value for the generated hashcode
*/
@Override
public int hashCode() {

/**
* Checks if an object in the node is the same as another object
* 
* @param obj
*            the object to be compared
* @return a boolean value
*/
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
* @param <E> generic object
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

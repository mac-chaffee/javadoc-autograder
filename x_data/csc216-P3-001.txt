---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The manager class is responsible for maintaining data structures for the entire application
*/
public class WolfResultsManager extends Observable implements Observer {

/** The file name of the file to load or save from */
private String filename;

/** Becomes true or false for if the instance has changed or not */
private boolean changed;

/** The single instance of the manager */
private static WolfResultsManager instance;

/** The list representing Races */
private RaceList list;

/**
* Gets the single instance of the manager
* @return instance	the instance of the manager
*/
public static WolfResultsManager getInstance() {

/**
* Constructs a WolfResultsManager
*/
private WolfResultsManager() {

/**
* Creates a new list for the RaceList field
*/
public void newList() {

/**
* Returns true if the instance is changed, false if it is not changed 
* based on the changed field
* @return changed	returns true or false if the instance is changed
*/
public boolean isChanged() {

/**
* Sets the changed field to true or false whether the instance is changed or not
* @param changed	the boolean parameter to set the changed field to
*/
private void setChanged(boolean changed) {

/**
* Gets the name of the file used to save to or load from
* @return filename	the name of the file to use
*/
public String getFilename() {

/**
* Sets the filename field to the given parameter string file name
* @param filename	the name to set the filename to
*/
public void setFilename(String filename) {

 - Throws: IllegalArgumentException

/**
* Loads a file for the program to read from
* @param filename	the name of the file to read from
*/
public void loadFile(String filename) {

/**
* Saves a new file with the given String parameter file name
* @param filename	the name of the file to save to
*/
public void saveFile(String filename) {

/**
* Gets the list from the private RaceList field
* @return list	the RaceList being returned from the private field
*/
public RaceList getRaceList() {

/**
* Updates the object
* @param obs	the observable
* @param obj	the object
*/
public void update(Observable obs, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reader class reads in data from a file and converts it into information the program can understand
*/
public class WolfResultsReader {

/**
* Reads the information from the file and converts it to a list usable by the program
* @param filename	the name of the file to read from
* @return raceList	returns the RaceList to be used in the program
* @throws FileNotFoundException
*/
public static RaceList readRaceListFile(String filename) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

//					throw new IllegalArgumentException();

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Writer class writes the data provided by the program as a new file
*/
public class WolfResultsWriter {

/**
* Writes the information from the RaceList parameter list into a new file using the filename
* @param filename	the name of the file to write to
* @param list		the RaceList to use information from
*/
public static void writeRaceFile(String filename, RaceList list) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents one runner's individual race result
*/
public class IndividualResult extends Observable implements Observer, Comparable<IndividualResult> {

/** The name of the runner */
private String name;

/** The age of the runner */
private int age;

/** The runner's total time */
private RaceTime time;

/** The runner's pace */
private RaceTime pace;

/** The race the runner ran */
private Race race;

/**
* Constructs an IndividualResult object
* @param race	the race the runner ran
* @param name	the name of the runner
* @param age	the age of the runner
* @param time	the runner's time
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* Private method to update the pace time
* @return pace	the new pace time
*/
private void updatePaceTime() {

/**
* Gets the Race the runner ran
* @return race	the race the runner ran
*/
public Race getRace() {

/**
* Gets the Name of the runner
* @return name	the name of the runner
*/
public String getName() {

/**
* Gets the age of the runner
* @return age	the age of the runner
*/
public int getAge() {

/**
* Gets the runner's time
* @return time	the runner's time
*/
public RaceTime getTime() {

/**
* Gets the runner's pace
* @return pace	the runner's pace
*/
public RaceTime getPace() {

/**
* Compares the Individual Result with another
* @param ir	the individual result to compare to
* @return a number
*/
@Override
public int compareTo(IndividualResult ir) {

/**
* Converts the individual result information to a string
* @return the string of the result
*/
public String toString() {

/**
* Updates the result
* @param obs	the observable object
* @param obj	the object to update
*/
public void update(Observable obs, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents one race and all of it's information
*/
public class Race extends Observable {

/** The name of the Race */
private String name;

/** The distance of the Race */
private double distance;

/** The date of the Race */
private LocalDate date;

/** The location of the Race */
private String location;

/** The Race Result List of results */
private RaceResultList results;

/**
* Constructs a Race
* @param name		the name of the race
* @param distance	the distance of the race
* @param date		the race date
* @param location	the location of the race
*/
public Race(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructs a Race
* @param name		the name of the race
* @param distance	the distance of the race
* @param date		the race date
* @param location	the location of the race
* @param results	the list of results
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the name of the race
* @return name	the name of the race
*/
public String getName() {

/**
* Gets the distance of the race
* @return distance	the distance of the race
*/
public double getDistance() {

/**
* Gets the date of the race
* @return date	the date of the race
*/
public LocalDate getDate() {

/**
* Gets the location of the race
* @return location	the location of the race
*/
public String getLocation() {

/**
* Gets the RaceResultList of results
* @return results	the list of results
*/
public RaceResultList getResults() {

/**
* Adds the individual result to the list race results
* @param result	the individual result to add
*/
public void addIndividualResult(IndividualResult result) {

/**
* Sets the distance field to the given distance parameter
* @param distance	the distance of the race to set
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* Filters the results based on 2 of the 4 parameters: by age or pace
* @param minAge	the minimum age to filter results by
* @param maxAge	the maximum age to filter results by
* @param minPace 	the minimum pace to filter results by
* @param maxPace	the maximum pace to filter results by
* @return the RaceResultList
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

/**
* Converts the Race information to a string
* @return the string of the Race information
*/
public String toString() {

/**
* The hashcode for the Race
* @return result	the result of the hashcode
*/
@Override
public int hashCode() {

/**
* If this is equal to the object to compare
* @return true or false if they are equal or not
*/
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The RaceList is an ArrayList of Races
*/
public class RaceList extends Observable implements Observer {

/** The array list of races */
private ArrayList races;

/**
* Constructs the Race List
*/
public RaceList() {

/**
* Adds the race based on the race parameter
* @param race	the race to add
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

/**
* Adds the race based on the race information from the parameters 
* @param name		the name of the race
* @param distance	the distance of the race
* @param date		the date of the race
* @param location	the location of the race
*/
public void addRace(String name, double distance, LocalDate date, String location) {

/**
* Removes the race at the given index from the parameter
* @param idx	the index of the race to remove from the array
*/
public void removeRace(int idx) {

/**
* Gets the Race at the given index
* @param idx	the index of the race to get from the array
* @return the Race
*/
public Race getRace(int idx) {

 - Throws: IndexOutOfBoundsException

 - Throws: IndexOutOfBoundsException

/**
* Gets the size of the array list
* @return the size
*/
public int size() {

@Override
public void update(Observable o, Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The RaceResultList has a SortedLinkedList of several IndividualResults
*/
public class RaceResultList {

/** The sorted linked list of results */
private SortedLinkedList<IndividualResult> results;

/**
* Constructs the RaceResultList
*/
public RaceResultList() {

/**
* Adds a new result based on the given IndividualResult parameter
* @param result	the individual result to add
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* Adds a new result based on the given the 4 parameters representing the runner's information
* @param race	the race the runner is in
* @param name	the name of the runner
* @param age	the age of the runner
* @param time	the runner's time
*/
public void addResult(Race race, String name, int age, RaceTime time) {

/**
* Gets the individual result of a runner based on the given index parameter
* @param idx	the index of the runner in the list
* @return the individual result
*/
public IndividualResult getResult(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the size of the list
* @return the size of the list
*/
public int size() {

/**
* Gets the results list as an array
* @return the String array of results
*/
public String[][] getResultsAsArray() {

/**
* Filters the results based on 2 of the 4 parameters: by age or pace
* @param minAge			the minimum age to filter results by
* @param maxAge			the maximum age to filter results by
* @param minPace 			the minimum pace to filter results by
* @param maxPace			the maximum pace to filter results by
* @return filteredResults	the RaceResultList
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

//	/**
//	private boolean alreadyAdded (RaceResultList firstList, IndividualResult result) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents the Array List that is used as the Race List. ArrayList implements the List interface
*/
public class ArrayList implements List {

/** The serial version UID */
@SuppressWarnings("unused")
private static long serialVersionUID = 1L;

/** The size to set the ArrayList to */
private static final int RESIZE = 10;

/** The list to make the ArrayList */
private Object[] list;

/** The size of the list */
private int size;

/**
* Constructs an empty array list at the RESIZE
*/
public ArrayList() {

/**
* Constructs an array list with a given size param
* @param capacity	the capacity of the list
*/
public ArrayList(int capacity) {

 - Throws: IllegalArgumentException

/**
* Adds the object parameter to the list and returns true if it is added
* @param element	the object to add
* @return true or false if the object was added
*/
public boolean add(Object element) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

@Override
public void add(int index, Object element) {

 - Throws: IndexOutOfBoundsException

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Checks if the list already contains the element parameter
* @param element	the element to check for in the list
* @return true or false if the element is in the list or not
*/
public boolean contains(Object element) {

/**
* Gets the element at the index parameter
* @param index	the index to get the element at
* @return the object at the index
*/
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Gets the index of the element parameter
* @param element	the element to get the index of
* @return the index of the element
*/
public int indexOf(Object element) {

/**
* Checks if the list is empty
* @return true or false if the list is empty
*/
public boolean isEmpty() {

/**
* Removes the element at the given index and shifts the rest of the elements down
* @param index		the index to remove the element at
* @return element	the element removed
*/
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* The size of the Array List
* @return size	the size of the Array List
*/
public int size() {

/**
* Changes the static size of the arrayList if the capacity is about to be
* reached
*/
private void growArray() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true (as specified by {@link Collection#add})
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index > size())
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents a race time in the form of h:mm:ss
*/
public class RaceTime {

/** The number of hours the runner took in the race */
private int hours;

/** The number of minutes the runner took in the race */
private int minutes;

/** The number of seconds the runner took in the race */
private int seconds;

/**
* Constructs a RaceTime
* @param hours		the hours the runner took
* @param minutes	the minutes the runner took
* @param seconds	the seconds the runner took
*/
public RaceTime(int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

/**
* Constructs a RaceTime given the full time takes in a time in the form of "hh:mm:ss"
* Got help from Dr. Heckman in office hours and used Lab FSM as a reference
* @param time	the time in string form
*/
public RaceTime(String time) {

 - Throws: IllegalArgumentException

//						throw new IllegalArgumentException("Invalid input time.");

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Gets the hours the runner took
* @return hours	the hours the runner took
*/
public int getHours() {

/**
* Gets the minutes the runner took
* @return minutes	the minutes the runner took
*/
public int getMinutes() {

/**
* Gets the seconds the runner took
* @return seconds	the seconds the runner took
*/
public int getSeconds() {

/**
* Gets the time in only seconds converting the hours and minutes
* @return the time in only seconds
*/
public int getTimeInSeconds() {

/**
* Converts the different times into a string formatted as h:mm:ss
* @return the string of the times
*/
public String toString() {

/**
* Compares the time to a different time
* @param other	the time to compare to
* @return a number
*/
public int compareTo(RaceTime other) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Represents the Sorted Linked List that is used for the Race Results. SortedLinkedList implements
* the SortedList interface.
* @param <E> element for sorted linked list
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E> {

/** the size of the list */
private int size;

/** the front node */
private Node<E> head;

/** 
* Constructs a sorted linked list
*/
public SortedLinkedList() {

/**
* the size param of the linked list
*/
@Override
public int size() {

/**
* the boolean if the list is empty or not
* @return if its empty
*/
@Override
public boolean isEmpty() {

/**
* boolean if the list contains an element passed it
* @param element	the element 
* @return true or false if it does contain the element or not
*/
@Override
public boolean contains(E element) {

/**
* the add method returns true if we can add the element passed in
* @return if it can be added
*/
@Override
public boolean add(E element) {

 - Throws: IllegalArgumentException

/**
* the get method gets an element from a certain index passed in
* @param index the index passed in
* @return E the element at that index
* @throws IndexOutOfBoundsException if the index is out of bounds
*/
@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* the remove method removes an element from the list
* @param index the index of the element we want to remove
* @return E the element being removed
* @throws IndexOutOfBoundsException if the index is out of bounds
*/
@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* the index of method returns the index of a certain element
* @param e the element we are searching for
* @return the index of the element as an int
*/
@Override
public int indexOf(E e) {

/**
* The hashcode for sorted linked list
* @return result	the result of the hashcode
*/
@Override
public int hashCode() {

/**
* Equals method for sorted linked list
* @param obj	the object to compare to
* @return true or false if this and obj are equal
*/
@Override
public boolean equals(Object obj) {

@SuppressWarnings("unchecked")
/**
* Turns SLL into string
*/
@Override
public String toString() {

/**
* Private inner class representing a node within the linked list
* @param <E>
*/
private class Node<E> {

/** List node after this node */
private Node<E> next;

/** Data this node has */
/**
* Construct a list node with the given data, which is followed by the next list
* node.
* 
* @param data	data to store in this list node
* @param next	next list node in this chain
*/
public Node(E data, Node<E> next) {

/**
* Hashcode for Node
* @return results	the result of hashcode
*/
@Override
public int hashCode() {

/**
* Equals method for Node
* @param obj	the object to compare to
* @return true or false if this and obj are equal
*/
@Override
public boolean equals(Object obj) {

//private SortedLinkedList getOuterType() {

//		private SortedLinkedList<E> getOuterType() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
* @param <E> the element param
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

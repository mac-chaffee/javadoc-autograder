---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/command/Command.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Encapsulates the information about a user command that would lead to a
* transition.
* 
*/
public class Command {

/**
* Enumeration representing one of the six possible commands that a user can
* make for the Incident Management FSM
*/
public enum CommandValue { INVESTIGATE, HOLD, RESOLVE, CONFIRM, REOPEN, CANCEL }
/** Enumeration representing the three possible on hold codes. */
public enum OnHoldReason { AWAITING_CALLER, AWAITING_CHANGE, AWAITING_VENDOR }
/** Enumeration representing the four possible resolution codes */
public enum ResolutionCode { PERMANENTLY_SOLVED, WORKAROUND, NOT_SOLVED, CALLER_CLOSED }
/** Enumeration representing the three possible cancellation codes */
public enum CancellationCode { DUPLICATE, UNNECESSARY, NOT_AN_INCIDENT }
/** String value of OnHoldReason - Awaiting Caller */
public static final String OH_CALLER = "Awaiting Caller";

/** String value of OnHoldReason - Awaiting Change */
public static final String OH_CHANGE = "Awaiting Change";

/** String value of OnHoldReason - Awaiting Vendor */
public static final String OH_VENDOR = "Awaiting Vendor";

/** String value of ResolutionCode - Permanently Solved */
public static final String RC_PERMANENTLY_SOLVED = "Permanently Solved";

/** String value of ResolutionCode - Workaround */
public static final String RC_WORKAROUND = "Workaround";

/** String value of ResolutionCode - Not Solved */
public static final String RC_NOT_SOLVED = "Not Solved";

/** String value of ResolutionCode - Caller Closed */
public static final String RC_CALLER_CLOSED = "Caller Closed";

/** String value of CancellationCode - Duplicate */
public static final String CC_DUPLICATE = "Duplicate";

/** String value of CancellationCode - Unnecessary */
public static final String CC_UNNECESSARY = "Unnecessary";

/** String value of CancellationCode - Not an Incident */
public static final String CC_NOT_AN_INCIDENT = "Not an Incident";

/** User id of the incident owner */
private String ownerId;

/** The work note */
private String note;

/** The Command Value */
private CommandValue c;

/** The On Hold Reason */
private OnHoldReason onHoldReason;

/** The Resolution Code */
private ResolutionCode resolutionCode;

/** The Cancellation Code */
private CancellationCode cancellationCode;

/**
* Constructs the Command object. An IllegalArgumentException is thrown for any
* of the following conditions: CommandValue is null, CommandValue is
* Investigate and ownerId is null or an empty string, CommandValue is Hold and
* onHoldReason is null, CommandValue is Resolve and resolutionCode is null,
* CommandValue is Cancel and cancellationCode is null, or note is null or an
* empty string.
* 
* @param c the command from the user
* @param ownerId user id of the incident owner
* @param onHoldReason the On Hold code
* @param resolutionCode the Resolution code
* @param cancellationCode the Cancellation code
* @param note String containing the work note
* @throws IllegalArgumentException if parameters are null or an empty string
*/
public Command(CommandValue c, String ownerId, OnHoldReason onHoldReason,
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the command value.
* 
* @return the command value
*/
public CommandValue getCommand() {

/**
* Returns the owner's Id.
* 
* @return the owner's Id
*/
public String getOwnerId() {

/**
* Returns the resolution code.
* 
* @return the resolution code
*/
public ResolutionCode getResolutionCode() {

/**
* Returns the work note.
* 
* @return the work note
*/
public String getWorkNote() {

/**
* Returns the on hold reason.
* 
* @return the on hold reason
*/
public OnHoldReason getOnHoldReason() {

/**
* Returns the cancellation code.
* 
* @return the cancellation code
*/
public CancellationCode getCancellationCode() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/IncidentState.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Interface for states in the Incident Manager State Pattern.  All 
* concrete incident states must implement the IncidentState interface.
* 
*/
public interface IncidentState {

/**
* Update the ManagedIncident based on the given Command.
* An UnsupportedOperationException is thrown if the CommandValue
* is not a valid action for the given state.  
* @param command Command describing the action that will update the ManagedIncident's
* state.
* @throws UnsupportedOperationException if the CommandValue is not a valid action
* for the given state.
*/
/**
* Returns the name of the current state as a String.
* @return the name of the current state as a String.
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/incident/ManagedIncident.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Concrete class representing the State Pattern context class. A
* ManagedIncident keeps track of all incident information including the current
* state. The state is updated when a Command encapsulating a transition is
* given to the ManagedIncident.
* 
*/
public class ManagedIncident {

/** Represents the five possible categories of incidents */
public enum Category { INQUIRY, SOFTWARE, HARDWARE, NETWORK, DATABASE }
/** Represents the four possible priorities for ManagedIncidents */
public enum Priority { URGENT, HIGH, MEDIUM, LOW }
/** String value of Category - Inquiry */
public static final String C_INQUIRY = "Inquiry";

/** String value of Category - Software */
public static final String C_SOFTWARE = "Software";

/** String value of Category - Hardware */
public static final String C_HARDWARE = "Hardware";

/** String value of Category - Network */
public static final String C_NETWORK = "Network";

/** String value of Category - Database */
public static final String C_DATABASE = "Database";

/** String value of Priority - Urgent */
public static final String P_URGENT = "Urgent";

/** String value of Priority - High */
public static final String P_HIGH = "High";

/** String value of Priority - Medium */
public static final String P_MEDIUM = "Medium";

/** String value of Priority - Low */
public static final String P_LOW = "Low";

/** Unique id for an incident */
private int incidentId;

/** User id of person who reported the incident */
private String caller;

/** User id of the incident owner */
private String owner;

/** The incidents name */
private String name;

/** Change request information for the incident */
private String changeRequest;

/** An ArrayList of notes */
private ArrayList<String> notes;

/** The current state */
private IncidentState state;

/** Category of the incident */
private Category category;

/** The Priority of the incident */
private Priority priority;

/** The On Hold Reason */
private OnHoldReason onHoldReason;

/** The Resolution Code */
private ResolutionCode resolutionCode;

/** The Cancellation Code */
private CancellationCode cancellationCode;

/** Final instance of the NewState inner class */
private final IncidentState newState = new NewState();

/** Final instance of the InProgressState inner class */
private final IncidentState inProgressState = new InProgressState();

/** Final instance of the OnHoldState inner class. */
private final IncidentState onHoldState = new OnHoldState();

/** Final instance of the ResolvedState inner class */
private final IncidentState resolvedState = new ResolvedState();

/** Final instance of the ClosedState inner class */
private final IncidentState closedState = new ClosedState();

/** Final instance of the CanceledState inner class */
private final IncidentState canceledState = new CanceledState();

/** String value of the new state */
public static final String NEW_NAME = "New";

/** String value of the in progress state */
public static final String IN_PROGRESS_NAME = "In Progress";

/** String value of the on hold state */
public static final String ON_HOLD_NAME = "On Hold";

/** String value of the resolved state */
public static final String RESOLVED_NAME = "Resolved";

/** String value of the closed state */
public static final String CLOSED_NAME = "Closed";

/** String value of the canceled state */
public static final String CANCELED_NAME = "Canceled";

/** String value for the line between work notes */
public static final String LINE = "\n-------\n";

/** Static field that keeps track of the id value */
private static int counter = 0;

/**
* Constructs a ManagedIncident from the provided parameters. If any of the
* parameters are null or empty strings (if a String type), then an
* IllegalArgumentException is thrown.
* 
* @param caller caller's id
* @param category the category
* @param priority the priority
* @param name the incident's name
* @param workNote the work note
* @throws IllegalArgumentException if any parameter is null or an empty string
*/
public ManagedIncident(String caller, Category category, Priority priority, String name,
 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* The fields of the ManagedIncident are set to the values from the Incident. If
* any of the required fields are null or empty strings (if a String type), then
* an IllegalArgumentException is thrown.
* 
* @param i the Incident to convert
* @throws IllegalArgumentException if any required field is null or an empty
*         string
*/
public ManagedIncident(Incident i) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Static method to increment the counter.
*/
public static void incrementCounter() {

/**
* Returns the incident's id.
* 
* @return the incidentId
*/
public int getIncidentId() {

/**
* Returns the change request information for the incident.
* 
* @return the changeRequest
*/
public String getChangeRequest() {

/**
* Returns the incident's category.
* 
* @return the category
*/
public Category getCategory() {

/**
* Returns the incident's category as a String.
* 
* @return the category String
*/
public String getCategoryString() {

/**
* Sets the incident's category from the given String. If the string cannot be
* converted to an appropriate category type, then an IllegalArgumentException
* is thrown.
* 
* @param categoryStr the String value of the category to set
* @throws IllegalArgumentException if string cannot be converted to an
*         appropriate category type
*/
private void setCategory(String categoryStr) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the incident's priority as a String.
*
* @return the priority String
*/
public String getPriorityString() {

/**
* Sets the incident's priority from the given String. If the string cannot be
* converted to an appropriate priority type, then an IllegalArgumentException
* is thrown.
* 
* @param priorityStr the String value of the priority to set
* @throws IllegalArgumentException if string cannot be converted to an
*         appropriate priority type
*/
private void setPriority(String priorityStr) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the on hold reason as a String.
* 
* @return the onHoldReason
*/
public String getOnHoldReasonString() {

/**
* Sets the on hold reason from the given String. If the string cannot be
* converted to an appropriate on hold reason, then an IllegalArgumentException
* is thrown.
* 
* @param onHoldStr the String value of the onHoldReason to set
* @throws IllegalArgumentException if string cannot be converted to an
*         appropriate on hold reason
*/
private void setOnHoldReason(String onHoldStr) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the cancellation code as a String.
* 
* @return the cancellationCode
*/
public String getCancellationCodeString() {

/**
* Sets the cancellation code from the given String. If the string cannot be
* converted to an appropriate cancellation code, then an
* IllegalArgumentException is thrown.
* 
* @param cancellationStr the String value of the cancellationCode to set
* @throws IllegalArgumentException if string cannot be converted to an
*         appropriate cancellation code
*/
private void setCancellationCode(String cancellationStr) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the incident's state.
* 
* @return the state
*/
public IncidentState getState() {

/**
* Sets the incident's state from the given String. If the string cannot be
* converted to an appropriate state type, then an IllegalArgumentException is
* thrown.
* 
* @param stateStr the String value of the state to set
* @throws IllegalArgumentException if string cannot be converted to an
*         appropriate state type
*/
private void setState(String stateStr) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the resolution code.
* 
* @return the resolutionCode
*/
public ResolutionCode getResolutionCode() {

/**
* Returns the resolution code as a String.
* 
* @return the resolutionCode
*/
public String getResolutionCodeString() {

/**
* Sets the resolution code from the given String. If the string cannot be
* converted to an appropriate resolution code, then an IllegalArgumentException
* is thrown.
* 
* @param resolutionStr the String value of the resolutionCode to set
* @throws IllegalArgumentException if string cannot be converted to an
*         appropriate resolution code
*/
private void setResolutionCode(String resolutionStr) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the owner's id.
* 
* @return the owner
*/
public String getOwner() {

/**
* Returns the incident's name.
* 
* @return the name
*/
public String getName() {

/**
* Returns the caller's id.
* 
* @return the caller
*/
public String getCaller() {

/**
* Returns the ArrayList of work notes.
* 
* @return the notes
*/
public ArrayList<String> getNotes() {

/**
* Returns the list of work notes as a String.
* 
* @return the notes
*/
public String getNotesString() {

/**
* Drives the finite state machine by delegating the Command to the current
* state and if successful adding non-null notes to the notes list. Throws an
* UnsupportedOperationException if the current state determines that the
* current transition is not appropriate for the FSM.
* 
* @param c the command to process
*/
public void update(Command c) {

/**
* Returns the ManagedIncident as an Incident object for writing to a file.
* 
* @return the Incident object
*/
public Incident getXMLIncident() {

/**
* Sets the counter for ManagedIncident id values.
* 
* @param counter the counter to set
*/
public static void setCounter(int counter) {

/**
* Concrete class that represents the new state of the Incident Management FSM.
*/
private class NewState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command Command describing the action that will update the
*        ManagedIncident's state.
* @throws UnsupportedOperationException if the CommandValue is not a valid
*         action for the given state.
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
public String getStateName() {

/**
* Concrete class that represents the in progress state of the Incident
* Management FSM.
*/
private class InProgressState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command Command describing the action that will update the
*        ManagedIncident's state.
* @throws UnsupportedOperationException if the CommandValue is not a valid
*         action for the given state.
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
public String getStateName() {

/**
* Concrete class that represents the on hold state of the Incident Management
* FSM.
*/
private class OnHoldState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command Command describing the action that will update the
*        ManagedIncident's state.
* @throws UnsupportedOperationException if the CommandValue is not a valid
*         action for the given state.
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
public String getStateName() {

/**
* Concrete class that represents the resolved state of the Incident Management
* FSM.
*/
private class ResolvedState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command Command describing the action that will update the
*        ManagedIncident's state.
* @throws UnsupportedOperationException if the CommandValue is not a valid
*         action for the given state.
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
public String getStateName() {

/**
* Concrete class that represents the closed state of the Incident Management
* FSM.
*/
private class ClosedState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command Command describing the action that will update the
*        ManagedIncident's state.
* @throws UnsupportedOperationException if the CommandValue is not a valid
*         action for the given state.
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
public String getStateName() {

/**
* Concrete class that represents the canceled state of the Incident Management
* FSM.
*/
private class CanceledState implements IncidentState {

/**
* Update the ManagedIncident based on the given Command. An
* UnsupportedOperationException is thrown if the CommandValue is not a valid
* action for the given state.
* 
* @param command Command describing the action that will update the
*        ManagedIncident's state.
* @throws UnsupportedOperationException if the CommandValue is not a valid
*         action for the given state.
*/
public void updateState(Command command) {

 - Throws: UnsupportedOperationException

/**
* Returns the name of the current state as a String.
* 
* @return the name of the current state as a String.
*/
public String getStateName() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/IncidentManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Concrete class that maintains the ManagedIncidentList and handles Commands
* from the GUI.
* 
*/
public class IncidentManager {

/** The singleton instance of the Incident Manager */
private static IncidentManager singleton;

/** The Managed Incident List */
private ManagedIncidentList incidentList;

/**
* Private instance constructor of the Incident Manager. Initializes with an
* empty ManagedIncidentList.
*/
private IncidentManager() {

/**
* Creates the single instance of the Incident Manager, if one doesnt yet
* exist. If it does exist, the method returns that instance.
* 
* @return the single instance of the Incident Manager
*/
public static IncidentManager getInstance() {

/**
* Saves all managed incidents in the list to the given file. Any
* IncidentIOExceptions are caught and an IllegalArgumentException is thrown.
* 
* @param fileName name of file to save the incident list to
* @throws IllegalArgumentException if an IncidentIOException is caught
*/
public void saveManagedIncidentsToFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Loads managed incidents into the list from the given file. Any
* IncidentIOExceptions are caught and an IllegalArgumentException is thrown.
* 
* @param fileName file containing list of ManagedIncidents
* @throws IllegalArgumentException if an IncidentIOException is caught
*/
public void loadManagedIncidentsFromFile(String fileName) {

 - Throws: IllegalArgumentException

/**
* Creates an empty ManagedIncidentList.
*/
public void createNewManagedIncidentList() {

/**
* Returns the list of Managed Incidents as an 2D Array. The array should have 1
* row for every ManagedIncident in the list and there should be 5 columns: Id
* number, Category, State name, Priority, and Name.
* 
* @return 2D Array of the ManagedIncidentList
*/
public String[][] getManagedIncidentsAsArray() {

/**
* Returns the list of Managed Incidents as a 2D Array filtered by Category. The
* array should have 1 row for every ManagedIncident in the Category and there
* should be 5 columns: Id number, Category, State name, Priority, and Name. If
* the Category parameter is null, an IllegalArgumentException is thrown.
* 
* @param category the Category to filter by
* @return 2D Array of the ManagedIncidentList filtered by Category
* @throws IllegalArgumentException if Category parameter is null
*/
public String[][] getManagedIncidentsAsArrayByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Returns the ManagedIncident with the given id in the incident list. If the
* incident id is not in the list, returns null.
* 
* @param incidentId the id of the ManagedIncident to find
* @return the ManagedIncident with the given id
*/
public ManagedIncident getManagedIncidentById(int incidentId) {

/**
* Updates the ManagedIncident with the given id through execution of a Command.
* 
* @param incidentId the id of the ManagedIncident to update
* @param command the command to execute
*/
public void executeCommand(int incidentId, Command command) {

/**
* Removes the ManagedIncident with the given id from the list.
* 
* @param incidentId the id of the ManagedIncident to remove
*/
public void deleteManagedIncidentById(int incidentId) {

/**
* Add a ManagedIncident with the given parameters to the list.
* 
* @param caller the caller's id
* @param category the incident's category
* @param priority the incident's priority
* @param name the incident's name
* @param workNote the work note
*/
public void addManagedIncidentToList(String caller, Category category, Priority priority,
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/incident_management/model/manager/ManagedIncidentList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Concrete class that maintains a current list of ManagedIncidents in the
* Incident Management system.
* 
*/
public class ManagedIncidentList {

/** A List of ManagedIncidents */
private List<ManagedIncident> incidents;

/**
* Constructs a new ManagedIncidentList and resets the ManagedIncidents counter
* to 0.
*/
public ManagedIncidentList() {

/**
* Add a ManagedIncident to the list and return its incident id number.
* 
* @param caller caller's id
* @param category the category
* @param priority the priority
* @param name the incident's name
* @param workNote the work note
* @return the incidentId of the ManagedIncident
*/
public int addIncident(String caller, Category category, Priority priority, String name,
/**
* Adds a List of Incidents from the XML library to the list and sets the
* ManagedIncidents counter to the maxId in the list of XML incidents plus 1.
* 
* @param incidentList the XML Incidents to add
*/
public void addXMLIncidents(List<Incident> incidentList) {

/**
* Returns the entire list of ManagedIncidents.
* 
* @return a List of ManagedIncidents
*/
public List<ManagedIncident> getManagedIncidents() {

/**
* Returns the sublist of ManagedIncidents filtered by Category. If the Category
* parameter is null, an IllegalArgumentException is thrown.
* 
* @param category the Category to filter
* @return a List of ManagedIncidents filtered by Category
* @throws IllegalArgumentException if Category parameter is null
*/
public List<ManagedIncident> getIncidentsByCategory(Category category) {

 - Throws: IllegalArgumentException

/**
* Returns the ManagedIncident with the given id in the incident list. If the
* incident id is not in the list, returns null.
* 
* @param incidentId the id of the ManagedIncident to find
* @return the ManagedIncident with the given id
*/
public ManagedIncident getIncidentById(int incidentId) {

/**
* Updates the ManagedIncident with the given id through execution of a Command.
* 
* @param incidentId the id of the ManagedIncident to update
* @param command the command to execute
*/
public void executeCommand(int incidentId, Command command) {

/**
* Removes the ManagedIncident with the given id from the list.
* 
* @param incidentId the id of the ManagedIncident to remove
*/
public void deleteIncidentById(int incidentId) {


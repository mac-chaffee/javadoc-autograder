---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* WolfResultsManager manages all of the Races and RaceLists for the program.
*
*/
public class WolfResultsManager extends Observable implements Observer {

/** Name of the file */
private String fileName;

/** Boolean to whether the data has been changed */
private boolean changed;

/** The single instance of WolfResultsManager that should exist */
private static WolfResultsManager singleton = new WolfResultsManager();

/** RaceList of races */
private RaceList list;

/**
* Constructor for WolfResultsManager
*/
private WolfResultsManager() {

/**
* Always returns the same instance of WolfResultsManager. Only one instance should ever exist.
* @return Instance of WolfResultsManager
*/
public static WolfResultsManager getInstance() {

/**
* Creates a new RaceList
*/
public void newList() {

/**
* Returns the boolean value of the changed class variable
* True if the data has been changed, false if it has not
* @return True if the data has been changed, false if it has not
*/
public boolean isChanged() {

/**
* Sets the boolean value of the class variable changed to the passed in boolean
* @param changed What this.changed is being set to
* @return New changed value
*/
private boolean setChanged(boolean changed) {

/**
* Returns the current fileName for WolfResultsManager
* @return String that is the file name
*/
public String getFilename() {

/**
* Sets the file name for WolfResultsManager
* @param fileName Name that the class fileName is being set to
*/
public void setFilename(String fileName) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Loads the file of the passed in fileName if the file exists
* Throws an exception if the file does not exist
* @param fileName Name of file being loaded
*/
public void loadFile(String fileName) {

/**
* Saves the current contents of WolfResultsManager to the passed in file name
* @param fileName Name that the file is being saved to
* @throws FileNotFoundException If fileName does not exist as a file
*/
public void saveFile(String fileName) throws FileNotFoundException {

/**
* Returns the RaceList stored by WolfResultsManager
* @return RaceList stored as class variable
*/
public RaceList getRaceList() {

/**
* Updates the observer
* @param observable Observable that the Observer belongs to
* @param object Object being observed
*/
public void update(Observable observable, Object object) {

if(object.getClass().equals(RaceList.class)) {

/**
* Adds observer to the object.
* @param o The observer to add.
*/
@Override
public synchronized void addObserver(Observer o) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reads files for valid RaceLists
*
*/
public class WolfResultsReader {

/**
* Reads a file and returns a RaceList of the given file of all the valid
* Races in the file
* @param fileName File being read for RaceLists
* @return RaceList of races from the file that was read
* @throws IllegalArgumentException If fileName does not exist as a file then the exception is thrown
*/
@SuppressWarnings("resource")
public static RaceList readRaceListFile(String fileName) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Writes RaceList to file
*
*/
public class WolfResultsWriter {

/**
* Writes the RaceList to the given file name
* @param fileName File that the RaceList is being saved to
* @param list RaceList that is being written to a file
* @throws IllegalArgumentException If fileName does not exist as a file
*/
public static void writeRaceFile(String fileName, RaceList list) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class is a representation of one persons race result for a single race in the WolfResultsManager application. 
*/
public class IndividualResult  extends Observable implements Observer, Comparable<IndividualResult> {

/** The name of the individual who was racing. */
private String name;

/** The age of the individual who was racing. */
private int age;

/** The time for the individual race result. */
private RaceTime time;

/** The pace for the individual race result. */
private RaceTime pace;

/** The race that results are being recorded for. */
private Race race;

/** The observer for this result */
private Observer observer;

/**
* The constructor for the Individual results for the race.
* @param race The race that is being run.
* @param name The name of the racer.
* @param age The age of the racer.
* @param time The time the racer recorded for the race.
*/
public IndividualResult( Race race, String name, int age, RaceTime time ) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Get's the name of the racer.
* @return The name
*/
public String getName() {

/**
* Get's the age of the racer.
* @return The age.
*/
public int getAge() {

/**
* Get's the time for the racer for this race.
* @return The time.
*/
public RaceTime getTime() {

/**
* Get's the race.
* @return The race.
*/
public Race getRace() {

/**
* Get's the runner's pace.
* @return The pace.
*/
public RaceTime getPace() {

/** 
* Converts the IndividualResults object to a String.
* @return The object in string.
*/
@Override
public String toString() {

/**
* Provides link for updating the object based off input from the gui.
* @param observed Used to notify this class when there has been a change in the gui.
* @param object The change that has been made.
*/
public void update( Observable observed, Object object ) {

/**
* Adds observer to the object.
* @param o The observer to add.
*/
@Override
public synchronized void addObserver(Observer o) {

/** 
* Compares two race times to see if they are greater than, less than or equal to one another.
* @param arg The IndividualResult object to be compared.
* @return Is less then 0 if time is less then this object, 
* greater then 0 if time is greater then this object, 
* and 0 if they are equal;

*/
@Override
public int compareTo(IndividualResult arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* This class handles the race objects.
*/
public class Race extends Observable {

/** The name of the race. */
private String name;

/** The distanced for the race. */
private double distance;

/** The date for the race. */
private LocalDate date;

/** The location for the race. */
private String location;

/** The racers list for this race. */
private RaceResultList list;

/**
* The constructor for the race object.
* @param name The name of the race.
* @param distance The distance of the race.
* @param date The date of the race.
* @param location The location of the race.
* @param list The results for the race.
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList list) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* The constructor for the race object.
* @param name The name of the race.
* @param distance The distance of the race.
* @param date The date of the race.
* @param location The location of the race.
*/
public Race(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Generates the hash code for the object.
* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

/** Checks the objects for equality.
* @param obj the object to check against.
* @return Returns true if objects are equal, and false if not.
*/
@Override
public boolean equals(Object obj) {

/**
* Get's the name of the race.
* @return The name of the race.
*/
public String getName() {

/**
* Get's the distance for the race.
* @return The distance of the race.
*/
public double getDistance() {

/**
* Set's the distance for the race.
* @param distance The distance of the race.
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* The date of the race.
* @return The date for the race.
*/
public LocalDate getDate() {

/**
* The location of the race.
* @return The location of race.
*/
public String getLocation() {

/**
* Get's the results of the race.
* @return The results.
*/
public RaceResultList getResults() {

/** 
* Adds a racers results to the race.
* @param result The individual results to add to the race.
*/
public void addIndividualResult(IndividualResult result) {

/** 
* Filters the race based on filters selected.
* @param minAge The minimum age to filter.
* @param maxAge The maximum age to filter.
* @param minPace The minimum pace to filter.
* @param maxPace The maximum pace to filter.
* @return The filtered RaceResult list.
*/
public RaceResultList filter( int minAge, int maxAge, String minPace, String maxPace) {

/** 
* Converts the Race object to a String.
* @return The object in string form
*/
@Override
public String toString() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceList uses a custom ArrayList to store Races. RaceList is used to manipulate the Races stored in the ArrayList.
*
*/
public class RaceList extends Observable implements Observer {

/** ArrayList of Races */
private ArrayList race;

/**
* Constructor for RaceList that constructs the ArrayList
*/
public RaceList() {

/**
* Adds race to the RaceList
* Throws IllegalArgumentException if race is null
* @param race Race that is being added
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

/**
* Creates and adds the Race to RaceList. 
* Throws an IllegalArgumentException if the Race constructor throws an exception
* @param name Name of the race
* @param distance Distance of the race
* @param date Date of the race
* @param location Location of the race
*/
public void addRace(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* Removes the race at the passed in index
* @param index Index of race being removed
*/
public void removeRace(int index) {

/** 
* Gets the race at the passed in index and returns it
* @param index Index of the race being returned
* @return Race at the passed in index
*/
public Race getRace(int index) {

/**
* Returns an int for the size of the RaceList
* @return Size of RaceList as an int
*/
public int size() {

/**
* Updates the observer
* @param obs Observable that the Observer belongs to
* @param obj Object being observed
*/
public void update(Observable obs, Object obj) {

if (obj.getClass().equals(Race.class)) {

/**
* Adds a Race to a given index in the race ArrayList 
* @param index index the race is being added to
* @param race race being added to the ArrayList
*/
public void addRace(int index, Race race) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A manageble list for individual race results.
*
*/
public class RaceResultList {

/** Holds the list to use. */
private SortedLinkedList<IndividualResult> results;

/**
* The constructor for the Race Result List.
*/
public RaceResultList() {

/**
* Adds the given Result to the list.
* @param result The racers results to add to the list.
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

/**
* Adds the given Result to the list.
* @param race The result will be added to.
* @param name The name of the racer.
* @param age The age of the racer.	 
* @param time The racers time in the race.
*/
public void addResult(Race race, String name, int age, RaceTime time) {

/**
* Adds the given Result to the list.
* @param idx The racers results to add to the list.
*/
public void remove(int idx) {

 - Throws: IndexOutOfBoundsException

/**
* Get's the result at the given index.
* @param index The index of the result to be retrieved.
* @return The IndividualResult object.
*/
public IndividualResult getResult(int index) {

 - Throws: IndexOutOfBoundsException

/**
* The size of the results list.
* @return The count of racers in the results.
*/
public int size() {

/**
* Returns the results list as an array.
* @return The double array of results.
*/
public String[][] getResultsAsArray() {

/** 
* Filters the race results based on filters selected.
* @param minAge The minumum age to filter.
* @param maxAge The maximum age to filter.
* @param minPace The minimum pace to filter.
* @param maxPace The maximum pace to filter.
* @return The filtered RaceResult list.
*/
public RaceResultList filter( int minAge, int maxAge, String minPace, String maxPace) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Custom ArrayList for RaceList
* 
* 
*/
public class ArrayList {

/** serialVersionUID for the ArrayList */
private static final long serialVersionUID = 0;

/** Class constant for RESIZE */
private static final int RESIZE = 10;

/** Array of Objects that make up the ArrayList */
private Object[] list;

/** Size of the ArrayList */
private int size;

/**
* Constructor for ArrayList
*/
public ArrayList() {

/**
* Constructor for ArrayList when an int is passed in
* @param length Length of the ArrayList
* @throws IllegalArgumentException Length cannot be 0
*/
public ArrayList(int length) {

 - Throws: IllegalArgumentException

/**
* Adds the passed in object to the end of the list
* @param o Object being added to the list
* @return true if it was added
* @throws IllegalArgumentException if the added object already exists in the arrays
*/
public boolean add(Object o) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Adds the passed in object to the list at the passed in index
* @param index index that 
* @param o object being added
*/
public void add(int index, Object o) {

 - Throws: NullPointerException

 - Throws: IndexOutOfBoundsException

 - Throws: IllegalArgumentException

/**
* Checks if the ArrayList containss the passed in object and returns a boolean value.
* @param o Object being searched for
* @return True if the object is in the ArrayList, false if it is not
*/
public boolean contains(Object o) {

/**
* Gets the object at the passed in index
* @param index Index of the object that will be returned
* @return Object at the passed in index
*/
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the index of the passed in object
* @param o Object that the index of is being retrieved
* @return Index of the passed in object
*/
public int indexOf(Object o) {

/**
* Checks to see if the ArrayList is empty. Returns true if it is.
* @return True if the ArrayList is empty, false if it is not
*/
public boolean isEmpty() {

/**
* Removes the object at the passed in index and returns the object
* @param index Index of the object being removed
* @return Object that was removed
* @throws IndexOutOfBoundsException Cannot remove from an empty array
*/
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

/**
* Returns the size of the ArrayList
* @return Size of the ArrayList
*/
public int size() {

/**
* Increases the size of the ArrayList by the RESIZE class constant value
*/
private void resize() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
*  Custon list interface
*
*/
public interface List {

/**
* Returns the size of the list
* @return size of the list
*/
/**
* Returns a boolean on whether or not the list is empty
* @return True if the list is empty, false if it is not
*/
/**
* Checks to see if the list contains the passed in object
* @param o Object being checked for in the list
* @return True if the object exists in the list, false if it does not
*/
/**
* Adds the passed in object to the list at the end
* @param o Object being added to the list
* @return True if the object was added to the list, false if it was not
*/
/**
* Gets the object at the passed in int index
* @param index Index the object is being retrieved from
* @return Object at the passed in index
*/
/**
* Adds the passed in object at the index that is passed in
* @param index Index that the object is being added at
* @param o Object that is being added at the passed in index
*/
/**
* Returns the index of the desired object if it is in the list
* @param o Object that is being looked for
* @return Index of the object as an int
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
*  A class that represents a race time hh:mm:ss.
*/
public class RaceTime {

/** Holds the time's hours. */
private int hours;

/** Holds the time's minutes. */
private int minutes;

/** Holds the time's seconds. */
private int seconds;

/**
* The constructor for the race time.
* @param hours The hours portion of the time.
* @param minutes The minutes portion of the time.
* @param seconds The seconds portion of the time. 
*/
public RaceTime( int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* The constructor for provided string.
* @param time The String version of the race time to be converted to RaceTime object.
*/
public RaceTime( String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Get's the hours for the RaceTime object.
* @return the hours for the race time.
*/
public int getHours() {

/**
* Get's the minutes for the RaceTime object.
* @return the minutes for the race time.
*/
public int getMinutes() {

/**
* Get's the seconds for the RaceTime object.
* @return the seconds for the race time.
*/
public int getSeconds() {

/**
* Get's the time in seconds
* @return the seconds for the race time.
*/
public int getTimeInSeconds() {

/** 
* Converts the RaceTime object to a String.
* @return The object in string form of HH:MM:SS
*/
@Override
public String toString() {

/** 
* Compares two race times to see if they are greater than, less than or equal to one another.
* @param time The RaceTime object to be compared.
* @return Is less then 0 if time is less then this object, 
* greater then 0 if time is greater then this object, 
* and 0 if they are equal;

*/
public int compareTo( RaceTime time ) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents a custom Linked List to be used for the race results.
* @param <E> element type that will be in the array list
*/
public class SortedLinkedList<E extends Comparable<E>>implements SortedList<E> {

/** The size of the list */
private int size;

/** Front instance of list node */
private Node front;

/** Back instance of list node */
private Node back;

/**
* Constructs the linked list
* and sets the capacity to the given parameter
* @throws IllegalArgumentException if the capacity is less than zero
*/
public SortedLinkedList() {

/**
* Returns the element at the given index
* @param i the index of the element to return
* @return the element at the given index
* @throws IndexOutOfBoundsException if the index is
* not valid for this list
*/
public E get(int i) {

 - Throws: IndexOutOfBoundsException

/**
* Adds the passed in object to the list at the end
* @param o Object being added to the list
* @return True if the object was added to the list, false if it was not
*/
@Override
public boolean add(Object o) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

/**
* Removes the element at the given index from the list
* and decreases the size of the list by one
* @param i the index of the element to remove
* @return the element that was removed
* @throws IndexOutOfBoundsException if the index is not valid for this list
*/
public E remove(int i) throws IndexOutOfBoundsException {

 - Throws: IndexOutOfBoundsException

 - Throws: IndexOutOfBoundsException

/**
* Returns the size of the list
* @return the size of the list
*/
public int size() {

/**
* Returns a boolean on whether or not the list is empty
* @return True if the list is empty, false if it is not
*/
public boolean isEmpty() {

/**
* Checks to see if the list contains the passed in object
* @param o Object being checked for in the list
* @return True if the object exists in the list, false if it does not
*/
public boolean contains(Object o) {

/**
* Returns the index of the desired object if it is in the list
* @param o Object that is being looked for
* @return Index of the object as an integer.
*/
public int indexOf(Object o) {

* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

/** 
* Override for SortedLinkedList toString method
*/
@Override
public String toString() {

/**
* Represents a single list node in the linked list.
* 
*
*/
private class Node {

/** The data in this list node */
private E data;

/** The reference to the next node in the list */
private Node next;

/**
* Constructs a list node that will be the last node in the list,
* setting the next node to null
* @param data the data to set to this list node
*/
public Node(E data) {

/**
* Constructs a list node that is not the last node in the list,
* and sets the reference to the next node
* @param data the data to set to this list node
* @param next the next node to set
*/
public Node(E data, Node next) {

/**
* Checks the hash code for the object.
* @return The hash in integer form.
*/
@Override
public int hashCode() {

/** 
* Checks the equality of two objects.
* @param obj The object to compare against.
* @return True if objects are equal, and false if not.
*/
@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* The interface for sorting list.
* 
* @param <E> element type that will be in the array list
*/
public interface SortedList<E> {

/**
* Returns the size of the list
* @return size of the list
*/
/**
* Returns a boolean on whether or not the list is empty
* @return True if the list is empty, false if it is not
*/
/**
* Checks to see if the list contains the passed in object
* @param o Object being checked for in the list
* @return True if the object exists in the list, false if it does not
*/
/**
* Adds the passed in object to the list at the end
* @param o Object being added to the list
* @return True if the object was added to the list, false if it was not
*/
/**
* Gets the object at the passed in int index
* @param index Index the object is being retrieved from
* @return Object at the passed in index
*/
/**
* Returns the index of the desired object if it is in the list
* @param o Object that is being looked for
* @return Index of the object as an int
*/

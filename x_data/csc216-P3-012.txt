---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/manager/WolfResultsManager.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* WolfResultsManager class. Serves as the controller for the program.
* Observes RaceList, observed by the WolfResultsGUI.
* 
*
*/
public class WolfResultsManager extends Observable implements Observer {

private String filename;

private boolean changed;

private RaceList list;

private static WolfResultsManager instance = null;

/**
* Returns the WolfResultsManager singleton instance, if instance is null,
* generates a new instance.
* 
* @return instance of WolfResultsManager
*/
public static WolfResultsManager getInstance() {

/**
* Private WolfResultsManager constructor that generates a new list,
* and adds itself as an observer of the new list
*/
private WolfResultsManager() {

/**
* Generates a new list object. Adds this WolfResultsManager as an observer
* of the newly generated list. Notifies observers of the change.
*/
public void newList() {

/**
* Returns boolean value stored in changed
* 
* @return true if has been changed, false if not changed
*/
public boolean isChanged() {

/**
* Sets the changed value based off the passed boolean parameter
* 
* @param changed - boolean value to set
*/
public void setChanged(boolean changed) {

/**
* Returns the filename
* 
* @return filename of the WolfResults file
*/
public String getFilename() {

/**
* Sets the filename
* 
* @param filename - name of the WolfResults file
*/
public void setFilename(String filename) {

 - Throws: IllegalArgumentException

/**
* Saves the WolfResults file
* 
* @param filename - name of the WolfResults file to save
*/
public void saveFile(String filename) {

/**
* Loads the WolfResults file
* 
* @param filename - name of the WolfResults file to load
*/
public void loadFile(String filename) {

/**
* Returns the file's list of Races
* 
* @return list of Races from this file
*/
public RaceList getRaceList() {

@Override
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsReader.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* WolfResultsReader provides static functionality to read files containing data pertaining to WolfResults
* 
*/
public class WolfResultsReader {

/**
* Reads a race list from a file
* @param fileName the file name
* @return The race list from the file
*/
public static RaceList readRaceListFile(String fileName) {

 - Throws: IllegalArgumentException

private static void readWithScanner(Scanner scanner, RaceList result) {

 - Throws: RuntimeException

 - Throws: RuntimeException

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/model/io/WolfResultsWriter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* WolfResultsWriter provides static functionality to write files containing data pertaining to WolfResults
* 
*/
public class WolfResultsWriter {

private static PrintWriter writer;

/**
* Writes a race to a file
* @param fileName the file name to write to
* @param list The list data to write
*/
public static void writeRaceFile(String fileName, RaceList list) {

 - Throws: IllegalArgumentException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/IndividualResult.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* IndividualResult class, implements Observer and Comparable<IndividualResult>.
* Used to log the results of runners who completed the race.
* 
*/
public class IndividualResult implements Observer, Comparable<IndividualResult> {

private Race race;

private String name;

private int age;

private RaceTime time;

private RaceTime pace;

/**
* Constructs an IndividualResult object with a race, name of the runner, age of the
* runner, and the time of the result. Pace is calculated based off of the time
* and the race distance.
* 
* @param race - race the result is from
* @param name - name of the runner
* @param age - age of the runner
* @param time - time of the result
*/
public IndividualResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

private void updatePace(double distance) {

/**
* Returns the race that the result is from as a Race object
* 
* @return race that the result is from
*/
public Race getRace() {

/**
* Returns the name of the runner that made the result as a String
* 
* @return name of the runner
*/
public String getName() {

/**
* Returns the age of the runner that made the result as an int
* 
* @return age of the runner
*/
public int getAge() {

/**
* Returns the time of the result as RaceTime object
* 
* @return time of the result
*/
public RaceTime getTime() {

/**
* Returns the pace of the result as RaceTime object
* 
* @return pace of the result
*/
public RaceTime getPace() {

/**
* Compares of two results and returns the difference of their time
* 
* @param other - result to be compared against
* @return the difference of the results' times
*/
public int compareTo(IndividualResult other) {

@Override
public boolean equals(Object obj) {

@Override
public String toString() {

/**
* Returns the result as a 2D string array
* 
* @return the result object as a 2D string array
*/
public String[] getResultArray() {

@Override
public void update(Observable o, Object arg) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/Race.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Race class 
* 
*/
public class Race extends Observable {

private String name;

private double distance;

private LocalDate date;

private String location;

private RaceResultList results;

/**
* Constructs Race object with name, distance, date, and location
* 
* @param name - name of the Race
* @param distance - distance of the Race
* @param date - date of the Race
* @param location - location of the Race
*/
public Race(String name, double distance, LocalDate date, String location) {

/**
* Constructs Race object with name, distance, date, location, and results
* 
* @param name - name of the Race
* @param distance - distance of the Race
* @param date - date of the Race
* @param location - location of the Race
* @param results - list of the Race's results
*/
public Race(String name, double distance, LocalDate date, String location, RaceResultList results) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the Race's name
* 
* @return name of the Race
*/
public String getName() {

/**
* Returns the Race's distance
* 
* @return the distance
*/
public double getDistance() {

/**
* Returns the Race's date
* 
* @return the date
*/
public LocalDate getDate() {

/**
* Returns the Race's location
* 
* @return the location
*/
public String getLocation() {

/**
* Returns the Race's list of results
* 
* @return list of results
*/
public RaceResultList getResults() {

/**
* Adds an IndividualRaceResult to the Race's list of results
* 
* @param result - IndividualResult to be added
*/
public void addIndividualResult(IndividualResult result) {

/**
* Sets the distance of the Race
* 
* @param distance - distance to be set in miles
*/
public void setDistance(double distance) {

 - Throws: IllegalArgumentException

/**
* Returns a list of results that meet the filter parameters
* 
* @param minAge - minimum age
* @param maxAge - maximum age
* @param minPace - minimum pace
* @param maxPace - maximum pace
* @return RaceResultList with results that satisfy the filter parameters
*/
public RaceResultList filter(int minAge, int maxAge, String minPace, String maxPace) {

 - Throws: IllegalArgumentException

@Override
public String toString() {

@Override
public int hashCode() {

@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Race List class
* 
*/
public class RaceList extends Observable implements Observer {

private ArrayList races;

/**
* Constructs a new RaceList with zero races
*/
public RaceList() {

/**
* Adds a race to the races list
* 
* @param race The race to add
*/
public void addRace(Race race) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Adds a race by value rather than by reference
* 
* @param name     The name of the race
* @param distance The distance in the race
* @param date     The local date the race occurred
* @param location The location of the race
*/
public void addRace(String name, double distance, LocalDate date, String location) {

 - Throws: IllegalArgumentException

/**
* Removes a race by index
* 
* @param idx The index of the race to remove
*/
public void removeRace(int idx) {

/**
* Gets the race at the index
* 
* @param idx The index to look up
* @return The race at the index
* @throws IndexOutOfBoundsException Throws if no race exists at the index
*/
public Race getRace(int idx) {

/**
* Gets the size of the race list
* 
* @return The size of the race list
*/
public int size() {

@Override
public void update(Observable o, Object arg) {

@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/race_results/RaceResultList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceResultList is for storing and managing a list of race results
* 
*/
public class RaceResultList {

private SortedLinkedList<IndividualResult> results;

/**
* Constructs a new RaceResultsList
*/
public RaceResultList() {

/**
* Adds a RaceResult by reference
* @param result The result to add
*/
public void addResult(IndividualResult result) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Adds a race result by value
* 
* @param race The race object
* @param name The name of the race
* @param age The age
* @param time The time
*/
public void addResult(Race race, String name, int age, RaceTime time) {

 - Throws: IllegalArgumentException

/**
* Gets the individual result at the specified index
* 
* @param i The index
* @return The result
*/
public IndividualResult getResult(int i) {

/**
* Returns the size
* 
* @return The size
*/
public int size() {

/**
* Returns the results in string array representation
* @return Returns the results as a string array
*/
public String[][] getResultsAsArray() {

/**
* Returns list of results such that runners age is between minAge and maxAge
* (inclusive) and runners pace is between minPace and maxPace (inclusive).
* 
* @param ageMin  The minimum age
* @param ageMax  The maximum age
* @param paceMin The minimum pace
* @param paceMax The maximum pace
* @return Returns a list of results wherein the runner's age is between minAge
*         and maxAge, and the pace is between minPace and maxPace
*/
public RaceResultList filter(int ageMin, int ageMax, String paceMin, String paceMax) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/ArrayList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* ArrayList class, utilizes 2D array to structure objects. Implementes List.
* 
*/
public class ArrayList implements List {

@SuppressWarnings("unused")
private static final long serialVersionUID = 1L;

private static final int DEFAULT_INITIAL_CAPACITY = 100;

private Object[] list;

private int size;

/**
* Default constructor that constructs an ArrayList with capacity 100
*/
public ArrayList() {

/**
* ArrayList constructor that constructs an empty array and sets size to 0
* 
* @param cap - capacity of the ArrayList
*/
public ArrayList(int cap) {

 - Throws: IllegalArgumentException

@Override
public int size() {

@Override
public boolean isEmpty() {

@Override
public boolean contains(Object obj) {

@Override
public boolean add(Object obj) {

@Override
public Object get(int index) {

 - Throws: IndexOutOfBoundsException

@Override
public void add(int index, Object obj) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

 - Throws: IndexOutOfBoundsException

private void ensureCapacity(int capacity) {

private void grow() {

@Override
public Object remove(int index) {

 - Throws: IndexOutOfBoundsException

@Override
public int indexOf(Object obj) {

@Override
public boolean equals(Object obj) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/List.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and does not
* require implementation of generic parameterization.
* 
* This interface is adapted from java.util.List.
* 
*
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*         The List interface provides four methods for positional (indexed)
*         access to list elements. Lists (like Java arrays) are zero based.
*         Note that these operations may execute in time proportional to the
*         index value for some implementations (the LinkedList class, for
*         example). Thus, iterating over the elements in a list is typically
*         preferable to indexing through it if the caller does not know the
*         implementation.
*
*         Note: While it is permissible for lists to contain themselves as
*         elements, extreme caution is advised: the equals and hashCode methods
*         are no longer well defined on such a list.
*
*         Some list implementations have restrictions on the elements that they
*         may contain. For example, some implementations prohibit null
*         elements, and some have restrictions on the types of their elements.
*         Attempting to add an ineligible element throws an unchecked
*         exception, typically NullPointerException or ClassCastException.
*         Attempting to query the presence of an ineligible element may throw
*         an exception, or it may simply return false; some implementations
*         will exhibit the former behavior and some will exhibit the latter.
*         More generally, attempting an operation on an ineligible element
*         whose completion would not result in the insertion of an ineligible
*         element into the list may throw an exception or it may succeed, at
*         the option of the implementation. Such exceptions are marked as
*         "optional" in the specification for this interface.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
*/
public interface List {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element e
* such that (o==null ? e==null : o.equals(e)).
*
* @param o element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Appends the specified element to the end of this list (optional
* operation).
*
* Lists that support this operation may place limitations on what elements
* may be added to this list. In particular, some lists will refuse to add
* null elements, and others will impose restrictions on the type of
* elements that may be added. List classes should clearly specify in their
* documentation any restrictions on what elements may be added.
*
* @param o element to be appended to this list
* @return true (as specified by {@link Collection#add})
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Inserts the specified element at the specified position in this list
* (optional operation). Shifts the element currently at that position (if
* any) and any subsequent elements to the right (adds one to their
* indices).
*
* @param index index at which the specified element is to be inserted
* @param element element to be inserted
* @throws NullPointerException if the specified element is null and this
*             list does not permit null elements
* @throws IllegalArgumentException if some property of the specified
*             element prevents it from being added to this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index > size())
*/
/**
* Removes the element at the specified position in this list (optional
* operation). Shifts any subsequent elements to the left (subtracts one
* from their indices). Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param o element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/RaceTime.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* RaceTime class that manages and compares the time taken to run the race
* 
*/
public class RaceTime {

private int hours;

private int minutes;

private int seconds;

/**
* Constructs a RaceTime object from given time as hours, minutes and seconds
* 
* @param hours - number of hours
* @param minutes - number of minutes
* @param seconds - number of seconds
*/
public RaceTime(int hours, int minutes, int seconds) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Constructs a RaceTime object from given time as string
* 
* @param time - time given as a string
*/
public RaceTime(String time) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Returns the number of hours
* 
* @return number of hours
*/
public int getHours() {

/**
* Returns the number of minutes
* 
* @return number of minutes
*/
public int getMinutes() {

/**
* Returns the number of seconds
* 
* @return number of seconds
*/
public int getSeconds() {

/**
* Calculates and returns the time in seconds
* 
* @return the time in seconds
*/
public int getTimeInSeconds() {

@Override
public boolean equals(Object obj) {

@Override
public String toString() {

/**
* Compares the times in seconds of two RaceTime objects
* 
* @param time - given time object to be used for comparison
* @return difference between this RaceTime object and the one passed
* for comparison
*/
public int compareTo(RaceTime time) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedLinkedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* SortedLinkedList class, implements SortedList. 
* 
*
* @param <E>
*/
public class SortedLinkedList<E extends Comparable<E>> implements SortedList<E>  {

private Node head;

private Node tail;

private int size;

/**
* Constructs a empty SortedLinkedList of size 0
*/
public SortedLinkedList() {

@Override
public int size() {

@Override
public boolean isEmpty() {

@Override
public boolean contains(E data) {

@Override
public boolean add(E data) {

 - Throws: NullPointerException

 - Throws: IllegalArgumentException

@Override
public E get(int index) {

 - Throws: IndexOutOfBoundsException

@Override
public E remove(int index) {

 - Throws: IndexOutOfBoundsException

@Override
public int indexOf(E data) {

* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

@SuppressWarnings("rawtypes")
@Override
public String toString() {

/**
* Inner class that defines the SortedLinkedList's nodes
* 
*/
public class Node {

private E data;

private Node prev;

private Node next;

/**
* Constructor for SortedLinkedList's nodes
* 
* @param data - data held within the node
* @param next - pointer to the next node
*/
public Node(E data, Node next) {

@Override
public String toString() {

* @see java.lang.Object#hashCode()
*/
@Override
public int hashCode() {

* @see java.lang.Object#equals(java.lang.Object)
*/
@Override
public boolean equals(Object obj) {

@SuppressWarnings("unchecked")
//		private SortedLinkedList getOuterType() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/wolf_results/util/SortedList.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This interface is a subset of the java.util.List interface and requires that
* elements be stored in sorted order based on Comparable. No duplicate items.
* 
* This interface is adapted from java.util.List.
* 
* 
*
*         An ordered collection (also known as a sequence). The user of this
*         interface has precise control over where in the list each element is
*         inserted. The user can access elements by their integer index
*         (position in the list), and search for elements in the list.
*
*
* @see Collection
* @see Set
* @see ArrayList
* @see LinkedList
* @see Vector
* @see Arrays#asList(Object[])
* @see Collections#nCopies(int, Object)
* @see Collections#EMPTY_LIST
* @see AbstractList
* @see AbstractSequentialList
* @since 1.2
* @param <E> extends Comparable
*/
public interface SortedList<E extends Comparable<E>> {

/**
* Returns the number of elements in this list. If this list contains more
* than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
*
* @return the number of elements in this list
*/
/**
* Returns true if this list contains no elements.
*
* @return true if this list contains no elements
*/
/**
* Returns true if this list contains the specified element. More formally,
* returns true if and only if this list contains at least one element a
* such that (o==null ? a==null : o.equals(a)).
*
* @param e element whose presence in this list is to be tested
* @return true if this list contains the specified element
*/
/**
* Adds the specified element to list in sorted order
*
* @param e element to be appended to this list
* @return true (as specified by {@link Collection#add})
* @throws NullPointerException if e is null
* @throws IllegalArgumentException if list already contains e
*/
/**
* Returns the element at the specified position in this list.
*
* @param index index of the element to return
* @return the element at the specified position in this list
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Removes the element at the specified position in this list. Shifts any
* subsequent elements to the left (subtracts one from their indices).
* Returns the element that was removed from the list.
*
* @param index the index of the element to be removed
* @return the element previously at the specified position
* @throws IndexOutOfBoundsException if the index is out of range (index < 0
*             || index >= size())
*/
/**
* Returns the index of the first occurrence of the specified element in
* this list, or -1 if this list does not contain the element. More
* formally, returns the lowest index i such that (o==null ? get(i)==null :
* o.equals(get(i))), or -1 if there is no such index.
*
* @param e element to search for
* @return the index of the first occurrence of the specified element in
*         this list, or -1 if this list does not contain the element
*/

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements TransitGroup and represents passengers in the terminal who have not yet joined a security checkpoint line 
*
*/
public class PreSecurity implements TransitGroup {

/** Instance of the passenger queue */
/**
* Constructor method for the preSecurity Area
* Creates the specified number of passengers by successive calls Ticketing.generatePassenger(), adding each to its PassengerQueue
* @param numberOfPassengers The number of passengers to be created.
* @param logger Logs data of the passengers in the passengerQueue
* @throws IllegalArgumentException if there is an invalid number of passengers.
*/
public PreSecurity(int numberOfPassengers, Reporter logger) throws IllegalArgumentException{

 - Throws: IllegalArgumentException

/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
public int departTimeNext() {

/**
* Determines which passenger is the next to to go.
* @return the passenger who is next to go
*/
public Passenger nextToGo() {

/**
* Checks if the passengerQueue has a next passenger in it.
* @return if there is a next passenger
*/
public boolean hasNext() {

/**
* Removes the next passenger in the line
* @return Passenger which is removed.
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents a security checkpoint line and its line. Front passenger is undergoing the actual security process.
* It delegates strict queue activities to its instance variable line.
*
*/
public class CheckPoint {

/** The time that the last passenger in line will clear security and leave the line */
private int timeWhenAvailable;

private int sumProcessTime;

/**Instance variable of line */
private PassengerQueue line;

/**
* Constructor for the checkpoint
*/
public CheckPoint() {

/**
* Creates an empty line.  All passengers in the previous queue are removed.
*/
public void newCheckpoint() {

/**
* What is the size of the checkPoints passengerQueue line
* @return integer the size of the line
*/
public int size() {

/**
* Removes the passenger from the line they are in.
* @return Passenger the passenger that was removed.
*/
public Passenger removeFromLine() {

 - Throws: NoSuchElementException

/**
* Checks if the checkpoint as another person.
* @return boolean if there is another passenger
*/
public boolean hasNext() {

/**
* What is the time that the next passenger will be departing.
* Sum of the arrival time, wait time, and process time of the front passenger.
* @return integer of the time that the next passenger is to depart.
*/
public int departTimeNext() {

/**
* Finds the passenger who is next to be processed. 
* @return Passenger who is next to go
*/
public Passenger nextToGo() {

/**
* Adds the passenger passed in to the line.
* @param passenger the Passenger who is to be added to the line
*/
public void addToLine(Passenger passenger) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents the collection of security checkpoints and their waiting lines.
*
*/
public class SecurityArea implements TransitGroup {

/** Maximum number of checkpoints that the security area can have */
private static final int MAX_CHECKPOINTS  = 17;

/** Minimum number of checkpoints that the security area can have */
private static final int MIN_CHECKPOINTS = 3;

/** The string that should be shown if the user enters an invalid number of checkpoints */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** The string that should be shown for any attempt to add to a security checkpoint line with an improper index. */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** Largest index for lines reserved for Fast Track passengers */
private int largestFastIndex;

/**  Index of the line reserved for TSA PreCheck/Trusted Travelers */
private int tsaPreIndex;

/** ArrayList representing all of the checkpoints in the airport */
private ArrayList<CheckPoint> checkPointArray;

/**
* Constructor for the security area. Sets the index for the fastTrack and PreCheck gates.
* @param numberOfCheckPoints which is the number of security checkpoints.
*/
public SecurityArea(int numberOfCheckPoints) {

 - Throws: IllegalArgumentException

/**
* Helper method that finds out if the number of gates that the passenger requested is valid.
* @param number of gates that are being checked
* @return boolean if the number of gates are valid.
*/
private boolean numGatesOK(int number) {

/**
* Getter method for the largest FastIndex
* @return the index of the last FastIndex.
*/
public int getLargestFastIndex() {

/**
* Setter Method for the index of the largest FastTrack gate.
* @param largestFastIndex the index of the largest Fast Track gate to set
*/
public void setLargestFastIndex(int largestFastIndex) {

/**
* Getter method for the index of the PreCheck gate (Last gate and largest index).
* @return the tsaPreIndex
*/
public int getTsaPreIndex() {

/**
* Setter Method for the index of the PreCheck gate (Last gate and largest index).
* @param tsaPreIndex the index of the PreCheck Gate
*/
public void setTsaPreIndex(int tsaPreIndex) {

/**
* Adds the passenger to the security line with the index given by the first parameter.
* @param gateNumber The gate that the passenger should be added to.
* @param passenger The Passenger that should be added to the gate.
*/
public void addToLine(int gateNumber, Passenger passenger) {

 - Throws: IllegalArgumentException

/**
* Finds the shortest security line for ordinary passengers.
* @return Index of the shortest security line that ordinary passengers are permitted to select
*/
public int shortestRegularLine() {

/**
* Finds the shortest security line for FastTrack passengers.
* @return Index of the shortest security line that FastTrack passengers are permitted to select
*/
public int shortestFastTrackLine() {

/**
* Finds the shortest security line for PreCheck passengers.
* @return Index of the shortest security line that PreCheck passengers are permitted to select
*/
public int shortestTSAPreLine() {

/**
* Number of passengers in the line with the given index. Throws an IllegalArgumentException if the index is out of range
* @param gate the index of the gate to be checked
* @return integer the number of passengers in that line.
*/
public int lengthOfLine(int gate) {

 - Throws: IllegalArgumentException

/**
* When will the next passenger leave the security area?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
public int departTimeNext() {

/**
* Who will be the next passenger to leave the SecurityArea?
* @return the next passenger
*/
public Passenger nextToGo() {

/**
* Removes the next passenger to leave the security Area.
* @return the passenger who is removed
*/
public Passenger removeNext() {

/**
* Determines the shortest line in the range of gates given.
* @param rangeBegin Start index of the range of gates
* @param rangeEnd End index of the range of gates
* @return the index of the shortest line.
*/
private int shortestLineInRange(int rangeBegin, int rangeEnd) {

/**
* Determines which line is next to clear
* @return Index of the line determined
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* 
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A passenger that is part of the PackAir fast track program.
*
*/
public class FastTrackPassenger extends Passenger {

/** Maximum expected process time for the fast Track Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/** Minimum red value for the passenger */
private static final int RED_MINIMUM = 153;

/** Minimum green value for the passenger */
private static final int GREEN_MINIMUM = 153;

/** Minimum blue value for the passenger */
private static final int BLUE_MINIMUM = 255;

/** Maximum color that the passenger can reach as their wait time gets longer */
private static final Color MAX_COLOR = Color.BLUE;

/** Color of the passenger. Visualizes the time they have spent waiting */
private Color color;

/**
* Constructor for the FastTrack Passenger.
* @param arrivalTime the time that the passenger arrived.
* @param processTime the time that the passenger will spend processing at the checkpoint.
* @param myLog log for the passenger to put all of their data in for data processing
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter myLog) {

 - Throws: IllegalArgumentException

/**
* Selects appropriate checkpoint waiting line, sets lineIndex, and then places passenger in the selected line.
* @param securityAreaTransit the list of the checkpoints to choose from.
*/
@Override
public void getInLine(TransitGroup securityAreaTransit) {

* @see edu.ncsu.csc216.transit.airport.travelers.Passenger#getColor()
*/
@Override
public Color getColor() {

/**
* Determines which line the passenger can go into. FastTrack passengers can enter fast track checkpoints or ordinary checkpoints.
* @param securityArea passes in the security area list of all gates.
* @return integer index of the line to enter
*/
private int pickLine(SecurityArea securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A passenger that is not part of any kind of special program.
*
*/
public class OrdinaryPassenger extends Passenger {

/** Maximum expected process time for the TrustedTraveler Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/** Minimum red value for the passenger */
private static final int RED_MINIMUM = 255;

/** Minimum green value for the passenger */
private static final int GREEN_MINIMUM = 153;

/** Minimum blue value for the passenger */
private static final int BLUE_MINIMUM = 153;

/** Maximum color that the passenger can reach as their wait time gets longer */
private static final Color MAX_COLOR = Color.RED;

/** Color of the passenger. Visualizes the time they have spent waiting */
private Color color;

/**
* Constructor for the Ordinary Passenger.
* @param arrivalTime the time that the passenger arrived.
* @param processTime the time that the passenger will spend processing at the checkpoint.
* @param myLog log for the passenger to put all of their data in for data processing
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Selects appropriate checkpoint waiting line, sets lineIndex, and then places passenger in the selected line.
* @param securityAreaTransit the list of the checkpoints to choose from.
*/
@Override
public void getInLine(TransitGroup securityAreaTransit) {

@Override
public Color getColor() {

/**
* Determines which line the passenger can go into. Ordinary passengers can only go into ordinary checkPoint lines.
* @param transitGroup Passes in the transit group that they are part of.
* @return integer index of the line to enter
*/
private int pickLine(SecurityArea securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents a single passenger as they progress through the airport.
*
*/
public abstract class Passenger {

/** Minimum processing time */
public static final int MIN_PROCESS_TIME = 20;

/** The time that the passenger joins a security line */
private int arrivalTime;

/** Time the passenger will spends in a security line. Computed once they join a security line */
private int waitTime;

/** Amount of time that the passenger spends at the front of the security line undergoing check */
private int processTime;

/** Index of the security checkpoint line that the passenger selects and joins. Should be -1 before they join a line */
private int lineIndex = -1;

/** False until the passenger selects and joins a security line */
private boolean waitingProcessing = false;

/** Reporting mechanism, initialized by the Reporter parameter in the constructor.*/
private Log myLog;

/**
* Adds the passenger to the appropriate security line.
* @param securityAreaTransit Transit group representing the list of security lines in the airport.
*/
public abstract void getInLine(TransitGroup securityAreaTransit);

/** What is the color of the passenger. Determined by their kind and process times.
* @return the color of the passenger 
*/
public abstract Color getColor();

/**
* Constructor for the passenger. Will be called upon by the other types of passengers.
* @param arrivalTime Time the passenger joins the security line.
* @param processTime amount of time that the passenger spends at the front of the security line actually undergoing the security check 
* @param myLog reporter instance to put data into.
*/
public Passenger(int arrivalTime, int processTime, Reporter myLog) {

/**
* Getter method for the arrival time of the passenger.
* @return the arrival time.
*/
public int getArrivalTime() {

/**
* Setter method for the time the passenger joins a security line.
* @param arrivalTime the passengers arrival time.
*/
public void setArrivalTime(int arrivalTime) {

/**
* Getter method for the wait time of the passenger.
* @return the wait time.
*/
public int getWaitTime() {

/**
* Setter method for the wait time of the passenger.
* @param waitTime the new wait time to be set
*/
public void setWaitTime(int waitTime) {

/**
* Setter method for the amount of time that the passenger spends at the front of the security
* line undergoing the actual security check
* @param processTime Time spent in a security line.
*/
public void setProcessTime(int processTime) {

/**
* Getter method for the process time of the passenger.
* @return the process time.
*/
public int getProcessTime() {

/**
* Getter method for the line index of the passenger.
* @return the line index.
*/
public int getLineIndex() {

/**
* Setter method for the log
* @param myLog the myLog to set
*/
private void setMyLog(Log myLog) {

/**
* Getter method for determining if the passenger is waiting in a security line.
* @return boolean true if they are waiting, false if not.
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when they finish security checks.
* Update their own fields to show that they have left Security (which fields relate to security?).
*/
public void clearSecurity() {

/**
* Sets the line index
* @param lineIndex index of the line to be set for the passenger.
*/
protected void setLineIndex(int lineIndex) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. Throws NoSuchElementException if the queue is empty
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* A passenger who is part of the TSA Pre-Check/ Trusted Traveler program.
*
*/
public class TrustedTraveler extends Passenger {

/** Maximum expected process time for the TrustedTraveler Passenger */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/** Minimum red value for the passenger */
private static final int RED_MINIMUM = 153;

/** Minimum green value for the passenger */
private static final int GREEN_MINIMUM = 255;

/** Minimum blue value for the passenger */
private static final int BLUE_MINIMUM = 153;

/** Maximum color that the passenger can reach as their wait time gets longer */
private static final Color MAX_COLOR = Color.GREEN;

/** Color of the passenger. Visualizes the time they have spent waiting */
private Color color;

/**
* Constructor for the Trusted Passenger.
* @param arrivalTime the time that the passenger arrived.
* @param processTime the time that the passenger will spend processing at the checkpoint.
* @param myLog log for the passenger to put all of their data in for data processing
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter myLog) {

/**
* Selects appropriate checkpoint waiting line, sets lineIndex, and then places passenger in the selected line.
* @param securityAreaTransit the list of the checkpoints to choose from.
*/
@Override
public void getInLine(TransitGroup securityAreaTransit) {

* @see edu.ncsu.csc216.transit.airport.travelers.Passenger#getColor()
*/
@Override
public Color getColor() {

/**
* Picks which line the TrustedTraveler passenger should go to. The passenger chooses the TSA PreCheck security checkpoint unless
* its line is not empty and is more than twice as long as the shortest (ordinary) line.
* @param transitGroup Passes in the transit group that they are part of.
* @return integer index of the line to enter.
*/
private int pickLine(SecurityArea securityArea) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Represents the event calendar. Determines which passenger to process next.
*
*/
public class EventCalendar {

private TransitGroup highPriority;

private TransitGroup lowPriority;

/**
*  Takes two TransitGroups as parameters. The first parameter has priority over the second one:
*  highPriority is initialized with the first parameter and lowPriority is initialized with the second.
* @param highPriority the high priority transit group that will be selected in case of a tie.
* @param lowPriority the second transit group that is secondary to the high priority in case of a tie.
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Helper method that actually determines which passenger should be processed next.
* @return The passenger from highPriority and lowPriority who will be the next to exit the group/line theyre currently at the front of.
* In case of a tie for the next to exit, the passenger from highPriority is returned. If both TransitGroups are empty (their next passengers are null),
*  an IllegalStateException is thrown. 
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Logging mechanism for the simulation, with behaviors specified by the interface it implements, Reporter.
*
*/
public class Log implements Reporter {

/** Number of passenger who have completed all of their activities */
private int numCompleted;

/** Total summation of wait time for all passengers. */
private int totalWaitTime;

/** Total summation of the process time for all passengers. */
private Double totalProcessTime;

/**
* Constructor method for the log to be used throughout the simulation.
*/
public Log() {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
public int getNumCompleted() {

/**
* Logs the data for a passenger.
* @param passenger - passenger whose data is to be logged
*/
public void logData(Passenger passenger) {

/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
public double averageWaitTime() {

/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* 
*/
/**
* Implements the back end process for the user entering their parameters and creating the simulation.
*
*/
public class Simulator {

/**Total number of passengers that are to be created */
private int numPassengers;

/** The TransitGroup that represents passengers who have not yet entered a checkpoint security line.*/
private TransitGroup inTicketing;

/** The TransitGroup that represents passengers who are in a checkpoint security line.*/
private SecurityArea inSecurity;

/** The passenger most recently returned by myCalendar.nextToAct().*/
private Passenger currentPassenger = null;

/** Log for the entire simulation instance */
private Log log = null;

/** Event calendar for this simulation instance*/
private EventCalendar myCalendar;

/**
* Checks valid parameters passed in, and initializes log to record the passenger statistics.
* Sets the passenger type distribution for the Ticketing factory.
* Establishes the security area.
* Creates the PreSecurity area and the EventCalendar.
* @param numberOfCheckPoints The total number of checkpoints that need to be created
* @param numberOfPassengers The total number of passengers that need to be created
* @param percentTrusted Percent of the total passengers that are Trusted Travelers
* @param percentFastTrack Percent of the total passengers that are Fast Track
* @param percentOrdinary Percent of the total passengers that are ordinary.
*/
public Simulator(int numberOfCheckPoints, int numberOfPassengers, int percentTrusted, int percentFastTrack, int percentOrdinary) {

/**
* Checks the parameters to make sure they are valid, throwing IllegalArgumentExceptions if they are not
* @param numberOfPassengers The total number of passengers that need to be created
* @param percentTrusted Percent of the total passengers that are Trusted Travelers
* @param percentFastTrack Percent of the total passengers that are Fast Track
* @param percentOrdinary Percent of the total passengers that are ordinary.
*/
private void checkParameters(int numberOfPassengers, int percentTrusted, int percentFastTrack, int percentOrdinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Sets up the distribution of the passengers
* @param percentTrusted Percent of the total passengers that are Trusted Travelers
* @param percentFastTrack Percent of the total passengers that are Fast Track
* @param percentOrdinary Percent of the total passengers that are ordinary.
*/
private void setUp(int numberOfPassengers, int percentTrusted, int percentFastTrack) {

/**
* Getter method for the reporter
* @return the reporter with the statistics of the simulation
*/
public Reporter getReporter() {

/**
* Goes through the action of the next passenger from the EventCalendar. If the passenger is from the ticketing area, this call to step() implements [UC5].
* If the passenger is from the security area, this call to step() implements [UC6]. In each case, the passenger is removed from the corresponding area.
* If there is no next passenger, an IllegalStateException is thrown.
*/
public void step() {

 - Throws: IllegalStateException

/**
* Checks to see if there are more steps to be done. Similar to hasNext
* @return if there are more steps in the simulation.
*/
public boolean moreSteps() {

/**
* Returns the index of the security checkpoint line for currentPassenger.
* @return index of the checkPoint for the passenger
*/
public int getCurrentIndex() {

/**
* Gets the color of the current Passenger
* @return Color of the passenger.
*/
public Color getCurrentPassengerColor() {

/**
* Determines if the currentPassenger has cleared security.
* @return boolean, true if currentPassenger just cleared a security checkpoint and finished processing and false otherwise, (including when currentPassenger is null).
*/
public boolean passengerClearedSecurity() {


---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* pre security place between 
*
*/
public class PreSecurity implements TransitGroup {

/**
* PreSecurity line
*/
private PassengerQueue outsideSecurity;

/**
* constructor for PreSecurity class
* @param numPassengers the number of passengers
* @param r logging Reporter
* @throw IllegalArgumentException when a is invalid
*/
public PreSecurity(int numPassengers, Reporter r) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* this method will return departure time of next passenger
* @return next departure time
*/
public int departTimeNext() {

/**
* the next passenger to security area
* @return next passenger to go
*/
public Passenger nextToGo() {

/**
* check if there is next passenger 
* @return true if there is a passenger
*/
public boolean hasNext() {

/**
* remove next passenger that will go out of presecurity
* @return the passenger that is removed
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* checkpoint that in the security checking area
*
*/
public class CheckPoint {

/**
* A passenger line in a particular checkpoint
*/
private PassengerQueue line;

/**
* the time when the last passenger in line will clear security and leave the line.
*/
private int timeWhenAvailable;

/**
* constructor for CheckPoint
*/
public CheckPoint() {

/**
* the size of the checkpoint line 
* @return number of passenger in line
*/
public int size() {

/**
* remove the passenger that will leave
* @return leaving passenger
*/
public Passenger removeFromLine() {

/**
* check if there is next passenger
* @return true if there is a passenger
*/
public boolean hasNext() {

/**
* returns the amount of time before the next passenger will clear security, 
* returning Integer.MAX_VALUE if the line is empty
* @return amount of time before next passenger will clear security
*/
public int departTimeNext() {

/**
* the next leaving passenger
* @return the passenger at the front of the security line.
*/
public Passenger nextToGo() {

/**
* adds the parameter to the end of the line, 
* setting their waitTime according to timeWhenAvailable as well as their arrivalTime and 
* processTime, and updating timeWhenAvailable.
* @param p adding passenger
*/
public void addToLine(Passenger p) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* the collection of security checkpoints and their waiting lines.
*
*/
public class SecurityArea implements TransitGroup {

/**
* all the check points and their lines
*/
private CheckPoint[] check;

/**
* maximum check points
*/
public static final int MAX_CHECKPOINTS = 17;

/**
* minimum check points
*/
public static final int MIN_CHECKPOINTS = 3;

/**
* error message for check points' number
*/
public static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/**
* Error message for any attempt to add to a security checkpoint line with an improper index.
*/
public static final String ERROR_INDEX = "Index out of range for this security area" ;

/**
* largest index for fast track line
*/
private int largestFastIndex;

/**
* index of tsa line
*/
private int tsaPreIndex;

/**
* constructor for SecurityArea
* @param num number of checkpoints
*/
public SecurityArea(int num) {

 - Throws: IllegalArgumentException

/**
* Check if the number is appropriate
* @param num number of check points
* @return true if number is valid
*/
private boolean numGatesOK(int num) {

/**
* adds the passenger to the security line with the index given by the first parameter.
* @param i index of check points
* @param p passenger 
*/
public void addToLine(int i, Passenger p) {

 - Throws: IllegalArgumentException

/**
* returns the index of the shortest security line that ordinary passengers are permitted to select
* @return the shortest line that all passengers can use
*/
public int shortestRegularLine() {

/**
* returns the index of the shortest security line restricted to Fast Track passengers
* @return index of shortest line for fast track passenger
*/
public int shortestFastTrackLine() {

/**
* returns the index of the shortest security line restricted to TSA PreCheck/Trusted Traveler passengers
* @return index of shortest line for TSA PASSENGER 
*/
public int shortestTSAPreLine() {

/**
* the number of passengers in the line with the given index.
* @param i given index
* @throws IllegalArgumentException if the index is out of range
* @return length of the check point line
*/
public int lengthOfLine(int i) {

 - Throws: IllegalArgumentException

/**
* returns the time that the next passenger will finish clearing security, 
* or Integer.MAX_VALUE if all lines are empty
* @return time next passenger move
*/
public int departTimeNext() {

/**
* returns the next passenger to clear security, or null if there is none.
* @return next pasenger to leave
*/
public Passenger nextToGo() {

/**
* removes and returns the next passenger to clear security.
* @return passenger that going to leave
*/
public Passenger removeNext() {

/**
* select shortest line in particular range
* @param a starting index of line
* @param b end index of line
* @return shortest line
*/
private int shortestLineInRange(int min, int max) {

/**
* return the line that will have a leaving paseenger
* @return line that going to clear
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* fast track passenger that belongs to passenger
*
*/
public class FastTrackPassenger extends Passenger {

/**
* maximum process time for fast track passenger
*/
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* Color for fast track passenger, which is blue
*/
private Color color;

/**
* constructor for fast track passenger
* @param arrivalTime arrival time for fast track passenger 
* @param processTime process time for fast track passenger
* @param r reporter that for passenger
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter r) {

@Override
public void getInLine(TransitGroup t) {

@Override
public Color getColor() {

/**
* pick a line to enter
* @param t transitgroup which is SecurityArea
* @return line index of the entering line
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* ordinary passenger that belongs to passenger
*
*/
public class OrdinaryPassenger extends Passenger {

/**
* maximum process time for ordinary passenger
*/
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* Color for ordinary passenger, which is red
*/
private Color color;

/**
* constructor for ordinary passenger
* @param arrivalTime arrival time for ordinary passenger 
* @param processTime process time for ordinary passenger
* @param r reporter that for passenger
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter r) {

/**
* an abstract method that adds the passenger to the appropriate security line.
* @param t the list of security lines in the airport (instantiated with SecurityArea) 
*/
public void getInLine(TransitGroup t) {

/**
* get the color of the passenger
* @return color that represent the waiting state of the passenger
*/
public Color getColor() {

/**
* pick a line to enter
* @param t TransitGroup which is SecurityArea
* @return line index of the entering line
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* passenger in simulation, created by Ticketing
*
*/
public abstract class Passenger {

/**
* Reporter that log data of every passengers
*/
private Reporter myLog;

/**
* minimum time for process
*/
public static final int MIN_PROCESS_TIME = 20;

/**
* time that the passenger joins a security line
*/
private int arrivalTime;

/**
* the time the passenger will spend in a security line. 
* It cannot be computed until the passenger joins a security line.
*/
private int waitTime;

/**
* the amount of time that the passenger spends at the front of 
* the security line actually undergoing the security check
*/
private int processTime;

/**
*  the index of the security checkpoint line that the passenger selects and joins. 
*  Before the passenger gets into line, the value should be -1.
*/
private int lineIndex;

/**
* false until the passenger selects and joins a security line, when it becomes true.
*/
private boolean waitingProcess;

/**
* constructor for passenger
* @param arrivalTime arrival time for passenger 
* @param processTime process time for passenger
* @param r reporter that for passenger
*/
public Passenger(int arrivalTime, int processTime , Reporter r) {

 - Throws: IllegalArgumentException

/**
* get the arrival time of a passenger
* @return arrival time for the passenger
*/
public int getArrivalTime() {

/**
* get the wait time of a passenger
* @return wait time for the passenger
*/
public int getWaitTime() {

/**
* set the wait time of a passenger
* @param  waitTime wait time that going to be compute
*/
public void setWaitTime(int waitTime) {

/**
* get the process time of a passenger
* @return process time for the passenger
*/
public int getProcessTime() {

/**
* get the index of a line for passenger
* @return index of the line
*/
public int getLineIndex() {

/**
* check if the passenger is waiting
* @return true if passenger in line waiting
*/
public boolean isWaitingInSecurityLine() {

/**
* removes the passenger from the waiting line when he/she finishes security checks
*/
public void clearSecurity() {

/**
* set line index for passenger
* @param lineIndex certain line that passenger going to join
*/
protected void setLineIndex(int lineIndex) {

/**
* an abstract method that adds the passenger to the appropriate security line.
* @param t the list of security lines in the airport (instantiated with SecurityArea) 
*/
public abstract void getInLine(TransitGroup t);

/**
* get the color of the passenger
* @return color that represent the waiting state of the passenger
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* trusted traveler that belongs to passenger
*
*/
public class TrustedTraveler extends Passenger {

/**
* maximum process time for trusted traveler
*/
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* Color for trusted traveler, which is green
*/
private Color color;

/**
* constructor for trusted traveler
* @param arrivalTime arrival time for trusted traveler
* @param processTime process time for trusted traveler
* @param r reporter that for passenger
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter r) {

/**
* an abstract method that adds the passenger to the appropriate security line.
* @param t the list of security lines in the airport (instantiated with SecurityArea) 
*/
public void getInLine(TransitGroup t) {

/**
* get the color of the passenger
* @return color that represent the waiting state of the passenger
*/
public Color getColor() {

/**
* pick a line to enter
* @param t TransitGroup which is SecurityArea
* @return line index of the entering line
*/
private int pickLine(TransitGroup t) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Event calendar for simulation
*
*/
public class EventCalendar {

/**
* transit group with higher priority
*/
private TransitGroup highPriority;

/**
* transit group with lower priority
*/
private TransitGroup lowPriority;

/**
* constructor for transit group
* @param highPriority transit group with higher priority
* @param lowPriority transit group with lower priority
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* the passenger from highPriority is returned. 
* @return the passenger from highPriority and lowPriority 
*         who will be the next to exit the group/line theyre currently at the front of 
* @throws  IllegalArgumentException if both TransitGroups are empty
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Log class implement Reporter interface
*
*/
public class Log implements Reporter {

/**
* number of passenger complete checking
*/
private int numCompleted;

/**
* total amount of waiting time for all passed passengers
*/
private int totalWaitTime;

/**
* total amount of process time for all passed passengers
*/
private int totalProcessTime;

/**
* constructor for Log
*/
public Log() {

@Override
public int getNumCompleted() {

@Override
public void logData(Passenger p) {

@Override
public double averageWaitTime() {

@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* simulate the process of checking
*
*/
public class Simulator {

/**
* Event calendar for simulation
*/
private EventCalendar myCalendar;

/**
* Reporter for simulation
*/
private Reporter log;

/**
* TransitGroup that represents passengers who have not yet entered a checkpoint security line.
*/
private TransitGroup inTicketing;

/**
* TransitGroup that represents passengers who are in a checkpoint security line.
*/
private TransitGroup inSecurity;

/**
* Initialized to null, is the passenger most recently returned by myCalendar.nextToAct().
*/
private Passenger currentPassenger;

/**
* number of all passengers
*/
private int numPassengers;

/**
* constructor for Simulator
* @param numCheckPoints number of checkpoints
* @param numPassengers number of passengers
* @param percentTSA percent of passengers who are TSA PreCheck/Trusted Travelers
* @param percentFast percent of passengers who are Fast Track
* @param percentOrdinary percent of passengers who do not qualify for special security checkpoints
*/
public Simulator(int numCheckPoints, int numPassengers, int percentTSA, int percentFast, int percentOrdinary) {

/**
* checks the parameters to make sure they are valid
* @param numPassengers number of passengers
* @param percentTSA percent of passengers who are TSA PreCheck/Trusted Travelers
* @param percentFast percent of passengers who are Fast Track
* @param percentOrdinary percent of passengers who do not qualify for special security checkpoints
* @throw IllegalArgumentException when a number is invalid
*/
private void checkParameters(int numPassengers, int percentTSA, int percentFast, int percentOrdinary) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* sets the passenger type distribution for the Ticketing factory
* @param numCheckPoints number of CheckPoints
* @param numPassengers number of passengers
* @param percentTSA percent of passengers who are TSA PreCheck/Trusted Travelers
* @param percentFast percent of passengers who are Fast Track
*/
private void setUp(int numCheckPoints, int numPassengers, int percentTSA, int percentFast) {

/**
* get the reporter from simulator
* @return reporter
*/
public Reporter getReporter() {

/**
* goes through the action of the next passenger from the EventCalendar. 
* If the passenger is from the ticketing area, this call to step() implements. 
* If the passenger is from the security area, this call to step() implements. 
* In each case, the passenger is removed from the corresponding area.
* @throws IllegalArgumentException if there is no next passenger
*/
public void step() {

 - Throws: IllegalArgumentException

/**
* check if there is more steps
* @return true if more steps exist
*/
public boolean moreSteps() {

/**
* the index of the security checkpoint line for currentPassenger
* @return index of the line
*/
public int getCurrentIndex() {

/**
* return color of certain passenger
* @return color
*/
public Color getCurrentPassengerColor() {

/**
* check  if currentPassenger just cleared a security checkpoint 
* and finished processing, (including when currentPassenger is null).
* @return true if clear
*/
public boolean passengerClearedSecurity() {


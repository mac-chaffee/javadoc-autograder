---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/PreSecurity.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents passengers in the terminal who have not yet joined a security checkpoint line.
*/
public class PreSecurity implements TransitGroup {

/** Holds Passenger objects created by the constructor. */
private PassengerQueue outsideSecurity;

/** The passenger currently in the front of the ticketing area, null if ticketing is empty. */
private Passenger frontPassenger;

/**
* A constructor for PreSecurity, creates the specified number of passengers by successive 
* calls Ticketing.generatePassenger(), adding each to its PassengerQueue.
* @param numPasssengers The number of Passenger objects PreSecurity makes. 
* @param reporter A reporter to relay Passenger data.
* @throws IllegalArgumentException if numPassengers has a value less than 1. 
*/
public PreSecurity(int numPasssengers, Reporter reporter) {

 - Throws: IllegalArgumentException

/**
* Retrieves the departure time,  which is it's arrival time, of the passenger in front of the 
* ticketing line returning Integer.MAX_VALUE if the line is empty.
* @return An Integer representing Passenger's departure time. 
*/
public int departTimeNext() {

/**
* Retrieves the passenger in front of the ticketing line. 
* @return A Passenger representing the passenger in front. 
*/
public Passenger nextToGo() {

/**
* Reports whether or not there is a passenger in the front of the ticketing line. 
* @return False if there is no Passenger in front of the queue, otherwise true. 
*/
public boolean hasNext() {

/**
* Removes the passenger in front from of the ticketing line. 
* @return the next passenger to clear security.
*/
public Passenger removeNext() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/entrance/Ticketing.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* A simple factory class to generate passengers to go through airport security.
*/
public class Ticketing {

/** Absolute time for passengers created for the simulation.  The time starts at zero
* and increases by up to MAX_PASSENGER_GENERATION_DELAY for each passenger created. */
private static int time = 0;

/** Random object with seed that allows for testing of simulation */
private static Random randomNumber = new Random(10);

/** Maximum delay between creation of passengers */
private static final double MAX_GENERATION_DELAY = 15;

/** Percentage of time a  Fast Track passenger should be created */
private static double pctExpedite = .50;  // initialize with default value
/** Percentage of time a trusted traveler/TSA PreCheck passenger should be created */
private static double pctTrust = .05;  // initialize with default value
/**
* Set the proportions of fast track, trusted traveler, and (by inference) ordinary passengers
* that should be generated. Proportion of ordinary passengers is 1 - (pctFast + pctTrusted).
* @param pctTrusted - proportion of passengers that are TrustedTravelers
* @param pctFast - proportion of passengers that are FastTrackPassengers
*/
public static void setDistribution(int pctTrusted, int pctFast) {

/**
* Generate a new passenger as described in the class comments. 
* @param log - where the passenger will log his/her data 
* @return the passenger created
*/
public static Passenger generatePassenger(Reporter log) {

/**
* Resets the factory.  You can use this for testing ONLY! Do not use it in your simulator.  
*/
public static void resetFactory() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/CheckPoint.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents a security checkpoint line and its line, where the front passenger is undergoing the actual security process.
* It delegates strict queue activities to its instance variable line.
*
*/
public class CheckPoint {

/** The time that the last passenger in line will clear security and leave the line. */
private int timeWhenAvailable;

/** Represents the line of the checkpoint and the passengers in it. */
private PassengerQueue line;

/**
* The constructor for a CheckPoint, creates a empty PassengerQueue representing the line. 
*/
public CheckPoint() {

/**
* Retrieves the length of the line of Passengers currently at the checkpoint. 
* @return An Integer representing the length of the line. 
*/
public int size() {

/**
* Removes the front passenger from the checkpoint line. 
* @return the next passenger the clear the checkpoint. 
*/
public Passenger removeFromLine() {

/**
* Reports whether or not there is a passenger in the front of the checkpoint.  
* @return False if there is no Passenger in front of the line, otherwise true. 
*/
public boolean hasNext() {

/**
* Retrieves the amount of time before the next passenger will clear security, returning Integer.MAX_VALUE if the line is empty.
* @return an Integer representing the time before the next passenger clears Checkpoint. 
*/
public int departTimeNext() {

/**
* Retrieves the passenger at the front of the security line.
* @return Passenger in front of Checkpoint. 
*/
public Passenger nextToGo() {

/**
* Adds a passenger to the end of the line, setting their waitTime according to timeWhenAvailable, their arrivalTime, and processTime updating timeWhenAvailable.
* @param traveler The Passenger to be added to the Checkpoint line. 
*/
public void addToLine(Passenger traveler) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/security/SecurityArea.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents the collection of security checkpoints and their waiting lines.
*/
public class SecurityArea implements TransitGroup {

/** Maximum boundary on the number of checkpoints. */
private static final int MAX_CHECKPOINTS = 17;

/** Minimum boundary on the number of checkpoints. */
private static final int MIN_CHECKPOINTS = 3;

/** The error message for any attempt to create an improper amount of checkpoints. */
private static final String ERROR_CHECKPOINTS = "Number of checkpoints must be at least 3 and at most 17.";

/** The error message for any attempt to add to a security checkpoint line with an improper index. */
private static final String ERROR_INDEX = "Index out of range for this security area";

/** The largest index for lines reserved for Fast Track passengers. */
private int largestFastIndex;

/** The index of the line reserved for TSA PreCheck/Trusted Travelers. */
private int tsaPreIndex;

/** Represents all the checkpoints in the security area. */
private ArrayList<CheckPoint> check;

/**
* The constructor for the SecurityArea object. 
* @param numCheckpoints The number of checkpoints the SecurityArea should be made with. 
*/
public SecurityArea(int numCheckpoints) {

 - Throws: IllegalArgumentException

/**
* A helper method to determine if a given number of gates is proper as specified by requirements. 
* @param num The number of gates. 
* @return True if the number of gates is appropriate, otherwise false. 
*/
private boolean numGatesOK(int num) {

/**
* Adds the passenger to the security line with the index given.
* @param idx The index of the line Passenger is added to. 
* @param traveler Passenger to be added to a line. 
*/
public void addToLine(int idx, Passenger traveler) {

/**
* Retrieves the index of the shortest security line that ordinary passengers are permitted to select.
* @return The index of the checkpoint with the shortest line. 
*/
public int shortestRegularLine() {

/**
* Retrieves the index of the shortest security line that fast tracks passengers are permitted to select.
* @return The index of the checkpoint with the shortest line. 
*/
public int shortestFastTrackLine() {

/**
* Retrieves the index of the shortest security line that TSA passengers are permitted to select.
* @return The index of the checkpoint with the shortest line. 
*/
public int shortestTSAPreLine() {

/**
* Retrieves the number of passengers in the line with the given index.
* @param index The index of the Checkpoint.
* @throws IllegalArgumentException if the index is out of range
* @return An Integer representing the number of Passenger objects in a Checkpoint line.  
*/
public int lengthOfLine(int index) {

 - Throws: IllegalArgumentException

/**
* Retrieves the time that the next passenger will finish clearing security, or Integer.MAX_VALUE if all lines are empty.
* @return The time a Passenger will clear the Checkpoint. 
*/
public int departTimeNext() {

/**
* Retrieves the next passenger to clear security, or null if there is none.
* @return The Passenger next in the Checkpoint or null. 
*/
public Passenger nextToGo() {

/**
* Removes a passenger from the security area. 
* @return the next passenger to clear security.
*/
public Passenger removeNext() {

/**
* Helper method that finds the index of the checkpoint within the given range with the shortest line. 
* @param lower The lower index boundary. 
* @param upper The higher index boundary. 
* @return A Integer representing the index of the checkpoint with the shortest line. 
*/
private int shortestLineInRange(int lower, int upper) {

/**
* Helper method that finds the index of the checkpoint with the next passenger to process in the security area. 
* @return An Integer representing the index of the checkpoint. 
*/
private int lineWithNextToClear() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/TransitGroup.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* TransitGroup declares some common operations for a group of passengers in which there is a "next" passenger
*   and a time when that passenger will leave the group. 
*/
public interface TransitGroup {

/**
* Who will be the next passenger to leave the group?
* @return the next passenger
*/
/**
* When will the next passenger leave the group?
* @return departure time of the next passenger or Integer.MAX_VALUE if the group is empty
*/
/**
* Removes the next passenger to leave the group.
* @return the passenger who is removed
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/FastTrackPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represens a specialized FastTrack program passenger in the terminal. 
*
*/
public class FastTrackPassenger extends Passenger {

/** The color of this passenger in simulation. */
private Color color;

/** the maximum time this passenger will spend at the front of the security line undergoing the security check. */
public static final int MAX_EXPECTED_PROCESS_TIME = 330;

/**
* The constuctor for a FastTrackPassenger object, assigning it an arrival and process time. 
* @param arrivalTime The arrival time of the FastTrack passenger. 
* @param processTime The time the FastTrack passenger is expected to spend in the checkpoint. 
* @param log A reporter used to create a Reporter for FastTrackPassenger. 
*/
public FastTrackPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* Retrieves the color representation of a fast track passenger in the simulator. 
* @return A Color object representing the color of FastTrackPassenger in simulation. 
*/
public Color getColor() {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the parent class, and then places this passenger in the selected line.
* @param lines the list of security lines in the airport.
*/
public void getInLine(TransitGroup lines) {

/**
* Helper method that selects the appropriate checkpoint waiting line for a FastTrackPassenger. 
* @param line Represents all security lines.
* @return An Integer representing the index of the checkpoint.
*/
private int pickLine(TransitGroup line) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/OrdinaryPassenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents the specialized regualar passenger in the terminal. 
*
*/
public class OrdinaryPassenger extends Passenger {

/** The color of this passenger in simulation. */
private Color color;

/** the maximum time this passenger will spend at the front of the security line undergoing the security check. */
public static final int MAX_EXPECTED_PROCESS_TIME = 150;

/**
* The constuctor for a OrdinaryPassenger object, assigning it an arrival and process time. 
* @param arrivalTime The arrival time of the Ordinary passenger. 
* @param processTime The time the Ordinary passenger is expected to spend in the checkpoint. 
* @param log A reporter used to create a Reporter for OrdinaryPassenger. 
*/
public OrdinaryPassenger(int arrivalTime, int processTime, Reporter log) {

/**
* Retrieves the color representation of a ordinary passenger in the simulator. 
* @return A Color object representing the color of OrdinaryPassenger in simulation. 
*/
public Color getColor() {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the parent class, and then places this passenger in the selected line.
* @param lines the list of security lines in the airport.
*/
public void getInLine(TransitGroup lines) {

/**
* Helper method that selects the appropriate checkpoint waiting line for a OrdinaryPassenger.
* @param line Represents all security lines.
* @return An Integer representing the index of the checkpoint.
*/
private int pickLine(TransitGroup line) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/Passenger.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents a generic passenger within the terminal. 
*
*/
public abstract class Passenger {

/** the minimum time the passenger will spend at the front of the security line undergoing the security check. */
public static final int MIN_PROCESS_TIME = 20;

/** the time that the passenger joins a security line. */
private int arrivalTime;

/** the time the passenger will spend in a security line. */
private int waitTime;

/** the amount of time that the passenger spends at the front of the security line actually undergoing the security check. */
private int processTime;

/** the index of the security checkpoint line that the passenger selects and joins. has a value of -1 before passenger joins a line. */
private int lineIndex;

/** false until the passenger selects and joins a security line, when it becomes true. */
private boolean waitingProcessing;

/** The reporting mechanism [UC6,S2], initialized by the Reporter parameter in the constructor. */
private Reporter myLog;

/**
* The constuctor for a Passenger object, assigning it an arrival and process time. 
* @param arrivalTime The arrival time of the passenger. 
* @param processTime The time the passenger is expected to spend in the checkpoint. 
* @param log A reporter used to create a Reporter for Passenger. 
*/
public Passenger(int arrivalTime, int processTime, Reporter log) {

/**
* Retrieves the passenger's arrival time.
* @return the arrival time of Passenger. 
*/
public int getArrivalTime() {

/**
* Retrieves the passenger's wait time.
* @return the wait time of Passenger. 
*/
public int getWaitTime() {

/**
* Changes the initial value of the passengesr's wait time. 
* @param wait The final wait time of Passenger. 
*/
public void setWaitTime(int wait) {

/**
* Retrieves the passengers process time.
* @return the process time of Passenger. 
*/
public int getProcessTime() {

/**
* Retrieves the position of the passenger in a line. 
* @return An Integer representing the index of the passenger in a queue.  
*/
public int getLineIndex() {

/**
* Determines whether or not a passenger is in a security checkpoint line. 
* @return True if Passenger is in the queue of a security checkpoint, otherwise false. 
*/
public boolean isWaitingInSecurityLine() {

/**
* Removes the passenger from the waiting line when he/she finishes security checks.
*/
public void clearSecurity() {

/**
* Changes the initial value of the passenger's index to a given final value. 
* @param index The final value of the index. 
*/
protected void setLineIndex(int index) {

/**
* Abstract method that adds the passenger to the appropriate security line.
* @param lines the list of security lines in the airport
*/
public abstract void getInLine(TransitGroup lines);

/**
* Retrieves the color representation of a passenger in the simulator. 
* @return A Color object representing the color of Passenger in simulation. 
*/
public abstract Color getColor();

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/PassengerQueue.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Implements a simple queue where the elements are Passengers.
*/
public class PassengerQueue {

/** The underlying queue data structure. */
private LinkedList<Passenger> queue;

/**
* Creates an empty queue.
*/
public PassengerQueue() {

/**
* Returns the number of Passengers in the queue.
* @return the number of Passengers
*/
public int size() {

/**
* Adds a new Passenger to the end of the queue.
* @param person the Passenger to add
*/
public void add(Passenger person){

/**
* Removes and returns the front Passenger from the queue. 
* @return the Passenger at the front of the queue.
*/
public Passenger remove() {

/**
* Gets the front Passenger of the queue without removing it, or null
* if the queue is empty. Does not remove the Passenger from the queue.
* @return the front Passenger or null if the queue is empty
*/
public Passenger front() {

/**
* Returns true if the queue is empty, false otherwise.
* @return true if the queue has no elements
*/
public boolean isEmpty() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/airport/travelers/TrustedTraveler.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents a specialized TSA passenger in the terminal. 
*
*/
public class TrustedTraveler extends Passenger {

/** The color of this passenger in simulation. */
private Color color;

/** the maximum time this passenger will spend at the front of the security line undergoing the security check. */
public static final int MAX_EXPECTED_PROCESS_TIME = 180;

/**
* The constuctor for a TrustedTraveler object, assigning it an arrival and process time. 
* @param arrivalTime The arrival time of the passenger. 
* @param processTime The time the passenger is expected to spend in the checkpoint. 
* @param log A reporter used to create a Reporter for TrustedTraveler. 
*/
public TrustedTraveler(int arrivalTime, int processTime, Reporter log) {

/**
* Retrieves the color representation of a trusted traveler passenger in the simulator. 
* @return A Color object representing the color of TrustedTraveler in simulation. 
*/
public Color getColor() {

/**
* Selects the appropriate checkpoint waiting line, sets lineIndex from the parent class, and then places this passenger in the selected line.
* @param lines the list of security lines in the airport.
*/
public void getInLine(TransitGroup lines) {

/**
* Helper method that selects the appropriate checkpoint waiting line for a TrustedTraveler.
* @param line Represents all security lines.
* @return An Integer representing the index of the checkpoint.
*/
private int pickLine(TransitGroup line) {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/EventCalendar.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* While not all passengers have left the simulation, the EventCalendar class determines which passenger to process next one by one. 
*
*/
public class EventCalendar {

/** Line of passengers with high priority in processing passengers. */
private TransitGroup highPriority;

/** Line of passengers with low priority in processing passengers. */
private TransitGroup lowPriority;

/**
* The constructor takes two TransitGroups as parameters. 
* The first parameter has priority over the second one: highPriority is initialized with the first parameter and lowPriority is initialized with the second.
* @param highPriority Line of passengers with  the highest priority for processing. 
* @param lowPriority Line of passengers with  the lowest priority for processing. 
*/
public EventCalendar(TransitGroup highPriority, TransitGroup lowPriority) {

/**
* Retrieves the passenger from highPriority and lowPriority who will be the next to exit the group/line theyre currently at the front of. 
* In case of a tie for the next to exit, the passenger from highPriority is returned. 
* @return The Passenger to be processed next. 
* @throws IllegalStateException If both TransitGroups are empty (their next Passenger is null).
*/
public Passenger nextToAct() {

 - Throws: IllegalStateException

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Log.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class uses the Reporter interface to implement methods responsible for reporting Passenger data. 
*
*/
public class Log implements Reporter {

/** The number of Passenger's that have cleared security in the terminal. */
private int numCompleted;

/** The total wait time of all passengers in the terminal. */
private double totalWaitTime;

/** The total process time of all the passengers in the terminal. */
private double totalProcessTime;

/**
* Constructor for a Log object that will record passenger data. 
*/
public Log() {

/**
* Retrieves the number of passengers that have been cleared. 
* @return An Integer representing the number of passengers.
*/
@Override
public int getNumCompleted() {

/**
* Records the data of the given passenger.
* @param traveler The Passenger object that data is being retrieved from. 
*/
@Override
public void logData(Passenger traveler) {

/**
* Retrieves the average wait time of passengers going through the terminal.
* @return A Double representing the average wait time. 
*/
@Override
public double averageWaitTime() {

/**
* Retrieves the average process time of passengers going through the terminal.
* @return A Double representing the average process time. 
*/
@Override
public double averageProcessTime() {

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Reporter.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* Reporter describes reporting methods required to log passenger group activities. 
*/
public interface Reporter {

/**
* How many passengers have completed all their activities?
* @return the number of passengers who have completed their activities
*/
/**
* Log the data for a passenger.
* @param p - passenger whose data is to be logged
*/
/**
* How long did passengers have to wait before completing processing?
* @return average passenger waiting time (in minutes).
*/
/**
* How long did it take passengers to complete processing?
* @return average processing time
*/
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
src/edu/ncsu/csc216/transit/simulation_utils/Simulator.java
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

/**
* This class represents the simulator and handles the setting up and running of a simulation. 
*/
public class Simulator {

/** The total number of passengers in the terminal. */
@SuppressWarnings("unused")
private int numPassengers;

/** This Reporter is responsible for relaying passenger data. */
private Reporter log;

/** This EventCalendar determines the next passenger to be processed in the terminal. */
private EventCalendar myCalendar;

/** The TransitGroup that represents passengers who have not yet entered a checkpoint security line. */
private TransitGroup inTicketing;

/** The TransitGroup that represents passengers who are in a checkpoint security line. */
private TransitGroup inSecurity;

/** Initialized as null, this is the passenger most recently returned by myCalendar.nextToAct(). */
private Passenger currentPassenger;

/**
* The constuctor of Simulator sets up the simulation by, initializes log to record the passenger statistics,
* seting the passenger type distribution for the Ticketing, creates the Security and PreSecurity area, and the EventCalendar.
* @param numCheckpoints The number of checkpoints in the terminal. 
* @param numPassengers The number of passengers in the terminal. 
* @param percentTSA The percent of passengers who are TSA PreCheck/Trusted Travelers.
* @param percentFast The percent of passengers who are Fast Track eligible. 
* @param percentReg The percent of passengers who do not qualify for special security checkpoints.
* @throws IllegalArgumentException If the parameters are not valid according to system requirements. 
*/
public Simulator(int numCheckpoints, int numPassengers, int percentTSA, int percentFast, int percentReg) {

 - Throws: IllegalArgumentException

/**
* Helper method for the constructor that checks to ensusre that the checkpoint, and passenger distribution parameters are valid. 
* Specifically, that the number of checkpoints is between 3 and 17, inclusive, and that the passenger distrubutions add up to 100.
* @param numCheckpoints The number of checkpoints in the terminal. 
* @param percentTSA The percent of passengers who are TSA PreCheck/Trusted Travelers.
* @param percentFast The percent of passengers who are Fast Track eligible. 
* @param percentReg The percent of passengers who do not qualify for special security checkpoints.
*/
private void checkParameters(int numCheckpoints, int percentTSA, int percentFast, int percentReg) {

 - Throws: IllegalArgumentException

 - Throws: IllegalArgumentException

/**
* Helper method for the constructor sets the passenger distribution for Ticketing to generate passsengers.  
* @param percentTSA The percent of passengers who are TSA PreCheck/Trusted Travelers.
* @param percentFast The percent of passengers who are Fast Track eligible. 
* @param percentReg The percent of passengers who do not qualify for special security checkpoints.	 
*/
private void setUp(int percentTSA, int percentFast, int percentReg) {

/**
* Retrieves the reporter collecting passenger data. 
* @return A Reporter object collecting data from Passenger objects. 
*/
public Reporter getReporter() {

/**
* This method goes through the action of the next passenger from the EventCalendar. 
* If the passenger is from the ticketing area, the passenger joins the shortest security checkpoint line which depends the passengers kind. 
* If the passenger is from the security area, the passenger leaves the security checkpoint, logs their information, and leaves the simulation. 
* In each case, the passenger is removed from the corresponding area. 
* @throws  IllegalStateException If there is no next passenger.
*/
public void step() {

 - Throws: IllegalStateException

/**
* Determines whether there is another passenger to be processed. 
* @return True if another passenger needs to be processed, otherwise false. 
*/
public boolean moreSteps() {

/**
* Retrieves the index of the security checkpoint line for currentPassenger.
* @return An Integer presenting the index of the checkpoint line. 
*/
public int getCurrentIndex() {

/**
* Retrieves the color of the current passenger within the simulation. 
* @return A Color object representing the color of Passenger. 
*/
public Color getCurrentPassengerColor() {

/**
* Determines whether currentPassenger just cleared a security checkpoint and finished processing. 
* @return True if currentPassenger cleared a security checkpoint, false otherwise, (including when currentPassenger is null).
*/
public boolean passengerClearedSecurity() {


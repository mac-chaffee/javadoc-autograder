# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 15:32:36 2019

@author: Colton
"""

import numpy as np
import pandas as pd

df=pd.read_csv('all_repos_with_grades_and_data_and_correct_names.csv', sep=',')
train, validate, test = np.split(df.sample(frac=1), [int(.6*len(df)), int(.8*len(df))])

print(len(train))
print(len(test))
print(len(validate))

train.to_csv("train.csv")
test.to_csv("test.csv")
validate.to_csv("validate.csv")
# Javadoc Autograder
Autograder for CSC216 JavaDoc (a project for CSC422)

* Colton Botta (cgbotta)
* Mac Chaffee (machaffe)
* Nikhil Shirsekar (nnshirse)

## Development Environment Setup

1. Install [PyCharm Professional Edition](https://www.jetbrains.com/pycharm/download/#section=linux)
    * You can get a license by registering with a university email
2. Install [Python 3.7.2](https://www.python.org/downloads/)
    * We're using the latest version for preprocessing and training, but
      we can make the predictor script (that we give to TAs) backwards compatible with
      Python 2
3. Clone this repo
4. Open PyCharm
5. Click File > Open and select the folder where you cloned the repo
6. Go to File > Settings > Project: javadoc-autograder > Project Interpreter
7. Click the "Project Interpreter" dropdown and click "Show All..."
8. Click the green plus on the right side
9. Create a new "Virtualenv Environment"
    * Should look like this:
   
   ![New Virtualenv Environment window](https://i.imgur.com/8vsdgPn.png)
10. Open `main.py`. You should see a prompt telling you the packages aren't installed yet. Click "Install Packages".
11. Click View > Scientific View. This will auto-show documentation for any method you click on.
    It also shows any tables or graphs you create like in RStudio.
   
**Why do we have to do all this?**
* We have to set the project interpreter because you might have multiple 
versions of Python installed. 
* We need to create a [virtualenv](https://docs.python.org/3/tutorial/venv.html) 
so that the packages we install for this project don't get installed globally.

## Useful Links

* Python docs: https://docs.python.org/3/
* Python basics cheatsheet: http://datasciencefree.com/python.pdf
* PyCharm docs: https://www.jetbrains.com/help/pycharm/quick-start-guide.html
* NLTK docs: http://www.nltk.org/api/nltk.html
* Scikit Learn docs: https://scikit-learn.org/stable/documentation.html
   

from nltk import word_tokenize
from pandas import read_csv
import numpy as np
import os
import re


X_DATA_PATH = os.path.join("..", "x_data")
Y_DATA_PATH = os.path.join("..", "y_data")


def get_x_values(split_type, comment_type, vocab):
    """
    Gets the data in the format required for model.train() and Evaluator()
    :param split_type: 'train' | 'test' | 'validate'
    :param comment_type: 'class' | 'method'
    :param vocab the dictionary mapping words to their vectors and indices
    :return: a list of lists of words, where each inner list contains all words
    of a single file's class or method comments
    """
    assert split_type in ("train", "test", "validate")
    assert comment_type in ("class", "method")

    file_names = list(read_csv(os.path.join(Y_DATA_PATH, split_type + ".csv"), sep=",").iloc[:, 0])
    x_vals = []

    for file_name in file_names:
        with open(os.path.join(X_DATA_PATH, file_name), "rt") as f:
            raw_text = f.read()
            # Get a list of all words
            comments_list = get_class_and_method_comments(raw_text, split_each_block=False, comment_type=comment_type)
            # Convert each word into Vocab objects (or None if the word isn't in the vocab)
            comments_list = [vocab.get(word) for word in comments_list]
            # Convert Vocab objects into indices, or 0 (the padding value) if the word isn't in the vocab
            # Add 1 since index 0 is a vector of all zeroes for padding
            comments_list = [v.index + 1 if v is not None else 0 for v in comments_list]
            comments_array = np.array([np.array(x) for x in comments_list])
            x_vals.append(comments_array)
    return np.array(x_vals)


def get_y_values(split_type, comment_type):
    """
    Get a list of y values (grades) for a given split type and a given comment type
    :param split_type:
    :param comment_type:
    :return:
    """
    assert split_type in ("train", "test", "validate")
    if comment_type == "class":
        return np.array(read_csv(os.path.join(Y_DATA_PATH, split_type + ".csv"), sep=",").iloc[:, 2])
    elif comment_type == "method":
        return np.array(read_csv(os.path.join(Y_DATA_PATH, split_type + ".csv"), sep=",").iloc[:, 3])
    else:
        raise AssertionError("comment_type should be 'class', or 'method'")


def get_class_and_method_comments(raw_text, split_each_block=True, comment_type="all"):
    """
    :returns: a list of lists of strings, where each inner list is the words of the
    entire, cleaned javadoc:

    [['creates', 'the', 'presecurity', 'class'],
     ['...']]

    UNLESS split_each_block=False, in which case it returns a list of words.
    (used when creating train_x/test_x/validate_x)
    """
    lines = raw_text.splitlines()
    class_and_method_comments = []

    class_finder = re.compile(r"^(public |private |protected |)class")
    method_finder = re.compile(r"^(public |private |protected |).+\(\)")
    javadoc_start_finder = re.compile(r"(^/\*\*)")
    javadoc_end_finder = re.compile(r"(\*/)$")
    javadoc_start_index = 0

    if comment_type == "all":
        should_read_comment = lambda x: method_finder.match(x) or class_finder.match(x)
    elif comment_type == "class":
        should_read_comment = lambda x: class_finder.match(x)
    elif comment_type == "method":
        should_read_comment = lambda x: method_finder.match(x)
    else:
        raise AssertionError("comment_type should be 'all', 'class', or 'method'")

    for i in range(len(lines)):
        line = lines[i].lstrip()
        if javadoc_start_finder.match(line):
            javadoc_start_index = i
            continue
        if javadoc_end_finder.match(line) and i + 1 < len(lines):
            next_line = lines[i + 1].lstrip()
            # Make sure we hit a class or method comment
            if should_read_comment(next_line):
                # The javadoc string is on line(s) javadoc_start_index to i
                # Clean each line and add the result to class_and_method_comments
                words_in_current_block = []
                for j in range(javadoc_start_index, i):
                    clean_line = lines[j].lstrip("/*").strip().lower()
                    if len(clean_line) > 0:
                        split_words = word_tokenize(clean_line)
                        words_in_current_block.extend(split_words)
                if split_each_block:
                    class_and_method_comments.append(words_in_current_block)
                else:
                    class_and_method_comments.extend(words_in_current_block)
    return class_and_method_comments

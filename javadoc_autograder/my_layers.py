import keras.backend as K
from keras.engine.topology import Layer
from keras.layers.convolutional import Convolution1D


class Conv1DWithMasking(Convolution1D):
    def __init__(self, *args, **kwargs):
        self.supports_masking = True
        super(Conv1DWithMasking, self).__init__(*args, **kwargs)

    def compute_mask(self, x, mask=None):
        return mask


class MeanOverTime(Layer):
    def __init__(self, mask_zero=True, **kwargs):
        self.mask_zero = mask_zero
        self.supports_masking = True
        super(MeanOverTime, self).__init__(**kwargs)

    def call(self, x, mask=None):
        """
        :return: a vector of vectors of means of each input vector for each time step,
        making sure to ignore masked vectors (all zeroes) when calculating the mean
        """
        # TODO: Don't include masked vectors when calculating mean. The NEA code doesn't work
        # and even if you update the functions calls in the NEA code, the formula returns NaNs
        # due to dividing by zeroes
        return K.mean(x, axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]

    def compute_mask(self, x, mask=None):
        return None

    def get_config(self):
        config = {'mask_zero': self.mask_zero}
        base_config = super(MeanOverTime, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

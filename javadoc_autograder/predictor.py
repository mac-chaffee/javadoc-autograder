import os

import numpy as np
from gensim.models import Word2Vec
from keras.models import load_model
from keras_preprocessing.sequence import pad_sequences
from pandas import read_csv

from preprocessor import get_x_values
from trainer import EMBEDDINGS_PATH
from utils import convert_to_dataset_friendly_scores

# TODO: Fix this whole file to be usable by TAs
scores = np.array(read_csv(os.path.join("..", "y_data", "test.csv"), sep=",").iloc[:, 2])

word_embeddings = Word2Vec.load(os.path.join(EMBEDDINGS_PATH, "word_embeddings.pickle"))

test_x_vals = get_x_values("test", "class", word_embeddings.wv.vocab)[:10]
test_x_vals = pad_sequences(test_x_vals, maxlen=730, padding='post')
test_y_vals = scores[:10]

# Disable GPU acceleration
# (to avoid requiring TAs to have graphics card, updated drivers, or CUDA installed)
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
model = load_model("autograder.h5")

predictions = convert_to_dataset_friendly_scores(model.predict(test_x_vals, batch_size=10).squeeze())

for predicted_grade, actual_grade in zip(predictions, test_y_vals):
    print("Predicted: %d    Actual: %d" % (predicted_grade, actual_grade))

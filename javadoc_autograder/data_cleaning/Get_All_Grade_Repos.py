# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 10:54:07 2019

@author: Colton
"""

import os

cleaned_file = "class_comments_no_incompletes_cleaned_repo_names.txt"

with open(cleaned_file, 'w') as outfile:
    for filename in os.listdir(os.getcwd()):
        if filename.endswith(".txt"):
            repo = filename.split('_')[0]
            print (repo)
            repo = repo.rstrip('/')
            print(repo)
            outfile.write(repo + "\n")

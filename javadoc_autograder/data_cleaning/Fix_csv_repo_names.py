# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 16:12:01 2019

@author: Colton
"""

import pandas as pd

data = pd.read_csv('all_repos_with_grades_and_data.csv')

for index, row in data.iterrows():
    print(index)
    row[0] = row[0].split("_")[1]
    row[0] = row[0].rstrip('/')
    row[0] = row[0] + '.txt'
    print(row[0])
    data.loc[index,'Repo'] = row[0]
    
print(data)
data.to_csv('all_repos_with_grades_and_data_and_correct_names.csv')
    


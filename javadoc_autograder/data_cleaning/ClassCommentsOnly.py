# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 10:54:07 2019

@author: Colton
"""

import os
import csv

for filename in os.listdir(os.getcwd()):
    if filename.endswith(".txt"):
        try:
            f = open(filename, "r", encoding="utf8")
            print("Working on ", filename)
            lines = f.readlines()
            f.close()
            
            for line in lines:
                with open('class_comments.csv', mode='w') as class_comments:
                    employee_writer = csv.writer(class_comments, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                
                    comments
                
                    employee_writer.writerow([filename, comments])

            continue 
        except UnicodeEncodeError as exception:
            continue
    else:
        continue

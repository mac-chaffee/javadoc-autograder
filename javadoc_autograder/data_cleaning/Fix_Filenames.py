# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 12:55:14 2019

@author: Colton
"""

import os

for filename in os.listdir(os.getcwd()):
    if filename.endswith(".txt"):
        name = filename.split('_')[0]
        name = name + '.txt'
        print(name)
        os.rename(filename, name)             
    else:
        continue
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 11:44:23 2019

@author: Colton
"""
import csv

my_file_name = "class_comments.csv"
cleaned_file = "class_comments_no_incompletes.csv"

with open(my_file_name, 'r') as infile, \
     open(cleaned_file, 'w', newline = "") as outfile:
    writer = csv.writer(outfile)
    for line in csv.reader(infile):
        if 'INCOMPLETE' not in line:
            writer.writerow(line)
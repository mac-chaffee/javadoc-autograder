# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 15:32:54 2019

@author: Colton
"""

import os
directory = "C:/Users/Colton/Documents/JuniorS2/CSC422/javadoc-autograder/data"

for filename in os.listdir(os.getcwd()):
    x = 0
    if filename.endswith(".txt"):
        try:
            f = open(filename, "r", encoding="utf8")
            print("Working on ", filename)
            x = x + 1
            lines = f.readlines()
            f.close()
            
            f2 = open(filename, "w")
            for line in lines:
                if "@author" not in line:
                    f2.write(line)
            f2.close()
            continue 
        except UnicodeEncodeError as exception:
            continue
    else:
        continue
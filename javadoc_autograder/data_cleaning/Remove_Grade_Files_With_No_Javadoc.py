# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 12:55:14 2019

@author: Colton
"""

import os
import csv

all_repos_ready = open("all_repos_with_grades_and_data.csv", 'w', newline = "")
writer = csv.writer(all_repos_ready)

repos = open("class_comments_no_incompletes_cleaned_repo_names.txt", 'r')
data = open("class_comments_no_incompletes.csv", 'r')
data_reader = csv.reader(data)
my_list = list(data_reader)
lines = repos.readlines()
x = 0
for row in my_list:
    if(len(row[0]) > 4):
        repo_name = row[0]
        repo_name = repo_name.split("_")[1]
        repo_name = repo_name.replace("/", "")
        for line in lines:
            line = line.replace("\n", "")
            if repo_name in line:
                writer.writerow(row)
                
all_repos_ready.close()


              

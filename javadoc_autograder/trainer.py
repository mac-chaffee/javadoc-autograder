import logging
import os
from time import time

import keras.backend as K
import keras.optimizers as opt
import numpy as np
from gensim.models import KeyedVectors
from keras.initializers import Constant
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.preprocessing.sequence import pad_sequences

from evaluator import Evaluator
from my_layers import Conv1DWithMasking, MeanOverTime
from preprocessor import get_x_values, get_y_values
from utils import convert_to_dataset_friendly_scores, get_model_friendly_scores

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
EMBEDDINGS_PATH = os.path.join("..", "word_embeddings")


def load_word_embeddings():
    """
    WARNING: This function may take a few minutes and a few gigs of RAM to run
    Download the pre-trained embeddings here: https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit
    Unzip the file and rename to "google_news.bin"
    :returns an array of all words used (vocab) and an array of all vectors, with an extra row of zeros added
    to the start to be used as a mask for words not in the vocab.
    """
    with open(os.path.join(EMBEDDINGS_PATH, "google_news.bin"), "rb") as stream:
        word_embeddings = KeyedVectors.load_word2vec_format(stream, binary=True, unicode_errors='replace', limit=500000)
    all_word_vectors = word_embeddings.vectors
    # Add a vector of zeroes for padding (the model will ignore words with index 0)
    all_word_vectors = np.vstack([np.zeros(word_embeddings.vector_size), all_word_vectors])
    return word_embeddings.vocab, all_word_vectors


def train(vocab, all_word_vectors, comment_type="class", use_cnn=False):
    """
    Load and preprocess the data, train the model,
    and save the best model to a file.
    """
    assert comment_type in ("class", "method")
    train_x = get_x_values("train", comment_type, vocab)
    test_x = get_x_values("test", comment_type, vocab)
    validate_x = get_x_values("validate", comment_type, vocab)
    train_y = get_model_friendly_scores(get_y_values("train", comment_type))
    test_y = get_model_friendly_scores(get_y_values("test", comment_type))
    test_y_org = convert_to_dataset_friendly_scores(test_y)

    validate_y = get_model_friendly_scores(get_y_values("validate", comment_type))
    validate_y_org = convert_to_dataset_friendly_scores(validate_y)
    max_len = max([len(x) for x in train_x])
    train_x = pad_sequences(train_x, maxlen=max_len, padding='post')

    max_len = max([len(x) for x in test_x])
    test_x = pad_sequences(test_x, maxlen=max_len, padding='post')

    max_len = max([len(x) for x in validate_x])
    validate_x = pad_sequences(validate_x, maxlen=max_len, padding='post')

    mean_grade = np.expand_dims(train_y.mean(axis=0), axis=1)

    model = Sequential()
    model.add(Embedding(len(vocab) + 1, np.shape(all_word_vectors)[1], mask_zero=True, weights=np.array([all_word_vectors])))
    if use_cnn:
        model.add(Conv1DWithMasking(np.shape(all_word_vectors)[1], 5, padding='same', strides=1))
    model.add(LSTM(300, return_sequences=True, dropout=0.5, recurrent_dropout=0.1))
    model.add(Dropout(0.5))
    model.add(MeanOverTime(mask_zero=True))
    bias = (np.log(mean_grade) - np.log(1 - mean_grade)).astype(K.floatx())
    model.add(Dense(1, use_bias=True, bias_initializer=Constant(value=bias)))
    model.add(Activation('sigmoid'))

    optimizer = opt.RMSprop(lr=0.001, rho=0.9, epsilon=1e-06, clipnorm=10, clipvalue=0)
    model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['mean_absolute_error'])

    evl = Evaluator(validate_x, test_x, validate_y, test_y, validate_y_org, test_y_org)
    # Used to keep track of the best-performing model
    last_best_dev_ac = -1
    all_loss = []

    # Training epochs
    for i in range(50):
        start = time()
        train_history = model.fit(train_x, train_y, batch_size=32, epochs=1, verbose=0)
        evl.evaluate(model, i)
        end = time()

        train_loss = train_history.history['loss'][0]
        all_loss.append(train_loss)
        train_metric = train_history.history['mean_absolute_error'][0]
        print("Finished epoch %s, took %.3f sec" % (i, end - start))
        print('[Train] MSE: %.4f, MAE: %.4f' % (train_loss, train_metric))
        # Save if the current epoch improved AC, overwriting the old saved model if necessary
        if evl.best_dev[1] > last_best_dev_ac:
            model.save("autograder_%s_comments.h5" % comment_type)
    evl.print_final_info()
    return all_loss, evl.all_test_acs


# Execute a certain method if someone runs this file as a script.
if __name__ == "__main__":
    vocabulary, word_vectors = load_word_embeddings()
    # Use the CNN for class comments but not for method comments for best results
    train(vocabulary, word_vectors, comment_type="class", use_cnn=True)
    train(vocabulary, word_vectors, comment_type="method", use_cnn=False)
    # The models should now be saved in the current folder.
    # Run predictor.py to use these models to make new predictions

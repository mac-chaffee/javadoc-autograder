import logging
import numpy as np
from utils import (
    convert_to_dataset_friendly_scores,
    quadratic_weighted_kappa as qwk,
    agreement_coefficient as ac,
    mean_absolute_error as mae,
)

logger = logging.getLogger(__name__)


class Evaluator:
    def __init__(self, dev_x, test_x, dev_y, test_y, dev_y_org, test_y_org):
        self.dev_x, self.test_x = dev_x, test_x
        self.dev_y, self.test_y = dev_y, test_y
        self.dev_y_org, self.test_y_org = dev_y_org, test_y_org
        self.dev_mean = self.dev_y_org.mean()
        self.test_mean = self.test_y_org.mean()
        self.dev_std = self.dev_y_org.std()
        self.test_std = self.test_y_org.std()
        self.best_dev = [-1, -1]
        self.best_test = [-1, -1]
        self.best_dev_epoch = -1
        self.best_test_missed = -1
        self.best_test_missed_epoch = -1
        self.batch_size = 180
        self.low, self.high = 0, 3
        self.all_test_qwks = []
        self.all_dev_qwks = []
        self.all_test_acs = []
        self.all_dev_acs = []

    @staticmethod
    def assert_inputs(rater_a, rater_b):
        assert np.issubdtype(rater_a.dtype, np.integer), 'Integer array expected, got ' + str(rater_a.dtype)
        assert np.issubdtype(rater_b.dtype, np.integer), 'Integer array expected, got ' + str(rater_b.dtype)

    def calc_agreement(self, dev_pred, test_pred):
        # Kappa only supports integer values
        dev_pred_int = np.rint(dev_pred).astype('int32')
        test_pred_int = np.rint(test_pred).astype('int32')
        self.assert_inputs(self.dev_y_org, dev_pred_int)
        self.assert_inputs(self.test_y_org, test_pred_int)
        dev_qwk = qwk(self.dev_y_org, dev_pred_int, self.low, self.high)
        test_qwk = qwk(self.test_y_org, test_pred_int, self.low, self.high)
        dev_ac = ac(self.dev_y_org, dev_pred_int)
        test_ac = ac(self.test_y_org, test_pred_int)
        dev_mae = mae(self.dev_y_org, dev_pred_int)
        test_mae = mae(self.test_y_org, test_pred_int)
        return dev_qwk, test_qwk, test_mae, dev_mae, dev_ac, test_ac

    def evaluate(self, model, epoch):
        dev_pred = model.predict(self.dev_x, batch_size=self.batch_size).squeeze()
        test_pred = model.predict(self.test_x, batch_size=self.batch_size).squeeze()
        dev_pred = convert_to_dataset_friendly_scores(dev_pred)
        test_pred = convert_to_dataset_friendly_scores(test_pred)

        dev_qwk, test_qwk, test_mae, dev_mae, dev_ac, test_ac = self.calc_agreement(dev_pred, test_pred)

        logger.info("==== Epoch #%d Evaluation ====" % epoch)
        logger.info("[TEST] QWK: %.3f | MAE: %.3f | AC: %.3f" % (test_qwk, test_mae, test_ac))
        logger.info("[VALIDATION] QWK: %.3f | MAE: %.3f | AC: %.3f" % (dev_qwk, dev_mae, dev_ac))
        self.all_test_qwks.append(test_qwk)
        self.all_dev_qwks.append(dev_qwk)
        self.all_test_acs.append(test_ac)
        self.all_dev_acs.append(dev_ac)

        if dev_ac > self.best_dev[1]:
            self.best_dev = [dev_qwk, dev_ac]
            self.best_test = [test_qwk, dev_ac]
            self.best_dev_epoch = epoch

        if test_ac > self.best_test_missed:
            self.best_test_missed = test_ac
            self.best_test_missed_epoch = epoch

    def print_final_info(self):
        logger.info('-------------------------------------------------------------------------------------------------')
        logger.info('Missed @ Epoch %i:' % self.best_test_missed_epoch)
        logger.info('  [TEST] AC: %.3f' % self.best_test_missed)
        logger.info('Best AC @ Epoch %i:' % self.best_dev_epoch)
        logger.info('  [VALIDATE]  QWK: %.3f | AC: %.3f' % (
        self.best_dev[0], self.best_dev[1]))
        logger.info('  [TEST] QWK: %.3f | AC: %.3f' % (
        self.best_test[0], self.best_test[1]))
